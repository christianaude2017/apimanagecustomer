
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- banque
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `banque`;

CREATE TABLE `banque`
(
    `idBanque` INTEGER NOT NULL AUTO_INCREMENT,
    `LibelleBanque` VARCHAR(95),
    PRIMARY KEY (`idBanque`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- catgorieproduit
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `catgorieproduit`;

CREATE TABLE `catgorieproduit`
(
    `idcatgorieproduit` INTEGER NOT NULL AUTO_INCREMENT,
    `Libellecatgorieproduit` VARCHAR(145),
    `societe_idsociete` INTEGER NOT NULL,
    PRIMARY KEY (`idcatgorieproduit`),
    INDEX `fk_catgorieproduit_societe1_idx` (`societe_idsociete`),
    CONSTRAINT `fk_catgorieproduit_societe1`
        FOREIGN KEY (`societe_idsociete`)
        REFERENCES `societe` (`idsociete`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- commune
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `commune`;

CREATE TABLE `commune`
(
    `idcommune` INTEGER NOT NULL AUTO_INCREMENT,
    `Libellecommune` VARCHAR(45),
    `ville_idville` INTEGER NOT NULL,
    `ville_pays_idpays` INTEGER NOT NULL,
    PRIMARY KEY (`idcommune`),
    INDEX `fk_commune_ville1_idx` (`ville_idville`, `ville_pays_idpays`),
    CONSTRAINT `fk_commune_ville1`
        FOREIGN KEY (`ville_idville`,`ville_pays_idpays`)
        REFERENCES `ville` (`idville`,`pays_idpays`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- contrat
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `contrat`;

CREATE TABLE `contrat`
(
    `idContrat` INTEGER NOT NULL AUTO_INCREMENT,
    `Contrat_quantite_produit` INTEGER,
    `Personne_client` INTEGER NOT NULL,
    `idrolePersonne_client` INTEGER NOT NULL,
    `Personne_agent` INTEGER NOT NULL,
    `idrolePersonne_agent` INTEGER NOT NULL,
    `DateCreationContrat` DATE,
    `MontantContrat` INTEGER,
    `ContratMontantPrime` INTEGER,
    `produits_idproduits1` INTEGER NOT NULL,
    `produits_societe_idsociete` INTEGER NOT NULL,
    PRIMARY KEY (`idContrat`,`produits_idproduits1`,`produits_societe_idsociete`),
    INDEX `fk_produits_has_Personne_Personne1_idx` (`Personne_client`, `idrolePersonne_client`),
    INDEX `fk_produits_has_Personne_Personne2_idx` (`Personne_agent`, `idrolePersonne_agent`),
    INDEX `fk_Contrat_produits1_idx` (`produits_idproduits1`, `produits_societe_idsociete`),
    CONSTRAINT `fk_Contrat_produits1`
        FOREIGN KEY (`produits_idproduits1`,`produits_societe_idsociete`)
        REFERENCES `produits` (`idproduits`,`societe_idsociete`),
    CONSTRAINT `fk_produits_has_Personne_Personne1`
        FOREIGN KEY (`Personne_client`,`idrolePersonne_client`)
        REFERENCES `personne` (`idPersonne`,`rolePersonne_idrolePersonne`),
    CONSTRAINT `fk_produits_has_Personne_Personne2`
        FOREIGN KEY (`Personne_agent`,`idrolePersonne_agent`)
        REFERENCES `personne` (`idPersonne`,`rolePersonne_idrolePersonne`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- modepaiement
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `modepaiement`;

CREATE TABLE `modepaiement`
(
    `idmodePaiement` INTEGER NOT NULL AUTO_INCREMENT,
    `LibellemodePaiement` VARCHAR(45),
    `Banque_idBanque` INTEGER NOT NULL,
    `societe_idsociete` INTEGER NOT NULL,
    PRIMARY KEY (`idmodePaiement`),
    INDEX `fk_modePaiement_Banque1_idx` (`Banque_idBanque`),
    INDEX `fk_modePaiement_societe1_idx` (`societe_idsociete`),
    CONSTRAINT `fk_modePaiement_Banque1`
        FOREIGN KEY (`Banque_idBanque`)
        REFERENCES `banque` (`idBanque`),
    CONSTRAINT `fk_modePaiement_societe1`
        FOREIGN KEY (`societe_idsociete`)
        REFERENCES `societe` (`idsociete`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- mois
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `mois`;

CREATE TABLE `mois`
(
    `idmois` INTEGER NOT NULL AUTO_INCREMENT,
    `Libellemois` VARCHAR(95),
    PRIMARY KEY (`idmois`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- pays
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pays`;

CREATE TABLE `pays`
(
    `idpays` INTEGER NOT NULL AUTO_INCREMENT,
    `Libellepays` VARCHAR(85),
    PRIMARY KEY (`idpays`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- personne
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `personne`;

CREATE TABLE `personne`
(
    `idPersonne` INTEGER NOT NULL AUTO_INCREMENT,
    `NomPersonne` VARCHAR(95),
    `PrenomPersonne` VARCHAR(95),
    `sexe` VARCHAR(15),
    `datenaissance` DATE,
    `rolePersonne_idrolePersonne` INTEGER NOT NULL,
    `commune_idcommune` INTEGER NOT NULL,
    `ville_idville` INTEGER NOT NULL,
    `ville_pays_idpays` INTEGER NOT NULL,
    `societe_idsociete` INTEGER NOT NULL,
    PRIMARY KEY (`idPersonne`,`rolePersonne_idrolePersonne`),
    INDEX `fk_Personne_rolePersonne_idx` (`rolePersonne_idrolePersonne`),
    INDEX `fk_Personne_commune1_idx` (`commune_idcommune`),
    INDEX `fk_Personne_ville1_idx` (`ville_idville`, `ville_pays_idpays`),
    INDEX `fk_Personne_societe1_idx` (`societe_idsociete`),
    CONSTRAINT `fk_Personne_commune1`
        FOREIGN KEY (`commune_idcommune`)
        REFERENCES `commune` (`idcommune`),
    CONSTRAINT `fk_Personne_rolePersonne`
        FOREIGN KEY (`rolePersonne_idrolePersonne`)
        REFERENCES `rolepersonne` (`idrolePersonne`),
    CONSTRAINT `fk_Personne_societe1`
        FOREIGN KEY (`societe_idsociete`)
        REFERENCES `societe` (`idsociete`),
    CONSTRAINT `fk_Personne_ville1`
        FOREIGN KEY (`ville_idville`,`ville_pays_idpays`)
        REFERENCES `ville` (`idville`,`pays_idpays`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- prime
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `prime`;

CREATE TABLE `prime`
(
    `idprime` INTEGER NOT NULL AUTO_INCREMENT,
    `tauxprime` INTEGER,
    `produits_idproduits` INTEGER NOT NULL,
    `produits_societe_idsociete` INTEGER NOT NULL,
    PRIMARY KEY (`idprime`,`produits_idproduits`,`produits_societe_idsociete`),
    INDEX `fk_prime_produits1_idx` (`produits_idproduits`, `produits_societe_idsociete`),
    CONSTRAINT `fk_prime_produits1`
        FOREIGN KEY (`produits_idproduits`,`produits_societe_idsociete`)
        REFERENCES `produits` (`idproduits`,`societe_idsociete`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- produits
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `produits`;

CREATE TABLE `produits`
(
    `idproduits` INTEGER NOT NULL AUTO_INCREMENT,
    `Libelleproduits` VARCHAR(145),
    `idcatgorieproduit` INTEGER NOT NULL,
    `Montantproduits` INTEGER,
    `societe_idsociete` INTEGER NOT NULL,
    PRIMARY KEY (`idproduits`,`societe_idsociete`),
    INDEX `fk_produits_catgorieproduit1_idx` (`idcatgorieproduit`),
    INDEX `fk_produits_societe1_idx` (`societe_idsociete`),
    CONSTRAINT `fk_produits_catgorieproduit1`
        FOREIGN KEY (`idcatgorieproduit`)
        REFERENCES `catgorieproduit` (`idcatgorieproduit`),
    CONSTRAINT `fk_produits_societe1`
        FOREIGN KEY (`societe_idsociete`)
        REFERENCES `societe` (`idsociete`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- prospection
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `prospection`;

CREATE TABLE `prospection`
(
    `idprospection` INTEGER NOT NULL AUTO_INCREMENT,
    `NomProspect` VARCHAR(85),
    `Prenomprospection` VARCHAR(105),
    `TelProspect` VARCHAR(45),
    `EmailProspect` VARCHAR(45),
    `DatePriseRDV` DATE,
    `DateRDV` DATE,
    `typeprospect` INTEGER,
    `fonctionprospect` VARCHAR(100),
    `idPersonneAgent` INTEGER NOT NULL,
    `Personne_rolePersonne_idrolePersonne` INTEGER NOT NULL,
    `produits_idproduits` INTEGER NOT NULL,
    `produits_societe_idsociete` INTEGER NOT NULL,
    PRIMARY KEY (`idprospection`,`idPersonneAgent`,`Personne_rolePersonne_idrolePersonne`,`produits_idproduits`,`produits_societe_idsociete`),
    INDEX `fk_prospection_Personne1_idx` (`idPersonneAgent`, `Personne_rolePersonne_idrolePersonne`),
    INDEX `fk_prospection_produits1_idx` (`produits_idproduits`, `produits_societe_idsociete`),
    CONSTRAINT `fk_prospection_Personne1`
        FOREIGN KEY (`idPersonneAgent`,`Personne_rolePersonne_idrolePersonne`)
        REFERENCES `personne` (`idPersonne`,`rolePersonne_idrolePersonne`),
    CONSTRAINT `fk_prospection_produits1`
        FOREIGN KEY (`produits_idproduits`,`produits_societe_idsociete`)
        REFERENCES `produits` (`idproduits`,`societe_idsociete`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- responsable_agent
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `responsable_agent`;

CREATE TABLE `responsable_agent`
(
    `idResponsable` INTEGER NOT NULL AUTO_INCREMENT,
    `Personne_idResponsable` INTEGER NOT NULL,
    `idroleResponsable` INTEGER NOT NULL,
    `Personne_idAgent` INTEGER NOT NULL,
    `idroleAgent` INTEGER NOT NULL,
    `DateDebutResponsabilite` DATE,
    PRIMARY KEY (`idResponsable`,`Personne_idResponsable`,`idroleResponsable`,`Personne_idAgent`,`idroleAgent`),
    INDEX `fk_Personne_has_Personne_Personne2_idx` (`Personne_idAgent`, `idroleAgent`),
    INDEX `fk_Personne_has_Personne_Personne1_idx` (`Personne_idResponsable`, `idroleResponsable`),
    CONSTRAINT `fk_Personne_has_Personne_Personne1`
        FOREIGN KEY (`Personne_idResponsable`,`idroleResponsable`)
        REFERENCES `personne` (`idPersonne`,`rolePersonne_idrolePersonne`),
    CONSTRAINT `fk_Personne_has_Personne_Personne2`
        FOREIGN KEY (`Personne_idAgent`,`idroleAgent`)
        REFERENCES `personne` (`idPersonne`,`rolePersonne_idrolePersonne`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- rolepersonne
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rolepersonne`;

CREATE TABLE `rolepersonne`
(
    `idrolePersonne` INTEGER NOT NULL AUTO_INCREMENT,
    `LibellerolePersonne` VARCHAR(49),
    `societe_idsociete` INTEGER NOT NULL,
    PRIMARY KEY (`idrolePersonne`),
    INDEX `fk_rolePersonne_societe1_idx` (`societe_idsociete`),
    CONSTRAINT `fk_rolePersonne_societe1`
        FOREIGN KEY (`societe_idsociete`)
        REFERENCES `societe` (`idsociete`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- societe
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `societe`;

CREATE TABLE `societe`
(
    `idsociete` INTEGER NOT NULL AUTO_INCREMENT,
    `Libellesociete` VARCHAR(145),
    `ville_idville` INTEGER NOT NULL,
    `ville_pays_idpays` INTEGER NOT NULL,
    PRIMARY KEY (`idsociete`),
    INDEX `fk_societe_ville1_idx` (`ville_idville`, `ville_pays_idpays`),
    CONSTRAINT `fk_societe_ville1`
        FOREIGN KEY (`ville_idville`,`ville_pays_idpays`)
        REFERENCES `ville` (`idville`,`pays_idpays`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- vente
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vente`;

CREATE TABLE `vente`
(
    `idvente` INTEGER NOT NULL AUTO_INCREMENT,
    `produits_idproduits` INTEGER NOT NULL,
    `produits_societe_idsociete` INTEGER NOT NULL,
    `Datevente` VARCHAR(45),
    `Quantitevente` INTEGER,
    `Montantvente` INTEGER,
    `Personne_idPersonne` INTEGER NOT NULL,
    `Personne_rolePersonne_idrolePersonne` INTEGER NOT NULL,
    PRIMARY KEY (`idvente`,`produits_idproduits`,`produits_societe_idsociete`,`Personne_idPersonne`,`Personne_rolePersonne_idrolePersonne`),
    INDEX `fk_vente_produits1_idx` (`produits_idproduits`, `produits_societe_idsociete`),
    INDEX `fk_vente_Personne1_idx` (`Personne_idPersonne`, `Personne_rolePersonne_idrolePersonne`),
    CONSTRAINT `fk_vente_Personne1`
        FOREIGN KEY (`Personne_idPersonne`,`Personne_rolePersonne_idrolePersonne`)
        REFERENCES `personne` (`idPersonne`,`rolePersonne_idrolePersonne`),
    CONSTRAINT `fk_vente_produits1`
        FOREIGN KEY (`produits_idproduits`,`produits_societe_idsociete`)
        REFERENCES `produits` (`idproduits`,`societe_idsociete`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- versement
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `versement`;

CREATE TABLE `versement`
(
    `idversement` INTEGER NOT NULL AUTO_INCREMENT,
    `dateVersement` DATE,
    `montantversement` INTEGER,
    `NumeroCheque` VARCHAR(45),
    `versement_idmois` INTEGER NOT NULL,
    `Contrat_idContrat` INTEGER NOT NULL,
    `Contrat_produits_idproduits` INTEGER NOT NULL,
    `Contrat_produits_societe_idsociete` INTEGER NOT NULL,
    `modePaiement_idmodePaiement` INTEGER NOT NULL,
    PRIMARY KEY (`idversement`,`versement_idmois`,`Contrat_idContrat`,`Contrat_produits_idproduits`,`Contrat_produits_societe_idsociete`),
    INDEX `fk_versement_mois1_idx` (`versement_idmois`),
    INDEX `fk_versement_Contrat1_idx` (`Contrat_idContrat`, `Contrat_produits_idproduits`, `Contrat_produits_societe_idsociete`),
    INDEX `fk_versement_modePaiement1_idx` (`modePaiement_idmodePaiement`),
    CONSTRAINT `fk_versement_Contrat1`
        FOREIGN KEY (`Contrat_idContrat`,`Contrat_produits_idproduits`,`Contrat_produits_societe_idsociete`)
        REFERENCES `contrat` (`idContrat`,`produits_idproduits1`,`produits_societe_idsociete`),
    CONSTRAINT `fk_versement_modePaiement1`
        FOREIGN KEY (`modePaiement_idmodePaiement`)
        REFERENCES `modepaiement` (`idmodePaiement`),
    CONSTRAINT `fk_versement_mois1`
        FOREIGN KEY (`versement_idmois`)
        REFERENCES `mois` (`idmois`)
) ENGINE=InnoDB;

-- ---------------------------------------------------------------------
-- ville
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ville`;

CREATE TABLE `ville`
(
    `idville` INTEGER NOT NULL AUTO_INCREMENT,
    `Libelleville` VARCHAR(85),
    `pays_idpays` INTEGER NOT NULL,
    PRIMARY KEY (`idville`,`pays_idpays`),
    INDEX `fk_ville_pays1_idx` (`pays_idpays`),
    CONSTRAINT `fk_ville_pays1`
        FOREIGN KEY (`pays_idpays`)
        REFERENCES `pays` (`idpays`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
