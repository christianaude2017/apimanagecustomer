<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/addrole', function (Request $request, Response $response, array $args) {

    $libelleRole = $request->getParam('libelle');
    $societe = $request->getParam('societe');
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    $role = new Rolepersonne();
    //$role->setIdrolepersonne(1);
    $role->setLibellerolepersonne($libelleRole);
    $role->setSocieteIdsociete($societe);
    $result = $role->save();
    //var_dump($result);

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($result));
});

$app->post('/inscription', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    $denomination = $request->getParam('denomination');
    /*     * ****INSERER DANS LA TABLE SOCIETE***** */
    $req = "INSERT INTO societe SET Libellesociete = ?";
    $q = $this->db->prepare($req);
    $result = $q->execute(array($denomination));
    if ($result == false) {
        $message = "problème au niveau de société";
        return $message;
    }
    $req = "SELECT max(societe.idsociete) AS idsociete FROM societe";
    $q = $this->db->prepare($req);
    $q->execute();
    $idsociete = $q->fetch();
    
    //INSERER DANS LE ROLE ADMINISTRATEUR
    /*$req = "INSERT rolepersonne SET LibellerolePersonne = 'administrateur',societe_idsociete = ?";
    $q = $this->db->prepare($req);
    $q->execute(array($idsociete['idsociete']));
    
    //INSERER LE ROLE AGENT
    $req = "INSERT rolepersonne SET LibellerolePersonne = 'agent',societe_idsociete = ?";
    $q = $this->db->prepare($req);
    $q->execute(array($idsociete['idsociete']));*/

    /*     * *****INSERER DANS LA TABLE PERSONNE******* */
    $nomUser = $request->getParam('nomUser');
    $prenomUser = $request->getParam('prenomUser');

    $req = "INSERT INTO personne (NomPersonne,PrenomPersonne,societe_idsociete,rolePersonne_idrolePersonne) VALUES (?,?,?,?)";
    $q = $this->db->prepare($req);
    $result = $q->execute(array($nomUser, $prenomUser, $idsociete['idsociete'], 1));
    if ($result == false) {
        $message = "problème au niveau de personne";
        return $message;
    }
    $req = "SELECT max(personne.idPersonne) AS idpersonne FROM personne";
    $idpersonne = $this->db->prepare($req);
    $idpersonne->execute();
    $idpersonne = $idpersonne->fetch();

    /*     * ******INSERER DANS COMPTE****** */
    $login = $request->getParam('loginUser');
    $email = $request->getParam('emailUser');
    $motdepasse = $request->getParam('motdepasse');

    $req = "INSERT INTO compte (login,email,password,idpersonne,top_admin) VALUES (?,?,?,?,?)";
    $q = $this->db->prepare($req);
    $result = $q->execute(array($login, $email, $motdepasse, $idpersonne['idpersonne'], 1));
    if ($result == false) {
        $message = "problème au niveau de compte";
        return $message;
    }
    $message = "bien enrregistré";
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/updaterole', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    $role = \Base\RolepersonneQuery::create()->filterByIdrolepersonne(3)->findOne();
    $role->setLibellerolepersonne("Commercial freelance");
    $role->save();
    //var_dump($result);

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($role->save()));
});

$app->post('/addagent', function (Request $request, Response $response, array $args) {

    $message = "";

    $nomAgent = $request->getParam('nomAgent');
    $prenomAgent = $request->getParam('prenomAgent');
    $datenaissAgent = $request->getParam('datenaissAgent');
    $sexeAgent = $request->getParam('sexeAgent');
    $paysAgent = $request->getParam('paysAgent');
    $villehabitationAgent = $request->getParam('villehabitationAgent');
    $communehabitationAgent = $request->getParam('communehabitationAgent');
    $contact1Agent = $request->getParam('contact1Agent');
    $contact2Agent = $request->getParam('contact2Agent');
    $emailAgent = $request->getParam('emailAgent');
    $responsableAgent = $request->getParam('responsableAgent');
    $role = 2;
    $societe = $request->getParam('societe');

    try {

        $this->db->beginTransaction();
        
        //inserer dans la table personne
        $req = "INSERT INTO personne (NomPersonne,PrenomPersonne,sexe,"
                . "datenaissance,contact1,contact2,email,rolePersonne_idrolePersonne,"
                . "commune_idcommune,ville_idville,ville_pays_idpays,societe_idsociete) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        $q = $this->db->prepare($req);
        $retour = $q->execute(array($nomAgent, $prenomAgent, $sexeAgent, $datenaissAgent,
            $contact1Agent, $contact2Agent, $emailAgent, $role, $communehabitationAgent,
            $villehabitationAgent, $paysAgent, $societe));

        //recuperer le id de la personne
        $req = "SELECT max(idPersonne) AS idPersonne FROM personne";
        $q = $this->db->prepare($req);
        $q->execute();
        $idpersonne = $q->fetch();

        /* generer un mot de passe aleatoire */
        $chars = '01234bcdefghijklmnopqrstuvwxy56789azABCDEFGHIJKLMNOPQRSTUVWXYZ+-*/';
        $passgene = '';
        for ($i = 0; $i < 6; $i++) {
            $passgene .= $chars[rand(0, strlen($chars) - 1)];
        }
        /*         * ********************************************************************** */
        $req = "INSERT into compte SET login = ?, email = ? , password = ?, idpersonne = ?";
        $q = $this->db->prepare($req);
        $retour = $q->execute(array($nomAgent, $emailAgent, $passgene, $idpersonne['idPersonne']));
        $this->db->commit();

        $message = array('code' => 200, 'message' => "Insertion réussie");
    } catch (Exception $e) {

        $message = array('code' => 900, 'message' => "Echec d'insertion");
        echo $e->getMessage();
        $this->db->rollBack();
    }

    /* if ($retour) {
      $message = array('code' => 200, 'message' => "Insertion réussie");
      } else {
      $message = array('code' => 900, 'message' => "Echec d'insertion");
      } */

    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/updateagent', function (Request $request, Response $response, array $args) {

    $nomAgent = $request->getParam('nomAgent');
    $prenomAgent = $request->getParam('prenomAgent');
    $datenaissAgent = $request->getParam('datenaissAgent');
    $sexeAgent = $request->getParam('sexeAgent');
    $paysAgent = $request->getParam('paysAgent');
    $villehabitationAgent = $request->getParam('villehabitationAgent');
    $communehabitationAgent = $request->getParam('communehabitationAgent');
    $contact1Agent = $request->getParam('contact1Agent');
    $contact2Agent = $request->getParam('contact2Agent');
    $emailAgent = $request->getParam('emailAgent');
    $responsableAgent = $request->getParam('responsableAgent');
    $idpersonne = $request->getParam('idpersonne');
    $role = 2;
    $societe = $request->getParam('societe');

    //inserer dans la table personne
    $req = "UPDATE personne "
            . "set NomPersonne = ?,PrenomPersonne = ?,sexe = ?,"
            . "datenaissance = ?,contact1 = ?,contact2 = ?,email = ?,rolePersonne_idrolePersonne = ?,"
            . "commune_idcommune = ?,ville_idville = ?,ville_pays_idpays = ?,"
            . "societe_idsociete = ? WHERE idPersonne = ? ";
    $q = $this->db->prepare($req);
    $retour = $q->execute(array($nomAgent, $prenomAgent, $sexeAgent, $datenaissAgent,
        $contact1Agent, $contact2Agent, $emailAgent,
        $role, $communehabitationAgent, $villehabitationAgent,
        $paysAgent, $societe, $idpersonne));
    if ($retour) {
        $message = array('code' => 200, 'message' => "Insertion réussie");
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/login', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    $login = $request->getParam('loginUser');
    $password = $request->getParam('motdepasse');
    // Render index view
    $req = "SELECT * FROM compte where email = ? and password = ?";
    $q = $this->db->prepare($req);
    $q->execute(array($login, $password));
    $result = $q->fetch();
    $message = "";
    if ($result == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $result);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});
$app->post('/listagent', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    $req = "SELECT * FROM personne where top_actif = 1";
    $q = $this->db->prepare($req);
    $q->execute();
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($cont));
});

$app->post('/getagentbyid', function (Request $request, Response $response, array $args) {
    // Sample log message
    $idPersonne = $request->getParam('id');
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    $cont = \Base\PersonneQuery::create()->filterByIdpersonne($idPersonne)->findOne()->toArray();
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($cont));
});

$app->post('/delagentbyid', function (Request $request, Response $response, array $args) {
    // Sample log message
    //inserer dans la table personne
    $idpersonne = $request->getParam('id');
    //$topactif = $request->getParam('topActif');
    $req = "UPDATE personne set top_actif = 0 WHERE idPersonne = ? ";
    $q = $this->db->prepare($req);
    $retour = $q->execute(array($idpersonne));

    if ($retour) {
        $message = array('code' => 200, 'message' => "Insertion réussie");
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

/* $app->post('/addproduit', function (Request $request, Response $response, array $args) {
  // Sample log message
  $this->logger->info("Slim-Skeleton '/' route");
  // Render index view

  $idcategorieProduit = \Base\CatgorieproduitQuery::create()->filterByIdcatgorieproduit(1)->findOne();
  $categorieprod = new Catgorieproduit();
  //$categorieProduit->setLibellecatgorieproduit("produit assurance");
  //$vente = new Vente();


  $produit = new Produits();

  $produit->setLibelleproduits("assurance epargne");
  $produit->setCatgorieproduit($idcategorieProduit);
  $produit->setMontantproduits(756000);

  $retour = $produit->save();

  return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($retour));
  }); */

$app->post('/addRespo', function (Request $request, Response $response, array $args) {
    // Sample log message


    $listagent = $request->getParam('listagent');
    $idresponsable = $request->getParam('nomresponsable');
    $titreResponsabilite = $request->getParam('titreresponsabilite');

    $req = "DELETE FROM `responsable_agent` WHERE responsable_agent.Personne_idResponsable = ?";
    $q = $this->db->prepare($req);
    $q->execute(array(intval($idresponsable)));

    //$topactif = $request->getParam('topActif');
    $req = "UPDATE personne set top_respo = 1 ,titreresponsabilite = ? WHERE idPersonne = ?";
    $q = $this->db->prepare($req);
    $retour = $q->execute(array($titreResponsabilite, intval($idresponsable)));

    foreach ($listagent as $agent) {

        $req = "INSERT responsable_agent SET "
                . "Personne_idResponsable = ?,"
                . "Personne_idAgent = ?";

        $q = $this->db->prepare($req);
        $retour = $q->execute(array(intval($idresponsable), intval($agent)));
    }

    $data = array($listagent, $idresponsable, $titreResponsabilite);
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
});

$app->post('/addCategorie', function (Request $request, Response $response, array $args) {
    // Sample log message


    $categorie = $request->getParam('categorieLibelle');
    $idsociete = $request->getParam('idsociete');


    $req = "INSERT into catgorieproduit SET Libellecatgorieproduit = ?,societe_idsociete = ?";
    $q = $this->db->prepare($req);
    $data = $q->execute(array($categorie, $idsociete));

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($data));
});

$app->post('/addProduit', function (Request $request, Response $response, array $args) {

    // Sample log message
    $categorie = $request->getParam('categorie');
    $idsociete = $request->getParam('idsociete');
    $libelleProd = $request->getParam('libelleproduit');
    $montantProd = $request->getParam('montantproduit');
    $commission = $request->getParam('commission');

    $montantProd = str_replace(',', '', $montantProd);

    $req = "INSERT into produits SET idcatgorieproduit = ?,"
            . "societe_idsociete = ?,"
            . "Libelleproduits = ?,"
            . "Montantproduits = ?,"
            . "pourcentage_commission = ?";
    $q = $this->db->prepare($req);
    $data = $q->execute(array($categorie, $idsociete, $libelleProd, $montantProd, $commission));
    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/addPrime', function (Request $request, Response $response, array $args) {

    // Sample log message
    $produit = $request->getParam('produit');
    $montantprime = $request->getParam('montantprime');



    $req = "INSERT into prime SET tauxprime = ?,produits_idproduits = ?";
    $q = $this->db->prepare($req);
    $data = $q->execute(array($montantprime, $produit));
    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/addContrat', function (Request $request, Response $response, array $args) {

    // Sample log message
    $Agent = $request->getParam('Agent');
    $Client = $request->getParam('Client');
    $Produit = $request->getParam('Produit');
    $dateContrat = $request->getParam('dateContrat');
    $quantiteproduit = $request->getParam('quantiteproduit');
    $montanttotal = $request->getParam('montanttotal');
    $datedebut = $request->getParam('datedebut');
    $datefin = $request->getParam('datefin');
    $modepaiement = $request->getParam('modepaiement');
    $periode = $request->getParam('periode');
    $montantprime = $request->getParam('montantprime');
    $societe = $request->getParam('societe');

    $montanttotal = str_replace(',', '', $montanttotal);

    $req = "INSERT into contrats "
            . "SET idagent = ?,"
            . "idclient = ?,"
            . "idproduit = ?,"
            . "idsociete = ?,"
            . "dateContrat = ?,"
            . "moisContrat = MONTHNAME(dateContrat),"
            . "quantiteproduit = ?,"
            . "montantContrat = ?,"
            . "datedebutContrat = ?,"
            . "datefinContrat = ?,"
            . "modepaiement = ?,"
            . "periodicite = ?";

    $q = $this->db->prepare($req);
    $data = $q->execute(array($Agent, $Client, $Produit,$societe,
        $dateContrat, $quantiteproduit, $montanttotal, $datedebut, $datefin, $modepaiement, $periode));
    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/updateContrat', function (Request $request, Response $response, array $args) {

    // Sample log message
    $Agent = $request->getParam('Agent');
    $Client = $request->getParam('Client');
    $Produit = $request->getParam('Produit');
    $dateContrat = $request->getParam('dateContrat');
    $quantiteproduit = $request->getParam('quantiteproduit');
    $montanttotal = $request->getParam('montanttotal');
    $datedebut = $request->getParam('datedebut');
    $datefin = $request->getParam('datefin');
    $modepaiement = $request->getParam('modepaiement');
    $periode = $request->getParam('periode');
    $montantprime = $request->getParam('montantprime');
    $idContrat = $request->getParam('idContrat');

    $montanttotal = str_replace(',', '', $montanttotal);

    $req = "UPDATE contrats "
            . "SET idagent = ?,"
            . "idclient = ?,"
            . "idproduit = ?,"
            . "dateContrat = ?,"
            . "moisContrat = MONTHNAME(dateContrat),"
            . "quantiteproduit = ?,"
            . "montantContrat = ?,"
            . "datedebutContrat = ?,"
            . "datefinContrat = ?,"
            . "modepaiement = ?,"
            . "periodicite = ?"
            . "where idcontrat = ?";

    $q = $this->db->prepare($req);
    $data = $q->execute(array($Agent, $Client, $Produit,
        $dateContrat, $quantiteproduit, $montanttotal,
        $datedebut, $datefin, $modepaiement, $periode, $idContrat));

    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/updateProduit', function (Request $request, Response $response, array $args) {

    // Sample log message
    $idproduit = $request->getParam('produit');
    $idsociete = $request->getParam('idsociete');
    $libelleProd = $request->getParam('libelleproduit');
    $montantProd = $request->getParam('montantproduit');
    $commission = $request->getParam('commission');

    $montantProd = str_replace(',', '', $montantProd);

    $req = "UPDATE produits SET Libelleproduits = ?,Montantproduits = ? ,"
            . "pourcentage_commission = ?"
            . "WHERE idproduits = ? and societe_idsociete = ?";
    $q = $this->db->prepare($req);
    $data = $q->execute(array($libelleProd, $montantProd, $commission, $idproduit, $idsociete));
    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/activeProduit', function (Request $request, Response $response, array $args) {

    // Sample log message
    $idproduit = $request->getParam('produit');
    $idsociete = $request->getParam('idsociete');
    $etat = $request->getParam('etat');
    /* $libelleProd = $request->getParam('libelleproduit');
     * $categorie = $request->getParam('categorie');
      $montantProd = $request->getParam('montantproduit'); */


    $req = "UPDATE produits SET top_actif = ? WHERE idproduits = ?";
    $q = $this->db->prepare($req);
    $data = $q->execute(array($etat, $idproduit));
    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/addProspection', function (Request $request, Response $response, array $args) {


    //$tabParam = $_POST;
    try {

        $this->db->beginTransaction();

        $agentProspect = $request->getParam('agentProspect');
        $produitProspect = $request->getParam('produitProspect');
        $societe = $request->getParam('societe');
        $nomProspect = $request->getParam('nomProspect');
        $prenomProspect = $request->getParam('prenomProspect');
        $societeProspect = $request->getParam('societeProspect');
        $adresseProspect = $request->getParam('adresseProspect');
        $fonctionProspect = $request->getParam('fonctionProspect');
        $telProspect = $request->getParam('telProspect');
        $emailProspect = $request->getParam('emailProspect');
        $dateProspect = $request->getParam('dateProspect');
        $dateRDV = $request->getParam('dateRDV');

        $tabParam = array($agentProspect, $produitProspect,
            $societe, $nomProspect, $prenomProspect,
            $societeProspect, $adresseProspect,
            $fonctionProspect, $telProspect,
            $emailProspect, $dateProspect, $dateRDV);

        $sql = "INSERT prospections "
                . "SET agentprospection = ?,"
                . "produitprospection = ?,"
                . "idsociete = ?,"
                . "nomprospection = ?,"
                . "prenomprospection = ?,"
                . "societeprospection = ?,"
                . "adresseprospection = ?,"
                . "fonctionprospection = ?,"
                . "telephoneprospection = ? ,"
                . "emailprospection = ?,"
                . "dateprospection = ?,"
                . "jour_prospection = DAYNAME(dateprospection),"
                . "dateRDVprospection = ?";
        //var_dump($result);

        $sql1 = "INSERT rdvprospect "
                . "SET idprospection = ?,"
                . "idsociete = ?, "
                . "daterdv = ? ";

        $message = "";


        $q = $this->db->prepare($sql);
        $reponse = $q->execute($tabParam);

        //recuperer le dernier id de prospection
        $idlastprosp = $this->db->lastInsertId();

        $q = $this->db->prepare($sql1);
        $reponse2 = $q->execute(array($idlastprosp, $societe, $dateRDV));


        if ($reponse == true && $reponse2 == true) {
            $this->db->commit();
            //$message = $idlastprosp;
            $message = array('code' => 200, 'message' => "Insertion réussie");
        } else {
            $this->db->rollBack();
            $message = "erreur";
        }
    } catch (Exception $e) {

        $message = array('code' => 900, 'message' => "Echec d'insertion");
        echo $e->getMessage();
        $this->db->rollBack();
    }

    //return var_dump($tabParam);
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/addClient', function (Request $request, Response $response, array $args) {


    //$tabParam = $_POST;


    $typeclient = $request->getParam('typeclient');
    $denominationclient = $request->getParam('denominationclient');
    $rccmclient = $request->getParam('rccmclient');
    $nomclient = $request->getParam('nomclient');
    $prenomclient = $request->getParam('prenomclient');
    $idagent = $request->getParam('idagent');
    $idsociete = $request->getParam('idsociete');
    $datenaissClient = $request->getParam('datenaissClient');
    $lieuNaisssClient = $request->getParam('lieuNaisssClient');
    $societeclient = $request->getParam('societeclient');
    $adresseclient = $request->getParam('adresseclient');
    $fonctionclient = $request->getParam('fonctionclient');
    $telephoneclient = $request->getParam('telephoneclient');
    $emailclient = $request->getParam('emailclient');
    $idprospection = $request->getParam('idprospection');
    
    if($typeclient == "entreprise")$datenaissClient = null;

    $tabParam = array($typeclient,$denominationclient,$rccmclient,$nomclient, $prenomclient,
        $idagent, $idsociete, $datenaissClient, $lieuNaisssClient,
        $societeclient, $adresseclient,
        $fonctionclient, $telephoneclient, $emailclient, $idprospection);

    $sql = "INSERT client "
            . "SET "
            . "typeclient = ?,"
            . "denominationClient = ?,"
            . "rccmClient = ?,"
            . "NomClient = ?,"
            . "PrenomClient = ?,"
            . "idagent = ?,"
            . "idsociete = ?,"
            . "DateNaissClient = ?,"
            . "LieuNaissClient = ?,"
            . "societeClient = ?,"
            . "adresseClient = ?,"
            . "fonctionClient = ?,"
            . "telephoneClient = ? ,"
            . "emailClient = ?,"
            . "idprospection = ?,"
            . "dateAjout = now(),"
            . "jour_ajout_client = DAYNAME(dateAjout),"
            . "mois_ajout_client = MONTHNAME(dateAjout)";
    //var_dump($result);

    $q = $this->db->prepare($sql);
    $reponse = $q->execute($tabParam);
    $message = "";
    if ($reponse == true) {

        $message = "succès";
    } else {
        $message = "erreur";
    }
    //return var_dump($tabParam);
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/updateClient', function (Request $request, Response $response, array $args) {


    //$tabParam = $_POST;

    $typeclient = $request->getParam('typeclient');
    $denominationclient = $request->getParam('denominationclient');
    $rccmclient = $request->getParam('rccmclient');
    $nomclient = $request->getParam('nomclient');
    $prenomclient = $request->getParam('prenomclient');
    $idagent = $request->getParam('idagent');
    $idsociete = $request->getParam('idsociete');
    $datenaissClient = $request->getParam('datenaissClient');
    $lieuNaisssClient = $request->getParam('lieuNaisssClient');
    $societeclient = $request->getParam('societeclient');
    $adresseclient = $request->getParam('adresseclient');
    $fonctionclient = $request->getParam('fonctionclient');
    $telephoneclient = $request->getParam('telephoneclient');
    $emailclient = $request->getParam('emailclient');
    $idprospection = $request->getParam('idprospection');
    $idClient = $request->getParam('idClient');

    if($typeclient == "entreprise")$datenaissClient = null;
    
    $tabParam = array($typeclient,$denominationclient,$rccmclient,$nomclient, $prenomclient,
        $idagent, $idsociete, $datenaissClient, $lieuNaisssClient,
        $societeclient, $adresseclient,
        $fonctionclient, $telephoneclient, $emailclient, $idprospection, $idClient);

    $sql = "UPDATE client "
            . "SET "
            . "typeclient = ?,"
            . "denominationClient = ?,"
            . "rccmClient = ?,"
            . "NomClient = ?,"
            . "PrenomClient = ?,"
            . "idagent = ?,"
            . "idsociete = ?,"
            . "DateNaissClient = ?,"
            . "LieuNaissClient = ?,"
            . "societeClient = ?,"
            . "adresseClient = ?,"
            . "fonctionClient = ?,"
            . "telephoneClient = ? ,"
            . "emailClient = ?,"
            . "idprospection = ?"
            . "where idClient = ?";
    //var_dump($result);

    $q = $this->db->prepare($sql);
    $reponse = $q->execute($tabParam);
    $message = "";
    if ($reponse == true) {

        $message = "succès";
    } else {
        $message = "erreur";
    }
    //return var_dump($tabParam);
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/delClient', function (Request $request, Response $response, array $args) {
    // Sample log message
    //inserer dans la table personne
    $idClient = $request->getParam('id');
    //$topactif = $request->getParam('topActif');
    $req = "UPDATE client set top_actif = 0 WHERE idClient = ? ";
    $q = $this->db->prepare($req);
    $retour = $q->execute(array($idClient));

    if ($retour) {
        $message = array('code' => 200, 'message' => "Insertion réussie");
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/modifierProspection', function (Request $request, Response $response, array $args) {
    try {

        $this->db->beginTransaction();

        //$tabParam = $_POST;

        $agentProspect = $request->getParam('agentProspect');
        $produitProspect = $request->getParam('produitProspect');
        $societe = $request->getParam('societe');
        $nomProspect = $request->getParam('nomProspect');
        $prenomProspect = $request->getParam('prenomProspect');
        $societeProspect = $request->getParam('societeProspect');
        $adresseProspect = $request->getParam('adresseProspect');
        $fonctionProspect = $request->getParam('fonctionProspect');
        $telProspect = $request->getParam('telProspect');
        $emailProspect = $request->getParam('emailProspect');
        $dateProspect = $request->getParam('dateProspect');
        $dateRDV = $request->getParam('dateRDV');
        $idprospection = $request->getParam('idprospection');

        $tabParam = array($agentProspect, $produitProspect,
            $societe, $nomProspect, $prenomProspect,
            $societeProspect, $adresseProspect,
            $fonctionProspect, $telProspect,
            $emailProspect, $dateProspect, $dateRDV, $idprospection);

        $sql = "UPDATE prospections "
                . "SET agentprospection = ?,"
                . "produitprospection = ?,"
                . "idsociete = ?,"
                . "nomprospection = ?,"
                . "prenomprospection = ?,"
                . "societeprospection = ?,"
                . "adresseprospection = ?,"
                . "fonctionprospection = ?,"
                . "telephoneprospection = ? ,"
                . "emailprospection = ?,"
                . "dateprospection = ?,"
                . "dateRDVprospection = ?"
                . "WHERE idprospection = ?";
        //var_dump($result);

        $sql1 = "UPDATE rdvprospect "
                . "SET idprospection = ?,"
                . "idsociete = ?, "
                . "daterdv = ? "
                . "where idprospection = ?";

        $message = "";


        $q = $this->db->prepare($sql);
        $reponse = $q->execute($tabParam);

        //recuperer le dernier id de prospection
        //$idlastprosp = $this->db->lastInsertId();

        $q = $this->db->prepare($sql1);
        $reponse2 = $q->execute(array($idprospection, $societe, $dateRDV, $idprospection));


        if ($reponse == true && $reponse2 == true) {
            $this->db->commit();
            //$message = $idlastprosp;
            $message = array('code' => 200, 'message' => "succès d'insertion");
        } else {
            $this->db->rollBack();
            $message = "erreur";
        }
    } catch (Exception $e) {

        $message = array('code' => 900, 'message' => "Echec d'insertion");
        echo $e->getMessage();
        $this->db->rollBack();
    }
    //return var_dump($tabParam);
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/addRDV', function (Request $request, Response $response, array $args) {

    // Sample log message
    $dateRDV = $request->getParam('dateRDV');
    $idprospection = $request->getParam('idprospection');
    $societe = $request->getParam('societe');



    $sql1 = "INSERT rdvprospect "
            . "SET daterdv = ?, "
            . "idprospection = ?,"
            . "idsociete = ?";


    $q = $this->db->prepare($sql1);
    $data = $q->execute(array($dateRDV, $idprospection,$societe));
    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});


$app->post('/updateRDV', function (Request $request, Response $response, array $args) {

    // Sample log message
    $dateRDV = $request->getParam('dateRDV');
    $idprospection = $request->getParam('idprospection');



    $sql1 = "UPDATE rdvprospect "
            . "SET daterdv = ? "
            . "where idprospection = ?";


    $q = $this->db->prepare($sql1);
    $data = $q->execute(array($dateRDV, $idprospection));
    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/effectuerRDV', function (Request $request, Response $response, array $args) {

    // Sample log message
    
    $idprospection = $request->getParam('id');



    $sql1 = "UPDATE rdvprospect SET topEffectue = 0 where idrdvprospect = ? ";


    $q = $this->db->prepare($sql1);
    $data = $q->execute(array($idprospection));
    $message = "";
    if ($data == false) {

        $message = array("message" => "aucune correspondance");
    } else {
        $message = array("message" => "trouve", "data" => $data);
    }
    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/delprospectbyid', function (Request $request, Response $response, array $args) {
    // Sample log message
    //inserer dans la table personne
    $idprospection = $request->getParam('id');
    //$topactif = $request->getParam('topActif');
    $req = "UPDATE prospections set top_actif = 0 WHERE idprospection = ? ";
    $q = $this->db->prepare($req);
    $retour = $q->execute(array($idprospection));

    if ($retour) {
        $message = array('code' => 200, 'message' => "Insertion réussie");
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});

$app->post('/getProspectionByDay', function (Request $request, Response $response, array $args) {
    // Sample log message
    //inserer dans la table personne
    //$idprospection = $request->getParam('id');
    //$topactif = $request->getParam('topActif');
    $idagent = $request->getParam('idAgent');

    $req = "select count(prospections.idprospection) as nombreprospection
            from prospections where prospections.agentprospection = ? and prospections.top_actif = 1
            group by prospections.agentprospection";

    $q = $this->db->prepare($req);
    $retour = $q->execute(array($idagent));
    $reponse = $q->fetch();
    $tab = array();
    if ($retour) {
        $message = $reponse['nombreprospection'];
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write($message);
});

$app->post('/getStatByAgent', function (Request $request, Response $response, array $args) {
    // Sample log message
    //inserer dans la table personne
    //$idprospection = $request->getParam('id');
    //$topactif = $request->getParam('topActif');
    $idagent = $request->getParam('idAgent');
    $datedebut = $request->getParam('datedebut');
    $datefin = $request->getParam('datefin');



    $req1 = "select count(prospections.idprospection) as nombreprospection
            from prospections where prospections.agentprospection = ? and 
            prospections.dateprospection >= ? and prospections.dateprospection <= ? 
            and prospections.top_actif = 1
            group by prospections.agentprospection";

    $q = $this->db->prepare($req1);
    $retour = $q->execute(array($idagent,$datedebut,$datefin));
    $reponse1 = $q->fetch();

    $req2 = "select count(contrats.idagent)as nombreClient from contrats where contrats.idagent = ? 
            and contrats.dateContrat >= ? and contrats.dateContrat <= ? ";
    
    $q = $this->db->prepare($req2);
    $retour = $q->execute(array($idagent,$datedebut,$datefin));
    $reponse2 = $q->fetch();


    $req3 = "select count(client.idagent) as clientprosp
            from client,prospections
            where client.idprospection = prospections.idprospection
            and client.idagent = ? and client.dateAjout >= ? 
            and client.dateAjout <= ? ";

    $q = $this->db->prepare($req3);
    $retour = $q->execute(array($idagent,$datedebut,$datefin));
    $reponse3 = $q->fetch();


    $tab = array();
    if ($retour) {
        $message = [$reponse1['nombreprospection'], $reponse2['nombreClient'], $reponse3['clientprosp']];
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});


$app->post('/getProspByAgent', function (Request $request, Response $response, array $args) {
    // Sample log message
    //inserer dans la table personne
    //$idprospection = $request->getParam('id');
    //$topactif = $request->getParam('topActif');
    $idagent = $request->getParam('idAgent');
    //$datedebut = "2019-07-26";
    $datedebut = $request->getParam('datedebut');
    //$datefin = "2019-12-31";
    $datefin = $request->getParam('datefin');

    $req = "";
    $variables = array();

    $req1 = 'select *, 
             (select count(prospections.idprospection) from prospections 
             where prospections.agentprospection = ? and prospections.dateprospection >= ?
             and prospections.dateprospection <= ? and prospections.top_actif = 1)
             as nombreprospTotalAgent
             from prospections
             where prospections.agentprospection = ?
             and prospections.dateprospection >= ? and prospections.dateprospection <= ? and prospections.top_actif = 1
             ';

    $req2 = 'select *, 
             (select count(prospections.idprospection) from prospections 
             where prospections.agentprospection = ? and prospections.top_actif = 1)
             as nombreprospTotalAgent
             from prospections
             where prospections.agentprospection = ? and prospections.top_actif = 1
             ';

    if ($datedebut != "" && $datefin != "") {

        $req = $req1;
        $variables = array($idagent, $datedebut, $datefin, $idagent, $datedebut, $datefin);
    } else {
        $req = $req2;
        $variables = array($idagent, $idagent);
    }

    $q = $this->db->prepare($req);
    $retour = $q->execute($variables);
    $reponse1 = $q->fetchAll();


    $tab = array();
    if ($retour) {
        $message = $reponse1;
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});


$app->post('/getContratByAgent', function (Request $request, Response $response, array $args) {
    // Sample log message
    //inserer dans la table personne
    //$idprospection = $request->getParam('id');
    //$topactif = $request->getParam('topActif');
    $idagent = $request->getParam('idAgent');
    //$datedebut = "2019-07-26";
    $datedebut = $request->getParam('datedebut');
    //$datefin = "2019-12-31";
    $datefin = $request->getParam('datefin');

    $req = "";
    $variables = array();

    $req1 = 'select *,
            (select count(contrats.idcontrat) from contrats where contrats.idagent = ?) as nombrecontrat,
            (select SUM(contrats.montantContrat) from contrats where contrats.idagent = ?) as Totalmontantcontrat
            from contrats,client,produits
            where contrats.idclient = client.idClient
            and contrats.idproduit = produits.idproduits where contrats.idagent = ?
             ';

    $req2 = 'select *,
            (select count(contrats.idcontrat) from contrats where contrats.idagent = ?) as nombrecontrat,
            (select SUM(contrats.montantContrat) from contrats where contrats.idagent = ?) as Totalmontantcontrat
            from contrats,client,produits
            where contrats.idclient = client.idClient
            and contrats.idproduit = produits.idproduits and contrats.idagent = ?
             ';

    if ($datedebut != "" && $datefin != "") {

        $req = $req1;
        $variables = array($idagent, $idagent, $idagent);
    } else {
        $req = $req2;
        $variables = array($idagent, $idagent, $idagent);
    }

    $q = $this->db->prepare($req);
    $retour = $q->execute($variables);
    $reponse1 = $q->fetchAll();


    $tab = array();
    if ($retour) {
        $message = $reponse1;
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});


$app->post('/getAllCount', function (Request $request, Response $response, array $args) {
    // Sample log message
    //inserer dans la table personne
    //$idprospection = $request->getParam('id');
    //$topactif = $request->getParam('topActif');
    $idagent = $request->getParam('idAgentCount');
    //$datedebut = "2019-07-26";
    $datedebut = $request->getParam('datedebut');
    //$datefin = "2019-12-31";
    $datefin = $request->getParam('datefin');

    $req = "";
    $variables = array();

    $req1 = 'select distinct prospections.agentprospection as agent,
            (select count(client.idClient) from client where client.idagent = agent and client.top_actif = 1) as nombreClient,
            (select count(prospections.idprospection) from prospections where prospections.agentprospection = agent and prospections.top_actif = 1) as nombreProspection,
            (select count(contrats.idcontrat) from contrats where contrats.idagent = agent and contrats.top_actif = 1 ) as nombreContrat,
            
            (select count(contrats.idcontrat) from contrats where contrats.top_actif = 1 and contrats.idsociete = ?) as sommeContrat,
            (select count(client.idClient) from client where client.top_actif = 1 and client.idsociete = ?) as sommeClient,
            (select COUNT(prospections.idprospection) from prospections where prospections.top_actif = 1 and prospections.idsociete = ?) as sommeprospection,
            (select concat(personne.NomPersonne," ",personne.PrenomPersonne) from personne where personne.idpersonne = agent) as NomAgent,
            (select sum(contrats.montantContrat) from contrats where contrats.idagent = agent ) as chiffreAffaire ,
            (select sum(contrats.montantContrat) from contrats where contrats.idsociete = ? ) as sommeMontant
            from prospections
            where prospections.idsociete = ?
            order by nombreProspection desc';

    $req2 = 'select distinct prospections.agentprospection as agent,
            (select count(client.idClient) from client where client.idagent = agent and client.top_actif = 1) as nombreClient,
            (select count(prospections.idprospection) from prospections where prospections.agentprospection = agent and prospections.top_actif = 1) as nombreProspection,
            (select count(contrats.idcontrat) from contrats where contrats.idagent = agent and contrats.top_actif = 1 ) as nombreContrat,
            
            (select count(contrats.idcontrat) from contrats where contrats.top_actif = 1 and contrats.idsociete = ?) as sommeContrat,
            (select count(client.idClient) from client where client.top_actif = 1 and client.idsociete = ?) as sommeClient,
            (select COUNT(prospections.idprospection) from prospections where prospections.top_actif = 1 and prospections.idsociete = ?) as sommeprospection,
            (select concat(personne.NomPersonne," ",personne.PrenomPersonne) from personne where personne.idpersonne = agent) as NomAgent,
            (select sum(contrats.montantContrat) from contrats where contrats.idagent = agent ) as chiffreAffaire,
            (select sum(contrats.montantContrat) from contrats where contrats.idsociete = ? ) as sommeMontant
            from prospections
            where prospections.idsociete = ?
            order by nombreProspection desc';

    if ($datedebut != "" && $datefin != "") {

        $req = $req1;
        $variables = array($idagent ,$idagent, $idagent, $idagent,$idagent);
    } else {
        $req = $req2;
        $variables = array($idagent ,$idagent, $idagent, $idagent,$idagent);
    }

    $q = $this->db->prepare($req);
    $retour = $q->execute($variables);
    $reponse1 = $q->fetchAll();


    $tab = array();
    if ($retour) {
        $message = $reponse1;
    } else {
        $message = array('code' => 900, 'message' => "Echec d'insertion");
    }

    return $response->withStatus(200)->withHeader('Content-Type', 'application/json')->write(json_encode($message));
});
