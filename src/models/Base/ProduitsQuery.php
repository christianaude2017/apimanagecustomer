<?php

namespace Base;

use \Produits as ChildProduits;
use \ProduitsQuery as ChildProduitsQuery;
use \Exception;
use \PDO;
use Map\ProduitsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'produits' table.
 *
 *
 *
 * @method     ChildProduitsQuery orderByIdproduits($order = Criteria::ASC) Order by the idproduits column
 * @method     ChildProduitsQuery orderByLibelleproduits($order = Criteria::ASC) Order by the Libelleproduits column
 * @method     ChildProduitsQuery orderByIdcatgorieproduit($order = Criteria::ASC) Order by the idcatgorieproduit column
 * @method     ChildProduitsQuery orderByMontantproduits($order = Criteria::ASC) Order by the Montantproduits column
 * @method     ChildProduitsQuery orderBySocieteIdsociete($order = Criteria::ASC) Order by the societe_idsociete column
 *
 * @method     ChildProduitsQuery groupByIdproduits() Group by the idproduits column
 * @method     ChildProduitsQuery groupByLibelleproduits() Group by the Libelleproduits column
 * @method     ChildProduitsQuery groupByIdcatgorieproduit() Group by the idcatgorieproduit column
 * @method     ChildProduitsQuery groupByMontantproduits() Group by the Montantproduits column
 * @method     ChildProduitsQuery groupBySocieteIdsociete() Group by the societe_idsociete column
 *
 * @method     ChildProduitsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProduitsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProduitsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProduitsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProduitsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProduitsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProduitsQuery leftJoinCatgorieproduit($relationAlias = null) Adds a LEFT JOIN clause to the query using the Catgorieproduit relation
 * @method     ChildProduitsQuery rightJoinCatgorieproduit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Catgorieproduit relation
 * @method     ChildProduitsQuery innerJoinCatgorieproduit($relationAlias = null) Adds a INNER JOIN clause to the query using the Catgorieproduit relation
 *
 * @method     ChildProduitsQuery joinWithCatgorieproduit($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Catgorieproduit relation
 *
 * @method     ChildProduitsQuery leftJoinWithCatgorieproduit() Adds a LEFT JOIN clause and with to the query using the Catgorieproduit relation
 * @method     ChildProduitsQuery rightJoinWithCatgorieproduit() Adds a RIGHT JOIN clause and with to the query using the Catgorieproduit relation
 * @method     ChildProduitsQuery innerJoinWithCatgorieproduit() Adds a INNER JOIN clause and with to the query using the Catgorieproduit relation
 *
 * @method     ChildProduitsQuery leftJoinSociete($relationAlias = null) Adds a LEFT JOIN clause to the query using the Societe relation
 * @method     ChildProduitsQuery rightJoinSociete($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Societe relation
 * @method     ChildProduitsQuery innerJoinSociete($relationAlias = null) Adds a INNER JOIN clause to the query using the Societe relation
 *
 * @method     ChildProduitsQuery joinWithSociete($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Societe relation
 *
 * @method     ChildProduitsQuery leftJoinWithSociete() Adds a LEFT JOIN clause and with to the query using the Societe relation
 * @method     ChildProduitsQuery rightJoinWithSociete() Adds a RIGHT JOIN clause and with to the query using the Societe relation
 * @method     ChildProduitsQuery innerJoinWithSociete() Adds a INNER JOIN clause and with to the query using the Societe relation
 *
 * @method     ChildProduitsQuery leftJoinContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contrat relation
 * @method     ChildProduitsQuery rightJoinContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contrat relation
 * @method     ChildProduitsQuery innerJoinContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the Contrat relation
 *
 * @method     ChildProduitsQuery joinWithContrat($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Contrat relation
 *
 * @method     ChildProduitsQuery leftJoinWithContrat() Adds a LEFT JOIN clause and with to the query using the Contrat relation
 * @method     ChildProduitsQuery rightJoinWithContrat() Adds a RIGHT JOIN clause and with to the query using the Contrat relation
 * @method     ChildProduitsQuery innerJoinWithContrat() Adds a INNER JOIN clause and with to the query using the Contrat relation
 *
 * @method     ChildProduitsQuery leftJoinPrime($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prime relation
 * @method     ChildProduitsQuery rightJoinPrime($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prime relation
 * @method     ChildProduitsQuery innerJoinPrime($relationAlias = null) Adds a INNER JOIN clause to the query using the Prime relation
 *
 * @method     ChildProduitsQuery joinWithPrime($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prime relation
 *
 * @method     ChildProduitsQuery leftJoinWithPrime() Adds a LEFT JOIN clause and with to the query using the Prime relation
 * @method     ChildProduitsQuery rightJoinWithPrime() Adds a RIGHT JOIN clause and with to the query using the Prime relation
 * @method     ChildProduitsQuery innerJoinWithPrime() Adds a INNER JOIN clause and with to the query using the Prime relation
 *
 * @method     ChildProduitsQuery leftJoinProspection($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prospection relation
 * @method     ChildProduitsQuery rightJoinProspection($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prospection relation
 * @method     ChildProduitsQuery innerJoinProspection($relationAlias = null) Adds a INNER JOIN clause to the query using the Prospection relation
 *
 * @method     ChildProduitsQuery joinWithProspection($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prospection relation
 *
 * @method     ChildProduitsQuery leftJoinWithProspection() Adds a LEFT JOIN clause and with to the query using the Prospection relation
 * @method     ChildProduitsQuery rightJoinWithProspection() Adds a RIGHT JOIN clause and with to the query using the Prospection relation
 * @method     ChildProduitsQuery innerJoinWithProspection() Adds a INNER JOIN clause and with to the query using the Prospection relation
 *
 * @method     ChildProduitsQuery leftJoinVente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vente relation
 * @method     ChildProduitsQuery rightJoinVente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vente relation
 * @method     ChildProduitsQuery innerJoinVente($relationAlias = null) Adds a INNER JOIN clause to the query using the Vente relation
 *
 * @method     ChildProduitsQuery joinWithVente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Vente relation
 *
 * @method     ChildProduitsQuery leftJoinWithVente() Adds a LEFT JOIN clause and with to the query using the Vente relation
 * @method     ChildProduitsQuery rightJoinWithVente() Adds a RIGHT JOIN clause and with to the query using the Vente relation
 * @method     ChildProduitsQuery innerJoinWithVente() Adds a INNER JOIN clause and with to the query using the Vente relation
 *
 * @method     \CatgorieproduitQuery|\SocieteQuery|\ContratQuery|\PrimeQuery|\ProspectionQuery|\VenteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProduits findOne(ConnectionInterface $con = null) Return the first ChildProduits matching the query
 * @method     ChildProduits findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProduits matching the query, or a new ChildProduits object populated from the query conditions when no match is found
 *
 * @method     ChildProduits findOneByIdproduits(int $idproduits) Return the first ChildProduits filtered by the idproduits column
 * @method     ChildProduits findOneByLibelleproduits(string $Libelleproduits) Return the first ChildProduits filtered by the Libelleproduits column
 * @method     ChildProduits findOneByIdcatgorieproduit(int $idcatgorieproduit) Return the first ChildProduits filtered by the idcatgorieproduit column
 * @method     ChildProduits findOneByMontantproduits(int $Montantproduits) Return the first ChildProduits filtered by the Montantproduits column
 * @method     ChildProduits findOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildProduits filtered by the societe_idsociete column *

 * @method     ChildProduits requirePk($key, ConnectionInterface $con = null) Return the ChildProduits by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduits requireOne(ConnectionInterface $con = null) Return the first ChildProduits matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProduits requireOneByIdproduits(int $idproduits) Return the first ChildProduits filtered by the idproduits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduits requireOneByLibelleproduits(string $Libelleproduits) Return the first ChildProduits filtered by the Libelleproduits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduits requireOneByIdcatgorieproduit(int $idcatgorieproduit) Return the first ChildProduits filtered by the idcatgorieproduit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduits requireOneByMontantproduits(int $Montantproduits) Return the first ChildProduits filtered by the Montantproduits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduits requireOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildProduits filtered by the societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProduits[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProduits objects based on current ModelCriteria
 * @method     ChildProduits[]|ObjectCollection findByIdproduits(int $idproduits) Return ChildProduits objects filtered by the idproduits column
 * @method     ChildProduits[]|ObjectCollection findByLibelleproduits(string $Libelleproduits) Return ChildProduits objects filtered by the Libelleproduits column
 * @method     ChildProduits[]|ObjectCollection findByIdcatgorieproduit(int $idcatgorieproduit) Return ChildProduits objects filtered by the idcatgorieproduit column
 * @method     ChildProduits[]|ObjectCollection findByMontantproduits(int $Montantproduits) Return ChildProduits objects filtered by the Montantproduits column
 * @method     ChildProduits[]|ObjectCollection findBySocieteIdsociete(int $societe_idsociete) Return ChildProduits objects filtered by the societe_idsociete column
 * @method     ChildProduits[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProduitsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ProduitsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Produits', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProduitsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProduitsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProduitsQuery) {
            return $criteria;
        }
        $query = new ChildProduitsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$idproduits, $societe_idsociete] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProduits|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProduitsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProduitsTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProduits A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idproduits, Libelleproduits, idcatgorieproduit, Montantproduits, societe_idsociete FROM produits WHERE idproduits = :p0 AND societe_idsociete = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProduits $obj */
            $obj = new ChildProduits();
            $obj->hydrate($row);
            ProduitsTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProduits|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ProduitsTableMap::COL_IDPRODUITS, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ProduitsTableMap::COL_IDPRODUITS, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idproduits column
     *
     * Example usage:
     * <code>
     * $query->filterByIdproduits(1234); // WHERE idproduits = 1234
     * $query->filterByIdproduits(array(12, 34)); // WHERE idproduits IN (12, 34)
     * $query->filterByIdproduits(array('min' => 12)); // WHERE idproduits > 12
     * </code>
     *
     * @param     mixed $idproduits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByIdproduits($idproduits = null, $comparison = null)
    {
        if (is_array($idproduits)) {
            $useMinMax = false;
            if (isset($idproduits['min'])) {
                $this->addUsingAlias(ProduitsTableMap::COL_IDPRODUITS, $idproduits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idproduits['max'])) {
                $this->addUsingAlias(ProduitsTableMap::COL_IDPRODUITS, $idproduits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduitsTableMap::COL_IDPRODUITS, $idproduits, $comparison);
    }

    /**
     * Filter the query on the Libelleproduits column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleproduits('fooValue');   // WHERE Libelleproduits = 'fooValue'
     * $query->filterByLibelleproduits('%fooValue%', Criteria::LIKE); // WHERE Libelleproduits LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleproduits The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByLibelleproduits($libelleproduits = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleproduits)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduitsTableMap::COL_LIBELLEPRODUITS, $libelleproduits, $comparison);
    }

    /**
     * Filter the query on the idcatgorieproduit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcatgorieproduit(1234); // WHERE idcatgorieproduit = 1234
     * $query->filterByIdcatgorieproduit(array(12, 34)); // WHERE idcatgorieproduit IN (12, 34)
     * $query->filterByIdcatgorieproduit(array('min' => 12)); // WHERE idcatgorieproduit > 12
     * </code>
     *
     * @see       filterByCatgorieproduit()
     *
     * @param     mixed $idcatgorieproduit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByIdcatgorieproduit($idcatgorieproduit = null, $comparison = null)
    {
        if (is_array($idcatgorieproduit)) {
            $useMinMax = false;
            if (isset($idcatgorieproduit['min'])) {
                $this->addUsingAlias(ProduitsTableMap::COL_IDCATGORIEPRODUIT, $idcatgorieproduit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcatgorieproduit['max'])) {
                $this->addUsingAlias(ProduitsTableMap::COL_IDCATGORIEPRODUIT, $idcatgorieproduit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduitsTableMap::COL_IDCATGORIEPRODUIT, $idcatgorieproduit, $comparison);
    }

    /**
     * Filter the query on the Montantproduits column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantproduits(1234); // WHERE Montantproduits = 1234
     * $query->filterByMontantproduits(array(12, 34)); // WHERE Montantproduits IN (12, 34)
     * $query->filterByMontantproduits(array('min' => 12)); // WHERE Montantproduits > 12
     * </code>
     *
     * @param     mixed $montantproduits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByMontantproduits($montantproduits = null, $comparison = null)
    {
        if (is_array($montantproduits)) {
            $useMinMax = false;
            if (isset($montantproduits['min'])) {
                $this->addUsingAlias(ProduitsTableMap::COL_MONTANTPRODUITS, $montantproduits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantproduits['max'])) {
                $this->addUsingAlias(ProduitsTableMap::COL_MONTANTPRODUITS, $montantproduits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduitsTableMap::COL_MONTANTPRODUITS, $montantproduits, $comparison);
    }

    /**
     * Filter the query on the societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterBySocieteIdsociete(1234); // WHERE societe_idsociete = 1234
     * $query->filterBySocieteIdsociete(array(12, 34)); // WHERE societe_idsociete IN (12, 34)
     * $query->filterBySocieteIdsociete(array('min' => 12)); // WHERE societe_idsociete > 12
     * </code>
     *
     * @see       filterBySociete()
     *
     * @param     mixed $societeIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function filterBySocieteIdsociete($societeIdsociete = null, $comparison = null)
    {
        if (is_array($societeIdsociete)) {
            $useMinMax = false;
            if (isset($societeIdsociete['min'])) {
                $this->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($societeIdsociete['max'])) {
                $this->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete, $comparison);
    }

    /**
     * Filter the query by a related \Catgorieproduit object
     *
     * @param \Catgorieproduit|ObjectCollection $catgorieproduit The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByCatgorieproduit($catgorieproduit, $comparison = null)
    {
        if ($catgorieproduit instanceof \Catgorieproduit) {
            return $this
                ->addUsingAlias(ProduitsTableMap::COL_IDCATGORIEPRODUIT, $catgorieproduit->getIdcatgorieproduit(), $comparison);
        } elseif ($catgorieproduit instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProduitsTableMap::COL_IDCATGORIEPRODUIT, $catgorieproduit->toKeyValue('PrimaryKey', 'Idcatgorieproduit'), $comparison);
        } else {
            throw new PropelException('filterByCatgorieproduit() only accepts arguments of type \Catgorieproduit or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Catgorieproduit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function joinCatgorieproduit($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Catgorieproduit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Catgorieproduit');
        }

        return $this;
    }

    /**
     * Use the Catgorieproduit relation Catgorieproduit object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CatgorieproduitQuery A secondary query class using the current class as primary query
     */
    public function useCatgorieproduitQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCatgorieproduit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Catgorieproduit', '\CatgorieproduitQuery');
    }

    /**
     * Filter the query by a related \Societe object
     *
     * @param \Societe|ObjectCollection $societe The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProduitsQuery The current query, for fluid interface
     */
    public function filterBySociete($societe, $comparison = null)
    {
        if ($societe instanceof \Societe) {
            return $this
                ->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $societe->getIdsociete(), $comparison);
        } elseif ($societe instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $societe->toKeyValue('PrimaryKey', 'Idsociete'), $comparison);
        } else {
            throw new PropelException('filterBySociete() only accepts arguments of type \Societe or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Societe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function joinSociete($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Societe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Societe');
        }

        return $this;
    }

    /**
     * Use the Societe relation Societe object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SocieteQuery A secondary query class using the current class as primary query
     */
    public function useSocieteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSociete($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Societe', '\SocieteQuery');
    }

    /**
     * Filter the query by a related \Contrat object
     *
     * @param \Contrat|ObjectCollection $contrat the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByContrat($contrat, $comparison = null)
    {
        if ($contrat instanceof \Contrat) {
            return $this
                ->addUsingAlias(ProduitsTableMap::COL_IDPRODUITS, $contrat->getProduitsIdproduits1(), $comparison)
                ->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $contrat->getProduitsSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByContrat() only accepts arguments of type \Contrat');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function joinContrat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contrat');
        }

        return $this;
    }

    /**
     * Use the Contrat relation Contrat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ContratQuery A secondary query class using the current class as primary query
     */
    public function useContratQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contrat', '\ContratQuery');
    }

    /**
     * Filter the query by a related \Prime object
     *
     * @param \Prime|ObjectCollection $prime the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByPrime($prime, $comparison = null)
    {
        if ($prime instanceof \Prime) {
            return $this
                ->addUsingAlias(ProduitsTableMap::COL_IDPRODUITS, $prime->getProduitsIdproduits(), $comparison)
                ->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $prime->getProduitsSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByPrime() only accepts arguments of type \Prime');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prime relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function joinPrime($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prime');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prime');
        }

        return $this;
    }

    /**
     * Use the Prime relation Prime object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PrimeQuery A secondary query class using the current class as primary query
     */
    public function usePrimeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrime($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prime', '\PrimeQuery');
    }

    /**
     * Filter the query by a related \Prospection object
     *
     * @param \Prospection|ObjectCollection $prospection the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByProspection($prospection, $comparison = null)
    {
        if ($prospection instanceof \Prospection) {
            return $this
                ->addUsingAlias(ProduitsTableMap::COL_IDPRODUITS, $prospection->getProduitsIdproduits(), $comparison)
                ->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $prospection->getProduitsSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByProspection() only accepts arguments of type \Prospection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prospection relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function joinProspection($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prospection');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prospection');
        }

        return $this;
    }

    /**
     * Use the Prospection relation Prospection object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProspectionQuery A secondary query class using the current class as primary query
     */
    public function useProspectionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProspection($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prospection', '\ProspectionQuery');
    }

    /**
     * Filter the query by a related \Vente object
     *
     * @param \Vente|ObjectCollection $vente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProduitsQuery The current query, for fluid interface
     */
    public function filterByVente($vente, $comparison = null)
    {
        if ($vente instanceof \Vente) {
            return $this
                ->addUsingAlias(ProduitsTableMap::COL_IDPRODUITS, $vente->getProduitsIdproduits(), $comparison)
                ->addUsingAlias(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $vente->getProduitsSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByVente() only accepts arguments of type \Vente');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Vente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function joinVente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Vente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Vente');
        }

        return $this;
    }

    /**
     * Use the Vente relation Vente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VenteQuery A secondary query class using the current class as primary query
     */
    public function useVenteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Vente', '\VenteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProduits $produits Object to remove from the list of results
     *
     * @return $this|ChildProduitsQuery The current query, for fluid interface
     */
    public function prune($produits = null)
    {
        if ($produits) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ProduitsTableMap::COL_IDPRODUITS), $produits->getIdproduits(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ProduitsTableMap::COL_SOCIETE_IDSOCIETE), $produits->getSocieteIdsociete(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the produits table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduitsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProduitsTableMap::clearInstancePool();
            ProduitsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduitsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProduitsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProduitsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProduitsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProduitsQuery
