<?php

namespace Base;

use \Personne as ChildPersonne;
use \PersonneQuery as ChildPersonneQuery;
use \Exception;
use \PDO;
use Map\PersonneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'personne' table.
 *
 *
 *
 * @method     ChildPersonneQuery orderByIdpersonne($order = Criteria::ASC) Order by the idPersonne column
 * @method     ChildPersonneQuery orderByNompersonne($order = Criteria::ASC) Order by the NomPersonne column
 * @method     ChildPersonneQuery orderByPrenompersonne($order = Criteria::ASC) Order by the PrenomPersonne column
 * @method     ChildPersonneQuery orderBySexe($order = Criteria::ASC) Order by the sexe column
 * @method     ChildPersonneQuery orderByDatenaissance($order = Criteria::ASC) Order by the datenaissance column
 * @method     ChildPersonneQuery orderByRolepersonneIdrolepersonne($order = Criteria::ASC) Order by the rolePersonne_idrolePersonne column
 * @method     ChildPersonneQuery orderByCommuneIdcommune($order = Criteria::ASC) Order by the commune_idcommune column
 * @method     ChildPersonneQuery orderByVilleIdville($order = Criteria::ASC) Order by the ville_idville column
 * @method     ChildPersonneQuery orderByVillePaysIdpays($order = Criteria::ASC) Order by the ville_pays_idpays column
 * @method     ChildPersonneQuery orderBySocieteIdsociete($order = Criteria::ASC) Order by the societe_idsociete column
 *
 * @method     ChildPersonneQuery groupByIdpersonne() Group by the idPersonne column
 * @method     ChildPersonneQuery groupByNompersonne() Group by the NomPersonne column
 * @method     ChildPersonneQuery groupByPrenompersonne() Group by the PrenomPersonne column
 * @method     ChildPersonneQuery groupBySexe() Group by the sexe column
 * @method     ChildPersonneQuery groupByDatenaissance() Group by the datenaissance column
 * @method     ChildPersonneQuery groupByRolepersonneIdrolepersonne() Group by the rolePersonne_idrolePersonne column
 * @method     ChildPersonneQuery groupByCommuneIdcommune() Group by the commune_idcommune column
 * @method     ChildPersonneQuery groupByVilleIdville() Group by the ville_idville column
 * @method     ChildPersonneQuery groupByVillePaysIdpays() Group by the ville_pays_idpays column
 * @method     ChildPersonneQuery groupBySocieteIdsociete() Group by the societe_idsociete column
 *
 * @method     ChildPersonneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPersonneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPersonneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPersonneQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPersonneQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPersonneQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPersonneQuery leftJoinCommune($relationAlias = null) Adds a LEFT JOIN clause to the query using the Commune relation
 * @method     ChildPersonneQuery rightJoinCommune($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Commune relation
 * @method     ChildPersonneQuery innerJoinCommune($relationAlias = null) Adds a INNER JOIN clause to the query using the Commune relation
 *
 * @method     ChildPersonneQuery joinWithCommune($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Commune relation
 *
 * @method     ChildPersonneQuery leftJoinWithCommune() Adds a LEFT JOIN clause and with to the query using the Commune relation
 * @method     ChildPersonneQuery rightJoinWithCommune() Adds a RIGHT JOIN clause and with to the query using the Commune relation
 * @method     ChildPersonneQuery innerJoinWithCommune() Adds a INNER JOIN clause and with to the query using the Commune relation
 *
 * @method     ChildPersonneQuery leftJoinRolepersonne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Rolepersonne relation
 * @method     ChildPersonneQuery rightJoinRolepersonne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Rolepersonne relation
 * @method     ChildPersonneQuery innerJoinRolepersonne($relationAlias = null) Adds a INNER JOIN clause to the query using the Rolepersonne relation
 *
 * @method     ChildPersonneQuery joinWithRolepersonne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Rolepersonne relation
 *
 * @method     ChildPersonneQuery leftJoinWithRolepersonne() Adds a LEFT JOIN clause and with to the query using the Rolepersonne relation
 * @method     ChildPersonneQuery rightJoinWithRolepersonne() Adds a RIGHT JOIN clause and with to the query using the Rolepersonne relation
 * @method     ChildPersonneQuery innerJoinWithRolepersonne() Adds a INNER JOIN clause and with to the query using the Rolepersonne relation
 *
 * @method     ChildPersonneQuery leftJoinSociete($relationAlias = null) Adds a LEFT JOIN clause to the query using the Societe relation
 * @method     ChildPersonneQuery rightJoinSociete($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Societe relation
 * @method     ChildPersonneQuery innerJoinSociete($relationAlias = null) Adds a INNER JOIN clause to the query using the Societe relation
 *
 * @method     ChildPersonneQuery joinWithSociete($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Societe relation
 *
 * @method     ChildPersonneQuery leftJoinWithSociete() Adds a LEFT JOIN clause and with to the query using the Societe relation
 * @method     ChildPersonneQuery rightJoinWithSociete() Adds a RIGHT JOIN clause and with to the query using the Societe relation
 * @method     ChildPersonneQuery innerJoinWithSociete() Adds a INNER JOIN clause and with to the query using the Societe relation
 *
 * @method     ChildPersonneQuery leftJoinVille($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ville relation
 * @method     ChildPersonneQuery rightJoinVille($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ville relation
 * @method     ChildPersonneQuery innerJoinVille($relationAlias = null) Adds a INNER JOIN clause to the query using the Ville relation
 *
 * @method     ChildPersonneQuery joinWithVille($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ville relation
 *
 * @method     ChildPersonneQuery leftJoinWithVille() Adds a LEFT JOIN clause and with to the query using the Ville relation
 * @method     ChildPersonneQuery rightJoinWithVille() Adds a RIGHT JOIN clause and with to the query using the Ville relation
 * @method     ChildPersonneQuery innerJoinWithVille() Adds a INNER JOIN clause and with to the query using the Ville relation
 *
 * @method     ChildPersonneQuery leftJoinContratRelatedByPersonneClientIdrolepersonneClient($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContratRelatedByPersonneClientIdrolepersonneClient relation
 * @method     ChildPersonneQuery rightJoinContratRelatedByPersonneClientIdrolepersonneClient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContratRelatedByPersonneClientIdrolepersonneClient relation
 * @method     ChildPersonneQuery innerJoinContratRelatedByPersonneClientIdrolepersonneClient($relationAlias = null) Adds a INNER JOIN clause to the query using the ContratRelatedByPersonneClientIdrolepersonneClient relation
 *
 * @method     ChildPersonneQuery joinWithContratRelatedByPersonneClientIdrolepersonneClient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContratRelatedByPersonneClientIdrolepersonneClient relation
 *
 * @method     ChildPersonneQuery leftJoinWithContratRelatedByPersonneClientIdrolepersonneClient() Adds a LEFT JOIN clause and with to the query using the ContratRelatedByPersonneClientIdrolepersonneClient relation
 * @method     ChildPersonneQuery rightJoinWithContratRelatedByPersonneClientIdrolepersonneClient() Adds a RIGHT JOIN clause and with to the query using the ContratRelatedByPersonneClientIdrolepersonneClient relation
 * @method     ChildPersonneQuery innerJoinWithContratRelatedByPersonneClientIdrolepersonneClient() Adds a INNER JOIN clause and with to the query using the ContratRelatedByPersonneClientIdrolepersonneClient relation
 *
 * @method     ChildPersonneQuery leftJoinContratRelatedByPersonneAgentIdrolepersonneAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContratRelatedByPersonneAgentIdrolepersonneAgent relation
 * @method     ChildPersonneQuery rightJoinContratRelatedByPersonneAgentIdrolepersonneAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContratRelatedByPersonneAgentIdrolepersonneAgent relation
 * @method     ChildPersonneQuery innerJoinContratRelatedByPersonneAgentIdrolepersonneAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the ContratRelatedByPersonneAgentIdrolepersonneAgent relation
 *
 * @method     ChildPersonneQuery joinWithContratRelatedByPersonneAgentIdrolepersonneAgent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContratRelatedByPersonneAgentIdrolepersonneAgent relation
 *
 * @method     ChildPersonneQuery leftJoinWithContratRelatedByPersonneAgentIdrolepersonneAgent() Adds a LEFT JOIN clause and with to the query using the ContratRelatedByPersonneAgentIdrolepersonneAgent relation
 * @method     ChildPersonneQuery rightJoinWithContratRelatedByPersonneAgentIdrolepersonneAgent() Adds a RIGHT JOIN clause and with to the query using the ContratRelatedByPersonneAgentIdrolepersonneAgent relation
 * @method     ChildPersonneQuery innerJoinWithContratRelatedByPersonneAgentIdrolepersonneAgent() Adds a INNER JOIN clause and with to the query using the ContratRelatedByPersonneAgentIdrolepersonneAgent relation
 *
 * @method     ChildPersonneQuery leftJoinProspection($relationAlias = null) Adds a LEFT JOIN clause to the query using the Prospection relation
 * @method     ChildPersonneQuery rightJoinProspection($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Prospection relation
 * @method     ChildPersonneQuery innerJoinProspection($relationAlias = null) Adds a INNER JOIN clause to the query using the Prospection relation
 *
 * @method     ChildPersonneQuery joinWithProspection($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Prospection relation
 *
 * @method     ChildPersonneQuery leftJoinWithProspection() Adds a LEFT JOIN clause and with to the query using the Prospection relation
 * @method     ChildPersonneQuery rightJoinWithProspection() Adds a RIGHT JOIN clause and with to the query using the Prospection relation
 * @method     ChildPersonneQuery innerJoinWithProspection() Adds a INNER JOIN clause and with to the query using the Prospection relation
 *
 * @method     ChildPersonneQuery leftJoinResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($relationAlias = null) Adds a LEFT JOIN clause to the query using the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation
 * @method     ChildPersonneQuery rightJoinResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation
 * @method     ChildPersonneQuery innerJoinResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($relationAlias = null) Adds a INNER JOIN clause to the query using the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation
 *
 * @method     ChildPersonneQuery joinWithResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation
 *
 * @method     ChildPersonneQuery leftJoinWithResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable() Adds a LEFT JOIN clause and with to the query using the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation
 * @method     ChildPersonneQuery rightJoinWithResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable() Adds a RIGHT JOIN clause and with to the query using the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation
 * @method     ChildPersonneQuery innerJoinWithResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable() Adds a INNER JOIN clause and with to the query using the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation
 *
 * @method     ChildPersonneQuery leftJoinResponsableAgentRelatedByPersonneIdagentIdroleagent($relationAlias = null) Adds a LEFT JOIN clause to the query using the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation
 * @method     ChildPersonneQuery rightJoinResponsableAgentRelatedByPersonneIdagentIdroleagent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation
 * @method     ChildPersonneQuery innerJoinResponsableAgentRelatedByPersonneIdagentIdroleagent($relationAlias = null) Adds a INNER JOIN clause to the query using the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation
 *
 * @method     ChildPersonneQuery joinWithResponsableAgentRelatedByPersonneIdagentIdroleagent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation
 *
 * @method     ChildPersonneQuery leftJoinWithResponsableAgentRelatedByPersonneIdagentIdroleagent() Adds a LEFT JOIN clause and with to the query using the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation
 * @method     ChildPersonneQuery rightJoinWithResponsableAgentRelatedByPersonneIdagentIdroleagent() Adds a RIGHT JOIN clause and with to the query using the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation
 * @method     ChildPersonneQuery innerJoinWithResponsableAgentRelatedByPersonneIdagentIdroleagent() Adds a INNER JOIN clause and with to the query using the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation
 *
 * @method     ChildPersonneQuery leftJoinVente($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vente relation
 * @method     ChildPersonneQuery rightJoinVente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vente relation
 * @method     ChildPersonneQuery innerJoinVente($relationAlias = null) Adds a INNER JOIN clause to the query using the Vente relation
 *
 * @method     ChildPersonneQuery joinWithVente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Vente relation
 *
 * @method     ChildPersonneQuery leftJoinWithVente() Adds a LEFT JOIN clause and with to the query using the Vente relation
 * @method     ChildPersonneQuery rightJoinWithVente() Adds a RIGHT JOIN clause and with to the query using the Vente relation
 * @method     ChildPersonneQuery innerJoinWithVente() Adds a INNER JOIN clause and with to the query using the Vente relation
 *
 * @method     \CommuneQuery|\RolepersonneQuery|\SocieteQuery|\VilleQuery|\ContratQuery|\ProspectionQuery|\ResponsableAgentQuery|\VenteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPersonne findOne(ConnectionInterface $con = null) Return the first ChildPersonne matching the query
 * @method     ChildPersonne findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPersonne matching the query, or a new ChildPersonne object populated from the query conditions when no match is found
 *
 * @method     ChildPersonne findOneByIdpersonne(int $idPersonne) Return the first ChildPersonne filtered by the idPersonne column
 * @method     ChildPersonne findOneByNompersonne(string $NomPersonne) Return the first ChildPersonne filtered by the NomPersonne column
 * @method     ChildPersonne findOneByPrenompersonne(string $PrenomPersonne) Return the first ChildPersonne filtered by the PrenomPersonne column
 * @method     ChildPersonne findOneBySexe(string $sexe) Return the first ChildPersonne filtered by the sexe column
 * @method     ChildPersonne findOneByDatenaissance(string $datenaissance) Return the first ChildPersonne filtered by the datenaissance column
 * @method     ChildPersonne findOneByRolepersonneIdrolepersonne(int $rolePersonne_idrolePersonne) Return the first ChildPersonne filtered by the rolePersonne_idrolePersonne column
 * @method     ChildPersonne findOneByCommuneIdcommune(int $commune_idcommune) Return the first ChildPersonne filtered by the commune_idcommune column
 * @method     ChildPersonne findOneByVilleIdville(int $ville_idville) Return the first ChildPersonne filtered by the ville_idville column
 * @method     ChildPersonne findOneByVillePaysIdpays(int $ville_pays_idpays) Return the first ChildPersonne filtered by the ville_pays_idpays column
 * @method     ChildPersonne findOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildPersonne filtered by the societe_idsociete column *

 * @method     ChildPersonne requirePk($key, ConnectionInterface $con = null) Return the ChildPersonne by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOne(ConnectionInterface $con = null) Return the first ChildPersonne matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPersonne requireOneByIdpersonne(int $idPersonne) Return the first ChildPersonne filtered by the idPersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneByNompersonne(string $NomPersonne) Return the first ChildPersonne filtered by the NomPersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneByPrenompersonne(string $PrenomPersonne) Return the first ChildPersonne filtered by the PrenomPersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneBySexe(string $sexe) Return the first ChildPersonne filtered by the sexe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneByDatenaissance(string $datenaissance) Return the first ChildPersonne filtered by the datenaissance column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneByRolepersonneIdrolepersonne(int $rolePersonne_idrolePersonne) Return the first ChildPersonne filtered by the rolePersonne_idrolePersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneByCommuneIdcommune(int $commune_idcommune) Return the first ChildPersonne filtered by the commune_idcommune column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneByVilleIdville(int $ville_idville) Return the first ChildPersonne filtered by the ville_idville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneByVillePaysIdpays(int $ville_pays_idpays) Return the first ChildPersonne filtered by the ville_pays_idpays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPersonne requireOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildPersonne filtered by the societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPersonne[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPersonne objects based on current ModelCriteria
 * @method     ChildPersonne[]|ObjectCollection findByIdpersonne(int $idPersonne) Return ChildPersonne objects filtered by the idPersonne column
 * @method     ChildPersonne[]|ObjectCollection findByNompersonne(string $NomPersonne) Return ChildPersonne objects filtered by the NomPersonne column
 * @method     ChildPersonne[]|ObjectCollection findByPrenompersonne(string $PrenomPersonne) Return ChildPersonne objects filtered by the PrenomPersonne column
 * @method     ChildPersonne[]|ObjectCollection findBySexe(string $sexe) Return ChildPersonne objects filtered by the sexe column
 * @method     ChildPersonne[]|ObjectCollection findByDatenaissance(string $datenaissance) Return ChildPersonne objects filtered by the datenaissance column
 * @method     ChildPersonne[]|ObjectCollection findByRolepersonneIdrolepersonne(int $rolePersonne_idrolePersonne) Return ChildPersonne objects filtered by the rolePersonne_idrolePersonne column
 * @method     ChildPersonne[]|ObjectCollection findByCommuneIdcommune(int $commune_idcommune) Return ChildPersonne objects filtered by the commune_idcommune column
 * @method     ChildPersonne[]|ObjectCollection findByVilleIdville(int $ville_idville) Return ChildPersonne objects filtered by the ville_idville column
 * @method     ChildPersonne[]|ObjectCollection findByVillePaysIdpays(int $ville_pays_idpays) Return ChildPersonne objects filtered by the ville_pays_idpays column
 * @method     ChildPersonne[]|ObjectCollection findBySocieteIdsociete(int $societe_idsociete) Return ChildPersonne objects filtered by the societe_idsociete column
 * @method     ChildPersonne[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PersonneQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PersonneQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Personne', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPersonneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPersonneQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPersonneQuery) {
            return $criteria;
        }
        $query = new ChildPersonneQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$idPersonne, $rolePersonne_idrolePersonne] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPersonne|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PersonneTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PersonneTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPersonne A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idPersonne, NomPersonne, PrenomPersonne, sexe, datenaissance, rolePersonne_idrolePersonne, commune_idcommune, ville_idville, ville_pays_idpays, societe_idsociete FROM personne WHERE idPersonne = :p0 AND rolePersonne_idrolePersonne = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPersonne $obj */
            $obj = new ChildPersonne();
            $obj->hydrate($row);
            PersonneTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPersonne|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(PersonneTableMap::COL_IDPERSONNE, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idPersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByIdpersonne(1234); // WHERE idPersonne = 1234
     * $query->filterByIdpersonne(array(12, 34)); // WHERE idPersonne IN (12, 34)
     * $query->filterByIdpersonne(array('min' => 12)); // WHERE idPersonne > 12
     * </code>
     *
     * @param     mixed $idpersonne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByIdpersonne($idpersonne = null, $comparison = null)
    {
        if (is_array($idpersonne)) {
            $useMinMax = false;
            if (isset($idpersonne['min'])) {
                $this->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $idpersonne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idpersonne['max'])) {
                $this->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $idpersonne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $idpersonne, $comparison);
    }

    /**
     * Filter the query on the NomPersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByNompersonne('fooValue');   // WHERE NomPersonne = 'fooValue'
     * $query->filterByNompersonne('%fooValue%', Criteria::LIKE); // WHERE NomPersonne LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nompersonne The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByNompersonne($nompersonne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nompersonne)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_NOMPERSONNE, $nompersonne, $comparison);
    }

    /**
     * Filter the query on the PrenomPersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenompersonne('fooValue');   // WHERE PrenomPersonne = 'fooValue'
     * $query->filterByPrenompersonne('%fooValue%', Criteria::LIKE); // WHERE PrenomPersonne LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenompersonne The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByPrenompersonne($prenompersonne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenompersonne)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_PRENOMPERSONNE, $prenompersonne, $comparison);
    }

    /**
     * Filter the query on the sexe column
     *
     * Example usage:
     * <code>
     * $query->filterBySexe('fooValue');   // WHERE sexe = 'fooValue'
     * $query->filterBySexe('%fooValue%', Criteria::LIKE); // WHERE sexe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sexe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterBySexe($sexe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sexe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_SEXE, $sexe, $comparison);
    }

    /**
     * Filter the query on the datenaissance column
     *
     * Example usage:
     * <code>
     * $query->filterByDatenaissance('2011-03-14'); // WHERE datenaissance = '2011-03-14'
     * $query->filterByDatenaissance('now'); // WHERE datenaissance = '2011-03-14'
     * $query->filterByDatenaissance(array('max' => 'yesterday')); // WHERE datenaissance > '2011-03-13'
     * </code>
     *
     * @param     mixed $datenaissance The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByDatenaissance($datenaissance = null, $comparison = null)
    {
        if (is_array($datenaissance)) {
            $useMinMax = false;
            if (isset($datenaissance['min'])) {
                $this->addUsingAlias(PersonneTableMap::COL_DATENAISSANCE, $datenaissance['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datenaissance['max'])) {
                $this->addUsingAlias(PersonneTableMap::COL_DATENAISSANCE, $datenaissance['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_DATENAISSANCE, $datenaissance, $comparison);
    }

    /**
     * Filter the query on the rolePersonne_idrolePersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByRolepersonneIdrolepersonne(1234); // WHERE rolePersonne_idrolePersonne = 1234
     * $query->filterByRolepersonneIdrolepersonne(array(12, 34)); // WHERE rolePersonne_idrolePersonne IN (12, 34)
     * $query->filterByRolepersonneIdrolepersonne(array('min' => 12)); // WHERE rolePersonne_idrolePersonne > 12
     * </code>
     *
     * @see       filterByRolepersonne()
     *
     * @param     mixed $rolepersonneIdrolepersonne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByRolepersonneIdrolepersonne($rolepersonneIdrolepersonne = null, $comparison = null)
    {
        if (is_array($rolepersonneIdrolepersonne)) {
            $useMinMax = false;
            if (isset($rolepersonneIdrolepersonne['min'])) {
                $this->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $rolepersonneIdrolepersonne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rolepersonneIdrolepersonne['max'])) {
                $this->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $rolepersonneIdrolepersonne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $rolepersonneIdrolepersonne, $comparison);
    }

    /**
     * Filter the query on the commune_idcommune column
     *
     * Example usage:
     * <code>
     * $query->filterByCommuneIdcommune(1234); // WHERE commune_idcommune = 1234
     * $query->filterByCommuneIdcommune(array(12, 34)); // WHERE commune_idcommune IN (12, 34)
     * $query->filterByCommuneIdcommune(array('min' => 12)); // WHERE commune_idcommune > 12
     * </code>
     *
     * @see       filterByCommune()
     *
     * @param     mixed $communeIdcommune The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByCommuneIdcommune($communeIdcommune = null, $comparison = null)
    {
        if (is_array($communeIdcommune)) {
            $useMinMax = false;
            if (isset($communeIdcommune['min'])) {
                $this->addUsingAlias(PersonneTableMap::COL_COMMUNE_IDCOMMUNE, $communeIdcommune['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($communeIdcommune['max'])) {
                $this->addUsingAlias(PersonneTableMap::COL_COMMUNE_IDCOMMUNE, $communeIdcommune['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_COMMUNE_IDCOMMUNE, $communeIdcommune, $comparison);
    }

    /**
     * Filter the query on the ville_idville column
     *
     * Example usage:
     * <code>
     * $query->filterByVilleIdville(1234); // WHERE ville_idville = 1234
     * $query->filterByVilleIdville(array(12, 34)); // WHERE ville_idville IN (12, 34)
     * $query->filterByVilleIdville(array('min' => 12)); // WHERE ville_idville > 12
     * </code>
     *
     * @see       filterByVille()
     *
     * @param     mixed $villeIdville The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByVilleIdville($villeIdville = null, $comparison = null)
    {
        if (is_array($villeIdville)) {
            $useMinMax = false;
            if (isset($villeIdville['min'])) {
                $this->addUsingAlias(PersonneTableMap::COL_VILLE_IDVILLE, $villeIdville['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($villeIdville['max'])) {
                $this->addUsingAlias(PersonneTableMap::COL_VILLE_IDVILLE, $villeIdville['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_VILLE_IDVILLE, $villeIdville, $comparison);
    }

    /**
     * Filter the query on the ville_pays_idpays column
     *
     * Example usage:
     * <code>
     * $query->filterByVillePaysIdpays(1234); // WHERE ville_pays_idpays = 1234
     * $query->filterByVillePaysIdpays(array(12, 34)); // WHERE ville_pays_idpays IN (12, 34)
     * $query->filterByVillePaysIdpays(array('min' => 12)); // WHERE ville_pays_idpays > 12
     * </code>
     *
     * @see       filterByVille()
     *
     * @param     mixed $villePaysIdpays The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByVillePaysIdpays($villePaysIdpays = null, $comparison = null)
    {
        if (is_array($villePaysIdpays)) {
            $useMinMax = false;
            if (isset($villePaysIdpays['min'])) {
                $this->addUsingAlias(PersonneTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($villePaysIdpays['max'])) {
                $this->addUsingAlias(PersonneTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays, $comparison);
    }

    /**
     * Filter the query on the societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterBySocieteIdsociete(1234); // WHERE societe_idsociete = 1234
     * $query->filterBySocieteIdsociete(array(12, 34)); // WHERE societe_idsociete IN (12, 34)
     * $query->filterBySocieteIdsociete(array('min' => 12)); // WHERE societe_idsociete > 12
     * </code>
     *
     * @see       filterBySociete()
     *
     * @param     mixed $societeIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function filterBySocieteIdsociete($societeIdsociete = null, $comparison = null)
    {
        if (is_array($societeIdsociete)) {
            $useMinMax = false;
            if (isset($societeIdsociete['min'])) {
                $this->addUsingAlias(PersonneTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($societeIdsociete['max'])) {
                $this->addUsingAlias(PersonneTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PersonneTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete, $comparison);
    }

    /**
     * Filter the query by a related \Commune object
     *
     * @param \Commune|ObjectCollection $commune The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByCommune($commune, $comparison = null)
    {
        if ($commune instanceof \Commune) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_COMMUNE_IDCOMMUNE, $commune->getIdcommune(), $comparison);
        } elseif ($commune instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PersonneTableMap::COL_COMMUNE_IDCOMMUNE, $commune->toKeyValue('PrimaryKey', 'Idcommune'), $comparison);
        } else {
            throw new PropelException('filterByCommune() only accepts arguments of type \Commune or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Commune relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinCommune($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Commune');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Commune');
        }

        return $this;
    }

    /**
     * Use the Commune relation Commune object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CommuneQuery A secondary query class using the current class as primary query
     */
    public function useCommuneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommune($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Commune', '\CommuneQuery');
    }

    /**
     * Filter the query by a related \Rolepersonne object
     *
     * @param \Rolepersonne|ObjectCollection $rolepersonne The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByRolepersonne($rolepersonne, $comparison = null)
    {
        if ($rolepersonne instanceof \Rolepersonne) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $rolepersonne->getIdrolepersonne(), $comparison);
        } elseif ($rolepersonne instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $rolepersonne->toKeyValue('PrimaryKey', 'Idrolepersonne'), $comparison);
        } else {
            throw new PropelException('filterByRolepersonne() only accepts arguments of type \Rolepersonne or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Rolepersonne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinRolepersonne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Rolepersonne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Rolepersonne');
        }

        return $this;
    }

    /**
     * Use the Rolepersonne relation Rolepersonne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RolepersonneQuery A secondary query class using the current class as primary query
     */
    public function useRolepersonneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRolepersonne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Rolepersonne', '\RolepersonneQuery');
    }

    /**
     * Filter the query by a related \Societe object
     *
     * @param \Societe|ObjectCollection $societe The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterBySociete($societe, $comparison = null)
    {
        if ($societe instanceof \Societe) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_SOCIETE_IDSOCIETE, $societe->getIdsociete(), $comparison);
        } elseif ($societe instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(PersonneTableMap::COL_SOCIETE_IDSOCIETE, $societe->toKeyValue('PrimaryKey', 'Idsociete'), $comparison);
        } else {
            throw new PropelException('filterBySociete() only accepts arguments of type \Societe or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Societe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinSociete($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Societe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Societe');
        }

        return $this;
    }

    /**
     * Use the Societe relation Societe object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SocieteQuery A secondary query class using the current class as primary query
     */
    public function useSocieteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSociete($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Societe', '\SocieteQuery');
    }

    /**
     * Filter the query by a related \Ville object
     *
     * @param \Ville $ville The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByVille($ville, $comparison = null)
    {
        if ($ville instanceof \Ville) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_VILLE_IDVILLE, $ville->getIdville(), $comparison)
                ->addUsingAlias(PersonneTableMap::COL_VILLE_PAYS_IDPAYS, $ville->getPaysIdpays(), $comparison);
        } else {
            throw new PropelException('filterByVille() only accepts arguments of type \Ville');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ville relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinVille($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ville');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ville');
        }

        return $this;
    }

    /**
     * Use the Ville relation Ville object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VilleQuery A secondary query class using the current class as primary query
     */
    public function useVilleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVille($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ville', '\VilleQuery');
    }

    /**
     * Filter the query by a related \Contrat object
     *
     * @param \Contrat|ObjectCollection $contrat the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByContratRelatedByPersonneClientIdrolepersonneClient($contrat, $comparison = null)
    {
        if ($contrat instanceof \Contrat) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $contrat->getPersonneClient(), $comparison)
                ->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $contrat->getIdrolepersonneClient(), $comparison);
        } else {
            throw new PropelException('filterByContratRelatedByPersonneClientIdrolepersonneClient() only accepts arguments of type \Contrat');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContratRelatedByPersonneClientIdrolepersonneClient relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinContratRelatedByPersonneClientIdrolepersonneClient($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContratRelatedByPersonneClientIdrolepersonneClient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContratRelatedByPersonneClientIdrolepersonneClient');
        }

        return $this;
    }

    /**
     * Use the ContratRelatedByPersonneClientIdrolepersonneClient relation Contrat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ContratQuery A secondary query class using the current class as primary query
     */
    public function useContratRelatedByPersonneClientIdrolepersonneClientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContratRelatedByPersonneClientIdrolepersonneClient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContratRelatedByPersonneClientIdrolepersonneClient', '\ContratQuery');
    }

    /**
     * Filter the query by a related \Contrat object
     *
     * @param \Contrat|ObjectCollection $contrat the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByContratRelatedByPersonneAgentIdrolepersonneAgent($contrat, $comparison = null)
    {
        if ($contrat instanceof \Contrat) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $contrat->getPersonneAgent(), $comparison)
                ->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $contrat->getIdrolepersonneAgent(), $comparison);
        } else {
            throw new PropelException('filterByContratRelatedByPersonneAgentIdrolepersonneAgent() only accepts arguments of type \Contrat');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContratRelatedByPersonneAgentIdrolepersonneAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinContratRelatedByPersonneAgentIdrolepersonneAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContratRelatedByPersonneAgentIdrolepersonneAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContratRelatedByPersonneAgentIdrolepersonneAgent');
        }

        return $this;
    }

    /**
     * Use the ContratRelatedByPersonneAgentIdrolepersonneAgent relation Contrat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ContratQuery A secondary query class using the current class as primary query
     */
    public function useContratRelatedByPersonneAgentIdrolepersonneAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContratRelatedByPersonneAgentIdrolepersonneAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContratRelatedByPersonneAgentIdrolepersonneAgent', '\ContratQuery');
    }

    /**
     * Filter the query by a related \Prospection object
     *
     * @param \Prospection|ObjectCollection $prospection the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByProspection($prospection, $comparison = null)
    {
        if ($prospection instanceof \Prospection) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $prospection->getIdpersonneagent(), $comparison)
                ->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $prospection->getPersonneRolepersonneIdrolepersonne(), $comparison);
        } else {
            throw new PropelException('filterByProspection() only accepts arguments of type \Prospection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Prospection relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinProspection($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Prospection');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Prospection');
        }

        return $this;
    }

    /**
     * Use the Prospection relation Prospection object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProspectionQuery A secondary query class using the current class as primary query
     */
    public function useProspectionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProspection($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Prospection', '\ProspectionQuery');
    }

    /**
     * Filter the query by a related \ResponsableAgent object
     *
     * @param \ResponsableAgent|ObjectCollection $responsableAgent the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($responsableAgent, $comparison = null)
    {
        if ($responsableAgent instanceof \ResponsableAgent) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $responsableAgent->getPersonneIdresponsable(), $comparison)
                ->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $responsableAgent->getIdroleresponsable(), $comparison);
        } else {
            throw new PropelException('filterByResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable() only accepts arguments of type \ResponsableAgent');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable');
        }

        return $this;
    }

    /**
     * Use the ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable relation ResponsableAgent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ResponsableAgentQuery A secondary query class using the current class as primary query
     */
    public function useResponsableAgentRelatedByPersonneIdresponsableIdroleresponsableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable', '\ResponsableAgentQuery');
    }

    /**
     * Filter the query by a related \ResponsableAgent object
     *
     * @param \ResponsableAgent|ObjectCollection $responsableAgent the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByResponsableAgentRelatedByPersonneIdagentIdroleagent($responsableAgent, $comparison = null)
    {
        if ($responsableAgent instanceof \ResponsableAgent) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $responsableAgent->getPersonneIdagent(), $comparison)
                ->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $responsableAgent->getIdroleagent(), $comparison);
        } else {
            throw new PropelException('filterByResponsableAgentRelatedByPersonneIdagentIdroleagent() only accepts arguments of type \ResponsableAgent');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinResponsableAgentRelatedByPersonneIdagentIdroleagent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ResponsableAgentRelatedByPersonneIdagentIdroleagent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ResponsableAgentRelatedByPersonneIdagentIdroleagent');
        }

        return $this;
    }

    /**
     * Use the ResponsableAgentRelatedByPersonneIdagentIdroleagent relation ResponsableAgent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ResponsableAgentQuery A secondary query class using the current class as primary query
     */
    public function useResponsableAgentRelatedByPersonneIdagentIdroleagentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinResponsableAgentRelatedByPersonneIdagentIdroleagent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ResponsableAgentRelatedByPersonneIdagentIdroleagent', '\ResponsableAgentQuery');
    }

    /**
     * Filter the query by a related \Vente object
     *
     * @param \Vente|ObjectCollection $vente the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPersonneQuery The current query, for fluid interface
     */
    public function filterByVente($vente, $comparison = null)
    {
        if ($vente instanceof \Vente) {
            return $this
                ->addUsingAlias(PersonneTableMap::COL_IDPERSONNE, $vente->getPersonneIdpersonne(), $comparison)
                ->addUsingAlias(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $vente->getPersonneRolepersonneIdrolepersonne(), $comparison);
        } else {
            throw new PropelException('filterByVente() only accepts arguments of type \Vente');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Vente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function joinVente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Vente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Vente');
        }

        return $this;
    }

    /**
     * Use the Vente relation Vente object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VenteQuery A secondary query class using the current class as primary query
     */
    public function useVenteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Vente', '\VenteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPersonne $personne Object to remove from the list of results
     *
     * @return $this|ChildPersonneQuery The current query, for fluid interface
     */
    public function prune($personne = null)
    {
        if ($personne) {
            $this->addCond('pruneCond0', $this->getAliasedColName(PersonneTableMap::COL_IDPERSONNE), $personne->getIdpersonne(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE), $personne->getRolepersonneIdrolepersonne(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the personne table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersonneTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PersonneTableMap::clearInstancePool();
            PersonneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersonneTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PersonneTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PersonneTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PersonneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PersonneQuery
