<?php

namespace Base;

use \Catgorieproduit as ChildCatgorieproduit;
use \CatgorieproduitQuery as ChildCatgorieproduitQuery;
use \Contrat as ChildContrat;
use \ContratQuery as ChildContratQuery;
use \Prime as ChildPrime;
use \PrimeQuery as ChildPrimeQuery;
use \Produits as ChildProduits;
use \ProduitsQuery as ChildProduitsQuery;
use \Prospection as ChildProspection;
use \ProspectionQuery as ChildProspectionQuery;
use \Societe as ChildSociete;
use \SocieteQuery as ChildSocieteQuery;
use \Vente as ChildVente;
use \VenteQuery as ChildVenteQuery;
use \Exception;
use \PDO;
use Map\ContratTableMap;
use Map\PrimeTableMap;
use Map\ProduitsTableMap;
use Map\ProspectionTableMap;
use Map\VenteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'produits' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Produits implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\ProduitsTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idproduits field.
     *
     * @var        int
     */
    protected $idproduits;

    /**
     * The value for the libelleproduits field.
     *
     * @var        string
     */
    protected $libelleproduits;

    /**
     * The value for the idcatgorieproduit field.
     *
     * @var        int
     */
    protected $idcatgorieproduit;

    /**
     * The value for the montantproduits field.
     *
     * @var        int
     */
    protected $montantproduits;

    /**
     * The value for the societe_idsociete field.
     *
     * @var        int
     */
    protected $societe_idsociete;

    /**
     * @var        ChildCatgorieproduit
     */
    protected $aCatgorieproduit;

    /**
     * @var        ChildSociete
     */
    protected $aSociete;

    /**
     * @var        ObjectCollection|ChildContrat[] Collection to store aggregation of ChildContrat objects.
     */
    protected $collContrats;
    protected $collContratsPartial;

    /**
     * @var        ObjectCollection|ChildPrime[] Collection to store aggregation of ChildPrime objects.
     */
    protected $collPrimes;
    protected $collPrimesPartial;

    /**
     * @var        ObjectCollection|ChildProspection[] Collection to store aggregation of ChildProspection objects.
     */
    protected $collProspections;
    protected $collProspectionsPartial;

    /**
     * @var        ObjectCollection|ChildVente[] Collection to store aggregation of ChildVente objects.
     */
    protected $collVentes;
    protected $collVentesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContrat[]
     */
    protected $contratsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrime[]
     */
    protected $primesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProspection[]
     */
    protected $prospectionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildVente[]
     */
    protected $ventesScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Produits object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Produits</code> instance.  If
     * <code>obj</code> is an instance of <code>Produits</code>, delegates to
     * <code>equals(Produits)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Produits The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idproduits] column value.
     *
     * @return int
     */
    public function getIdproduits()
    {
        return $this->idproduits;
    }

    /**
     * Get the [libelleproduits] column value.
     *
     * @return string
     */
    public function getLibelleproduits()
    {
        return $this->libelleproduits;
    }

    /**
     * Get the [idcatgorieproduit] column value.
     *
     * @return int
     */
    public function getIdcatgorieproduit()
    {
        return $this->idcatgorieproduit;
    }

    /**
     * Get the [montantproduits] column value.
     *
     * @return int
     */
    public function getMontantproduits()
    {
        return $this->montantproduits;
    }

    /**
     * Get the [societe_idsociete] column value.
     *
     * @return int
     */
    public function getSocieteIdsociete()
    {
        return $this->societe_idsociete;
    }

    /**
     * Set the value of [idproduits] column.
     *
     * @param int $v new value
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function setIdproduits($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idproduits !== $v) {
            $this->idproduits = $v;
            $this->modifiedColumns[ProduitsTableMap::COL_IDPRODUITS] = true;
        }

        return $this;
    } // setIdproduits()

    /**
     * Set the value of [libelleproduits] column.
     *
     * @param string $v new value
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function setLibelleproduits($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->libelleproduits !== $v) {
            $this->libelleproduits = $v;
            $this->modifiedColumns[ProduitsTableMap::COL_LIBELLEPRODUITS] = true;
        }

        return $this;
    } // setLibelleproduits()

    /**
     * Set the value of [idcatgorieproduit] column.
     *
     * @param int $v new value
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function setIdcatgorieproduit($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idcatgorieproduit !== $v) {
            $this->idcatgorieproduit = $v;
            $this->modifiedColumns[ProduitsTableMap::COL_IDCATGORIEPRODUIT] = true;
        }

        if ($this->aCatgorieproduit !== null && $this->aCatgorieproduit->getIdcatgorieproduit() !== $v) {
            $this->aCatgorieproduit = null;
        }

        return $this;
    } // setIdcatgorieproduit()

    /**
     * Set the value of [montantproduits] column.
     *
     * @param int $v new value
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function setMontantproduits($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->montantproduits !== $v) {
            $this->montantproduits = $v;
            $this->modifiedColumns[ProduitsTableMap::COL_MONTANTPRODUITS] = true;
        }

        return $this;
    } // setMontantproduits()

    /**
     * Set the value of [societe_idsociete] column.
     *
     * @param int $v new value
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function setSocieteIdsociete($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->societe_idsociete !== $v) {
            $this->societe_idsociete = $v;
            $this->modifiedColumns[ProduitsTableMap::COL_SOCIETE_IDSOCIETE] = true;
        }

        if ($this->aSociete !== null && $this->aSociete->getIdsociete() !== $v) {
            $this->aSociete = null;
        }

        return $this;
    } // setSocieteIdsociete()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProduitsTableMap::translateFieldName('Idproduits', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idproduits = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProduitsTableMap::translateFieldName('Libelleproduits', TableMap::TYPE_PHPNAME, $indexType)];
            $this->libelleproduits = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProduitsTableMap::translateFieldName('Idcatgorieproduit', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idcatgorieproduit = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProduitsTableMap::translateFieldName('Montantproduits', TableMap::TYPE_PHPNAME, $indexType)];
            $this->montantproduits = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProduitsTableMap::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)];
            $this->societe_idsociete = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 5; // 5 = ProduitsTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Produits'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCatgorieproduit !== null && $this->idcatgorieproduit !== $this->aCatgorieproduit->getIdcatgorieproduit()) {
            $this->aCatgorieproduit = null;
        }
        if ($this->aSociete !== null && $this->societe_idsociete !== $this->aSociete->getIdsociete()) {
            $this->aSociete = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProduitsTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProduitsQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCatgorieproduit = null;
            $this->aSociete = null;
            $this->collContrats = null;

            $this->collPrimes = null;

            $this->collProspections = null;

            $this->collVentes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Produits::setDeleted()
     * @see Produits::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduitsTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProduitsQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduitsTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProduitsTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCatgorieproduit !== null) {
                if ($this->aCatgorieproduit->isModified() || $this->aCatgorieproduit->isNew()) {
                    $affectedRows += $this->aCatgorieproduit->save($con);
                }
                $this->setCatgorieproduit($this->aCatgorieproduit);
            }

            if ($this->aSociete !== null) {
                if ($this->aSociete->isModified() || $this->aSociete->isNew()) {
                    $affectedRows += $this->aSociete->save($con);
                }
                $this->setSociete($this->aSociete);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->contratsScheduledForDeletion !== null) {
                if (!$this->contratsScheduledForDeletion->isEmpty()) {
                    \ContratQuery::create()
                        ->filterByPrimaryKeys($this->contratsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contratsScheduledForDeletion = null;
                }
            }

            if ($this->collContrats !== null) {
                foreach ($this->collContrats as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->primesScheduledForDeletion !== null) {
                if (!$this->primesScheduledForDeletion->isEmpty()) {
                    \PrimeQuery::create()
                        ->filterByPrimaryKeys($this->primesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->primesScheduledForDeletion = null;
                }
            }

            if ($this->collPrimes !== null) {
                foreach ($this->collPrimes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prospectionsScheduledForDeletion !== null) {
                if (!$this->prospectionsScheduledForDeletion->isEmpty()) {
                    \ProspectionQuery::create()
                        ->filterByPrimaryKeys($this->prospectionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prospectionsScheduledForDeletion = null;
                }
            }

            if ($this->collProspections !== null) {
                foreach ($this->collProspections as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ventesScheduledForDeletion !== null) {
                if (!$this->ventesScheduledForDeletion->isEmpty()) {
                    \VenteQuery::create()
                        ->filterByPrimaryKeys($this->ventesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ventesScheduledForDeletion = null;
                }
            }

            if ($this->collVentes !== null) {
                foreach ($this->collVentes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ProduitsTableMap::COL_IDPRODUITS] = true;
        if (null !== $this->idproduits) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProduitsTableMap::COL_IDPRODUITS . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProduitsTableMap::COL_IDPRODUITS)) {
            $modifiedColumns[':p' . $index++]  = 'idproduits';
        }
        if ($this->isColumnModified(ProduitsTableMap::COL_LIBELLEPRODUITS)) {
            $modifiedColumns[':p' . $index++]  = 'Libelleproduits';
        }
        if ($this->isColumnModified(ProduitsTableMap::COL_IDCATGORIEPRODUIT)) {
            $modifiedColumns[':p' . $index++]  = 'idcatgorieproduit';
        }
        if ($this->isColumnModified(ProduitsTableMap::COL_MONTANTPRODUITS)) {
            $modifiedColumns[':p' . $index++]  = 'Montantproduits';
        }
        if ($this->isColumnModified(ProduitsTableMap::COL_SOCIETE_IDSOCIETE)) {
            $modifiedColumns[':p' . $index++]  = 'societe_idsociete';
        }

        $sql = sprintf(
            'INSERT INTO produits (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idproduits':
                        $stmt->bindValue($identifier, $this->idproduits, PDO::PARAM_INT);
                        break;
                    case 'Libelleproduits':
                        $stmt->bindValue($identifier, $this->libelleproduits, PDO::PARAM_STR);
                        break;
                    case 'idcatgorieproduit':
                        $stmt->bindValue($identifier, $this->idcatgorieproduit, PDO::PARAM_INT);
                        break;
                    case 'Montantproduits':
                        $stmt->bindValue($identifier, $this->montantproduits, PDO::PARAM_INT);
                        break;
                    case 'societe_idsociete':
                        $stmt->bindValue($identifier, $this->societe_idsociete, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdproduits($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProduitsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdproduits();
                break;
            case 1:
                return $this->getLibelleproduits();
                break;
            case 2:
                return $this->getIdcatgorieproduit();
                break;
            case 3:
                return $this->getMontantproduits();
                break;
            case 4:
                return $this->getSocieteIdsociete();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Produits'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Produits'][$this->hashCode()] = true;
        $keys = ProduitsTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdproduits(),
            $keys[1] => $this->getLibelleproduits(),
            $keys[2] => $this->getIdcatgorieproduit(),
            $keys[3] => $this->getMontantproduits(),
            $keys[4] => $this->getSocieteIdsociete(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCatgorieproduit) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'catgorieproduit';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'catgorieproduit';
                        break;
                    default:
                        $key = 'Catgorieproduit';
                }

                $result[$key] = $this->aCatgorieproduit->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSociete) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'societe';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'societe';
                        break;
                    default:
                        $key = 'Societe';
                }

                $result[$key] = $this->aSociete->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collContrats) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contrats';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contrats';
                        break;
                    default:
                        $key = 'Contrats';
                }

                $result[$key] = $this->collContrats->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPrimes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'primes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'primes';
                        break;
                    default:
                        $key = 'Primes';
                }

                $result[$key] = $this->collPrimes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProspections) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prospections';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prospections';
                        break;
                    default:
                        $key = 'Prospections';
                }

                $result[$key] = $this->collProspections->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVentes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ventes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ventes';
                        break;
                    default:
                        $key = 'Ventes';
                }

                $result[$key] = $this->collVentes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Produits
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProduitsTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Produits
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdproduits($value);
                break;
            case 1:
                $this->setLibelleproduits($value);
                break;
            case 2:
                $this->setIdcatgorieproduit($value);
                break;
            case 3:
                $this->setMontantproduits($value);
                break;
            case 4:
                $this->setSocieteIdsociete($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProduitsTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdproduits($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setLibelleproduits($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIdcatgorieproduit($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setMontantproduits($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSocieteIdsociete($arr[$keys[4]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Produits The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProduitsTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProduitsTableMap::COL_IDPRODUITS)) {
            $criteria->add(ProduitsTableMap::COL_IDPRODUITS, $this->idproduits);
        }
        if ($this->isColumnModified(ProduitsTableMap::COL_LIBELLEPRODUITS)) {
            $criteria->add(ProduitsTableMap::COL_LIBELLEPRODUITS, $this->libelleproduits);
        }
        if ($this->isColumnModified(ProduitsTableMap::COL_IDCATGORIEPRODUIT)) {
            $criteria->add(ProduitsTableMap::COL_IDCATGORIEPRODUIT, $this->idcatgorieproduit);
        }
        if ($this->isColumnModified(ProduitsTableMap::COL_MONTANTPRODUITS)) {
            $criteria->add(ProduitsTableMap::COL_MONTANTPRODUITS, $this->montantproduits);
        }
        if ($this->isColumnModified(ProduitsTableMap::COL_SOCIETE_IDSOCIETE)) {
            $criteria->add(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $this->societe_idsociete);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProduitsQuery::create();
        $criteria->add(ProduitsTableMap::COL_IDPRODUITS, $this->idproduits);
        $criteria->add(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $this->societe_idsociete);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdproduits() &&
            null !== $this->getSocieteIdsociete();

        $validPrimaryKeyFKs = 1;
        $primaryKeyFKs = [];

        //relation fk_produits_societe1 to table societe
        if ($this->aSociete && $hash = spl_object_hash($this->aSociete)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getIdproduits();
        $pks[1] = $this->getSocieteIdsociete();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param      array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setIdproduits($keys[0]);
        $this->setSocieteIdsociete($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return (null === $this->getIdproduits()) && (null === $this->getSocieteIdsociete());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Produits (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLibelleproduits($this->getLibelleproduits());
        $copyObj->setIdcatgorieproduit($this->getIdcatgorieproduit());
        $copyObj->setMontantproduits($this->getMontantproduits());
        $copyObj->setSocieteIdsociete($this->getSocieteIdsociete());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getContrats() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContrat($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPrimes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrime($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProspections() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProspection($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVentes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVente($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdproduits(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Produits Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCatgorieproduit object.
     *
     * @param  ChildCatgorieproduit $v
     * @return $this|\Produits The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCatgorieproduit(ChildCatgorieproduit $v = null)
    {
        if ($v === null) {
            $this->setIdcatgorieproduit(NULL);
        } else {
            $this->setIdcatgorieproduit($v->getIdcatgorieproduit());
        }

        $this->aCatgorieproduit = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCatgorieproduit object, it will not be re-added.
        if ($v !== null) {
            $v->addProduits($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCatgorieproduit object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCatgorieproduit The associated ChildCatgorieproduit object.
     * @throws PropelException
     */
    public function getCatgorieproduit(ConnectionInterface $con = null)
    {
        if ($this->aCatgorieproduit === null && ($this->idcatgorieproduit != 0)) {
            $this->aCatgorieproduit = ChildCatgorieproduitQuery::create()->findPk($this->idcatgorieproduit, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCatgorieproduit->addProduitss($this);
             */
        }

        return $this->aCatgorieproduit;
    }

    /**
     * Declares an association between this object and a ChildSociete object.
     *
     * @param  ChildSociete $v
     * @return $this|\Produits The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSociete(ChildSociete $v = null)
    {
        if ($v === null) {
            $this->setSocieteIdsociete(NULL);
        } else {
            $this->setSocieteIdsociete($v->getIdsociete());
        }

        $this->aSociete = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSociete object, it will not be re-added.
        if ($v !== null) {
            $v->addProduits($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSociete object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSociete The associated ChildSociete object.
     * @throws PropelException
     */
    public function getSociete(ConnectionInterface $con = null)
    {
        if ($this->aSociete === null && ($this->societe_idsociete != 0)) {
            $this->aSociete = ChildSocieteQuery::create()->findPk($this->societe_idsociete, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSociete->addProduitss($this);
             */
        }

        return $this->aSociete;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Contrat' == $relationName) {
            $this->initContrats();
            return;
        }
        if ('Prime' == $relationName) {
            $this->initPrimes();
            return;
        }
        if ('Prospection' == $relationName) {
            $this->initProspections();
            return;
        }
        if ('Vente' == $relationName) {
            $this->initVentes();
            return;
        }
    }

    /**
     * Clears out the collContrats collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContrats()
     */
    public function clearContrats()
    {
        $this->collContrats = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContrats collection loaded partially.
     */
    public function resetPartialContrats($v = true)
    {
        $this->collContratsPartial = $v;
    }

    /**
     * Initializes the collContrats collection.
     *
     * By default this just sets the collContrats collection to an empty array (like clearcollContrats());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContrats($overrideExisting = true)
    {
        if (null !== $this->collContrats && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContratTableMap::getTableMap()->getCollectionClassName();

        $this->collContrats = new $collectionClassName;
        $this->collContrats->setModel('\Contrat');
    }

    /**
     * Gets an array of ChildContrat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduits is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContrat[] List of ChildContrat objects
     * @throws PropelException
     */
    public function getContrats(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContratsPartial && !$this->isNew();
        if (null === $this->collContrats || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContrats) {
                // return empty collection
                $this->initContrats();
            } else {
                $collContrats = ChildContratQuery::create(null, $criteria)
                    ->filterByProduits($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContratsPartial && count($collContrats)) {
                        $this->initContrats(false);

                        foreach ($collContrats as $obj) {
                            if (false == $this->collContrats->contains($obj)) {
                                $this->collContrats->append($obj);
                            }
                        }

                        $this->collContratsPartial = true;
                    }

                    return $collContrats;
                }

                if ($partial && $this->collContrats) {
                    foreach ($this->collContrats as $obj) {
                        if ($obj->isNew()) {
                            $collContrats[] = $obj;
                        }
                    }
                }

                $this->collContrats = $collContrats;
                $this->collContratsPartial = false;
            }
        }

        return $this->collContrats;
    }

    /**
     * Sets a collection of ChildContrat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contrats A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduits The current object (for fluent API support)
     */
    public function setContrats(Collection $contrats, ConnectionInterface $con = null)
    {
        /** @var ChildContrat[] $contratsToDelete */
        $contratsToDelete = $this->getContrats(new Criteria(), $con)->diff($contrats);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->contratsScheduledForDeletion = clone $contratsToDelete;

        foreach ($contratsToDelete as $contratRemoved) {
            $contratRemoved->setProduits(null);
        }

        $this->collContrats = null;
        foreach ($contrats as $contrat) {
            $this->addContrat($contrat);
        }

        $this->collContrats = $contrats;
        $this->collContratsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Contrat objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Contrat objects.
     * @throws PropelException
     */
    public function countContrats(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContratsPartial && !$this->isNew();
        if (null === $this->collContrats || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContrats) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContrats());
            }

            $query = ChildContratQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduits($this)
                ->count($con);
        }

        return count($this->collContrats);
    }

    /**
     * Method called to associate a ChildContrat object to this object
     * through the ChildContrat foreign key attribute.
     *
     * @param  ChildContrat $l ChildContrat
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function addContrat(ChildContrat $l)
    {
        if ($this->collContrats === null) {
            $this->initContrats();
            $this->collContratsPartial = true;
        }

        if (!$this->collContrats->contains($l)) {
            $this->doAddContrat($l);

            if ($this->contratsScheduledForDeletion and $this->contratsScheduledForDeletion->contains($l)) {
                $this->contratsScheduledForDeletion->remove($this->contratsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContrat $contrat The ChildContrat object to add.
     */
    protected function doAddContrat(ChildContrat $contrat)
    {
        $this->collContrats[]= $contrat;
        $contrat->setProduits($this);
    }

    /**
     * @param  ChildContrat $contrat The ChildContrat object to remove.
     * @return $this|ChildProduits The current object (for fluent API support)
     */
    public function removeContrat(ChildContrat $contrat)
    {
        if ($this->getContrats()->contains($contrat)) {
            $pos = $this->collContrats->search($contrat);
            $this->collContrats->remove($pos);
            if (null === $this->contratsScheduledForDeletion) {
                $this->contratsScheduledForDeletion = clone $this->collContrats;
                $this->contratsScheduledForDeletion->clear();
            }
            $this->contratsScheduledForDeletion[]= clone $contrat;
            $contrat->setProduits(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produits is new, it will return
     * an empty collection; or if this Produits has previously
     * been saved, it will retrieve related Contrats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produits.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContrat[] List of ChildContrat objects
     */
    public function getContratsJoinPersonneRelatedByPersonneClientIdrolepersonneClient(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContratQuery::create(null, $criteria);
        $query->joinWith('PersonneRelatedByPersonneClientIdrolepersonneClient', $joinBehavior);

        return $this->getContrats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produits is new, it will return
     * an empty collection; or if this Produits has previously
     * been saved, it will retrieve related Contrats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produits.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContrat[] List of ChildContrat objects
     */
    public function getContratsJoinPersonneRelatedByPersonneAgentIdrolepersonneAgent(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContratQuery::create(null, $criteria);
        $query->joinWith('PersonneRelatedByPersonneAgentIdrolepersonneAgent', $joinBehavior);

        return $this->getContrats($query, $con);
    }

    /**
     * Clears out the collPrimes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPrimes()
     */
    public function clearPrimes()
    {
        $this->collPrimes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPrimes collection loaded partially.
     */
    public function resetPartialPrimes($v = true)
    {
        $this->collPrimesPartial = $v;
    }

    /**
     * Initializes the collPrimes collection.
     *
     * By default this just sets the collPrimes collection to an empty array (like clearcollPrimes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPrimes($overrideExisting = true)
    {
        if (null !== $this->collPrimes && !$overrideExisting) {
            return;
        }

        $collectionClassName = PrimeTableMap::getTableMap()->getCollectionClassName();

        $this->collPrimes = new $collectionClassName;
        $this->collPrimes->setModel('\Prime');
    }

    /**
     * Gets an array of ChildPrime objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduits is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrime[] List of ChildPrime objects
     * @throws PropelException
     */
    public function getPrimes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPrimesPartial && !$this->isNew();
        if (null === $this->collPrimes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPrimes) {
                // return empty collection
                $this->initPrimes();
            } else {
                $collPrimes = ChildPrimeQuery::create(null, $criteria)
                    ->filterByProduits($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPrimesPartial && count($collPrimes)) {
                        $this->initPrimes(false);

                        foreach ($collPrimes as $obj) {
                            if (false == $this->collPrimes->contains($obj)) {
                                $this->collPrimes->append($obj);
                            }
                        }

                        $this->collPrimesPartial = true;
                    }

                    return $collPrimes;
                }

                if ($partial && $this->collPrimes) {
                    foreach ($this->collPrimes as $obj) {
                        if ($obj->isNew()) {
                            $collPrimes[] = $obj;
                        }
                    }
                }

                $this->collPrimes = $collPrimes;
                $this->collPrimesPartial = false;
            }
        }

        return $this->collPrimes;
    }

    /**
     * Sets a collection of ChildPrime objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $primes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduits The current object (for fluent API support)
     */
    public function setPrimes(Collection $primes, ConnectionInterface $con = null)
    {
        /** @var ChildPrime[] $primesToDelete */
        $primesToDelete = $this->getPrimes(new Criteria(), $con)->diff($primes);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->primesScheduledForDeletion = clone $primesToDelete;

        foreach ($primesToDelete as $primeRemoved) {
            $primeRemoved->setProduits(null);
        }

        $this->collPrimes = null;
        foreach ($primes as $prime) {
            $this->addPrime($prime);
        }

        $this->collPrimes = $primes;
        $this->collPrimesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prime objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prime objects.
     * @throws PropelException
     */
    public function countPrimes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPrimesPartial && !$this->isNew();
        if (null === $this->collPrimes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPrimes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPrimes());
            }

            $query = ChildPrimeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduits($this)
                ->count($con);
        }

        return count($this->collPrimes);
    }

    /**
     * Method called to associate a ChildPrime object to this object
     * through the ChildPrime foreign key attribute.
     *
     * @param  ChildPrime $l ChildPrime
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function addPrime(ChildPrime $l)
    {
        if ($this->collPrimes === null) {
            $this->initPrimes();
            $this->collPrimesPartial = true;
        }

        if (!$this->collPrimes->contains($l)) {
            $this->doAddPrime($l);

            if ($this->primesScheduledForDeletion and $this->primesScheduledForDeletion->contains($l)) {
                $this->primesScheduledForDeletion->remove($this->primesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrime $prime The ChildPrime object to add.
     */
    protected function doAddPrime(ChildPrime $prime)
    {
        $this->collPrimes[]= $prime;
        $prime->setProduits($this);
    }

    /**
     * @param  ChildPrime $prime The ChildPrime object to remove.
     * @return $this|ChildProduits The current object (for fluent API support)
     */
    public function removePrime(ChildPrime $prime)
    {
        if ($this->getPrimes()->contains($prime)) {
            $pos = $this->collPrimes->search($prime);
            $this->collPrimes->remove($pos);
            if (null === $this->primesScheduledForDeletion) {
                $this->primesScheduledForDeletion = clone $this->collPrimes;
                $this->primesScheduledForDeletion->clear();
            }
            $this->primesScheduledForDeletion[]= clone $prime;
            $prime->setProduits(null);
        }

        return $this;
    }

    /**
     * Clears out the collProspections collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProspections()
     */
    public function clearProspections()
    {
        $this->collProspections = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProspections collection loaded partially.
     */
    public function resetPartialProspections($v = true)
    {
        $this->collProspectionsPartial = $v;
    }

    /**
     * Initializes the collProspections collection.
     *
     * By default this just sets the collProspections collection to an empty array (like clearcollProspections());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProspections($overrideExisting = true)
    {
        if (null !== $this->collProspections && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProspectionTableMap::getTableMap()->getCollectionClassName();

        $this->collProspections = new $collectionClassName;
        $this->collProspections->setModel('\Prospection');
    }

    /**
     * Gets an array of ChildProspection objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduits is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProspection[] List of ChildProspection objects
     * @throws PropelException
     */
    public function getProspections(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProspectionsPartial && !$this->isNew();
        if (null === $this->collProspections || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProspections) {
                // return empty collection
                $this->initProspections();
            } else {
                $collProspections = ChildProspectionQuery::create(null, $criteria)
                    ->filterByProduits($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProspectionsPartial && count($collProspections)) {
                        $this->initProspections(false);

                        foreach ($collProspections as $obj) {
                            if (false == $this->collProspections->contains($obj)) {
                                $this->collProspections->append($obj);
                            }
                        }

                        $this->collProspectionsPartial = true;
                    }

                    return $collProspections;
                }

                if ($partial && $this->collProspections) {
                    foreach ($this->collProspections as $obj) {
                        if ($obj->isNew()) {
                            $collProspections[] = $obj;
                        }
                    }
                }

                $this->collProspections = $collProspections;
                $this->collProspectionsPartial = false;
            }
        }

        return $this->collProspections;
    }

    /**
     * Sets a collection of ChildProspection objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prospections A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduits The current object (for fluent API support)
     */
    public function setProspections(Collection $prospections, ConnectionInterface $con = null)
    {
        /** @var ChildProspection[] $prospectionsToDelete */
        $prospectionsToDelete = $this->getProspections(new Criteria(), $con)->diff($prospections);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->prospectionsScheduledForDeletion = clone $prospectionsToDelete;

        foreach ($prospectionsToDelete as $prospectionRemoved) {
            $prospectionRemoved->setProduits(null);
        }

        $this->collProspections = null;
        foreach ($prospections as $prospection) {
            $this->addProspection($prospection);
        }

        $this->collProspections = $prospections;
        $this->collProspectionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prospection objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prospection objects.
     * @throws PropelException
     */
    public function countProspections(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProspectionsPartial && !$this->isNew();
        if (null === $this->collProspections || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProspections) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProspections());
            }

            $query = ChildProspectionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduits($this)
                ->count($con);
        }

        return count($this->collProspections);
    }

    /**
     * Method called to associate a ChildProspection object to this object
     * through the ChildProspection foreign key attribute.
     *
     * @param  ChildProspection $l ChildProspection
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function addProspection(ChildProspection $l)
    {
        if ($this->collProspections === null) {
            $this->initProspections();
            $this->collProspectionsPartial = true;
        }

        if (!$this->collProspections->contains($l)) {
            $this->doAddProspection($l);

            if ($this->prospectionsScheduledForDeletion and $this->prospectionsScheduledForDeletion->contains($l)) {
                $this->prospectionsScheduledForDeletion->remove($this->prospectionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProspection $prospection The ChildProspection object to add.
     */
    protected function doAddProspection(ChildProspection $prospection)
    {
        $this->collProspections[]= $prospection;
        $prospection->setProduits($this);
    }

    /**
     * @param  ChildProspection $prospection The ChildProspection object to remove.
     * @return $this|ChildProduits The current object (for fluent API support)
     */
    public function removeProspection(ChildProspection $prospection)
    {
        if ($this->getProspections()->contains($prospection)) {
            $pos = $this->collProspections->search($prospection);
            $this->collProspections->remove($pos);
            if (null === $this->prospectionsScheduledForDeletion) {
                $this->prospectionsScheduledForDeletion = clone $this->collProspections;
                $this->prospectionsScheduledForDeletion->clear();
            }
            $this->prospectionsScheduledForDeletion[]= clone $prospection;
            $prospection->setProduits(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produits is new, it will return
     * an empty collection; or if this Produits has previously
     * been saved, it will retrieve related Prospections from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produits.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProspection[] List of ChildProspection objects
     */
    public function getProspectionsJoinPersonne(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProspectionQuery::create(null, $criteria);
        $query->joinWith('Personne', $joinBehavior);

        return $this->getProspections($query, $con);
    }

    /**
     * Clears out the collVentes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addVentes()
     */
    public function clearVentes()
    {
        $this->collVentes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collVentes collection loaded partially.
     */
    public function resetPartialVentes($v = true)
    {
        $this->collVentesPartial = $v;
    }

    /**
     * Initializes the collVentes collection.
     *
     * By default this just sets the collVentes collection to an empty array (like clearcollVentes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVentes($overrideExisting = true)
    {
        if (null !== $this->collVentes && !$overrideExisting) {
            return;
        }

        $collectionClassName = VenteTableMap::getTableMap()->getCollectionClassName();

        $this->collVentes = new $collectionClassName;
        $this->collVentes->setModel('\Vente');
    }

    /**
     * Gets an array of ChildVente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduits is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildVente[] List of ChildVente objects
     * @throws PropelException
     */
    public function getVentes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collVentesPartial && !$this->isNew();
        if (null === $this->collVentes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVentes) {
                // return empty collection
                $this->initVentes();
            } else {
                $collVentes = ChildVenteQuery::create(null, $criteria)
                    ->filterByProduits($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collVentesPartial && count($collVentes)) {
                        $this->initVentes(false);

                        foreach ($collVentes as $obj) {
                            if (false == $this->collVentes->contains($obj)) {
                                $this->collVentes->append($obj);
                            }
                        }

                        $this->collVentesPartial = true;
                    }

                    return $collVentes;
                }

                if ($partial && $this->collVentes) {
                    foreach ($this->collVentes as $obj) {
                        if ($obj->isNew()) {
                            $collVentes[] = $obj;
                        }
                    }
                }

                $this->collVentes = $collVentes;
                $this->collVentesPartial = false;
            }
        }

        return $this->collVentes;
    }

    /**
     * Sets a collection of ChildVente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ventes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduits The current object (for fluent API support)
     */
    public function setVentes(Collection $ventes, ConnectionInterface $con = null)
    {
        /** @var ChildVente[] $ventesToDelete */
        $ventesToDelete = $this->getVentes(new Criteria(), $con)->diff($ventes);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->ventesScheduledForDeletion = clone $ventesToDelete;

        foreach ($ventesToDelete as $venteRemoved) {
            $venteRemoved->setProduits(null);
        }

        $this->collVentes = null;
        foreach ($ventes as $vente) {
            $this->addVente($vente);
        }

        $this->collVentes = $ventes;
        $this->collVentesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Vente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Vente objects.
     * @throws PropelException
     */
    public function countVentes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collVentesPartial && !$this->isNew();
        if (null === $this->collVentes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVentes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getVentes());
            }

            $query = ChildVenteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduits($this)
                ->count($con);
        }

        return count($this->collVentes);
    }

    /**
     * Method called to associate a ChildVente object to this object
     * through the ChildVente foreign key attribute.
     *
     * @param  ChildVente $l ChildVente
     * @return $this|\Produits The current object (for fluent API support)
     */
    public function addVente(ChildVente $l)
    {
        if ($this->collVentes === null) {
            $this->initVentes();
            $this->collVentesPartial = true;
        }

        if (!$this->collVentes->contains($l)) {
            $this->doAddVente($l);

            if ($this->ventesScheduledForDeletion and $this->ventesScheduledForDeletion->contains($l)) {
                $this->ventesScheduledForDeletion->remove($this->ventesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildVente $vente The ChildVente object to add.
     */
    protected function doAddVente(ChildVente $vente)
    {
        $this->collVentes[]= $vente;
        $vente->setProduits($this);
    }

    /**
     * @param  ChildVente $vente The ChildVente object to remove.
     * @return $this|ChildProduits The current object (for fluent API support)
     */
    public function removeVente(ChildVente $vente)
    {
        if ($this->getVentes()->contains($vente)) {
            $pos = $this->collVentes->search($vente);
            $this->collVentes->remove($pos);
            if (null === $this->ventesScheduledForDeletion) {
                $this->ventesScheduledForDeletion = clone $this->collVentes;
                $this->ventesScheduledForDeletion->clear();
            }
            $this->ventesScheduledForDeletion[]= clone $vente;
            $vente->setProduits(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Produits is new, it will return
     * an empty collection; or if this Produits has previously
     * been saved, it will retrieve related Ventes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Produits.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildVente[] List of ChildVente objects
     */
    public function getVentesJoinPersonne(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildVenteQuery::create(null, $criteria);
        $query->joinWith('Personne', $joinBehavior);

        return $this->getVentes($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCatgorieproduit) {
            $this->aCatgorieproduit->removeProduits($this);
        }
        if (null !== $this->aSociete) {
            $this->aSociete->removeProduits($this);
        }
        $this->idproduits = null;
        $this->libelleproduits = null;
        $this->idcatgorieproduit = null;
        $this->montantproduits = null;
        $this->societe_idsociete = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collContrats) {
                foreach ($this->collContrats as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPrimes) {
                foreach ($this->collPrimes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProspections) {
                foreach ($this->collProspections as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVentes) {
                foreach ($this->collVentes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collContrats = null;
        $this->collPrimes = null;
        $this->collProspections = null;
        $this->collVentes = null;
        $this->aCatgorieproduit = null;
        $this->aSociete = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProduitsTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
