<?php

namespace Base;

use \Personne as ChildPersonne;
use \PersonneQuery as ChildPersonneQuery;
use \Produits as ChildProduits;
use \ProduitsQuery as ChildProduitsQuery;
use \ProspectionQuery as ChildProspectionQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\ProspectionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'prospection' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Prospection implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\ProspectionTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idprospection field.
     *
     * @var        int
     */
    protected $idprospection;

    /**
     * The value for the nomprospect field.
     *
     * @var        string
     */
    protected $nomprospect;

    /**
     * The value for the prenomprospection field.
     *
     * @var        string
     */
    protected $prenomprospection;

    /**
     * The value for the telprospect field.
     *
     * @var        string
     */
    protected $telprospect;

    /**
     * The value for the emailprospect field.
     *
     * @var        string
     */
    protected $emailprospect;

    /**
     * The value for the datepriserdv field.
     *
     * @var        DateTime
     */
    protected $datepriserdv;

    /**
     * The value for the daterdv field.
     *
     * @var        DateTime
     */
    protected $daterdv;

    /**
     * The value for the typeprospect field.
     *
     * @var        int
     */
    protected $typeprospect;

    /**
     * The value for the fonctionprospect field.
     *
     * @var        string
     */
    protected $fonctionprospect;

    /**
     * The value for the idpersonneagent field.
     *
     * @var        int
     */
    protected $idpersonneagent;

    /**
     * The value for the personne_rolepersonne_idrolepersonne field.
     *
     * @var        int
     */
    protected $personne_rolepersonne_idrolepersonne;

    /**
     * The value for the produits_idproduits field.
     *
     * @var        int
     */
    protected $produits_idproduits;

    /**
     * The value for the produits_societe_idsociete field.
     *
     * @var        int
     */
    protected $produits_societe_idsociete;

    /**
     * @var        ChildPersonne
     */
    protected $aPersonne;

    /**
     * @var        ChildProduits
     */
    protected $aProduits;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\Prospection object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Prospection</code> instance.  If
     * <code>obj</code> is an instance of <code>Prospection</code>, delegates to
     * <code>equals(Prospection)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Prospection The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idprospection] column value.
     *
     * @return int
     */
    public function getIdprospection()
    {
        return $this->idprospection;
    }

    /**
     * Get the [nomprospect] column value.
     *
     * @return string
     */
    public function getNomprospect()
    {
        return $this->nomprospect;
    }

    /**
     * Get the [prenomprospection] column value.
     *
     * @return string
     */
    public function getPrenomprospection()
    {
        return $this->prenomprospection;
    }

    /**
     * Get the [telprospect] column value.
     *
     * @return string
     */
    public function getTelprospect()
    {
        return $this->telprospect;
    }

    /**
     * Get the [emailprospect] column value.
     *
     * @return string
     */
    public function getEmailprospect()
    {
        return $this->emailprospect;
    }

    /**
     * Get the [optionally formatted] temporal [datepriserdv] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatepriserdv($format = NULL)
    {
        if ($format === null) {
            return $this->datepriserdv;
        } else {
            return $this->datepriserdv instanceof \DateTimeInterface ? $this->datepriserdv->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [daterdv] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDaterdv($format = NULL)
    {
        if ($format === null) {
            return $this->daterdv;
        } else {
            return $this->daterdv instanceof \DateTimeInterface ? $this->daterdv->format($format) : null;
        }
    }

    /**
     * Get the [typeprospect] column value.
     *
     * @return int
     */
    public function getTypeprospect()
    {
        return $this->typeprospect;
    }

    /**
     * Get the [fonctionprospect] column value.
     *
     * @return string
     */
    public function getFonctionprospect()
    {
        return $this->fonctionprospect;
    }

    /**
     * Get the [idpersonneagent] column value.
     *
     * @return int
     */
    public function getIdpersonneagent()
    {
        return $this->idpersonneagent;
    }

    /**
     * Get the [personne_rolepersonne_idrolepersonne] column value.
     *
     * @return int
     */
    public function getPersonneRolepersonneIdrolepersonne()
    {
        return $this->personne_rolepersonne_idrolepersonne;
    }

    /**
     * Get the [produits_idproduits] column value.
     *
     * @return int
     */
    public function getProduitsIdproduits()
    {
        return $this->produits_idproduits;
    }

    /**
     * Get the [produits_societe_idsociete] column value.
     *
     * @return int
     */
    public function getProduitsSocieteIdsociete()
    {
        return $this->produits_societe_idsociete;
    }

    /**
     * Set the value of [idprospection] column.
     *
     * @param int $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setIdprospection($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idprospection !== $v) {
            $this->idprospection = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_IDPROSPECTION] = true;
        }

        return $this;
    } // setIdprospection()

    /**
     * Set the value of [nomprospect] column.
     *
     * @param string $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setNomprospect($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nomprospect !== $v) {
            $this->nomprospect = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_NOMPROSPECT] = true;
        }

        return $this;
    } // setNomprospect()

    /**
     * Set the value of [prenomprospection] column.
     *
     * @param string $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setPrenomprospection($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prenomprospection !== $v) {
            $this->prenomprospection = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_PRENOMPROSPECTION] = true;
        }

        return $this;
    } // setPrenomprospection()

    /**
     * Set the value of [telprospect] column.
     *
     * @param string $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setTelprospect($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telprospect !== $v) {
            $this->telprospect = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_TELPROSPECT] = true;
        }

        return $this;
    } // setTelprospect()

    /**
     * Set the value of [emailprospect] column.
     *
     * @param string $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setEmailprospect($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->emailprospect !== $v) {
            $this->emailprospect = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_EMAILPROSPECT] = true;
        }

        return $this;
    } // setEmailprospect()

    /**
     * Sets the value of [datepriserdv] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setDatepriserdv($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->datepriserdv !== null || $dt !== null) {
            if ($this->datepriserdv === null || $dt === null || $dt->format("Y-m-d") !== $this->datepriserdv->format("Y-m-d")) {
                $this->datepriserdv = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProspectionTableMap::COL_DATEPRISERDV] = true;
            }
        } // if either are not null

        return $this;
    } // setDatepriserdv()

    /**
     * Sets the value of [daterdv] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setDaterdv($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->daterdv !== null || $dt !== null) {
            if ($this->daterdv === null || $dt === null || $dt->format("Y-m-d") !== $this->daterdv->format("Y-m-d")) {
                $this->daterdv = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProspectionTableMap::COL_DATERDV] = true;
            }
        } // if either are not null

        return $this;
    } // setDaterdv()

    /**
     * Set the value of [typeprospect] column.
     *
     * @param int $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setTypeprospect($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->typeprospect !== $v) {
            $this->typeprospect = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_TYPEPROSPECT] = true;
        }

        return $this;
    } // setTypeprospect()

    /**
     * Set the value of [fonctionprospect] column.
     *
     * @param string $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setFonctionprospect($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fonctionprospect !== $v) {
            $this->fonctionprospect = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_FONCTIONPROSPECT] = true;
        }

        return $this;
    } // setFonctionprospect()

    /**
     * Set the value of [idpersonneagent] column.
     *
     * @param int $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setIdpersonneagent($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idpersonneagent !== $v) {
            $this->idpersonneagent = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_IDPERSONNEAGENT] = true;
        }

        if ($this->aPersonne !== null && $this->aPersonne->getIdpersonne() !== $v) {
            $this->aPersonne = null;
        }

        return $this;
    } // setIdpersonneagent()

    /**
     * Set the value of [personne_rolepersonne_idrolepersonne] column.
     *
     * @param int $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setPersonneRolepersonneIdrolepersonne($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->personne_rolepersonne_idrolepersonne !== $v) {
            $this->personne_rolepersonne_idrolepersonne = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE] = true;
        }

        if ($this->aPersonne !== null && $this->aPersonne->getRolepersonneIdrolepersonne() !== $v) {
            $this->aPersonne = null;
        }

        return $this;
    } // setPersonneRolepersonneIdrolepersonne()

    /**
     * Set the value of [produits_idproduits] column.
     *
     * @param int $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setProduitsIdproduits($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->produits_idproduits !== $v) {
            $this->produits_idproduits = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_PRODUITS_IDPRODUITS] = true;
        }

        if ($this->aProduits !== null && $this->aProduits->getIdproduits() !== $v) {
            $this->aProduits = null;
        }

        return $this;
    } // setProduitsIdproduits()

    /**
     * Set the value of [produits_societe_idsociete] column.
     *
     * @param int $v new value
     * @return $this|\Prospection The current object (for fluent API support)
     */
    public function setProduitsSocieteIdsociete($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->produits_societe_idsociete !== $v) {
            $this->produits_societe_idsociete = $v;
            $this->modifiedColumns[ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE] = true;
        }

        if ($this->aProduits !== null && $this->aProduits->getSocieteIdsociete() !== $v) {
            $this->aProduits = null;
        }

        return $this;
    } // setProduitsSocieteIdsociete()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProspectionTableMap::translateFieldName('Idprospection', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idprospection = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProspectionTableMap::translateFieldName('Nomprospect', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nomprospect = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProspectionTableMap::translateFieldName('Prenomprospection', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prenomprospection = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProspectionTableMap::translateFieldName('Telprospect', TableMap::TYPE_PHPNAME, $indexType)];
            $this->telprospect = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProspectionTableMap::translateFieldName('Emailprospect', TableMap::TYPE_PHPNAME, $indexType)];
            $this->emailprospect = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ProspectionTableMap::translateFieldName('Datepriserdv', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->datepriserdv = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ProspectionTableMap::translateFieldName('Daterdv', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->daterdv = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ProspectionTableMap::translateFieldName('Typeprospect', TableMap::TYPE_PHPNAME, $indexType)];
            $this->typeprospect = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ProspectionTableMap::translateFieldName('Fonctionprospect', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fonctionprospect = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ProspectionTableMap::translateFieldName('Idpersonneagent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idpersonneagent = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ProspectionTableMap::translateFieldName('PersonneRolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->personne_rolepersonne_idrolepersonne = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ProspectionTableMap::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)];
            $this->produits_idproduits = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ProspectionTableMap::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)];
            $this->produits_societe_idsociete = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = ProspectionTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Prospection'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aPersonne !== null && $this->idpersonneagent !== $this->aPersonne->getIdpersonne()) {
            $this->aPersonne = null;
        }
        if ($this->aPersonne !== null && $this->personne_rolepersonne_idrolepersonne !== $this->aPersonne->getRolepersonneIdrolepersonne()) {
            $this->aPersonne = null;
        }
        if ($this->aProduits !== null && $this->produits_idproduits !== $this->aProduits->getIdproduits()) {
            $this->aProduits = null;
        }
        if ($this->aProduits !== null && $this->produits_societe_idsociete !== $this->aProduits->getSocieteIdsociete()) {
            $this->aProduits = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProspectionTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProspectionQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPersonne = null;
            $this->aProduits = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Prospection::setDeleted()
     * @see Prospection::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProspectionTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProspectionQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProspectionTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProspectionTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPersonne !== null) {
                if ($this->aPersonne->isModified() || $this->aPersonne->isNew()) {
                    $affectedRows += $this->aPersonne->save($con);
                }
                $this->setPersonne($this->aPersonne);
            }

            if ($this->aProduits !== null) {
                if ($this->aProduits->isModified() || $this->aProduits->isNew()) {
                    $affectedRows += $this->aProduits->save($con);
                }
                $this->setProduits($this->aProduits);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ProspectionTableMap::COL_IDPROSPECTION] = true;
        if (null !== $this->idprospection) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProspectionTableMap::COL_IDPROSPECTION . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProspectionTableMap::COL_IDPROSPECTION)) {
            $modifiedColumns[':p' . $index++]  = 'idprospection';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_NOMPROSPECT)) {
            $modifiedColumns[':p' . $index++]  = 'NomProspect';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_PRENOMPROSPECTION)) {
            $modifiedColumns[':p' . $index++]  = 'Prenomprospection';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_TELPROSPECT)) {
            $modifiedColumns[':p' . $index++]  = 'TelProspect';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_EMAILPROSPECT)) {
            $modifiedColumns[':p' . $index++]  = 'EmailProspect';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_DATEPRISERDV)) {
            $modifiedColumns[':p' . $index++]  = 'DatePriseRDV';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_DATERDV)) {
            $modifiedColumns[':p' . $index++]  = 'DateRDV';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_TYPEPROSPECT)) {
            $modifiedColumns[':p' . $index++]  = 'typeprospect';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_FONCTIONPROSPECT)) {
            $modifiedColumns[':p' . $index++]  = 'fonctionprospect';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_IDPERSONNEAGENT)) {
            $modifiedColumns[':p' . $index++]  = 'idPersonneAgent';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE)) {
            $modifiedColumns[':p' . $index++]  = 'Personne_rolePersonne_idrolePersonne';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_PRODUITS_IDPRODUITS)) {
            $modifiedColumns[':p' . $index++]  = 'produits_idproduits';
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE)) {
            $modifiedColumns[':p' . $index++]  = 'produits_societe_idsociete';
        }

        $sql = sprintf(
            'INSERT INTO prospection (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idprospection':
                        $stmt->bindValue($identifier, $this->idprospection, PDO::PARAM_INT);
                        break;
                    case 'NomProspect':
                        $stmt->bindValue($identifier, $this->nomprospect, PDO::PARAM_STR);
                        break;
                    case 'Prenomprospection':
                        $stmt->bindValue($identifier, $this->prenomprospection, PDO::PARAM_STR);
                        break;
                    case 'TelProspect':
                        $stmt->bindValue($identifier, $this->telprospect, PDO::PARAM_STR);
                        break;
                    case 'EmailProspect':
                        $stmt->bindValue($identifier, $this->emailprospect, PDO::PARAM_STR);
                        break;
                    case 'DatePriseRDV':
                        $stmt->bindValue($identifier, $this->datepriserdv ? $this->datepriserdv->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'DateRDV':
                        $stmt->bindValue($identifier, $this->daterdv ? $this->daterdv->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'typeprospect':
                        $stmt->bindValue($identifier, $this->typeprospect, PDO::PARAM_INT);
                        break;
                    case 'fonctionprospect':
                        $stmt->bindValue($identifier, $this->fonctionprospect, PDO::PARAM_STR);
                        break;
                    case 'idPersonneAgent':
                        $stmt->bindValue($identifier, $this->idpersonneagent, PDO::PARAM_INT);
                        break;
                    case 'Personne_rolePersonne_idrolePersonne':
                        $stmt->bindValue($identifier, $this->personne_rolepersonne_idrolepersonne, PDO::PARAM_INT);
                        break;
                    case 'produits_idproduits':
                        $stmt->bindValue($identifier, $this->produits_idproduits, PDO::PARAM_INT);
                        break;
                    case 'produits_societe_idsociete':
                        $stmt->bindValue($identifier, $this->produits_societe_idsociete, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdprospection($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProspectionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdprospection();
                break;
            case 1:
                return $this->getNomprospect();
                break;
            case 2:
                return $this->getPrenomprospection();
                break;
            case 3:
                return $this->getTelprospect();
                break;
            case 4:
                return $this->getEmailprospect();
                break;
            case 5:
                return $this->getDatepriserdv();
                break;
            case 6:
                return $this->getDaterdv();
                break;
            case 7:
                return $this->getTypeprospect();
                break;
            case 8:
                return $this->getFonctionprospect();
                break;
            case 9:
                return $this->getIdpersonneagent();
                break;
            case 10:
                return $this->getPersonneRolepersonneIdrolepersonne();
                break;
            case 11:
                return $this->getProduitsIdproduits();
                break;
            case 12:
                return $this->getProduitsSocieteIdsociete();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Prospection'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Prospection'][$this->hashCode()] = true;
        $keys = ProspectionTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdprospection(),
            $keys[1] => $this->getNomprospect(),
            $keys[2] => $this->getPrenomprospection(),
            $keys[3] => $this->getTelprospect(),
            $keys[4] => $this->getEmailprospect(),
            $keys[5] => $this->getDatepriserdv(),
            $keys[6] => $this->getDaterdv(),
            $keys[7] => $this->getTypeprospect(),
            $keys[8] => $this->getFonctionprospect(),
            $keys[9] => $this->getIdpersonneagent(),
            $keys[10] => $this->getPersonneRolepersonneIdrolepersonne(),
            $keys[11] => $this->getProduitsIdproduits(),
            $keys[12] => $this->getProduitsSocieteIdsociete(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPersonne) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'personne';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'personne';
                        break;
                    default:
                        $key = 'Personne';
                }

                $result[$key] = $this->aPersonne->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aProduits) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'produits';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'produits';
                        break;
                    default:
                        $key = 'Produits';
                }

                $result[$key] = $this->aProduits->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Prospection
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProspectionTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Prospection
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdprospection($value);
                break;
            case 1:
                $this->setNomprospect($value);
                break;
            case 2:
                $this->setPrenomprospection($value);
                break;
            case 3:
                $this->setTelprospect($value);
                break;
            case 4:
                $this->setEmailprospect($value);
                break;
            case 5:
                $this->setDatepriserdv($value);
                break;
            case 6:
                $this->setDaterdv($value);
                break;
            case 7:
                $this->setTypeprospect($value);
                break;
            case 8:
                $this->setFonctionprospect($value);
                break;
            case 9:
                $this->setIdpersonneagent($value);
                break;
            case 10:
                $this->setPersonneRolepersonneIdrolepersonne($value);
                break;
            case 11:
                $this->setProduitsIdproduits($value);
                break;
            case 12:
                $this->setProduitsSocieteIdsociete($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProspectionTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdprospection($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNomprospect($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPrenomprospection($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTelprospect($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setEmailprospect($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDatepriserdv($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDaterdv($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTypeprospect($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setFonctionprospect($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIdpersonneagent($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPersonneRolepersonneIdrolepersonne($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setProduitsIdproduits($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setProduitsSocieteIdsociete($arr[$keys[12]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Prospection The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProspectionTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProspectionTableMap::COL_IDPROSPECTION)) {
            $criteria->add(ProspectionTableMap::COL_IDPROSPECTION, $this->idprospection);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_NOMPROSPECT)) {
            $criteria->add(ProspectionTableMap::COL_NOMPROSPECT, $this->nomprospect);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_PRENOMPROSPECTION)) {
            $criteria->add(ProspectionTableMap::COL_PRENOMPROSPECTION, $this->prenomprospection);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_TELPROSPECT)) {
            $criteria->add(ProspectionTableMap::COL_TELPROSPECT, $this->telprospect);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_EMAILPROSPECT)) {
            $criteria->add(ProspectionTableMap::COL_EMAILPROSPECT, $this->emailprospect);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_DATEPRISERDV)) {
            $criteria->add(ProspectionTableMap::COL_DATEPRISERDV, $this->datepriserdv);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_DATERDV)) {
            $criteria->add(ProspectionTableMap::COL_DATERDV, $this->daterdv);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_TYPEPROSPECT)) {
            $criteria->add(ProspectionTableMap::COL_TYPEPROSPECT, $this->typeprospect);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_FONCTIONPROSPECT)) {
            $criteria->add(ProspectionTableMap::COL_FONCTIONPROSPECT, $this->fonctionprospect);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_IDPERSONNEAGENT)) {
            $criteria->add(ProspectionTableMap::COL_IDPERSONNEAGENT, $this->idpersonneagent);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE)) {
            $criteria->add(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $this->personne_rolepersonne_idrolepersonne);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_PRODUITS_IDPRODUITS)) {
            $criteria->add(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $this->produits_idproduits);
        }
        if ($this->isColumnModified(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE)) {
            $criteria->add(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $this->produits_societe_idsociete);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProspectionQuery::create();
        $criteria->add(ProspectionTableMap::COL_IDPROSPECTION, $this->idprospection);
        $criteria->add(ProspectionTableMap::COL_IDPERSONNEAGENT, $this->idpersonneagent);
        $criteria->add(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $this->personne_rolepersonne_idrolepersonne);
        $criteria->add(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $this->produits_idproduits);
        $criteria->add(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $this->produits_societe_idsociete);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdprospection() &&
            null !== $this->getIdpersonneagent() &&
            null !== $this->getPersonneRolepersonneIdrolepersonne() &&
            null !== $this->getProduitsIdproduits() &&
            null !== $this->getProduitsSocieteIdsociete();

        $validPrimaryKeyFKs = 4;
        $primaryKeyFKs = [];

        //relation fk_prospection_Personne1 to table personne
        if ($this->aPersonne && $hash = spl_object_hash($this->aPersonne)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        //relation fk_prospection_produits1 to table produits
        if ($this->aProduits && $hash = spl_object_hash($this->aProduits)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getIdprospection();
        $pks[1] = $this->getIdpersonneagent();
        $pks[2] = $this->getPersonneRolepersonneIdrolepersonne();
        $pks[3] = $this->getProduitsIdproduits();
        $pks[4] = $this->getProduitsSocieteIdsociete();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param      array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setIdprospection($keys[0]);
        $this->setIdpersonneagent($keys[1]);
        $this->setPersonneRolepersonneIdrolepersonne($keys[2]);
        $this->setProduitsIdproduits($keys[3]);
        $this->setProduitsSocieteIdsociete($keys[4]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return (null === $this->getIdprospection()) && (null === $this->getIdpersonneagent()) && (null === $this->getPersonneRolepersonneIdrolepersonne()) && (null === $this->getProduitsIdproduits()) && (null === $this->getProduitsSocieteIdsociete());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Prospection (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNomprospect($this->getNomprospect());
        $copyObj->setPrenomprospection($this->getPrenomprospection());
        $copyObj->setTelprospect($this->getTelprospect());
        $copyObj->setEmailprospect($this->getEmailprospect());
        $copyObj->setDatepriserdv($this->getDatepriserdv());
        $copyObj->setDaterdv($this->getDaterdv());
        $copyObj->setTypeprospect($this->getTypeprospect());
        $copyObj->setFonctionprospect($this->getFonctionprospect());
        $copyObj->setIdpersonneagent($this->getIdpersonneagent());
        $copyObj->setPersonneRolepersonneIdrolepersonne($this->getPersonneRolepersonneIdrolepersonne());
        $copyObj->setProduitsIdproduits($this->getProduitsIdproduits());
        $copyObj->setProduitsSocieteIdsociete($this->getProduitsSocieteIdsociete());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdprospection(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Prospection Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPersonne object.
     *
     * @param  ChildPersonne $v
     * @return $this|\Prospection The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPersonne(ChildPersonne $v = null)
    {
        if ($v === null) {
            $this->setIdpersonneagent(NULL);
        } else {
            $this->setIdpersonneagent($v->getIdpersonne());
        }

        if ($v === null) {
            $this->setPersonneRolepersonneIdrolepersonne(NULL);
        } else {
            $this->setPersonneRolepersonneIdrolepersonne($v->getRolepersonneIdrolepersonne());
        }

        $this->aPersonne = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPersonne object, it will not be re-added.
        if ($v !== null) {
            $v->addProspection($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPersonne object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPersonne The associated ChildPersonne object.
     * @throws PropelException
     */
    public function getPersonne(ConnectionInterface $con = null)
    {
        if ($this->aPersonne === null && ($this->idpersonneagent != 0 && $this->personne_rolepersonne_idrolepersonne != 0)) {
            $this->aPersonne = ChildPersonneQuery::create()->findPk(array($this->idpersonneagent, $this->personne_rolepersonne_idrolepersonne), $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPersonne->addProspections($this);
             */
        }

        return $this->aPersonne;
    }

    /**
     * Declares an association between this object and a ChildProduits object.
     *
     * @param  ChildProduits $v
     * @return $this|\Prospection The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProduits(ChildProduits $v = null)
    {
        if ($v === null) {
            $this->setProduitsIdproduits(NULL);
        } else {
            $this->setProduitsIdproduits($v->getIdproduits());
        }

        if ($v === null) {
            $this->setProduitsSocieteIdsociete(NULL);
        } else {
            $this->setProduitsSocieteIdsociete($v->getSocieteIdsociete());
        }

        $this->aProduits = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildProduits object, it will not be re-added.
        if ($v !== null) {
            $v->addProspection($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildProduits object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildProduits The associated ChildProduits object.
     * @throws PropelException
     */
    public function getProduits(ConnectionInterface $con = null)
    {
        if ($this->aProduits === null && ($this->produits_idproduits != 0 && $this->produits_societe_idsociete != 0)) {
            $this->aProduits = ChildProduitsQuery::create()->findPk(array($this->produits_idproduits, $this->produits_societe_idsociete), $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProduits->addProspections($this);
             */
        }

        return $this->aProduits;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPersonne) {
            $this->aPersonne->removeProspection($this);
        }
        if (null !== $this->aProduits) {
            $this->aProduits->removeProspection($this);
        }
        $this->idprospection = null;
        $this->nomprospect = null;
        $this->prenomprospection = null;
        $this->telprospect = null;
        $this->emailprospect = null;
        $this->datepriserdv = null;
        $this->daterdv = null;
        $this->typeprospect = null;
        $this->fonctionprospect = null;
        $this->idpersonneagent = null;
        $this->personne_rolepersonne_idrolepersonne = null;
        $this->produits_idproduits = null;
        $this->produits_societe_idsociete = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aPersonne = null;
        $this->aProduits = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProspectionTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
