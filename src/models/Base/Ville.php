<?php

namespace Base;

use \Commune as ChildCommune;
use \CommuneQuery as ChildCommuneQuery;
use \Pays as ChildPays;
use \PaysQuery as ChildPaysQuery;
use \Personne as ChildPersonne;
use \PersonneQuery as ChildPersonneQuery;
use \Societe as ChildSociete;
use \SocieteQuery as ChildSocieteQuery;
use \Ville as ChildVille;
use \VilleQuery as ChildVilleQuery;
use \Exception;
use \PDO;
use Map\CommuneTableMap;
use Map\PersonneTableMap;
use Map\SocieteTableMap;
use Map\VilleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'ville' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Ville implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\VilleTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idville field.
     *
     * @var        int
     */
    protected $idville;

    /**
     * The value for the libelleville field.
     *
     * @var        string
     */
    protected $libelleville;

    /**
     * The value for the pays_idpays field.
     *
     * @var        int
     */
    protected $pays_idpays;

    /**
     * @var        ChildPays
     */
    protected $aPays;

    /**
     * @var        ObjectCollection|ChildCommune[] Collection to store aggregation of ChildCommune objects.
     */
    protected $collCommunes;
    protected $collCommunesPartial;

    /**
     * @var        ObjectCollection|ChildPersonne[] Collection to store aggregation of ChildPersonne objects.
     */
    protected $collPersonnes;
    protected $collPersonnesPartial;

    /**
     * @var        ObjectCollection|ChildSociete[] Collection to store aggregation of ChildSociete objects.
     */
    protected $collSocietes;
    protected $collSocietesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCommune[]
     */
    protected $communesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPersonne[]
     */
    protected $personnesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSociete[]
     */
    protected $societesScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Ville object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Ville</code> instance.  If
     * <code>obj</code> is an instance of <code>Ville</code>, delegates to
     * <code>equals(Ville)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Ville The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idville] column value.
     *
     * @return int
     */
    public function getIdville()
    {
        return $this->idville;
    }

    /**
     * Get the [libelleville] column value.
     *
     * @return string
     */
    public function getLibelleville()
    {
        return $this->libelleville;
    }

    /**
     * Get the [pays_idpays] column value.
     *
     * @return int
     */
    public function getPaysIdpays()
    {
        return $this->pays_idpays;
    }

    /**
     * Set the value of [idville] column.
     *
     * @param int $v new value
     * @return $this|\Ville The current object (for fluent API support)
     */
    public function setIdville($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idville !== $v) {
            $this->idville = $v;
            $this->modifiedColumns[VilleTableMap::COL_IDVILLE] = true;
        }

        return $this;
    } // setIdville()

    /**
     * Set the value of [libelleville] column.
     *
     * @param string $v new value
     * @return $this|\Ville The current object (for fluent API support)
     */
    public function setLibelleville($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->libelleville !== $v) {
            $this->libelleville = $v;
            $this->modifiedColumns[VilleTableMap::COL_LIBELLEVILLE] = true;
        }

        return $this;
    } // setLibelleville()

    /**
     * Set the value of [pays_idpays] column.
     *
     * @param int $v new value
     * @return $this|\Ville The current object (for fluent API support)
     */
    public function setPaysIdpays($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->pays_idpays !== $v) {
            $this->pays_idpays = $v;
            $this->modifiedColumns[VilleTableMap::COL_PAYS_IDPAYS] = true;
        }

        if ($this->aPays !== null && $this->aPays->getIdpays() !== $v) {
            $this->aPays = null;
        }

        return $this;
    } // setPaysIdpays()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : VilleTableMap::translateFieldName('Idville', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idville = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : VilleTableMap::translateFieldName('Libelleville', TableMap::TYPE_PHPNAME, $indexType)];
            $this->libelleville = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : VilleTableMap::translateFieldName('PaysIdpays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pays_idpays = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 3; // 3 = VilleTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Ville'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aPays !== null && $this->pays_idpays !== $this->aPays->getIdpays()) {
            $this->aPays = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VilleTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildVilleQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPays = null;
            $this->collCommunes = null;

            $this->collPersonnes = null;

            $this->collSocietes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Ville::setDeleted()
     * @see Ville::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(VilleTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildVilleQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(VilleTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                VilleTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPays !== null) {
                if ($this->aPays->isModified() || $this->aPays->isNew()) {
                    $affectedRows += $this->aPays->save($con);
                }
                $this->setPays($this->aPays);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->communesScheduledForDeletion !== null) {
                if (!$this->communesScheduledForDeletion->isEmpty()) {
                    \CommuneQuery::create()
                        ->filterByPrimaryKeys($this->communesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->communesScheduledForDeletion = null;
                }
            }

            if ($this->collCommunes !== null) {
                foreach ($this->collCommunes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->personnesScheduledForDeletion !== null) {
                if (!$this->personnesScheduledForDeletion->isEmpty()) {
                    \PersonneQuery::create()
                        ->filterByPrimaryKeys($this->personnesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->personnesScheduledForDeletion = null;
                }
            }

            if ($this->collPersonnes !== null) {
                foreach ($this->collPersonnes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->societesScheduledForDeletion !== null) {
                if (!$this->societesScheduledForDeletion->isEmpty()) {
                    \SocieteQuery::create()
                        ->filterByPrimaryKeys($this->societesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->societesScheduledForDeletion = null;
                }
            }

            if ($this->collSocietes !== null) {
                foreach ($this->collSocietes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[VilleTableMap::COL_IDVILLE] = true;
        if (null !== $this->idville) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . VilleTableMap::COL_IDVILLE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(VilleTableMap::COL_IDVILLE)) {
            $modifiedColumns[':p' . $index++]  = 'idville';
        }
        if ($this->isColumnModified(VilleTableMap::COL_LIBELLEVILLE)) {
            $modifiedColumns[':p' . $index++]  = 'Libelleville';
        }
        if ($this->isColumnModified(VilleTableMap::COL_PAYS_IDPAYS)) {
            $modifiedColumns[':p' . $index++]  = 'pays_idpays';
        }

        $sql = sprintf(
            'INSERT INTO ville (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idville':
                        $stmt->bindValue($identifier, $this->idville, PDO::PARAM_INT);
                        break;
                    case 'Libelleville':
                        $stmt->bindValue($identifier, $this->libelleville, PDO::PARAM_STR);
                        break;
                    case 'pays_idpays':
                        $stmt->bindValue($identifier, $this->pays_idpays, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdville($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = VilleTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdville();
                break;
            case 1:
                return $this->getLibelleville();
                break;
            case 2:
                return $this->getPaysIdpays();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Ville'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Ville'][$this->hashCode()] = true;
        $keys = VilleTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdville(),
            $keys[1] => $this->getLibelleville(),
            $keys[2] => $this->getPaysIdpays(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPays) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'pays';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'pays';
                        break;
                    default:
                        $key = 'Pays';
                }

                $result[$key] = $this->aPays->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommunes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'communes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'communes';
                        break;
                    default:
                        $key = 'Communes';
                }

                $result[$key] = $this->collCommunes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPersonnes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'personnes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'personnes';
                        break;
                    default:
                        $key = 'Personnes';
                }

                $result[$key] = $this->collPersonnes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSocietes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'societes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'societes';
                        break;
                    default:
                        $key = 'Societes';
                }

                $result[$key] = $this->collSocietes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Ville
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = VilleTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Ville
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdville($value);
                break;
            case 1:
                $this->setLibelleville($value);
                break;
            case 2:
                $this->setPaysIdpays($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = VilleTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdville($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setLibelleville($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPaysIdpays($arr[$keys[2]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Ville The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(VilleTableMap::DATABASE_NAME);

        if ($this->isColumnModified(VilleTableMap::COL_IDVILLE)) {
            $criteria->add(VilleTableMap::COL_IDVILLE, $this->idville);
        }
        if ($this->isColumnModified(VilleTableMap::COL_LIBELLEVILLE)) {
            $criteria->add(VilleTableMap::COL_LIBELLEVILLE, $this->libelleville);
        }
        if ($this->isColumnModified(VilleTableMap::COL_PAYS_IDPAYS)) {
            $criteria->add(VilleTableMap::COL_PAYS_IDPAYS, $this->pays_idpays);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildVilleQuery::create();
        $criteria->add(VilleTableMap::COL_IDVILLE, $this->idville);
        $criteria->add(VilleTableMap::COL_PAYS_IDPAYS, $this->pays_idpays);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdville() &&
            null !== $this->getPaysIdpays();

        $validPrimaryKeyFKs = 1;
        $primaryKeyFKs = [];

        //relation fk_ville_pays1 to table pays
        if ($this->aPays && $hash = spl_object_hash($this->aPays)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getIdville();
        $pks[1] = $this->getPaysIdpays();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param      array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setIdville($keys[0]);
        $this->setPaysIdpays($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return (null === $this->getIdville()) && (null === $this->getPaysIdpays());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Ville (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLibelleville($this->getLibelleville());
        $copyObj->setPaysIdpays($this->getPaysIdpays());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCommunes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommune($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPersonnes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPersonne($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSocietes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSociete($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdville(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Ville Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPays object.
     *
     * @param  ChildPays $v
     * @return $this|\Ville The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPays(ChildPays $v = null)
    {
        if ($v === null) {
            $this->setPaysIdpays(NULL);
        } else {
            $this->setPaysIdpays($v->getIdpays());
        }

        $this->aPays = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPays object, it will not be re-added.
        if ($v !== null) {
            $v->addVille($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPays object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPays The associated ChildPays object.
     * @throws PropelException
     */
    public function getPays(ConnectionInterface $con = null)
    {
        if ($this->aPays === null && ($this->pays_idpays != 0)) {
            $this->aPays = ChildPaysQuery::create()->findPk($this->pays_idpays, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPays->addVilles($this);
             */
        }

        return $this->aPays;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Commune' == $relationName) {
            $this->initCommunes();
            return;
        }
        if ('Personne' == $relationName) {
            $this->initPersonnes();
            return;
        }
        if ('Societe' == $relationName) {
            $this->initSocietes();
            return;
        }
    }

    /**
     * Clears out the collCommunes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCommunes()
     */
    public function clearCommunes()
    {
        $this->collCommunes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCommunes collection loaded partially.
     */
    public function resetPartialCommunes($v = true)
    {
        $this->collCommunesPartial = $v;
    }

    /**
     * Initializes the collCommunes collection.
     *
     * By default this just sets the collCommunes collection to an empty array (like clearcollCommunes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommunes($overrideExisting = true)
    {
        if (null !== $this->collCommunes && !$overrideExisting) {
            return;
        }

        $collectionClassName = CommuneTableMap::getTableMap()->getCollectionClassName();

        $this->collCommunes = new $collectionClassName;
        $this->collCommunes->setModel('\Commune');
    }

    /**
     * Gets an array of ChildCommune objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildVille is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCommune[] List of ChildCommune objects
     * @throws PropelException
     */
    public function getCommunes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCommunesPartial && !$this->isNew();
        if (null === $this->collCommunes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommunes) {
                // return empty collection
                $this->initCommunes();
            } else {
                $collCommunes = ChildCommuneQuery::create(null, $criteria)
                    ->filterByVille($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCommunesPartial && count($collCommunes)) {
                        $this->initCommunes(false);

                        foreach ($collCommunes as $obj) {
                            if (false == $this->collCommunes->contains($obj)) {
                                $this->collCommunes->append($obj);
                            }
                        }

                        $this->collCommunesPartial = true;
                    }

                    return $collCommunes;
                }

                if ($partial && $this->collCommunes) {
                    foreach ($this->collCommunes as $obj) {
                        if ($obj->isNew()) {
                            $collCommunes[] = $obj;
                        }
                    }
                }

                $this->collCommunes = $collCommunes;
                $this->collCommunesPartial = false;
            }
        }

        return $this->collCommunes;
    }

    /**
     * Sets a collection of ChildCommune objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $communes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildVille The current object (for fluent API support)
     */
    public function setCommunes(Collection $communes, ConnectionInterface $con = null)
    {
        /** @var ChildCommune[] $communesToDelete */
        $communesToDelete = $this->getCommunes(new Criteria(), $con)->diff($communes);


        $this->communesScheduledForDeletion = $communesToDelete;

        foreach ($communesToDelete as $communeRemoved) {
            $communeRemoved->setVille(null);
        }

        $this->collCommunes = null;
        foreach ($communes as $commune) {
            $this->addCommune($commune);
        }

        $this->collCommunes = $communes;
        $this->collCommunesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Commune objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Commune objects.
     * @throws PropelException
     */
    public function countCommunes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCommunesPartial && !$this->isNew();
        if (null === $this->collCommunes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommunes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommunes());
            }

            $query = ChildCommuneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByVille($this)
                ->count($con);
        }

        return count($this->collCommunes);
    }

    /**
     * Method called to associate a ChildCommune object to this object
     * through the ChildCommune foreign key attribute.
     *
     * @param  ChildCommune $l ChildCommune
     * @return $this|\Ville The current object (for fluent API support)
     */
    public function addCommune(ChildCommune $l)
    {
        if ($this->collCommunes === null) {
            $this->initCommunes();
            $this->collCommunesPartial = true;
        }

        if (!$this->collCommunes->contains($l)) {
            $this->doAddCommune($l);

            if ($this->communesScheduledForDeletion and $this->communesScheduledForDeletion->contains($l)) {
                $this->communesScheduledForDeletion->remove($this->communesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCommune $commune The ChildCommune object to add.
     */
    protected function doAddCommune(ChildCommune $commune)
    {
        $this->collCommunes[]= $commune;
        $commune->setVille($this);
    }

    /**
     * @param  ChildCommune $commune The ChildCommune object to remove.
     * @return $this|ChildVille The current object (for fluent API support)
     */
    public function removeCommune(ChildCommune $commune)
    {
        if ($this->getCommunes()->contains($commune)) {
            $pos = $this->collCommunes->search($commune);
            $this->collCommunes->remove($pos);
            if (null === $this->communesScheduledForDeletion) {
                $this->communesScheduledForDeletion = clone $this->collCommunes;
                $this->communesScheduledForDeletion->clear();
            }
            $this->communesScheduledForDeletion[]= clone $commune;
            $commune->setVille(null);
        }

        return $this;
    }

    /**
     * Clears out the collPersonnes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPersonnes()
     */
    public function clearPersonnes()
    {
        $this->collPersonnes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPersonnes collection loaded partially.
     */
    public function resetPartialPersonnes($v = true)
    {
        $this->collPersonnesPartial = $v;
    }

    /**
     * Initializes the collPersonnes collection.
     *
     * By default this just sets the collPersonnes collection to an empty array (like clearcollPersonnes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPersonnes($overrideExisting = true)
    {
        if (null !== $this->collPersonnes && !$overrideExisting) {
            return;
        }

        $collectionClassName = PersonneTableMap::getTableMap()->getCollectionClassName();

        $this->collPersonnes = new $collectionClassName;
        $this->collPersonnes->setModel('\Personne');
    }

    /**
     * Gets an array of ChildPersonne objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildVille is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPersonne[] List of ChildPersonne objects
     * @throws PropelException
     */
    public function getPersonnes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPersonnesPartial && !$this->isNew();
        if (null === $this->collPersonnes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPersonnes) {
                // return empty collection
                $this->initPersonnes();
            } else {
                $collPersonnes = ChildPersonneQuery::create(null, $criteria)
                    ->filterByVille($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPersonnesPartial && count($collPersonnes)) {
                        $this->initPersonnes(false);

                        foreach ($collPersonnes as $obj) {
                            if (false == $this->collPersonnes->contains($obj)) {
                                $this->collPersonnes->append($obj);
                            }
                        }

                        $this->collPersonnesPartial = true;
                    }

                    return $collPersonnes;
                }

                if ($partial && $this->collPersonnes) {
                    foreach ($this->collPersonnes as $obj) {
                        if ($obj->isNew()) {
                            $collPersonnes[] = $obj;
                        }
                    }
                }

                $this->collPersonnes = $collPersonnes;
                $this->collPersonnesPartial = false;
            }
        }

        return $this->collPersonnes;
    }

    /**
     * Sets a collection of ChildPersonne objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $personnes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildVille The current object (for fluent API support)
     */
    public function setPersonnes(Collection $personnes, ConnectionInterface $con = null)
    {
        /** @var ChildPersonne[] $personnesToDelete */
        $personnesToDelete = $this->getPersonnes(new Criteria(), $con)->diff($personnes);


        $this->personnesScheduledForDeletion = $personnesToDelete;

        foreach ($personnesToDelete as $personneRemoved) {
            $personneRemoved->setVille(null);
        }

        $this->collPersonnes = null;
        foreach ($personnes as $personne) {
            $this->addPersonne($personne);
        }

        $this->collPersonnes = $personnes;
        $this->collPersonnesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Personne objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Personne objects.
     * @throws PropelException
     */
    public function countPersonnes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPersonnesPartial && !$this->isNew();
        if (null === $this->collPersonnes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPersonnes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPersonnes());
            }

            $query = ChildPersonneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByVille($this)
                ->count($con);
        }

        return count($this->collPersonnes);
    }

    /**
     * Method called to associate a ChildPersonne object to this object
     * through the ChildPersonne foreign key attribute.
     *
     * @param  ChildPersonne $l ChildPersonne
     * @return $this|\Ville The current object (for fluent API support)
     */
    public function addPersonne(ChildPersonne $l)
    {
        if ($this->collPersonnes === null) {
            $this->initPersonnes();
            $this->collPersonnesPartial = true;
        }

        if (!$this->collPersonnes->contains($l)) {
            $this->doAddPersonne($l);

            if ($this->personnesScheduledForDeletion and $this->personnesScheduledForDeletion->contains($l)) {
                $this->personnesScheduledForDeletion->remove($this->personnesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPersonne $personne The ChildPersonne object to add.
     */
    protected function doAddPersonne(ChildPersonne $personne)
    {
        $this->collPersonnes[]= $personne;
        $personne->setVille($this);
    }

    /**
     * @param  ChildPersonne $personne The ChildPersonne object to remove.
     * @return $this|ChildVille The current object (for fluent API support)
     */
    public function removePersonne(ChildPersonne $personne)
    {
        if ($this->getPersonnes()->contains($personne)) {
            $pos = $this->collPersonnes->search($personne);
            $this->collPersonnes->remove($pos);
            if (null === $this->personnesScheduledForDeletion) {
                $this->personnesScheduledForDeletion = clone $this->collPersonnes;
                $this->personnesScheduledForDeletion->clear();
            }
            $this->personnesScheduledForDeletion[]= clone $personne;
            $personne->setVille(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ville is new, it will return
     * an empty collection; or if this Ville has previously
     * been saved, it will retrieve related Personnes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ville.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPersonne[] List of ChildPersonne objects
     */
    public function getPersonnesJoinCommune(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPersonneQuery::create(null, $criteria);
        $query->joinWith('Commune', $joinBehavior);

        return $this->getPersonnes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ville is new, it will return
     * an empty collection; or if this Ville has previously
     * been saved, it will retrieve related Personnes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ville.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPersonne[] List of ChildPersonne objects
     */
    public function getPersonnesJoinRolepersonne(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPersonneQuery::create(null, $criteria);
        $query->joinWith('Rolepersonne', $joinBehavior);

        return $this->getPersonnes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Ville is new, it will return
     * an empty collection; or if this Ville has previously
     * been saved, it will retrieve related Personnes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Ville.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPersonne[] List of ChildPersonne objects
     */
    public function getPersonnesJoinSociete(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPersonneQuery::create(null, $criteria);
        $query->joinWith('Societe', $joinBehavior);

        return $this->getPersonnes($query, $con);
    }

    /**
     * Clears out the collSocietes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSocietes()
     */
    public function clearSocietes()
    {
        $this->collSocietes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSocietes collection loaded partially.
     */
    public function resetPartialSocietes($v = true)
    {
        $this->collSocietesPartial = $v;
    }

    /**
     * Initializes the collSocietes collection.
     *
     * By default this just sets the collSocietes collection to an empty array (like clearcollSocietes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSocietes($overrideExisting = true)
    {
        if (null !== $this->collSocietes && !$overrideExisting) {
            return;
        }

        $collectionClassName = SocieteTableMap::getTableMap()->getCollectionClassName();

        $this->collSocietes = new $collectionClassName;
        $this->collSocietes->setModel('\Societe');
    }

    /**
     * Gets an array of ChildSociete objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildVille is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSociete[] List of ChildSociete objects
     * @throws PropelException
     */
    public function getSocietes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSocietesPartial && !$this->isNew();
        if (null === $this->collSocietes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collSocietes) {
                // return empty collection
                $this->initSocietes();
            } else {
                $collSocietes = ChildSocieteQuery::create(null, $criteria)
                    ->filterByVille($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSocietesPartial && count($collSocietes)) {
                        $this->initSocietes(false);

                        foreach ($collSocietes as $obj) {
                            if (false == $this->collSocietes->contains($obj)) {
                                $this->collSocietes->append($obj);
                            }
                        }

                        $this->collSocietesPartial = true;
                    }

                    return $collSocietes;
                }

                if ($partial && $this->collSocietes) {
                    foreach ($this->collSocietes as $obj) {
                        if ($obj->isNew()) {
                            $collSocietes[] = $obj;
                        }
                    }
                }

                $this->collSocietes = $collSocietes;
                $this->collSocietesPartial = false;
            }
        }

        return $this->collSocietes;
    }

    /**
     * Sets a collection of ChildSociete objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $societes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildVille The current object (for fluent API support)
     */
    public function setSocietes(Collection $societes, ConnectionInterface $con = null)
    {
        /** @var ChildSociete[] $societesToDelete */
        $societesToDelete = $this->getSocietes(new Criteria(), $con)->diff($societes);


        $this->societesScheduledForDeletion = $societesToDelete;

        foreach ($societesToDelete as $societeRemoved) {
            $societeRemoved->setVille(null);
        }

        $this->collSocietes = null;
        foreach ($societes as $societe) {
            $this->addSociete($societe);
        }

        $this->collSocietes = $societes;
        $this->collSocietesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Societe objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Societe objects.
     * @throws PropelException
     */
    public function countSocietes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSocietesPartial && !$this->isNew();
        if (null === $this->collSocietes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSocietes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSocietes());
            }

            $query = ChildSocieteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByVille($this)
                ->count($con);
        }

        return count($this->collSocietes);
    }

    /**
     * Method called to associate a ChildSociete object to this object
     * through the ChildSociete foreign key attribute.
     *
     * @param  ChildSociete $l ChildSociete
     * @return $this|\Ville The current object (for fluent API support)
     */
    public function addSociete(ChildSociete $l)
    {
        if ($this->collSocietes === null) {
            $this->initSocietes();
            $this->collSocietesPartial = true;
        }

        if (!$this->collSocietes->contains($l)) {
            $this->doAddSociete($l);

            if ($this->societesScheduledForDeletion and $this->societesScheduledForDeletion->contains($l)) {
                $this->societesScheduledForDeletion->remove($this->societesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSociete $societe The ChildSociete object to add.
     */
    protected function doAddSociete(ChildSociete $societe)
    {
        $this->collSocietes[]= $societe;
        $societe->setVille($this);
    }

    /**
     * @param  ChildSociete $societe The ChildSociete object to remove.
     * @return $this|ChildVille The current object (for fluent API support)
     */
    public function removeSociete(ChildSociete $societe)
    {
        if ($this->getSocietes()->contains($societe)) {
            $pos = $this->collSocietes->search($societe);
            $this->collSocietes->remove($pos);
            if (null === $this->societesScheduledForDeletion) {
                $this->societesScheduledForDeletion = clone $this->collSocietes;
                $this->societesScheduledForDeletion->clear();
            }
            $this->societesScheduledForDeletion[]= clone $societe;
            $societe->setVille(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPays) {
            $this->aPays->removeVille($this);
        }
        $this->idville = null;
        $this->libelleville = null;
        $this->pays_idpays = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCommunes) {
                foreach ($this->collCommunes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPersonnes) {
                foreach ($this->collPersonnes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSocietes) {
                foreach ($this->collSocietes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCommunes = null;
        $this->collPersonnes = null;
        $this->collSocietes = null;
        $this->aPays = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(VilleTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
