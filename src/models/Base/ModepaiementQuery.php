<?php

namespace Base;

use \Modepaiement as ChildModepaiement;
use \ModepaiementQuery as ChildModepaiementQuery;
use \Exception;
use \PDO;
use Map\ModepaiementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'modepaiement' table.
 *
 *
 *
 * @method     ChildModepaiementQuery orderByIdmodepaiement($order = Criteria::ASC) Order by the idmodePaiement column
 * @method     ChildModepaiementQuery orderByLibellemodepaiement($order = Criteria::ASC) Order by the LibellemodePaiement column
 * @method     ChildModepaiementQuery orderByBanqueIdbanque($order = Criteria::ASC) Order by the Banque_idBanque column
 * @method     ChildModepaiementQuery orderBySocieteIdsociete($order = Criteria::ASC) Order by the societe_idsociete column
 *
 * @method     ChildModepaiementQuery groupByIdmodepaiement() Group by the idmodePaiement column
 * @method     ChildModepaiementQuery groupByLibellemodepaiement() Group by the LibellemodePaiement column
 * @method     ChildModepaiementQuery groupByBanqueIdbanque() Group by the Banque_idBanque column
 * @method     ChildModepaiementQuery groupBySocieteIdsociete() Group by the societe_idsociete column
 *
 * @method     ChildModepaiementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildModepaiementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildModepaiementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildModepaiementQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildModepaiementQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildModepaiementQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildModepaiementQuery leftJoinBanque($relationAlias = null) Adds a LEFT JOIN clause to the query using the Banque relation
 * @method     ChildModepaiementQuery rightJoinBanque($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Banque relation
 * @method     ChildModepaiementQuery innerJoinBanque($relationAlias = null) Adds a INNER JOIN clause to the query using the Banque relation
 *
 * @method     ChildModepaiementQuery joinWithBanque($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Banque relation
 *
 * @method     ChildModepaiementQuery leftJoinWithBanque() Adds a LEFT JOIN clause and with to the query using the Banque relation
 * @method     ChildModepaiementQuery rightJoinWithBanque() Adds a RIGHT JOIN clause and with to the query using the Banque relation
 * @method     ChildModepaiementQuery innerJoinWithBanque() Adds a INNER JOIN clause and with to the query using the Banque relation
 *
 * @method     ChildModepaiementQuery leftJoinSociete($relationAlias = null) Adds a LEFT JOIN clause to the query using the Societe relation
 * @method     ChildModepaiementQuery rightJoinSociete($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Societe relation
 * @method     ChildModepaiementQuery innerJoinSociete($relationAlias = null) Adds a INNER JOIN clause to the query using the Societe relation
 *
 * @method     ChildModepaiementQuery joinWithSociete($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Societe relation
 *
 * @method     ChildModepaiementQuery leftJoinWithSociete() Adds a LEFT JOIN clause and with to the query using the Societe relation
 * @method     ChildModepaiementQuery rightJoinWithSociete() Adds a RIGHT JOIN clause and with to the query using the Societe relation
 * @method     ChildModepaiementQuery innerJoinWithSociete() Adds a INNER JOIN clause and with to the query using the Societe relation
 *
 * @method     ChildModepaiementQuery leftJoinVersement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Versement relation
 * @method     ChildModepaiementQuery rightJoinVersement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Versement relation
 * @method     ChildModepaiementQuery innerJoinVersement($relationAlias = null) Adds a INNER JOIN clause to the query using the Versement relation
 *
 * @method     ChildModepaiementQuery joinWithVersement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Versement relation
 *
 * @method     ChildModepaiementQuery leftJoinWithVersement() Adds a LEFT JOIN clause and with to the query using the Versement relation
 * @method     ChildModepaiementQuery rightJoinWithVersement() Adds a RIGHT JOIN clause and with to the query using the Versement relation
 * @method     ChildModepaiementQuery innerJoinWithVersement() Adds a INNER JOIN clause and with to the query using the Versement relation
 *
 * @method     \BanqueQuery|\SocieteQuery|\VersementQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildModepaiement findOne(ConnectionInterface $con = null) Return the first ChildModepaiement matching the query
 * @method     ChildModepaiement findOneOrCreate(ConnectionInterface $con = null) Return the first ChildModepaiement matching the query, or a new ChildModepaiement object populated from the query conditions when no match is found
 *
 * @method     ChildModepaiement findOneByIdmodepaiement(int $idmodePaiement) Return the first ChildModepaiement filtered by the idmodePaiement column
 * @method     ChildModepaiement findOneByLibellemodepaiement(string $LibellemodePaiement) Return the first ChildModepaiement filtered by the LibellemodePaiement column
 * @method     ChildModepaiement findOneByBanqueIdbanque(int $Banque_idBanque) Return the first ChildModepaiement filtered by the Banque_idBanque column
 * @method     ChildModepaiement findOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildModepaiement filtered by the societe_idsociete column *

 * @method     ChildModepaiement requirePk($key, ConnectionInterface $con = null) Return the ChildModepaiement by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModepaiement requireOne(ConnectionInterface $con = null) Return the first ChildModepaiement matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildModepaiement requireOneByIdmodepaiement(int $idmodePaiement) Return the first ChildModepaiement filtered by the idmodePaiement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModepaiement requireOneByLibellemodepaiement(string $LibellemodePaiement) Return the first ChildModepaiement filtered by the LibellemodePaiement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModepaiement requireOneByBanqueIdbanque(int $Banque_idBanque) Return the first ChildModepaiement filtered by the Banque_idBanque column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModepaiement requireOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildModepaiement filtered by the societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildModepaiement[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildModepaiement objects based on current ModelCriteria
 * @method     ChildModepaiement[]|ObjectCollection findByIdmodepaiement(int $idmodePaiement) Return ChildModepaiement objects filtered by the idmodePaiement column
 * @method     ChildModepaiement[]|ObjectCollection findByLibellemodepaiement(string $LibellemodePaiement) Return ChildModepaiement objects filtered by the LibellemodePaiement column
 * @method     ChildModepaiement[]|ObjectCollection findByBanqueIdbanque(int $Banque_idBanque) Return ChildModepaiement objects filtered by the Banque_idBanque column
 * @method     ChildModepaiement[]|ObjectCollection findBySocieteIdsociete(int $societe_idsociete) Return ChildModepaiement objects filtered by the societe_idsociete column
 * @method     ChildModepaiement[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ModepaiementQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ModepaiementQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Modepaiement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildModepaiementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildModepaiementQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildModepaiementQuery) {
            return $criteria;
        }
        $query = new ChildModepaiementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildModepaiement|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ModepaiementTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ModepaiementTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildModepaiement A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idmodePaiement, LibellemodePaiement, Banque_idBanque, societe_idsociete FROM modepaiement WHERE idmodePaiement = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildModepaiement $obj */
            $obj = new ChildModepaiement();
            $obj->hydrate($row);
            ModepaiementTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildModepaiement|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ModepaiementTableMap::COL_IDMODEPAIEMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ModepaiementTableMap::COL_IDMODEPAIEMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idmodePaiement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdmodepaiement(1234); // WHERE idmodePaiement = 1234
     * $query->filterByIdmodepaiement(array(12, 34)); // WHERE idmodePaiement IN (12, 34)
     * $query->filterByIdmodepaiement(array('min' => 12)); // WHERE idmodePaiement > 12
     * </code>
     *
     * @param     mixed $idmodepaiement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterByIdmodepaiement($idmodepaiement = null, $comparison = null)
    {
        if (is_array($idmodepaiement)) {
            $useMinMax = false;
            if (isset($idmodepaiement['min'])) {
                $this->addUsingAlias(ModepaiementTableMap::COL_IDMODEPAIEMENT, $idmodepaiement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idmodepaiement['max'])) {
                $this->addUsingAlias(ModepaiementTableMap::COL_IDMODEPAIEMENT, $idmodepaiement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModepaiementTableMap::COL_IDMODEPAIEMENT, $idmodepaiement, $comparison);
    }

    /**
     * Filter the query on the LibellemodePaiement column
     *
     * Example usage:
     * <code>
     * $query->filterByLibellemodepaiement('fooValue');   // WHERE LibellemodePaiement = 'fooValue'
     * $query->filterByLibellemodepaiement('%fooValue%', Criteria::LIKE); // WHERE LibellemodePaiement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libellemodepaiement The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterByLibellemodepaiement($libellemodepaiement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libellemodepaiement)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModepaiementTableMap::COL_LIBELLEMODEPAIEMENT, $libellemodepaiement, $comparison);
    }

    /**
     * Filter the query on the Banque_idBanque column
     *
     * Example usage:
     * <code>
     * $query->filterByBanqueIdbanque(1234); // WHERE Banque_idBanque = 1234
     * $query->filterByBanqueIdbanque(array(12, 34)); // WHERE Banque_idBanque IN (12, 34)
     * $query->filterByBanqueIdbanque(array('min' => 12)); // WHERE Banque_idBanque > 12
     * </code>
     *
     * @see       filterByBanque()
     *
     * @param     mixed $banqueIdbanque The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterByBanqueIdbanque($banqueIdbanque = null, $comparison = null)
    {
        if (is_array($banqueIdbanque)) {
            $useMinMax = false;
            if (isset($banqueIdbanque['min'])) {
                $this->addUsingAlias(ModepaiementTableMap::COL_BANQUE_IDBANQUE, $banqueIdbanque['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($banqueIdbanque['max'])) {
                $this->addUsingAlias(ModepaiementTableMap::COL_BANQUE_IDBANQUE, $banqueIdbanque['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModepaiementTableMap::COL_BANQUE_IDBANQUE, $banqueIdbanque, $comparison);
    }

    /**
     * Filter the query on the societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterBySocieteIdsociete(1234); // WHERE societe_idsociete = 1234
     * $query->filterBySocieteIdsociete(array(12, 34)); // WHERE societe_idsociete IN (12, 34)
     * $query->filterBySocieteIdsociete(array('min' => 12)); // WHERE societe_idsociete > 12
     * </code>
     *
     * @see       filterBySociete()
     *
     * @param     mixed $societeIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterBySocieteIdsociete($societeIdsociete = null, $comparison = null)
    {
        if (is_array($societeIdsociete)) {
            $useMinMax = false;
            if (isset($societeIdsociete['min'])) {
                $this->addUsingAlias(ModepaiementTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($societeIdsociete['max'])) {
                $this->addUsingAlias(ModepaiementTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModepaiementTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete, $comparison);
    }

    /**
     * Filter the query by a related \Banque object
     *
     * @param \Banque|ObjectCollection $banque The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterByBanque($banque, $comparison = null)
    {
        if ($banque instanceof \Banque) {
            return $this
                ->addUsingAlias(ModepaiementTableMap::COL_BANQUE_IDBANQUE, $banque->getIdbanque(), $comparison);
        } elseif ($banque instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ModepaiementTableMap::COL_BANQUE_IDBANQUE, $banque->toKeyValue('PrimaryKey', 'Idbanque'), $comparison);
        } else {
            throw new PropelException('filterByBanque() only accepts arguments of type \Banque or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Banque relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function joinBanque($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Banque');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Banque');
        }

        return $this;
    }

    /**
     * Use the Banque relation Banque object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BanqueQuery A secondary query class using the current class as primary query
     */
    public function useBanqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBanque($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Banque', '\BanqueQuery');
    }

    /**
     * Filter the query by a related \Societe object
     *
     * @param \Societe|ObjectCollection $societe The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterBySociete($societe, $comparison = null)
    {
        if ($societe instanceof \Societe) {
            return $this
                ->addUsingAlias(ModepaiementTableMap::COL_SOCIETE_IDSOCIETE, $societe->getIdsociete(), $comparison);
        } elseif ($societe instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ModepaiementTableMap::COL_SOCIETE_IDSOCIETE, $societe->toKeyValue('PrimaryKey', 'Idsociete'), $comparison);
        } else {
            throw new PropelException('filterBySociete() only accepts arguments of type \Societe or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Societe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function joinSociete($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Societe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Societe');
        }

        return $this;
    }

    /**
     * Use the Societe relation Societe object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SocieteQuery A secondary query class using the current class as primary query
     */
    public function useSocieteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSociete($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Societe', '\SocieteQuery');
    }

    /**
     * Filter the query by a related \Versement object
     *
     * @param \Versement|ObjectCollection $versement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildModepaiementQuery The current query, for fluid interface
     */
    public function filterByVersement($versement, $comparison = null)
    {
        if ($versement instanceof \Versement) {
            return $this
                ->addUsingAlias(ModepaiementTableMap::COL_IDMODEPAIEMENT, $versement->getModepaiementIdmodepaiement(), $comparison);
        } elseif ($versement instanceof ObjectCollection) {
            return $this
                ->useVersementQuery()
                ->filterByPrimaryKeys($versement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVersement() only accepts arguments of type \Versement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Versement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function joinVersement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Versement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Versement');
        }

        return $this;
    }

    /**
     * Use the Versement relation Versement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VersementQuery A secondary query class using the current class as primary query
     */
    public function useVersementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVersement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Versement', '\VersementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildModepaiement $modepaiement Object to remove from the list of results
     *
     * @return $this|ChildModepaiementQuery The current query, for fluid interface
     */
    public function prune($modepaiement = null)
    {
        if ($modepaiement) {
            $this->addUsingAlias(ModepaiementTableMap::COL_IDMODEPAIEMENT, $modepaiement->getIdmodepaiement(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the modepaiement table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ModepaiementTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ModepaiementTableMap::clearInstancePool();
            ModepaiementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ModepaiementTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ModepaiementTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ModepaiementTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ModepaiementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ModepaiementQuery
