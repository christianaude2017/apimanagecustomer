<?php

namespace Base;

use \Ville as ChildVille;
use \VilleQuery as ChildVilleQuery;
use \Exception;
use \PDO;
use Map\VilleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ville' table.
 *
 *
 *
 * @method     ChildVilleQuery orderByIdville($order = Criteria::ASC) Order by the idville column
 * @method     ChildVilleQuery orderByLibelleville($order = Criteria::ASC) Order by the Libelleville column
 * @method     ChildVilleQuery orderByPaysIdpays($order = Criteria::ASC) Order by the pays_idpays column
 *
 * @method     ChildVilleQuery groupByIdville() Group by the idville column
 * @method     ChildVilleQuery groupByLibelleville() Group by the Libelleville column
 * @method     ChildVilleQuery groupByPaysIdpays() Group by the pays_idpays column
 *
 * @method     ChildVilleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVilleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVilleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVilleQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVilleQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVilleQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVilleQuery leftJoinPays($relationAlias = null) Adds a LEFT JOIN clause to the query using the Pays relation
 * @method     ChildVilleQuery rightJoinPays($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Pays relation
 * @method     ChildVilleQuery innerJoinPays($relationAlias = null) Adds a INNER JOIN clause to the query using the Pays relation
 *
 * @method     ChildVilleQuery joinWithPays($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Pays relation
 *
 * @method     ChildVilleQuery leftJoinWithPays() Adds a LEFT JOIN clause and with to the query using the Pays relation
 * @method     ChildVilleQuery rightJoinWithPays() Adds a RIGHT JOIN clause and with to the query using the Pays relation
 * @method     ChildVilleQuery innerJoinWithPays() Adds a INNER JOIN clause and with to the query using the Pays relation
 *
 * @method     ChildVilleQuery leftJoinCommune($relationAlias = null) Adds a LEFT JOIN clause to the query using the Commune relation
 * @method     ChildVilleQuery rightJoinCommune($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Commune relation
 * @method     ChildVilleQuery innerJoinCommune($relationAlias = null) Adds a INNER JOIN clause to the query using the Commune relation
 *
 * @method     ChildVilleQuery joinWithCommune($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Commune relation
 *
 * @method     ChildVilleQuery leftJoinWithCommune() Adds a LEFT JOIN clause and with to the query using the Commune relation
 * @method     ChildVilleQuery rightJoinWithCommune() Adds a RIGHT JOIN clause and with to the query using the Commune relation
 * @method     ChildVilleQuery innerJoinWithCommune() Adds a INNER JOIN clause and with to the query using the Commune relation
 *
 * @method     ChildVilleQuery leftJoinPersonne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Personne relation
 * @method     ChildVilleQuery rightJoinPersonne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Personne relation
 * @method     ChildVilleQuery innerJoinPersonne($relationAlias = null) Adds a INNER JOIN clause to the query using the Personne relation
 *
 * @method     ChildVilleQuery joinWithPersonne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Personne relation
 *
 * @method     ChildVilleQuery leftJoinWithPersonne() Adds a LEFT JOIN clause and with to the query using the Personne relation
 * @method     ChildVilleQuery rightJoinWithPersonne() Adds a RIGHT JOIN clause and with to the query using the Personne relation
 * @method     ChildVilleQuery innerJoinWithPersonne() Adds a INNER JOIN clause and with to the query using the Personne relation
 *
 * @method     ChildVilleQuery leftJoinSociete($relationAlias = null) Adds a LEFT JOIN clause to the query using the Societe relation
 * @method     ChildVilleQuery rightJoinSociete($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Societe relation
 * @method     ChildVilleQuery innerJoinSociete($relationAlias = null) Adds a INNER JOIN clause to the query using the Societe relation
 *
 * @method     ChildVilleQuery joinWithSociete($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Societe relation
 *
 * @method     ChildVilleQuery leftJoinWithSociete() Adds a LEFT JOIN clause and with to the query using the Societe relation
 * @method     ChildVilleQuery rightJoinWithSociete() Adds a RIGHT JOIN clause and with to the query using the Societe relation
 * @method     ChildVilleQuery innerJoinWithSociete() Adds a INNER JOIN clause and with to the query using the Societe relation
 *
 * @method     \PaysQuery|\CommuneQuery|\PersonneQuery|\SocieteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVille findOne(ConnectionInterface $con = null) Return the first ChildVille matching the query
 * @method     ChildVille findOneOrCreate(ConnectionInterface $con = null) Return the first ChildVille matching the query, or a new ChildVille object populated from the query conditions when no match is found
 *
 * @method     ChildVille findOneByIdville(int $idville) Return the first ChildVille filtered by the idville column
 * @method     ChildVille findOneByLibelleville(string $Libelleville) Return the first ChildVille filtered by the Libelleville column
 * @method     ChildVille findOneByPaysIdpays(int $pays_idpays) Return the first ChildVille filtered by the pays_idpays column *

 * @method     ChildVille requirePk($key, ConnectionInterface $con = null) Return the ChildVille by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVille requireOne(ConnectionInterface $con = null) Return the first ChildVille matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVille requireOneByIdville(int $idville) Return the first ChildVille filtered by the idville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVille requireOneByLibelleville(string $Libelleville) Return the first ChildVille filtered by the Libelleville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVille requireOneByPaysIdpays(int $pays_idpays) Return the first ChildVille filtered by the pays_idpays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVille[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildVille objects based on current ModelCriteria
 * @method     ChildVille[]|ObjectCollection findByIdville(int $idville) Return ChildVille objects filtered by the idville column
 * @method     ChildVille[]|ObjectCollection findByLibelleville(string $Libelleville) Return ChildVille objects filtered by the Libelleville column
 * @method     ChildVille[]|ObjectCollection findByPaysIdpays(int $pays_idpays) Return ChildVille objects filtered by the pays_idpays column
 * @method     ChildVille[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VilleQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VilleQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Ville', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVilleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVilleQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildVilleQuery) {
            return $criteria;
        }
        $query = new ChildVilleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array[$idville, $pays_idpays] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVille|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VilleTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VilleTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVille A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idville, Libelleville, pays_idpays FROM ville WHERE idville = :p0 AND pays_idpays = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVille $obj */
            $obj = new ChildVille();
            $obj->hydrate($row);
            VilleTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildVille|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(VilleTableMap::COL_IDVILLE, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(VilleTableMap::COL_IDVILLE, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(VilleTableMap::COL_PAYS_IDPAYS, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idville column
     *
     * Example usage:
     * <code>
     * $query->filterByIdville(1234); // WHERE idville = 1234
     * $query->filterByIdville(array(12, 34)); // WHERE idville IN (12, 34)
     * $query->filterByIdville(array('min' => 12)); // WHERE idville > 12
     * </code>
     *
     * @param     mixed $idville The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function filterByIdville($idville = null, $comparison = null)
    {
        if (is_array($idville)) {
            $useMinMax = false;
            if (isset($idville['min'])) {
                $this->addUsingAlias(VilleTableMap::COL_IDVILLE, $idville['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idville['max'])) {
                $this->addUsingAlias(VilleTableMap::COL_IDVILLE, $idville['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VilleTableMap::COL_IDVILLE, $idville, $comparison);
    }

    /**
     * Filter the query on the Libelleville column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleville('fooValue');   // WHERE Libelleville = 'fooValue'
     * $query->filterByLibelleville('%fooValue%', Criteria::LIKE); // WHERE Libelleville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleville The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function filterByLibelleville($libelleville = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleville)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VilleTableMap::COL_LIBELLEVILLE, $libelleville, $comparison);
    }

    /**
     * Filter the query on the pays_idpays column
     *
     * Example usage:
     * <code>
     * $query->filterByPaysIdpays(1234); // WHERE pays_idpays = 1234
     * $query->filterByPaysIdpays(array(12, 34)); // WHERE pays_idpays IN (12, 34)
     * $query->filterByPaysIdpays(array('min' => 12)); // WHERE pays_idpays > 12
     * </code>
     *
     * @see       filterByPays()
     *
     * @param     mixed $paysIdpays The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function filterByPaysIdpays($paysIdpays = null, $comparison = null)
    {
        if (is_array($paysIdpays)) {
            $useMinMax = false;
            if (isset($paysIdpays['min'])) {
                $this->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $paysIdpays['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paysIdpays['max'])) {
                $this->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $paysIdpays['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $paysIdpays, $comparison);
    }

    /**
     * Filter the query by a related \Pays object
     *
     * @param \Pays|ObjectCollection $pays The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVilleQuery The current query, for fluid interface
     */
    public function filterByPays($pays, $comparison = null)
    {
        if ($pays instanceof \Pays) {
            return $this
                ->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $pays->getIdpays(), $comparison);
        } elseif ($pays instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $pays->toKeyValue('PrimaryKey', 'Idpays'), $comparison);
        } else {
            throw new PropelException('filterByPays() only accepts arguments of type \Pays or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Pays relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function joinPays($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Pays');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Pays');
        }

        return $this;
    }

    /**
     * Use the Pays relation Pays object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PaysQuery A secondary query class using the current class as primary query
     */
    public function usePaysQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPays($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Pays', '\PaysQuery');
    }

    /**
     * Filter the query by a related \Commune object
     *
     * @param \Commune|ObjectCollection $commune the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildVilleQuery The current query, for fluid interface
     */
    public function filterByCommune($commune, $comparison = null)
    {
        if ($commune instanceof \Commune) {
            return $this
                ->addUsingAlias(VilleTableMap::COL_IDVILLE, $commune->getVilleIdville(), $comparison)
                ->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $commune->getVillePaysIdpays(), $comparison);
        } else {
            throw new PropelException('filterByCommune() only accepts arguments of type \Commune');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Commune relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function joinCommune($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Commune');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Commune');
        }

        return $this;
    }

    /**
     * Use the Commune relation Commune object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CommuneQuery A secondary query class using the current class as primary query
     */
    public function useCommuneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommune($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Commune', '\CommuneQuery');
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne|ObjectCollection $personne the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildVilleQuery The current query, for fluid interface
     */
    public function filterByPersonne($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(VilleTableMap::COL_IDVILLE, $personne->getVilleIdville(), $comparison)
                ->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $personne->getVillePaysIdpays(), $comparison);
        } else {
            throw new PropelException('filterByPersonne() only accepts arguments of type \Personne');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Personne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function joinPersonne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Personne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Personne');
        }

        return $this;
    }

    /**
     * Use the Personne relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Personne', '\PersonneQuery');
    }

    /**
     * Filter the query by a related \Societe object
     *
     * @param \Societe|ObjectCollection $societe the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildVilleQuery The current query, for fluid interface
     */
    public function filterBySociete($societe, $comparison = null)
    {
        if ($societe instanceof \Societe) {
            return $this
                ->addUsingAlias(VilleTableMap::COL_IDVILLE, $societe->getVilleIdville(), $comparison)
                ->addUsingAlias(VilleTableMap::COL_PAYS_IDPAYS, $societe->getVillePaysIdpays(), $comparison);
        } else {
            throw new PropelException('filterBySociete() only accepts arguments of type \Societe');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Societe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function joinSociete($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Societe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Societe');
        }

        return $this;
    }

    /**
     * Use the Societe relation Societe object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SocieteQuery A secondary query class using the current class as primary query
     */
    public function useSocieteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSociete($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Societe', '\SocieteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildVille $ville Object to remove from the list of results
     *
     * @return $this|ChildVilleQuery The current query, for fluid interface
     */
    public function prune($ville = null)
    {
        if ($ville) {
            $this->addCond('pruneCond0', $this->getAliasedColName(VilleTableMap::COL_IDVILLE), $ville->getIdville(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(VilleTableMap::COL_PAYS_IDPAYS), $ville->getPaysIdpays(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the ville table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VilleTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VilleTableMap::clearInstancePool();
            VilleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VilleTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VilleTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VilleTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VilleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // VilleQuery
