<?php

namespace Base;

use \Catgorieproduit as ChildCatgorieproduit;
use \CatgorieproduitQuery as ChildCatgorieproduitQuery;
use \Exception;
use \PDO;
use Map\CatgorieproduitTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'catgorieproduit' table.
 *
 *
 *
 * @method     ChildCatgorieproduitQuery orderByIdcatgorieproduit($order = Criteria::ASC) Order by the idcatgorieproduit column
 * @method     ChildCatgorieproduitQuery orderByLibellecatgorieproduit($order = Criteria::ASC) Order by the Libellecatgorieproduit column
 * @method     ChildCatgorieproduitQuery orderBySocieteIdsociete($order = Criteria::ASC) Order by the societe_idsociete column
 *
 * @method     ChildCatgorieproduitQuery groupByIdcatgorieproduit() Group by the idcatgorieproduit column
 * @method     ChildCatgorieproduitQuery groupByLibellecatgorieproduit() Group by the Libellecatgorieproduit column
 * @method     ChildCatgorieproduitQuery groupBySocieteIdsociete() Group by the societe_idsociete column
 *
 * @method     ChildCatgorieproduitQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCatgorieproduitQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCatgorieproduitQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCatgorieproduitQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCatgorieproduitQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCatgorieproduitQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCatgorieproduitQuery leftJoinSociete($relationAlias = null) Adds a LEFT JOIN clause to the query using the Societe relation
 * @method     ChildCatgorieproduitQuery rightJoinSociete($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Societe relation
 * @method     ChildCatgorieproduitQuery innerJoinSociete($relationAlias = null) Adds a INNER JOIN clause to the query using the Societe relation
 *
 * @method     ChildCatgorieproduitQuery joinWithSociete($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Societe relation
 *
 * @method     ChildCatgorieproduitQuery leftJoinWithSociete() Adds a LEFT JOIN clause and with to the query using the Societe relation
 * @method     ChildCatgorieproduitQuery rightJoinWithSociete() Adds a RIGHT JOIN clause and with to the query using the Societe relation
 * @method     ChildCatgorieproduitQuery innerJoinWithSociete() Adds a INNER JOIN clause and with to the query using the Societe relation
 *
 * @method     ChildCatgorieproduitQuery leftJoinProduits($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produits relation
 * @method     ChildCatgorieproduitQuery rightJoinProduits($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produits relation
 * @method     ChildCatgorieproduitQuery innerJoinProduits($relationAlias = null) Adds a INNER JOIN clause to the query using the Produits relation
 *
 * @method     ChildCatgorieproduitQuery joinWithProduits($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produits relation
 *
 * @method     ChildCatgorieproduitQuery leftJoinWithProduits() Adds a LEFT JOIN clause and with to the query using the Produits relation
 * @method     ChildCatgorieproduitQuery rightJoinWithProduits() Adds a RIGHT JOIN clause and with to the query using the Produits relation
 * @method     ChildCatgorieproduitQuery innerJoinWithProduits() Adds a INNER JOIN clause and with to the query using the Produits relation
 *
 * @method     \SocieteQuery|\ProduitsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCatgorieproduit findOne(ConnectionInterface $con = null) Return the first ChildCatgorieproduit matching the query
 * @method     ChildCatgorieproduit findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCatgorieproduit matching the query, or a new ChildCatgorieproduit object populated from the query conditions when no match is found
 *
 * @method     ChildCatgorieproduit findOneByIdcatgorieproduit(int $idcatgorieproduit) Return the first ChildCatgorieproduit filtered by the idcatgorieproduit column
 * @method     ChildCatgorieproduit findOneByLibellecatgorieproduit(string $Libellecatgorieproduit) Return the first ChildCatgorieproduit filtered by the Libellecatgorieproduit column
 * @method     ChildCatgorieproduit findOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildCatgorieproduit filtered by the societe_idsociete column *

 * @method     ChildCatgorieproduit requirePk($key, ConnectionInterface $con = null) Return the ChildCatgorieproduit by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCatgorieproduit requireOne(ConnectionInterface $con = null) Return the first ChildCatgorieproduit matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCatgorieproduit requireOneByIdcatgorieproduit(int $idcatgorieproduit) Return the first ChildCatgorieproduit filtered by the idcatgorieproduit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCatgorieproduit requireOneByLibellecatgorieproduit(string $Libellecatgorieproduit) Return the first ChildCatgorieproduit filtered by the Libellecatgorieproduit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCatgorieproduit requireOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildCatgorieproduit filtered by the societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCatgorieproduit[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCatgorieproduit objects based on current ModelCriteria
 * @method     ChildCatgorieproduit[]|ObjectCollection findByIdcatgorieproduit(int $idcatgorieproduit) Return ChildCatgorieproduit objects filtered by the idcatgorieproduit column
 * @method     ChildCatgorieproduit[]|ObjectCollection findByLibellecatgorieproduit(string $Libellecatgorieproduit) Return ChildCatgorieproduit objects filtered by the Libellecatgorieproduit column
 * @method     ChildCatgorieproduit[]|ObjectCollection findBySocieteIdsociete(int $societe_idsociete) Return ChildCatgorieproduit objects filtered by the societe_idsociete column
 * @method     ChildCatgorieproduit[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CatgorieproduitQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CatgorieproduitQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Catgorieproduit', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCatgorieproduitQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCatgorieproduitQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCatgorieproduitQuery) {
            return $criteria;
        }
        $query = new ChildCatgorieproduitQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCatgorieproduit|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CatgorieproduitTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CatgorieproduitTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCatgorieproduit A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcatgorieproduit, Libellecatgorieproduit, societe_idsociete FROM catgorieproduit WHERE idcatgorieproduit = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCatgorieproduit $obj */
            $obj = new ChildCatgorieproduit();
            $obj->hydrate($row);
            CatgorieproduitTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCatgorieproduit|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CatgorieproduitTableMap::COL_IDCATGORIEPRODUIT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CatgorieproduitTableMap::COL_IDCATGORIEPRODUIT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcatgorieproduit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcatgorieproduit(1234); // WHERE idcatgorieproduit = 1234
     * $query->filterByIdcatgorieproduit(array(12, 34)); // WHERE idcatgorieproduit IN (12, 34)
     * $query->filterByIdcatgorieproduit(array('min' => 12)); // WHERE idcatgorieproduit > 12
     * </code>
     *
     * @param     mixed $idcatgorieproduit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function filterByIdcatgorieproduit($idcatgorieproduit = null, $comparison = null)
    {
        if (is_array($idcatgorieproduit)) {
            $useMinMax = false;
            if (isset($idcatgorieproduit['min'])) {
                $this->addUsingAlias(CatgorieproduitTableMap::COL_IDCATGORIEPRODUIT, $idcatgorieproduit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcatgorieproduit['max'])) {
                $this->addUsingAlias(CatgorieproduitTableMap::COL_IDCATGORIEPRODUIT, $idcatgorieproduit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CatgorieproduitTableMap::COL_IDCATGORIEPRODUIT, $idcatgorieproduit, $comparison);
    }

    /**
     * Filter the query on the Libellecatgorieproduit column
     *
     * Example usage:
     * <code>
     * $query->filterByLibellecatgorieproduit('fooValue');   // WHERE Libellecatgorieproduit = 'fooValue'
     * $query->filterByLibellecatgorieproduit('%fooValue%', Criteria::LIKE); // WHERE Libellecatgorieproduit LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libellecatgorieproduit The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function filterByLibellecatgorieproduit($libellecatgorieproduit = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libellecatgorieproduit)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CatgorieproduitTableMap::COL_LIBELLECATGORIEPRODUIT, $libellecatgorieproduit, $comparison);
    }

    /**
     * Filter the query on the societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterBySocieteIdsociete(1234); // WHERE societe_idsociete = 1234
     * $query->filterBySocieteIdsociete(array(12, 34)); // WHERE societe_idsociete IN (12, 34)
     * $query->filterBySocieteIdsociete(array('min' => 12)); // WHERE societe_idsociete > 12
     * </code>
     *
     * @see       filterBySociete()
     *
     * @param     mixed $societeIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function filterBySocieteIdsociete($societeIdsociete = null, $comparison = null)
    {
        if (is_array($societeIdsociete)) {
            $useMinMax = false;
            if (isset($societeIdsociete['min'])) {
                $this->addUsingAlias(CatgorieproduitTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($societeIdsociete['max'])) {
                $this->addUsingAlias(CatgorieproduitTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CatgorieproduitTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete, $comparison);
    }

    /**
     * Filter the query by a related \Societe object
     *
     * @param \Societe|ObjectCollection $societe The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function filterBySociete($societe, $comparison = null)
    {
        if ($societe instanceof \Societe) {
            return $this
                ->addUsingAlias(CatgorieproduitTableMap::COL_SOCIETE_IDSOCIETE, $societe->getIdsociete(), $comparison);
        } elseif ($societe instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CatgorieproduitTableMap::COL_SOCIETE_IDSOCIETE, $societe->toKeyValue('PrimaryKey', 'Idsociete'), $comparison);
        } else {
            throw new PropelException('filterBySociete() only accepts arguments of type \Societe or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Societe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function joinSociete($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Societe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Societe');
        }

        return $this;
    }

    /**
     * Use the Societe relation Societe object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SocieteQuery A secondary query class using the current class as primary query
     */
    public function useSocieteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSociete($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Societe', '\SocieteQuery');
    }

    /**
     * Filter the query by a related \Produits object
     *
     * @param \Produits|ObjectCollection $produits the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function filterByProduits($produits, $comparison = null)
    {
        if ($produits instanceof \Produits) {
            return $this
                ->addUsingAlias(CatgorieproduitTableMap::COL_IDCATGORIEPRODUIT, $produits->getIdcatgorieproduit(), $comparison);
        } elseif ($produits instanceof ObjectCollection) {
            return $this
                ->useProduitsQuery()
                ->filterByPrimaryKeys($produits->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProduits() only accepts arguments of type \Produits or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produits relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function joinProduits($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produits');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produits');
        }

        return $this;
    }

    /**
     * Use the Produits relation Produits object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProduitsQuery A secondary query class using the current class as primary query
     */
    public function useProduitsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduits($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produits', '\ProduitsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCatgorieproduit $catgorieproduit Object to remove from the list of results
     *
     * @return $this|ChildCatgorieproduitQuery The current query, for fluid interface
     */
    public function prune($catgorieproduit = null)
    {
        if ($catgorieproduit) {
            $this->addUsingAlias(CatgorieproduitTableMap::COL_IDCATGORIEPRODUIT, $catgorieproduit->getIdcatgorieproduit(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the catgorieproduit table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CatgorieproduitTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CatgorieproduitTableMap::clearInstancePool();
            CatgorieproduitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CatgorieproduitTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CatgorieproduitTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CatgorieproduitTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CatgorieproduitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CatgorieproduitQuery
