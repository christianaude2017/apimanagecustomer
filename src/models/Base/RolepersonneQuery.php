<?php

namespace Base;

use \Rolepersonne as ChildRolepersonne;
use \RolepersonneQuery as ChildRolepersonneQuery;
use \Exception;
use \PDO;
use Map\RolepersonneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'rolepersonne' table.
 *
 *
 *
 * @method     ChildRolepersonneQuery orderByIdrolepersonne($order = Criteria::ASC) Order by the idrolePersonne column
 * @method     ChildRolepersonneQuery orderByLibellerolepersonne($order = Criteria::ASC) Order by the LibellerolePersonne column
 * @method     ChildRolepersonneQuery orderBySocieteIdsociete($order = Criteria::ASC) Order by the societe_idsociete column
 *
 * @method     ChildRolepersonneQuery groupByIdrolepersonne() Group by the idrolePersonne column
 * @method     ChildRolepersonneQuery groupByLibellerolepersonne() Group by the LibellerolePersonne column
 * @method     ChildRolepersonneQuery groupBySocieteIdsociete() Group by the societe_idsociete column
 *
 * @method     ChildRolepersonneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRolepersonneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRolepersonneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRolepersonneQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRolepersonneQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRolepersonneQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRolepersonneQuery leftJoinSociete($relationAlias = null) Adds a LEFT JOIN clause to the query using the Societe relation
 * @method     ChildRolepersonneQuery rightJoinSociete($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Societe relation
 * @method     ChildRolepersonneQuery innerJoinSociete($relationAlias = null) Adds a INNER JOIN clause to the query using the Societe relation
 *
 * @method     ChildRolepersonneQuery joinWithSociete($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Societe relation
 *
 * @method     ChildRolepersonneQuery leftJoinWithSociete() Adds a LEFT JOIN clause and with to the query using the Societe relation
 * @method     ChildRolepersonneQuery rightJoinWithSociete() Adds a RIGHT JOIN clause and with to the query using the Societe relation
 * @method     ChildRolepersonneQuery innerJoinWithSociete() Adds a INNER JOIN clause and with to the query using the Societe relation
 *
 * @method     ChildRolepersonneQuery leftJoinPersonne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Personne relation
 * @method     ChildRolepersonneQuery rightJoinPersonne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Personne relation
 * @method     ChildRolepersonneQuery innerJoinPersonne($relationAlias = null) Adds a INNER JOIN clause to the query using the Personne relation
 *
 * @method     ChildRolepersonneQuery joinWithPersonne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Personne relation
 *
 * @method     ChildRolepersonneQuery leftJoinWithPersonne() Adds a LEFT JOIN clause and with to the query using the Personne relation
 * @method     ChildRolepersonneQuery rightJoinWithPersonne() Adds a RIGHT JOIN clause and with to the query using the Personne relation
 * @method     ChildRolepersonneQuery innerJoinWithPersonne() Adds a INNER JOIN clause and with to the query using the Personne relation
 *
 * @method     \SocieteQuery|\PersonneQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRolepersonne findOne(ConnectionInterface $con = null) Return the first ChildRolepersonne matching the query
 * @method     ChildRolepersonne findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRolepersonne matching the query, or a new ChildRolepersonne object populated from the query conditions when no match is found
 *
 * @method     ChildRolepersonne findOneByIdrolepersonne(int $idrolePersonne) Return the first ChildRolepersonne filtered by the idrolePersonne column
 * @method     ChildRolepersonne findOneByLibellerolepersonne(string $LibellerolePersonne) Return the first ChildRolepersonne filtered by the LibellerolePersonne column
 * @method     ChildRolepersonne findOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildRolepersonne filtered by the societe_idsociete column *

 * @method     ChildRolepersonne requirePk($key, ConnectionInterface $con = null) Return the ChildRolepersonne by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolepersonne requireOne(ConnectionInterface $con = null) Return the first ChildRolepersonne matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRolepersonne requireOneByIdrolepersonne(int $idrolePersonne) Return the first ChildRolepersonne filtered by the idrolePersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolepersonne requireOneByLibellerolepersonne(string $LibellerolePersonne) Return the first ChildRolepersonne filtered by the LibellerolePersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolepersonne requireOneBySocieteIdsociete(int $societe_idsociete) Return the first ChildRolepersonne filtered by the societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRolepersonne[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRolepersonne objects based on current ModelCriteria
 * @method     ChildRolepersonne[]|ObjectCollection findByIdrolepersonne(int $idrolePersonne) Return ChildRolepersonne objects filtered by the idrolePersonne column
 * @method     ChildRolepersonne[]|ObjectCollection findByLibellerolepersonne(string $LibellerolePersonne) Return ChildRolepersonne objects filtered by the LibellerolePersonne column
 * @method     ChildRolepersonne[]|ObjectCollection findBySocieteIdsociete(int $societe_idsociete) Return ChildRolepersonne objects filtered by the societe_idsociete column
 * @method     ChildRolepersonne[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RolepersonneQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RolepersonneQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Rolepersonne', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRolepersonneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRolepersonneQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRolepersonneQuery) {
            return $criteria;
        }
        $query = new ChildRolepersonneQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRolepersonne|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RolepersonneTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RolepersonneTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRolepersonne A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idrolePersonne, LibellerolePersonne, societe_idsociete FROM rolepersonne WHERE idrolePersonne = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRolepersonne $obj */
            $obj = new ChildRolepersonne();
            $obj->hydrate($row);
            RolepersonneTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRolepersonne|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRolepersonneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RolepersonneTableMap::COL_IDROLEPERSONNE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRolepersonneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RolepersonneTableMap::COL_IDROLEPERSONNE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idrolePersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByIdrolepersonne(1234); // WHERE idrolePersonne = 1234
     * $query->filterByIdrolepersonne(array(12, 34)); // WHERE idrolePersonne IN (12, 34)
     * $query->filterByIdrolepersonne(array('min' => 12)); // WHERE idrolePersonne > 12
     * </code>
     *
     * @param     mixed $idrolepersonne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolepersonneQuery The current query, for fluid interface
     */
    public function filterByIdrolepersonne($idrolepersonne = null, $comparison = null)
    {
        if (is_array($idrolepersonne)) {
            $useMinMax = false;
            if (isset($idrolepersonne['min'])) {
                $this->addUsingAlias(RolepersonneTableMap::COL_IDROLEPERSONNE, $idrolepersonne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idrolepersonne['max'])) {
                $this->addUsingAlias(RolepersonneTableMap::COL_IDROLEPERSONNE, $idrolepersonne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolepersonneTableMap::COL_IDROLEPERSONNE, $idrolepersonne, $comparison);
    }

    /**
     * Filter the query on the LibellerolePersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByLibellerolepersonne('fooValue');   // WHERE LibellerolePersonne = 'fooValue'
     * $query->filterByLibellerolepersonne('%fooValue%', Criteria::LIKE); // WHERE LibellerolePersonne LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libellerolepersonne The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolepersonneQuery The current query, for fluid interface
     */
    public function filterByLibellerolepersonne($libellerolepersonne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libellerolepersonne)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolepersonneTableMap::COL_LIBELLEROLEPERSONNE, $libellerolepersonne, $comparison);
    }

    /**
     * Filter the query on the societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterBySocieteIdsociete(1234); // WHERE societe_idsociete = 1234
     * $query->filterBySocieteIdsociete(array(12, 34)); // WHERE societe_idsociete IN (12, 34)
     * $query->filterBySocieteIdsociete(array('min' => 12)); // WHERE societe_idsociete > 12
     * </code>
     *
     * @see       filterBySociete()
     *
     * @param     mixed $societeIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolepersonneQuery The current query, for fluid interface
     */
    public function filterBySocieteIdsociete($societeIdsociete = null, $comparison = null)
    {
        if (is_array($societeIdsociete)) {
            $useMinMax = false;
            if (isset($societeIdsociete['min'])) {
                $this->addUsingAlias(RolepersonneTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($societeIdsociete['max'])) {
                $this->addUsingAlias(RolepersonneTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolepersonneTableMap::COL_SOCIETE_IDSOCIETE, $societeIdsociete, $comparison);
    }

    /**
     * Filter the query by a related \Societe object
     *
     * @param \Societe|ObjectCollection $societe The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRolepersonneQuery The current query, for fluid interface
     */
    public function filterBySociete($societe, $comparison = null)
    {
        if ($societe instanceof \Societe) {
            return $this
                ->addUsingAlias(RolepersonneTableMap::COL_SOCIETE_IDSOCIETE, $societe->getIdsociete(), $comparison);
        } elseif ($societe instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RolepersonneTableMap::COL_SOCIETE_IDSOCIETE, $societe->toKeyValue('PrimaryKey', 'Idsociete'), $comparison);
        } else {
            throw new PropelException('filterBySociete() only accepts arguments of type \Societe or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Societe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRolepersonneQuery The current query, for fluid interface
     */
    public function joinSociete($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Societe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Societe');
        }

        return $this;
    }

    /**
     * Use the Societe relation Societe object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \SocieteQuery A secondary query class using the current class as primary query
     */
    public function useSocieteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSociete($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Societe', '\SocieteQuery');
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne|ObjectCollection $personne the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildRolepersonneQuery The current query, for fluid interface
     */
    public function filterByPersonne($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(RolepersonneTableMap::COL_IDROLEPERSONNE, $personne->getRolepersonneIdrolepersonne(), $comparison);
        } elseif ($personne instanceof ObjectCollection) {
            return $this
                ->usePersonneQuery()
                ->filterByPrimaryKeys($personne->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPersonne() only accepts arguments of type \Personne or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Personne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRolepersonneQuery The current query, for fluid interface
     */
    public function joinPersonne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Personne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Personne');
        }

        return $this;
    }

    /**
     * Use the Personne relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Personne', '\PersonneQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRolepersonne $rolepersonne Object to remove from the list of results
     *
     * @return $this|ChildRolepersonneQuery The current query, for fluid interface
     */
    public function prune($rolepersonne = null)
    {
        if ($rolepersonne) {
            $this->addUsingAlias(RolepersonneTableMap::COL_IDROLEPERSONNE, $rolepersonne->getIdrolepersonne(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the rolepersonne table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolepersonneTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RolepersonneTableMap::clearInstancePool();
            RolepersonneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolepersonneTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RolepersonneTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RolepersonneTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RolepersonneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RolepersonneQuery
