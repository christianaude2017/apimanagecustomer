<?php

namespace Base;

use \Catgorieproduit as ChildCatgorieproduit;
use \CatgorieproduitQuery as ChildCatgorieproduitQuery;
use \Modepaiement as ChildModepaiement;
use \ModepaiementQuery as ChildModepaiementQuery;
use \Personne as ChildPersonne;
use \PersonneQuery as ChildPersonneQuery;
use \Produits as ChildProduits;
use \ProduitsQuery as ChildProduitsQuery;
use \Rolepersonne as ChildRolepersonne;
use \RolepersonneQuery as ChildRolepersonneQuery;
use \Societe as ChildSociete;
use \SocieteQuery as ChildSocieteQuery;
use \Ville as ChildVille;
use \VilleQuery as ChildVilleQuery;
use \Exception;
use \PDO;
use Map\CatgorieproduitTableMap;
use Map\ModepaiementTableMap;
use Map\PersonneTableMap;
use Map\ProduitsTableMap;
use Map\RolepersonneTableMap;
use Map\SocieteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'societe' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Societe implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\SocieteTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idsociete field.
     *
     * @var        int
     */
    protected $idsociete;

    /**
     * The value for the libellesociete field.
     *
     * @var        string
     */
    protected $libellesociete;

    /**
     * The value for the ville_idville field.
     *
     * @var        int
     */
    protected $ville_idville;

    /**
     * The value for the ville_pays_idpays field.
     *
     * @var        int
     */
    protected $ville_pays_idpays;

    /**
     * @var        ChildVille
     */
    protected $aVille;

    /**
     * @var        ObjectCollection|ChildCatgorieproduit[] Collection to store aggregation of ChildCatgorieproduit objects.
     */
    protected $collCatgorieproduits;
    protected $collCatgorieproduitsPartial;

    /**
     * @var        ObjectCollection|ChildModepaiement[] Collection to store aggregation of ChildModepaiement objects.
     */
    protected $collModepaiements;
    protected $collModepaiementsPartial;

    /**
     * @var        ObjectCollection|ChildPersonne[] Collection to store aggregation of ChildPersonne objects.
     */
    protected $collPersonnes;
    protected $collPersonnesPartial;

    /**
     * @var        ObjectCollection|ChildProduits[] Collection to store aggregation of ChildProduits objects.
     */
    protected $collProduitss;
    protected $collProduitssPartial;

    /**
     * @var        ObjectCollection|ChildRolepersonne[] Collection to store aggregation of ChildRolepersonne objects.
     */
    protected $collRolepersonnes;
    protected $collRolepersonnesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCatgorieproduit[]
     */
    protected $catgorieproduitsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildModepaiement[]
     */
    protected $modepaiementsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPersonne[]
     */
    protected $personnesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProduits[]
     */
    protected $produitssScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRolepersonne[]
     */
    protected $rolepersonnesScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Societe object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Societe</code> instance.  If
     * <code>obj</code> is an instance of <code>Societe</code>, delegates to
     * <code>equals(Societe)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Societe The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idsociete] column value.
     *
     * @return int
     */
    public function getIdsociete()
    {
        return $this->idsociete;
    }

    /**
     * Get the [libellesociete] column value.
     *
     * @return string
     */
    public function getLibellesociete()
    {
        return $this->libellesociete;
    }

    /**
     * Get the [ville_idville] column value.
     *
     * @return int
     */
    public function getVilleIdville()
    {
        return $this->ville_idville;
    }

    /**
     * Get the [ville_pays_idpays] column value.
     *
     * @return int
     */
    public function getVillePaysIdpays()
    {
        return $this->ville_pays_idpays;
    }

    /**
     * Set the value of [idsociete] column.
     *
     * @param int $v new value
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function setIdsociete($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idsociete !== $v) {
            $this->idsociete = $v;
            $this->modifiedColumns[SocieteTableMap::COL_IDSOCIETE] = true;
        }

        return $this;
    } // setIdsociete()

    /**
     * Set the value of [libellesociete] column.
     *
     * @param string $v new value
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function setLibellesociete($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->libellesociete !== $v) {
            $this->libellesociete = $v;
            $this->modifiedColumns[SocieteTableMap::COL_LIBELLESOCIETE] = true;
        }

        return $this;
    } // setLibellesociete()

    /**
     * Set the value of [ville_idville] column.
     *
     * @param int $v new value
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function setVilleIdville($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ville_idville !== $v) {
            $this->ville_idville = $v;
            $this->modifiedColumns[SocieteTableMap::COL_VILLE_IDVILLE] = true;
        }

        if ($this->aVille !== null && $this->aVille->getIdville() !== $v) {
            $this->aVille = null;
        }

        return $this;
    } // setVilleIdville()

    /**
     * Set the value of [ville_pays_idpays] column.
     *
     * @param int $v new value
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function setVillePaysIdpays($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ville_pays_idpays !== $v) {
            $this->ville_pays_idpays = $v;
            $this->modifiedColumns[SocieteTableMap::COL_VILLE_PAYS_IDPAYS] = true;
        }

        if ($this->aVille !== null && $this->aVille->getPaysIdpays() !== $v) {
            $this->aVille = null;
        }

        return $this;
    } // setVillePaysIdpays()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SocieteTableMap::translateFieldName('Idsociete', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idsociete = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SocieteTableMap::translateFieldName('Libellesociete', TableMap::TYPE_PHPNAME, $indexType)];
            $this->libellesociete = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SocieteTableMap::translateFieldName('VilleIdville', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ville_idville = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SocieteTableMap::translateFieldName('VillePaysIdpays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ville_pays_idpays = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 4; // 4 = SocieteTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Societe'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aVille !== null && $this->ville_idville !== $this->aVille->getIdville()) {
            $this->aVille = null;
        }
        if ($this->aVille !== null && $this->ville_pays_idpays !== $this->aVille->getPaysIdpays()) {
            $this->aVille = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SocieteTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSocieteQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aVille = null;
            $this->collCatgorieproduits = null;

            $this->collModepaiements = null;

            $this->collPersonnes = null;

            $this->collProduitss = null;

            $this->collRolepersonnes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Societe::setDeleted()
     * @see Societe::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SocieteTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSocieteQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SocieteTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SocieteTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aVille !== null) {
                if ($this->aVille->isModified() || $this->aVille->isNew()) {
                    $affectedRows += $this->aVille->save($con);
                }
                $this->setVille($this->aVille);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->catgorieproduitsScheduledForDeletion !== null) {
                if (!$this->catgorieproduitsScheduledForDeletion->isEmpty()) {
                    \CatgorieproduitQuery::create()
                        ->filterByPrimaryKeys($this->catgorieproduitsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->catgorieproduitsScheduledForDeletion = null;
                }
            }

            if ($this->collCatgorieproduits !== null) {
                foreach ($this->collCatgorieproduits as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->modepaiementsScheduledForDeletion !== null) {
                if (!$this->modepaiementsScheduledForDeletion->isEmpty()) {
                    \ModepaiementQuery::create()
                        ->filterByPrimaryKeys($this->modepaiementsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->modepaiementsScheduledForDeletion = null;
                }
            }

            if ($this->collModepaiements !== null) {
                foreach ($this->collModepaiements as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->personnesScheduledForDeletion !== null) {
                if (!$this->personnesScheduledForDeletion->isEmpty()) {
                    \PersonneQuery::create()
                        ->filterByPrimaryKeys($this->personnesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->personnesScheduledForDeletion = null;
                }
            }

            if ($this->collPersonnes !== null) {
                foreach ($this->collPersonnes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->produitssScheduledForDeletion !== null) {
                if (!$this->produitssScheduledForDeletion->isEmpty()) {
                    \ProduitsQuery::create()
                        ->filterByPrimaryKeys($this->produitssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->produitssScheduledForDeletion = null;
                }
            }

            if ($this->collProduitss !== null) {
                foreach ($this->collProduitss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rolepersonnesScheduledForDeletion !== null) {
                if (!$this->rolepersonnesScheduledForDeletion->isEmpty()) {
                    \RolepersonneQuery::create()
                        ->filterByPrimaryKeys($this->rolepersonnesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rolepersonnesScheduledForDeletion = null;
                }
            }

            if ($this->collRolepersonnes !== null) {
                foreach ($this->collRolepersonnes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SocieteTableMap::COL_IDSOCIETE] = true;
        if (null !== $this->idsociete) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SocieteTableMap::COL_IDSOCIETE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SocieteTableMap::COL_IDSOCIETE)) {
            $modifiedColumns[':p' . $index++]  = 'idsociete';
        }
        if ($this->isColumnModified(SocieteTableMap::COL_LIBELLESOCIETE)) {
            $modifiedColumns[':p' . $index++]  = 'Libellesociete';
        }
        if ($this->isColumnModified(SocieteTableMap::COL_VILLE_IDVILLE)) {
            $modifiedColumns[':p' . $index++]  = 'ville_idville';
        }
        if ($this->isColumnModified(SocieteTableMap::COL_VILLE_PAYS_IDPAYS)) {
            $modifiedColumns[':p' . $index++]  = 'ville_pays_idpays';
        }

        $sql = sprintf(
            'INSERT INTO societe (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idsociete':
                        $stmt->bindValue($identifier, $this->idsociete, PDO::PARAM_INT);
                        break;
                    case 'Libellesociete':
                        $stmt->bindValue($identifier, $this->libellesociete, PDO::PARAM_STR);
                        break;
                    case 'ville_idville':
                        $stmt->bindValue($identifier, $this->ville_idville, PDO::PARAM_INT);
                        break;
                    case 'ville_pays_idpays':
                        $stmt->bindValue($identifier, $this->ville_pays_idpays, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdsociete($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SocieteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdsociete();
                break;
            case 1:
                return $this->getLibellesociete();
                break;
            case 2:
                return $this->getVilleIdville();
                break;
            case 3:
                return $this->getVillePaysIdpays();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Societe'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Societe'][$this->hashCode()] = true;
        $keys = SocieteTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdsociete(),
            $keys[1] => $this->getLibellesociete(),
            $keys[2] => $this->getVilleIdville(),
            $keys[3] => $this->getVillePaysIdpays(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aVille) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ville';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ville';
                        break;
                    default:
                        $key = 'Ville';
                }

                $result[$key] = $this->aVille->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCatgorieproduits) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'catgorieproduits';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'catgorieproduits';
                        break;
                    default:
                        $key = 'Catgorieproduits';
                }

                $result[$key] = $this->collCatgorieproduits->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collModepaiements) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'modepaiements';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'modepaiements';
                        break;
                    default:
                        $key = 'Modepaiements';
                }

                $result[$key] = $this->collModepaiements->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPersonnes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'personnes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'personnes';
                        break;
                    default:
                        $key = 'Personnes';
                }

                $result[$key] = $this->collPersonnes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProduitss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'produitss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'produitss';
                        break;
                    default:
                        $key = 'Produitss';
                }

                $result[$key] = $this->collProduitss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRolepersonnes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'rolepersonnes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'rolepersonnes';
                        break;
                    default:
                        $key = 'Rolepersonnes';
                }

                $result[$key] = $this->collRolepersonnes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Societe
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SocieteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Societe
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdsociete($value);
                break;
            case 1:
                $this->setLibellesociete($value);
                break;
            case 2:
                $this->setVilleIdville($value);
                break;
            case 3:
                $this->setVillePaysIdpays($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SocieteTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdsociete($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setLibellesociete($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setVilleIdville($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setVillePaysIdpays($arr[$keys[3]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Societe The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SocieteTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SocieteTableMap::COL_IDSOCIETE)) {
            $criteria->add(SocieteTableMap::COL_IDSOCIETE, $this->idsociete);
        }
        if ($this->isColumnModified(SocieteTableMap::COL_LIBELLESOCIETE)) {
            $criteria->add(SocieteTableMap::COL_LIBELLESOCIETE, $this->libellesociete);
        }
        if ($this->isColumnModified(SocieteTableMap::COL_VILLE_IDVILLE)) {
            $criteria->add(SocieteTableMap::COL_VILLE_IDVILLE, $this->ville_idville);
        }
        if ($this->isColumnModified(SocieteTableMap::COL_VILLE_PAYS_IDPAYS)) {
            $criteria->add(SocieteTableMap::COL_VILLE_PAYS_IDPAYS, $this->ville_pays_idpays);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSocieteQuery::create();
        $criteria->add(SocieteTableMap::COL_IDSOCIETE, $this->idsociete);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdsociete();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdsociete();
    }

    /**
     * Generic method to set the primary key (idsociete column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdsociete($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getIdsociete();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Societe (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLibellesociete($this->getLibellesociete());
        $copyObj->setVilleIdville($this->getVilleIdville());
        $copyObj->setVillePaysIdpays($this->getVillePaysIdpays());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCatgorieproduits() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCatgorieproduit($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getModepaiements() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addModepaiement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPersonnes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPersonne($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProduitss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProduits($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRolepersonnes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRolepersonne($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdsociete(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Societe Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildVille object.
     *
     * @param  ChildVille $v
     * @return $this|\Societe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setVille(ChildVille $v = null)
    {
        if ($v === null) {
            $this->setVilleIdville(NULL);
        } else {
            $this->setVilleIdville($v->getIdville());
        }

        if ($v === null) {
            $this->setVillePaysIdpays(NULL);
        } else {
            $this->setVillePaysIdpays($v->getPaysIdpays());
        }

        $this->aVille = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildVille object, it will not be re-added.
        if ($v !== null) {
            $v->addSociete($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildVille object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildVille The associated ChildVille object.
     * @throws PropelException
     */
    public function getVille(ConnectionInterface $con = null)
    {
        if ($this->aVille === null && ($this->ville_idville != 0 && $this->ville_pays_idpays != 0)) {
            $this->aVille = ChildVilleQuery::create()->findPk(array($this->ville_idville, $this->ville_pays_idpays), $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aVille->addSocietes($this);
             */
        }

        return $this->aVille;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Catgorieproduit' == $relationName) {
            $this->initCatgorieproduits();
            return;
        }
        if ('Modepaiement' == $relationName) {
            $this->initModepaiements();
            return;
        }
        if ('Personne' == $relationName) {
            $this->initPersonnes();
            return;
        }
        if ('Produits' == $relationName) {
            $this->initProduitss();
            return;
        }
        if ('Rolepersonne' == $relationName) {
            $this->initRolepersonnes();
            return;
        }
    }

    /**
     * Clears out the collCatgorieproduits collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCatgorieproduits()
     */
    public function clearCatgorieproduits()
    {
        $this->collCatgorieproduits = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCatgorieproduits collection loaded partially.
     */
    public function resetPartialCatgorieproduits($v = true)
    {
        $this->collCatgorieproduitsPartial = $v;
    }

    /**
     * Initializes the collCatgorieproduits collection.
     *
     * By default this just sets the collCatgorieproduits collection to an empty array (like clearcollCatgorieproduits());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCatgorieproduits($overrideExisting = true)
    {
        if (null !== $this->collCatgorieproduits && !$overrideExisting) {
            return;
        }

        $collectionClassName = CatgorieproduitTableMap::getTableMap()->getCollectionClassName();

        $this->collCatgorieproduits = new $collectionClassName;
        $this->collCatgorieproduits->setModel('\Catgorieproduit');
    }

    /**
     * Gets an array of ChildCatgorieproduit objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSociete is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCatgorieproduit[] List of ChildCatgorieproduit objects
     * @throws PropelException
     */
    public function getCatgorieproduits(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCatgorieproduitsPartial && !$this->isNew();
        if (null === $this->collCatgorieproduits || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCatgorieproduits) {
                // return empty collection
                $this->initCatgorieproduits();
            } else {
                $collCatgorieproduits = ChildCatgorieproduitQuery::create(null, $criteria)
                    ->filterBySociete($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCatgorieproduitsPartial && count($collCatgorieproduits)) {
                        $this->initCatgorieproduits(false);

                        foreach ($collCatgorieproduits as $obj) {
                            if (false == $this->collCatgorieproduits->contains($obj)) {
                                $this->collCatgorieproduits->append($obj);
                            }
                        }

                        $this->collCatgorieproduitsPartial = true;
                    }

                    return $collCatgorieproduits;
                }

                if ($partial && $this->collCatgorieproduits) {
                    foreach ($this->collCatgorieproduits as $obj) {
                        if ($obj->isNew()) {
                            $collCatgorieproduits[] = $obj;
                        }
                    }
                }

                $this->collCatgorieproduits = $collCatgorieproduits;
                $this->collCatgorieproduitsPartial = false;
            }
        }

        return $this->collCatgorieproduits;
    }

    /**
     * Sets a collection of ChildCatgorieproduit objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $catgorieproduits A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function setCatgorieproduits(Collection $catgorieproduits, ConnectionInterface $con = null)
    {
        /** @var ChildCatgorieproduit[] $catgorieproduitsToDelete */
        $catgorieproduitsToDelete = $this->getCatgorieproduits(new Criteria(), $con)->diff($catgorieproduits);


        $this->catgorieproduitsScheduledForDeletion = $catgorieproduitsToDelete;

        foreach ($catgorieproduitsToDelete as $catgorieproduitRemoved) {
            $catgorieproduitRemoved->setSociete(null);
        }

        $this->collCatgorieproduits = null;
        foreach ($catgorieproduits as $catgorieproduit) {
            $this->addCatgorieproduit($catgorieproduit);
        }

        $this->collCatgorieproduits = $catgorieproduits;
        $this->collCatgorieproduitsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Catgorieproduit objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Catgorieproduit objects.
     * @throws PropelException
     */
    public function countCatgorieproduits(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCatgorieproduitsPartial && !$this->isNew();
        if (null === $this->collCatgorieproduits || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCatgorieproduits) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCatgorieproduits());
            }

            $query = ChildCatgorieproduitQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySociete($this)
                ->count($con);
        }

        return count($this->collCatgorieproduits);
    }

    /**
     * Method called to associate a ChildCatgorieproduit object to this object
     * through the ChildCatgorieproduit foreign key attribute.
     *
     * @param  ChildCatgorieproduit $l ChildCatgorieproduit
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function addCatgorieproduit(ChildCatgorieproduit $l)
    {
        if ($this->collCatgorieproduits === null) {
            $this->initCatgorieproduits();
            $this->collCatgorieproduitsPartial = true;
        }

        if (!$this->collCatgorieproduits->contains($l)) {
            $this->doAddCatgorieproduit($l);

            if ($this->catgorieproduitsScheduledForDeletion and $this->catgorieproduitsScheduledForDeletion->contains($l)) {
                $this->catgorieproduitsScheduledForDeletion->remove($this->catgorieproduitsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCatgorieproduit $catgorieproduit The ChildCatgorieproduit object to add.
     */
    protected function doAddCatgorieproduit(ChildCatgorieproduit $catgorieproduit)
    {
        $this->collCatgorieproduits[]= $catgorieproduit;
        $catgorieproduit->setSociete($this);
    }

    /**
     * @param  ChildCatgorieproduit $catgorieproduit The ChildCatgorieproduit object to remove.
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function removeCatgorieproduit(ChildCatgorieproduit $catgorieproduit)
    {
        if ($this->getCatgorieproduits()->contains($catgorieproduit)) {
            $pos = $this->collCatgorieproduits->search($catgorieproduit);
            $this->collCatgorieproduits->remove($pos);
            if (null === $this->catgorieproduitsScheduledForDeletion) {
                $this->catgorieproduitsScheduledForDeletion = clone $this->collCatgorieproduits;
                $this->catgorieproduitsScheduledForDeletion->clear();
            }
            $this->catgorieproduitsScheduledForDeletion[]= clone $catgorieproduit;
            $catgorieproduit->setSociete(null);
        }

        return $this;
    }

    /**
     * Clears out the collModepaiements collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addModepaiements()
     */
    public function clearModepaiements()
    {
        $this->collModepaiements = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collModepaiements collection loaded partially.
     */
    public function resetPartialModepaiements($v = true)
    {
        $this->collModepaiementsPartial = $v;
    }

    /**
     * Initializes the collModepaiements collection.
     *
     * By default this just sets the collModepaiements collection to an empty array (like clearcollModepaiements());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initModepaiements($overrideExisting = true)
    {
        if (null !== $this->collModepaiements && !$overrideExisting) {
            return;
        }

        $collectionClassName = ModepaiementTableMap::getTableMap()->getCollectionClassName();

        $this->collModepaiements = new $collectionClassName;
        $this->collModepaiements->setModel('\Modepaiement');
    }

    /**
     * Gets an array of ChildModepaiement objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSociete is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildModepaiement[] List of ChildModepaiement objects
     * @throws PropelException
     */
    public function getModepaiements(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collModepaiementsPartial && !$this->isNew();
        if (null === $this->collModepaiements || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collModepaiements) {
                // return empty collection
                $this->initModepaiements();
            } else {
                $collModepaiements = ChildModepaiementQuery::create(null, $criteria)
                    ->filterBySociete($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collModepaiementsPartial && count($collModepaiements)) {
                        $this->initModepaiements(false);

                        foreach ($collModepaiements as $obj) {
                            if (false == $this->collModepaiements->contains($obj)) {
                                $this->collModepaiements->append($obj);
                            }
                        }

                        $this->collModepaiementsPartial = true;
                    }

                    return $collModepaiements;
                }

                if ($partial && $this->collModepaiements) {
                    foreach ($this->collModepaiements as $obj) {
                        if ($obj->isNew()) {
                            $collModepaiements[] = $obj;
                        }
                    }
                }

                $this->collModepaiements = $collModepaiements;
                $this->collModepaiementsPartial = false;
            }
        }

        return $this->collModepaiements;
    }

    /**
     * Sets a collection of ChildModepaiement objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $modepaiements A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function setModepaiements(Collection $modepaiements, ConnectionInterface $con = null)
    {
        /** @var ChildModepaiement[] $modepaiementsToDelete */
        $modepaiementsToDelete = $this->getModepaiements(new Criteria(), $con)->diff($modepaiements);


        $this->modepaiementsScheduledForDeletion = $modepaiementsToDelete;

        foreach ($modepaiementsToDelete as $modepaiementRemoved) {
            $modepaiementRemoved->setSociete(null);
        }

        $this->collModepaiements = null;
        foreach ($modepaiements as $modepaiement) {
            $this->addModepaiement($modepaiement);
        }

        $this->collModepaiements = $modepaiements;
        $this->collModepaiementsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Modepaiement objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Modepaiement objects.
     * @throws PropelException
     */
    public function countModepaiements(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collModepaiementsPartial && !$this->isNew();
        if (null === $this->collModepaiements || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collModepaiements) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getModepaiements());
            }

            $query = ChildModepaiementQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySociete($this)
                ->count($con);
        }

        return count($this->collModepaiements);
    }

    /**
     * Method called to associate a ChildModepaiement object to this object
     * through the ChildModepaiement foreign key attribute.
     *
     * @param  ChildModepaiement $l ChildModepaiement
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function addModepaiement(ChildModepaiement $l)
    {
        if ($this->collModepaiements === null) {
            $this->initModepaiements();
            $this->collModepaiementsPartial = true;
        }

        if (!$this->collModepaiements->contains($l)) {
            $this->doAddModepaiement($l);

            if ($this->modepaiementsScheduledForDeletion and $this->modepaiementsScheduledForDeletion->contains($l)) {
                $this->modepaiementsScheduledForDeletion->remove($this->modepaiementsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildModepaiement $modepaiement The ChildModepaiement object to add.
     */
    protected function doAddModepaiement(ChildModepaiement $modepaiement)
    {
        $this->collModepaiements[]= $modepaiement;
        $modepaiement->setSociete($this);
    }

    /**
     * @param  ChildModepaiement $modepaiement The ChildModepaiement object to remove.
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function removeModepaiement(ChildModepaiement $modepaiement)
    {
        if ($this->getModepaiements()->contains($modepaiement)) {
            $pos = $this->collModepaiements->search($modepaiement);
            $this->collModepaiements->remove($pos);
            if (null === $this->modepaiementsScheduledForDeletion) {
                $this->modepaiementsScheduledForDeletion = clone $this->collModepaiements;
                $this->modepaiementsScheduledForDeletion->clear();
            }
            $this->modepaiementsScheduledForDeletion[]= clone $modepaiement;
            $modepaiement->setSociete(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Societe is new, it will return
     * an empty collection; or if this Societe has previously
     * been saved, it will retrieve related Modepaiements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Societe.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildModepaiement[] List of ChildModepaiement objects
     */
    public function getModepaiementsJoinBanque(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildModepaiementQuery::create(null, $criteria);
        $query->joinWith('Banque', $joinBehavior);

        return $this->getModepaiements($query, $con);
    }

    /**
     * Clears out the collPersonnes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPersonnes()
     */
    public function clearPersonnes()
    {
        $this->collPersonnes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPersonnes collection loaded partially.
     */
    public function resetPartialPersonnes($v = true)
    {
        $this->collPersonnesPartial = $v;
    }

    /**
     * Initializes the collPersonnes collection.
     *
     * By default this just sets the collPersonnes collection to an empty array (like clearcollPersonnes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPersonnes($overrideExisting = true)
    {
        if (null !== $this->collPersonnes && !$overrideExisting) {
            return;
        }

        $collectionClassName = PersonneTableMap::getTableMap()->getCollectionClassName();

        $this->collPersonnes = new $collectionClassName;
        $this->collPersonnes->setModel('\Personne');
    }

    /**
     * Gets an array of ChildPersonne objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSociete is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPersonne[] List of ChildPersonne objects
     * @throws PropelException
     */
    public function getPersonnes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPersonnesPartial && !$this->isNew();
        if (null === $this->collPersonnes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPersonnes) {
                // return empty collection
                $this->initPersonnes();
            } else {
                $collPersonnes = ChildPersonneQuery::create(null, $criteria)
                    ->filterBySociete($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPersonnesPartial && count($collPersonnes)) {
                        $this->initPersonnes(false);

                        foreach ($collPersonnes as $obj) {
                            if (false == $this->collPersonnes->contains($obj)) {
                                $this->collPersonnes->append($obj);
                            }
                        }

                        $this->collPersonnesPartial = true;
                    }

                    return $collPersonnes;
                }

                if ($partial && $this->collPersonnes) {
                    foreach ($this->collPersonnes as $obj) {
                        if ($obj->isNew()) {
                            $collPersonnes[] = $obj;
                        }
                    }
                }

                $this->collPersonnes = $collPersonnes;
                $this->collPersonnesPartial = false;
            }
        }

        return $this->collPersonnes;
    }

    /**
     * Sets a collection of ChildPersonne objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $personnes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function setPersonnes(Collection $personnes, ConnectionInterface $con = null)
    {
        /** @var ChildPersonne[] $personnesToDelete */
        $personnesToDelete = $this->getPersonnes(new Criteria(), $con)->diff($personnes);


        $this->personnesScheduledForDeletion = $personnesToDelete;

        foreach ($personnesToDelete as $personneRemoved) {
            $personneRemoved->setSociete(null);
        }

        $this->collPersonnes = null;
        foreach ($personnes as $personne) {
            $this->addPersonne($personne);
        }

        $this->collPersonnes = $personnes;
        $this->collPersonnesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Personne objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Personne objects.
     * @throws PropelException
     */
    public function countPersonnes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPersonnesPartial && !$this->isNew();
        if (null === $this->collPersonnes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPersonnes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPersonnes());
            }

            $query = ChildPersonneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySociete($this)
                ->count($con);
        }

        return count($this->collPersonnes);
    }

    /**
     * Method called to associate a ChildPersonne object to this object
     * through the ChildPersonne foreign key attribute.
     *
     * @param  ChildPersonne $l ChildPersonne
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function addPersonne(ChildPersonne $l)
    {
        if ($this->collPersonnes === null) {
            $this->initPersonnes();
            $this->collPersonnesPartial = true;
        }

        if (!$this->collPersonnes->contains($l)) {
            $this->doAddPersonne($l);

            if ($this->personnesScheduledForDeletion and $this->personnesScheduledForDeletion->contains($l)) {
                $this->personnesScheduledForDeletion->remove($this->personnesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPersonne $personne The ChildPersonne object to add.
     */
    protected function doAddPersonne(ChildPersonne $personne)
    {
        $this->collPersonnes[]= $personne;
        $personne->setSociete($this);
    }

    /**
     * @param  ChildPersonne $personne The ChildPersonne object to remove.
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function removePersonne(ChildPersonne $personne)
    {
        if ($this->getPersonnes()->contains($personne)) {
            $pos = $this->collPersonnes->search($personne);
            $this->collPersonnes->remove($pos);
            if (null === $this->personnesScheduledForDeletion) {
                $this->personnesScheduledForDeletion = clone $this->collPersonnes;
                $this->personnesScheduledForDeletion->clear();
            }
            $this->personnesScheduledForDeletion[]= clone $personne;
            $personne->setSociete(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Societe is new, it will return
     * an empty collection; or if this Societe has previously
     * been saved, it will retrieve related Personnes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Societe.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPersonne[] List of ChildPersonne objects
     */
    public function getPersonnesJoinCommune(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPersonneQuery::create(null, $criteria);
        $query->joinWith('Commune', $joinBehavior);

        return $this->getPersonnes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Societe is new, it will return
     * an empty collection; or if this Societe has previously
     * been saved, it will retrieve related Personnes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Societe.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPersonne[] List of ChildPersonne objects
     */
    public function getPersonnesJoinRolepersonne(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPersonneQuery::create(null, $criteria);
        $query->joinWith('Rolepersonne', $joinBehavior);

        return $this->getPersonnes($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Societe is new, it will return
     * an empty collection; or if this Societe has previously
     * been saved, it will retrieve related Personnes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Societe.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPersonne[] List of ChildPersonne objects
     */
    public function getPersonnesJoinVille(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPersonneQuery::create(null, $criteria);
        $query->joinWith('Ville', $joinBehavior);

        return $this->getPersonnes($query, $con);
    }

    /**
     * Clears out the collProduitss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProduitss()
     */
    public function clearProduitss()
    {
        $this->collProduitss = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProduitss collection loaded partially.
     */
    public function resetPartialProduitss($v = true)
    {
        $this->collProduitssPartial = $v;
    }

    /**
     * Initializes the collProduitss collection.
     *
     * By default this just sets the collProduitss collection to an empty array (like clearcollProduitss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProduitss($overrideExisting = true)
    {
        if (null !== $this->collProduitss && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProduitsTableMap::getTableMap()->getCollectionClassName();

        $this->collProduitss = new $collectionClassName;
        $this->collProduitss->setModel('\Produits');
    }

    /**
     * Gets an array of ChildProduits objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSociete is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProduits[] List of ChildProduits objects
     * @throws PropelException
     */
    public function getProduitss(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProduitssPartial && !$this->isNew();
        if (null === $this->collProduitss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProduitss) {
                // return empty collection
                $this->initProduitss();
            } else {
                $collProduitss = ChildProduitsQuery::create(null, $criteria)
                    ->filterBySociete($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProduitssPartial && count($collProduitss)) {
                        $this->initProduitss(false);

                        foreach ($collProduitss as $obj) {
                            if (false == $this->collProduitss->contains($obj)) {
                                $this->collProduitss->append($obj);
                            }
                        }

                        $this->collProduitssPartial = true;
                    }

                    return $collProduitss;
                }

                if ($partial && $this->collProduitss) {
                    foreach ($this->collProduitss as $obj) {
                        if ($obj->isNew()) {
                            $collProduitss[] = $obj;
                        }
                    }
                }

                $this->collProduitss = $collProduitss;
                $this->collProduitssPartial = false;
            }
        }

        return $this->collProduitss;
    }

    /**
     * Sets a collection of ChildProduits objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $produitss A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function setProduitss(Collection $produitss, ConnectionInterface $con = null)
    {
        /** @var ChildProduits[] $produitssToDelete */
        $produitssToDelete = $this->getProduitss(new Criteria(), $con)->diff($produitss);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->produitssScheduledForDeletion = clone $produitssToDelete;

        foreach ($produitssToDelete as $produitsRemoved) {
            $produitsRemoved->setSociete(null);
        }

        $this->collProduitss = null;
        foreach ($produitss as $produits) {
            $this->addProduits($produits);
        }

        $this->collProduitss = $produitss;
        $this->collProduitssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Produits objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Produits objects.
     * @throws PropelException
     */
    public function countProduitss(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProduitssPartial && !$this->isNew();
        if (null === $this->collProduitss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProduitss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProduitss());
            }

            $query = ChildProduitsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySociete($this)
                ->count($con);
        }

        return count($this->collProduitss);
    }

    /**
     * Method called to associate a ChildProduits object to this object
     * through the ChildProduits foreign key attribute.
     *
     * @param  ChildProduits $l ChildProduits
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function addProduits(ChildProduits $l)
    {
        if ($this->collProduitss === null) {
            $this->initProduitss();
            $this->collProduitssPartial = true;
        }

        if (!$this->collProduitss->contains($l)) {
            $this->doAddProduits($l);

            if ($this->produitssScheduledForDeletion and $this->produitssScheduledForDeletion->contains($l)) {
                $this->produitssScheduledForDeletion->remove($this->produitssScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProduits $produits The ChildProduits object to add.
     */
    protected function doAddProduits(ChildProduits $produits)
    {
        $this->collProduitss[]= $produits;
        $produits->setSociete($this);
    }

    /**
     * @param  ChildProduits $produits The ChildProduits object to remove.
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function removeProduits(ChildProduits $produits)
    {
        if ($this->getProduitss()->contains($produits)) {
            $pos = $this->collProduitss->search($produits);
            $this->collProduitss->remove($pos);
            if (null === $this->produitssScheduledForDeletion) {
                $this->produitssScheduledForDeletion = clone $this->collProduitss;
                $this->produitssScheduledForDeletion->clear();
            }
            $this->produitssScheduledForDeletion[]= clone $produits;
            $produits->setSociete(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Societe is new, it will return
     * an empty collection; or if this Societe has previously
     * been saved, it will retrieve related Produitss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Societe.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProduits[] List of ChildProduits objects
     */
    public function getProduitssJoinCatgorieproduit(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProduitsQuery::create(null, $criteria);
        $query->joinWith('Catgorieproduit', $joinBehavior);

        return $this->getProduitss($query, $con);
    }

    /**
     * Clears out the collRolepersonnes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRolepersonnes()
     */
    public function clearRolepersonnes()
    {
        $this->collRolepersonnes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRolepersonnes collection loaded partially.
     */
    public function resetPartialRolepersonnes($v = true)
    {
        $this->collRolepersonnesPartial = $v;
    }

    /**
     * Initializes the collRolepersonnes collection.
     *
     * By default this just sets the collRolepersonnes collection to an empty array (like clearcollRolepersonnes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRolepersonnes($overrideExisting = true)
    {
        if (null !== $this->collRolepersonnes && !$overrideExisting) {
            return;
        }

        $collectionClassName = RolepersonneTableMap::getTableMap()->getCollectionClassName();

        $this->collRolepersonnes = new $collectionClassName;
        $this->collRolepersonnes->setModel('\Rolepersonne');
    }

    /**
     * Gets an array of ChildRolepersonne objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSociete is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRolepersonne[] List of ChildRolepersonne objects
     * @throws PropelException
     */
    public function getRolepersonnes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRolepersonnesPartial && !$this->isNew();
        if (null === $this->collRolepersonnes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRolepersonnes) {
                // return empty collection
                $this->initRolepersonnes();
            } else {
                $collRolepersonnes = ChildRolepersonneQuery::create(null, $criteria)
                    ->filterBySociete($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRolepersonnesPartial && count($collRolepersonnes)) {
                        $this->initRolepersonnes(false);

                        foreach ($collRolepersonnes as $obj) {
                            if (false == $this->collRolepersonnes->contains($obj)) {
                                $this->collRolepersonnes->append($obj);
                            }
                        }

                        $this->collRolepersonnesPartial = true;
                    }

                    return $collRolepersonnes;
                }

                if ($partial && $this->collRolepersonnes) {
                    foreach ($this->collRolepersonnes as $obj) {
                        if ($obj->isNew()) {
                            $collRolepersonnes[] = $obj;
                        }
                    }
                }

                $this->collRolepersonnes = $collRolepersonnes;
                $this->collRolepersonnesPartial = false;
            }
        }

        return $this->collRolepersonnes;
    }

    /**
     * Sets a collection of ChildRolepersonne objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $rolepersonnes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function setRolepersonnes(Collection $rolepersonnes, ConnectionInterface $con = null)
    {
        /** @var ChildRolepersonne[] $rolepersonnesToDelete */
        $rolepersonnesToDelete = $this->getRolepersonnes(new Criteria(), $con)->diff($rolepersonnes);


        $this->rolepersonnesScheduledForDeletion = $rolepersonnesToDelete;

        foreach ($rolepersonnesToDelete as $rolepersonneRemoved) {
            $rolepersonneRemoved->setSociete(null);
        }

        $this->collRolepersonnes = null;
        foreach ($rolepersonnes as $rolepersonne) {
            $this->addRolepersonne($rolepersonne);
        }

        $this->collRolepersonnes = $rolepersonnes;
        $this->collRolepersonnesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Rolepersonne objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Rolepersonne objects.
     * @throws PropelException
     */
    public function countRolepersonnes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRolepersonnesPartial && !$this->isNew();
        if (null === $this->collRolepersonnes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRolepersonnes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRolepersonnes());
            }

            $query = ChildRolepersonneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySociete($this)
                ->count($con);
        }

        return count($this->collRolepersonnes);
    }

    /**
     * Method called to associate a ChildRolepersonne object to this object
     * through the ChildRolepersonne foreign key attribute.
     *
     * @param  ChildRolepersonne $l ChildRolepersonne
     * @return $this|\Societe The current object (for fluent API support)
     */
    public function addRolepersonne(ChildRolepersonne $l)
    {
        if ($this->collRolepersonnes === null) {
            $this->initRolepersonnes();
            $this->collRolepersonnesPartial = true;
        }

        if (!$this->collRolepersonnes->contains($l)) {
            $this->doAddRolepersonne($l);

            if ($this->rolepersonnesScheduledForDeletion and $this->rolepersonnesScheduledForDeletion->contains($l)) {
                $this->rolepersonnesScheduledForDeletion->remove($this->rolepersonnesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRolepersonne $rolepersonne The ChildRolepersonne object to add.
     */
    protected function doAddRolepersonne(ChildRolepersonne $rolepersonne)
    {
        $this->collRolepersonnes[]= $rolepersonne;
        $rolepersonne->setSociete($this);
    }

    /**
     * @param  ChildRolepersonne $rolepersonne The ChildRolepersonne object to remove.
     * @return $this|ChildSociete The current object (for fluent API support)
     */
    public function removeRolepersonne(ChildRolepersonne $rolepersonne)
    {
        if ($this->getRolepersonnes()->contains($rolepersonne)) {
            $pos = $this->collRolepersonnes->search($rolepersonne);
            $this->collRolepersonnes->remove($pos);
            if (null === $this->rolepersonnesScheduledForDeletion) {
                $this->rolepersonnesScheduledForDeletion = clone $this->collRolepersonnes;
                $this->rolepersonnesScheduledForDeletion->clear();
            }
            $this->rolepersonnesScheduledForDeletion[]= clone $rolepersonne;
            $rolepersonne->setSociete(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aVille) {
            $this->aVille->removeSociete($this);
        }
        $this->idsociete = null;
        $this->libellesociete = null;
        $this->ville_idville = null;
        $this->ville_pays_idpays = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCatgorieproduits) {
                foreach ($this->collCatgorieproduits as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collModepaiements) {
                foreach ($this->collModepaiements as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPersonnes) {
                foreach ($this->collPersonnes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProduitss) {
                foreach ($this->collProduitss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRolepersonnes) {
                foreach ($this->collRolepersonnes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCatgorieproduits = null;
        $this->collModepaiements = null;
        $this->collPersonnes = null;
        $this->collProduitss = null;
        $this->collRolepersonnes = null;
        $this->aVille = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SocieteTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
