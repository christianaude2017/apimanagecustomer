<?php

namespace Base;

use \Versement as ChildVersement;
use \VersementQuery as ChildVersementQuery;
use \Exception;
use \PDO;
use Map\VersementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'versement' table.
 *
 *
 *
 * @method     ChildVersementQuery orderByIdversement($order = Criteria::ASC) Order by the idversement column
 * @method     ChildVersementQuery orderByDateversement($order = Criteria::ASC) Order by the dateVersement column
 * @method     ChildVersementQuery orderByMontantversement($order = Criteria::ASC) Order by the montantversement column
 * @method     ChildVersementQuery orderByNumerocheque($order = Criteria::ASC) Order by the NumeroCheque column
 * @method     ChildVersementQuery orderByVersementIdmois($order = Criteria::ASC) Order by the versement_idmois column
 * @method     ChildVersementQuery orderByContratIdcontrat($order = Criteria::ASC) Order by the Contrat_idContrat column
 * @method     ChildVersementQuery orderByContratProduitsIdproduits($order = Criteria::ASC) Order by the Contrat_produits_idproduits column
 * @method     ChildVersementQuery orderByContratProduitsSocieteIdsociete($order = Criteria::ASC) Order by the Contrat_produits_societe_idsociete column
 * @method     ChildVersementQuery orderByModepaiementIdmodepaiement($order = Criteria::ASC) Order by the modePaiement_idmodePaiement column
 *
 * @method     ChildVersementQuery groupByIdversement() Group by the idversement column
 * @method     ChildVersementQuery groupByDateversement() Group by the dateVersement column
 * @method     ChildVersementQuery groupByMontantversement() Group by the montantversement column
 * @method     ChildVersementQuery groupByNumerocheque() Group by the NumeroCheque column
 * @method     ChildVersementQuery groupByVersementIdmois() Group by the versement_idmois column
 * @method     ChildVersementQuery groupByContratIdcontrat() Group by the Contrat_idContrat column
 * @method     ChildVersementQuery groupByContratProduitsIdproduits() Group by the Contrat_produits_idproduits column
 * @method     ChildVersementQuery groupByContratProduitsSocieteIdsociete() Group by the Contrat_produits_societe_idsociete column
 * @method     ChildVersementQuery groupByModepaiementIdmodepaiement() Group by the modePaiement_idmodePaiement column
 *
 * @method     ChildVersementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVersementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVersementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVersementQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVersementQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVersementQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVersementQuery leftJoinContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Contrat relation
 * @method     ChildVersementQuery rightJoinContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Contrat relation
 * @method     ChildVersementQuery innerJoinContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the Contrat relation
 *
 * @method     ChildVersementQuery joinWithContrat($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Contrat relation
 *
 * @method     ChildVersementQuery leftJoinWithContrat() Adds a LEFT JOIN clause and with to the query using the Contrat relation
 * @method     ChildVersementQuery rightJoinWithContrat() Adds a RIGHT JOIN clause and with to the query using the Contrat relation
 * @method     ChildVersementQuery innerJoinWithContrat() Adds a INNER JOIN clause and with to the query using the Contrat relation
 *
 * @method     ChildVersementQuery leftJoinModepaiement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Modepaiement relation
 * @method     ChildVersementQuery rightJoinModepaiement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Modepaiement relation
 * @method     ChildVersementQuery innerJoinModepaiement($relationAlias = null) Adds a INNER JOIN clause to the query using the Modepaiement relation
 *
 * @method     ChildVersementQuery joinWithModepaiement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Modepaiement relation
 *
 * @method     ChildVersementQuery leftJoinWithModepaiement() Adds a LEFT JOIN clause and with to the query using the Modepaiement relation
 * @method     ChildVersementQuery rightJoinWithModepaiement() Adds a RIGHT JOIN clause and with to the query using the Modepaiement relation
 * @method     ChildVersementQuery innerJoinWithModepaiement() Adds a INNER JOIN clause and with to the query using the Modepaiement relation
 *
 * @method     ChildVersementQuery leftJoinMois($relationAlias = null) Adds a LEFT JOIN clause to the query using the Mois relation
 * @method     ChildVersementQuery rightJoinMois($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Mois relation
 * @method     ChildVersementQuery innerJoinMois($relationAlias = null) Adds a INNER JOIN clause to the query using the Mois relation
 *
 * @method     ChildVersementQuery joinWithMois($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Mois relation
 *
 * @method     ChildVersementQuery leftJoinWithMois() Adds a LEFT JOIN clause and with to the query using the Mois relation
 * @method     ChildVersementQuery rightJoinWithMois() Adds a RIGHT JOIN clause and with to the query using the Mois relation
 * @method     ChildVersementQuery innerJoinWithMois() Adds a INNER JOIN clause and with to the query using the Mois relation
 *
 * @method     \ContratQuery|\ModepaiementQuery|\MoisQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVersement findOne(ConnectionInterface $con = null) Return the first ChildVersement matching the query
 * @method     ChildVersement findOneOrCreate(ConnectionInterface $con = null) Return the first ChildVersement matching the query, or a new ChildVersement object populated from the query conditions when no match is found
 *
 * @method     ChildVersement findOneByIdversement(int $idversement) Return the first ChildVersement filtered by the idversement column
 * @method     ChildVersement findOneByDateversement(string $dateVersement) Return the first ChildVersement filtered by the dateVersement column
 * @method     ChildVersement findOneByMontantversement(int $montantversement) Return the first ChildVersement filtered by the montantversement column
 * @method     ChildVersement findOneByNumerocheque(string $NumeroCheque) Return the first ChildVersement filtered by the NumeroCheque column
 * @method     ChildVersement findOneByVersementIdmois(int $versement_idmois) Return the first ChildVersement filtered by the versement_idmois column
 * @method     ChildVersement findOneByContratIdcontrat(int $Contrat_idContrat) Return the first ChildVersement filtered by the Contrat_idContrat column
 * @method     ChildVersement findOneByContratProduitsIdproduits(int $Contrat_produits_idproduits) Return the first ChildVersement filtered by the Contrat_produits_idproduits column
 * @method     ChildVersement findOneByContratProduitsSocieteIdsociete(int $Contrat_produits_societe_idsociete) Return the first ChildVersement filtered by the Contrat_produits_societe_idsociete column
 * @method     ChildVersement findOneByModepaiementIdmodepaiement(int $modePaiement_idmodePaiement) Return the first ChildVersement filtered by the modePaiement_idmodePaiement column *

 * @method     ChildVersement requirePk($key, ConnectionInterface $con = null) Return the ChildVersement by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOne(ConnectionInterface $con = null) Return the first ChildVersement matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVersement requireOneByIdversement(int $idversement) Return the first ChildVersement filtered by the idversement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOneByDateversement(string $dateVersement) Return the first ChildVersement filtered by the dateVersement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOneByMontantversement(int $montantversement) Return the first ChildVersement filtered by the montantversement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOneByNumerocheque(string $NumeroCheque) Return the first ChildVersement filtered by the NumeroCheque column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOneByVersementIdmois(int $versement_idmois) Return the first ChildVersement filtered by the versement_idmois column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOneByContratIdcontrat(int $Contrat_idContrat) Return the first ChildVersement filtered by the Contrat_idContrat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOneByContratProduitsIdproduits(int $Contrat_produits_idproduits) Return the first ChildVersement filtered by the Contrat_produits_idproduits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOneByContratProduitsSocieteIdsociete(int $Contrat_produits_societe_idsociete) Return the first ChildVersement filtered by the Contrat_produits_societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVersement requireOneByModepaiementIdmodepaiement(int $modePaiement_idmodePaiement) Return the first ChildVersement filtered by the modePaiement_idmodePaiement column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVersement[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildVersement objects based on current ModelCriteria
 * @method     ChildVersement[]|ObjectCollection findByIdversement(int $idversement) Return ChildVersement objects filtered by the idversement column
 * @method     ChildVersement[]|ObjectCollection findByDateversement(string $dateVersement) Return ChildVersement objects filtered by the dateVersement column
 * @method     ChildVersement[]|ObjectCollection findByMontantversement(int $montantversement) Return ChildVersement objects filtered by the montantversement column
 * @method     ChildVersement[]|ObjectCollection findByNumerocheque(string $NumeroCheque) Return ChildVersement objects filtered by the NumeroCheque column
 * @method     ChildVersement[]|ObjectCollection findByVersementIdmois(int $versement_idmois) Return ChildVersement objects filtered by the versement_idmois column
 * @method     ChildVersement[]|ObjectCollection findByContratIdcontrat(int $Contrat_idContrat) Return ChildVersement objects filtered by the Contrat_idContrat column
 * @method     ChildVersement[]|ObjectCollection findByContratProduitsIdproduits(int $Contrat_produits_idproduits) Return ChildVersement objects filtered by the Contrat_produits_idproduits column
 * @method     ChildVersement[]|ObjectCollection findByContratProduitsSocieteIdsociete(int $Contrat_produits_societe_idsociete) Return ChildVersement objects filtered by the Contrat_produits_societe_idsociete column
 * @method     ChildVersement[]|ObjectCollection findByModepaiementIdmodepaiement(int $modePaiement_idmodePaiement) Return ChildVersement objects filtered by the modePaiement_idmodePaiement column
 * @method     ChildVersement[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VersementQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VersementQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Versement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVersementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVersementQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildVersementQuery) {
            return $criteria;
        }
        $query = new ChildVersementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56, 78, 91), $con);
     * </code>
     *
     * @param array[$idversement, $versement_idmois, $Contrat_idContrat, $Contrat_produits_idproduits, $Contrat_produits_societe_idsociete] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVersement|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VersementTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VersementTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2]), (null === $key[3] || is_scalar($key[3]) || is_callable([$key[3], '__toString']) ? (string) $key[3] : $key[3]), (null === $key[4] || is_scalar($key[4]) || is_callable([$key[4], '__toString']) ? (string) $key[4] : $key[4])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVersement A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idversement, dateVersement, montantversement, NumeroCheque, versement_idmois, Contrat_idContrat, Contrat_produits_idproduits, Contrat_produits_societe_idsociete, modePaiement_idmodePaiement FROM versement WHERE idversement = :p0 AND versement_idmois = :p1 AND Contrat_idContrat = :p2 AND Contrat_produits_idproduits = :p3 AND Contrat_produits_societe_idsociete = :p4';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->bindValue(':p3', $key[3], PDO::PARAM_INT);
            $stmt->bindValue(':p4', $key[4], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVersement $obj */
            $obj = new ChildVersement();
            $obj->hydrate($row);
            VersementTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2]), (null === $key[3] || is_scalar($key[3]) || is_callable([$key[3], '__toString']) ? (string) $key[3] : $key[3]), (null === $key[4] || is_scalar($key[4]) || is_callable([$key[4], '__toString']) ? (string) $key[4] : $key[4])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildVersement|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(VersementTableMap::COL_IDVERSEMENT, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(VersementTableMap::COL_VERSEMENT_IDMOIS, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(VersementTableMap::COL_CONTRAT_IDCONTRAT, $key[2], Criteria::EQUAL);
        $this->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $key[3], Criteria::EQUAL);
        $this->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $key[4], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(VersementTableMap::COL_IDVERSEMENT, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(VersementTableMap::COL_VERSEMENT_IDMOIS, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(VersementTableMap::COL_CONTRAT_IDCONTRAT, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $cton3 = $this->getNewCriterion(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $key[3], Criteria::EQUAL);
            $cton0->addAnd($cton3);
            $cton4 = $this->getNewCriterion(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $key[4], Criteria::EQUAL);
            $cton0->addAnd($cton4);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idversement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdversement(1234); // WHERE idversement = 1234
     * $query->filterByIdversement(array(12, 34)); // WHERE idversement IN (12, 34)
     * $query->filterByIdversement(array('min' => 12)); // WHERE idversement > 12
     * </code>
     *
     * @param     mixed $idversement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByIdversement($idversement = null, $comparison = null)
    {
        if (is_array($idversement)) {
            $useMinMax = false;
            if (isset($idversement['min'])) {
                $this->addUsingAlias(VersementTableMap::COL_IDVERSEMENT, $idversement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idversement['max'])) {
                $this->addUsingAlias(VersementTableMap::COL_IDVERSEMENT, $idversement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_IDVERSEMENT, $idversement, $comparison);
    }

    /**
     * Filter the query on the dateVersement column
     *
     * Example usage:
     * <code>
     * $query->filterByDateversement('2011-03-14'); // WHERE dateVersement = '2011-03-14'
     * $query->filterByDateversement('now'); // WHERE dateVersement = '2011-03-14'
     * $query->filterByDateversement(array('max' => 'yesterday')); // WHERE dateVersement > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateversement The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByDateversement($dateversement = null, $comparison = null)
    {
        if (is_array($dateversement)) {
            $useMinMax = false;
            if (isset($dateversement['min'])) {
                $this->addUsingAlias(VersementTableMap::COL_DATEVERSEMENT, $dateversement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateversement['max'])) {
                $this->addUsingAlias(VersementTableMap::COL_DATEVERSEMENT, $dateversement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_DATEVERSEMENT, $dateversement, $comparison);
    }

    /**
     * Filter the query on the montantversement column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantversement(1234); // WHERE montantversement = 1234
     * $query->filterByMontantversement(array(12, 34)); // WHERE montantversement IN (12, 34)
     * $query->filterByMontantversement(array('min' => 12)); // WHERE montantversement > 12
     * </code>
     *
     * @param     mixed $montantversement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByMontantversement($montantversement = null, $comparison = null)
    {
        if (is_array($montantversement)) {
            $useMinMax = false;
            if (isset($montantversement['min'])) {
                $this->addUsingAlias(VersementTableMap::COL_MONTANTVERSEMENT, $montantversement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantversement['max'])) {
                $this->addUsingAlias(VersementTableMap::COL_MONTANTVERSEMENT, $montantversement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_MONTANTVERSEMENT, $montantversement, $comparison);
    }

    /**
     * Filter the query on the NumeroCheque column
     *
     * Example usage:
     * <code>
     * $query->filterByNumerocheque('fooValue');   // WHERE NumeroCheque = 'fooValue'
     * $query->filterByNumerocheque('%fooValue%', Criteria::LIKE); // WHERE NumeroCheque LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numerocheque The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByNumerocheque($numerocheque = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numerocheque)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_NUMEROCHEQUE, $numerocheque, $comparison);
    }

    /**
     * Filter the query on the versement_idmois column
     *
     * Example usage:
     * <code>
     * $query->filterByVersementIdmois(1234); // WHERE versement_idmois = 1234
     * $query->filterByVersementIdmois(array(12, 34)); // WHERE versement_idmois IN (12, 34)
     * $query->filterByVersementIdmois(array('min' => 12)); // WHERE versement_idmois > 12
     * </code>
     *
     * @see       filterByMois()
     *
     * @param     mixed $versementIdmois The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByVersementIdmois($versementIdmois = null, $comparison = null)
    {
        if (is_array($versementIdmois)) {
            $useMinMax = false;
            if (isset($versementIdmois['min'])) {
                $this->addUsingAlias(VersementTableMap::COL_VERSEMENT_IDMOIS, $versementIdmois['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($versementIdmois['max'])) {
                $this->addUsingAlias(VersementTableMap::COL_VERSEMENT_IDMOIS, $versementIdmois['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_VERSEMENT_IDMOIS, $versementIdmois, $comparison);
    }

    /**
     * Filter the query on the Contrat_idContrat column
     *
     * Example usage:
     * <code>
     * $query->filterByContratIdcontrat(1234); // WHERE Contrat_idContrat = 1234
     * $query->filterByContratIdcontrat(array(12, 34)); // WHERE Contrat_idContrat IN (12, 34)
     * $query->filterByContratIdcontrat(array('min' => 12)); // WHERE Contrat_idContrat > 12
     * </code>
     *
     * @see       filterByContrat()
     *
     * @param     mixed $contratIdcontrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByContratIdcontrat($contratIdcontrat = null, $comparison = null)
    {
        if (is_array($contratIdcontrat)) {
            $useMinMax = false;
            if (isset($contratIdcontrat['min'])) {
                $this->addUsingAlias(VersementTableMap::COL_CONTRAT_IDCONTRAT, $contratIdcontrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contratIdcontrat['max'])) {
                $this->addUsingAlias(VersementTableMap::COL_CONTRAT_IDCONTRAT, $contratIdcontrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_CONTRAT_IDCONTRAT, $contratIdcontrat, $comparison);
    }

    /**
     * Filter the query on the Contrat_produits_idproduits column
     *
     * Example usage:
     * <code>
     * $query->filterByContratProduitsIdproduits(1234); // WHERE Contrat_produits_idproduits = 1234
     * $query->filterByContratProduitsIdproduits(array(12, 34)); // WHERE Contrat_produits_idproduits IN (12, 34)
     * $query->filterByContratProduitsIdproduits(array('min' => 12)); // WHERE Contrat_produits_idproduits > 12
     * </code>
     *
     * @see       filterByContrat()
     *
     * @param     mixed $contratProduitsIdproduits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByContratProduitsIdproduits($contratProduitsIdproduits = null, $comparison = null)
    {
        if (is_array($contratProduitsIdproduits)) {
            $useMinMax = false;
            if (isset($contratProduitsIdproduits['min'])) {
                $this->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $contratProduitsIdproduits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contratProduitsIdproduits['max'])) {
                $this->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $contratProduitsIdproduits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $contratProduitsIdproduits, $comparison);
    }

    /**
     * Filter the query on the Contrat_produits_societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterByContratProduitsSocieteIdsociete(1234); // WHERE Contrat_produits_societe_idsociete = 1234
     * $query->filterByContratProduitsSocieteIdsociete(array(12, 34)); // WHERE Contrat_produits_societe_idsociete IN (12, 34)
     * $query->filterByContratProduitsSocieteIdsociete(array('min' => 12)); // WHERE Contrat_produits_societe_idsociete > 12
     * </code>
     *
     * @see       filterByContrat()
     *
     * @param     mixed $contratProduitsSocieteIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByContratProduitsSocieteIdsociete($contratProduitsSocieteIdsociete = null, $comparison = null)
    {
        if (is_array($contratProduitsSocieteIdsociete)) {
            $useMinMax = false;
            if (isset($contratProduitsSocieteIdsociete['min'])) {
                $this->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $contratProduitsSocieteIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contratProduitsSocieteIdsociete['max'])) {
                $this->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $contratProduitsSocieteIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $contratProduitsSocieteIdsociete, $comparison);
    }

    /**
     * Filter the query on the modePaiement_idmodePaiement column
     *
     * Example usage:
     * <code>
     * $query->filterByModepaiementIdmodepaiement(1234); // WHERE modePaiement_idmodePaiement = 1234
     * $query->filterByModepaiementIdmodepaiement(array(12, 34)); // WHERE modePaiement_idmodePaiement IN (12, 34)
     * $query->filterByModepaiementIdmodepaiement(array('min' => 12)); // WHERE modePaiement_idmodePaiement > 12
     * </code>
     *
     * @see       filterByModepaiement()
     *
     * @param     mixed $modepaiementIdmodepaiement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function filterByModepaiementIdmodepaiement($modepaiementIdmodepaiement = null, $comparison = null)
    {
        if (is_array($modepaiementIdmodepaiement)) {
            $useMinMax = false;
            if (isset($modepaiementIdmodepaiement['min'])) {
                $this->addUsingAlias(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT, $modepaiementIdmodepaiement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modepaiementIdmodepaiement['max'])) {
                $this->addUsingAlias(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT, $modepaiementIdmodepaiement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT, $modepaiementIdmodepaiement, $comparison);
    }

    /**
     * Filter the query by a related \Contrat object
     *
     * @param \Contrat $contrat The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVersementQuery The current query, for fluid interface
     */
    public function filterByContrat($contrat, $comparison = null)
    {
        if ($contrat instanceof \Contrat) {
            return $this
                ->addUsingAlias(VersementTableMap::COL_CONTRAT_IDCONTRAT, $contrat->getIdcontrat(), $comparison)
                ->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $contrat->getProduitsIdproduits1(), $comparison)
                ->addUsingAlias(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $contrat->getProduitsSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByContrat() only accepts arguments of type \Contrat');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Contrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function joinContrat($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Contrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Contrat');
        }

        return $this;
    }

    /**
     * Use the Contrat relation Contrat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ContratQuery A secondary query class using the current class as primary query
     */
    public function useContratQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Contrat', '\ContratQuery');
    }

    /**
     * Filter the query by a related \Modepaiement object
     *
     * @param \Modepaiement|ObjectCollection $modepaiement The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVersementQuery The current query, for fluid interface
     */
    public function filterByModepaiement($modepaiement, $comparison = null)
    {
        if ($modepaiement instanceof \Modepaiement) {
            return $this
                ->addUsingAlias(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT, $modepaiement->getIdmodepaiement(), $comparison);
        } elseif ($modepaiement instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT, $modepaiement->toKeyValue('PrimaryKey', 'Idmodepaiement'), $comparison);
        } else {
            throw new PropelException('filterByModepaiement() only accepts arguments of type \Modepaiement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Modepaiement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function joinModepaiement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Modepaiement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Modepaiement');
        }

        return $this;
    }

    /**
     * Use the Modepaiement relation Modepaiement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ModepaiementQuery A secondary query class using the current class as primary query
     */
    public function useModepaiementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinModepaiement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Modepaiement', '\ModepaiementQuery');
    }

    /**
     * Filter the query by a related \Mois object
     *
     * @param \Mois|ObjectCollection $mois The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVersementQuery The current query, for fluid interface
     */
    public function filterByMois($mois, $comparison = null)
    {
        if ($mois instanceof \Mois) {
            return $this
                ->addUsingAlias(VersementTableMap::COL_VERSEMENT_IDMOIS, $mois->getIdmois(), $comparison);
        } elseif ($mois instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(VersementTableMap::COL_VERSEMENT_IDMOIS, $mois->toKeyValue('PrimaryKey', 'Idmois'), $comparison);
        } else {
            throw new PropelException('filterByMois() only accepts arguments of type \Mois or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Mois relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function joinMois($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Mois');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Mois');
        }

        return $this;
    }

    /**
     * Use the Mois relation Mois object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \MoisQuery A secondary query class using the current class as primary query
     */
    public function useMoisQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMois($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Mois', '\MoisQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildVersement $versement Object to remove from the list of results
     *
     * @return $this|ChildVersementQuery The current query, for fluid interface
     */
    public function prune($versement = null)
    {
        if ($versement) {
            $this->addCond('pruneCond0', $this->getAliasedColName(VersementTableMap::COL_IDVERSEMENT), $versement->getIdversement(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(VersementTableMap::COL_VERSEMENT_IDMOIS), $versement->getVersementIdmois(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(VersementTableMap::COL_CONTRAT_IDCONTRAT), $versement->getContratIdcontrat(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond3', $this->getAliasedColName(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS), $versement->getContratProduitsIdproduits(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond4', $this->getAliasedColName(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE), $versement->getContratProduitsSocieteIdsociete(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2', 'pruneCond3', 'pruneCond4'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the versement table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VersementTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VersementTableMap::clearInstancePool();
            VersementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VersementTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VersementTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VersementTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VersementTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // VersementQuery
