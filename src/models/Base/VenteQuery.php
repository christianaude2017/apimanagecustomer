<?php

namespace Base;

use \Vente as ChildVente;
use \VenteQuery as ChildVenteQuery;
use \Exception;
use \PDO;
use Map\VenteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'vente' table.
 *
 *
 *
 * @method     ChildVenteQuery orderByIdvente($order = Criteria::ASC) Order by the idvente column
 * @method     ChildVenteQuery orderByProduitsIdproduits($order = Criteria::ASC) Order by the produits_idproduits column
 * @method     ChildVenteQuery orderByProduitsSocieteIdsociete($order = Criteria::ASC) Order by the produits_societe_idsociete column
 * @method     ChildVenteQuery orderByDatevente($order = Criteria::ASC) Order by the Datevente column
 * @method     ChildVenteQuery orderByQuantitevente($order = Criteria::ASC) Order by the Quantitevente column
 * @method     ChildVenteQuery orderByMontantvente($order = Criteria::ASC) Order by the Montantvente column
 * @method     ChildVenteQuery orderByPersonneIdpersonne($order = Criteria::ASC) Order by the Personne_idPersonne column
 * @method     ChildVenteQuery orderByPersonneRolepersonneIdrolepersonne($order = Criteria::ASC) Order by the Personne_rolePersonne_idrolePersonne column
 *
 * @method     ChildVenteQuery groupByIdvente() Group by the idvente column
 * @method     ChildVenteQuery groupByProduitsIdproduits() Group by the produits_idproduits column
 * @method     ChildVenteQuery groupByProduitsSocieteIdsociete() Group by the produits_societe_idsociete column
 * @method     ChildVenteQuery groupByDatevente() Group by the Datevente column
 * @method     ChildVenteQuery groupByQuantitevente() Group by the Quantitevente column
 * @method     ChildVenteQuery groupByMontantvente() Group by the Montantvente column
 * @method     ChildVenteQuery groupByPersonneIdpersonne() Group by the Personne_idPersonne column
 * @method     ChildVenteQuery groupByPersonneRolepersonneIdrolepersonne() Group by the Personne_rolePersonne_idrolePersonne column
 *
 * @method     ChildVenteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildVenteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildVenteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildVenteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildVenteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildVenteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildVenteQuery leftJoinPersonne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Personne relation
 * @method     ChildVenteQuery rightJoinPersonne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Personne relation
 * @method     ChildVenteQuery innerJoinPersonne($relationAlias = null) Adds a INNER JOIN clause to the query using the Personne relation
 *
 * @method     ChildVenteQuery joinWithPersonne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Personne relation
 *
 * @method     ChildVenteQuery leftJoinWithPersonne() Adds a LEFT JOIN clause and with to the query using the Personne relation
 * @method     ChildVenteQuery rightJoinWithPersonne() Adds a RIGHT JOIN clause and with to the query using the Personne relation
 * @method     ChildVenteQuery innerJoinWithPersonne() Adds a INNER JOIN clause and with to the query using the Personne relation
 *
 * @method     ChildVenteQuery leftJoinProduits($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produits relation
 * @method     ChildVenteQuery rightJoinProduits($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produits relation
 * @method     ChildVenteQuery innerJoinProduits($relationAlias = null) Adds a INNER JOIN clause to the query using the Produits relation
 *
 * @method     ChildVenteQuery joinWithProduits($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produits relation
 *
 * @method     ChildVenteQuery leftJoinWithProduits() Adds a LEFT JOIN clause and with to the query using the Produits relation
 * @method     ChildVenteQuery rightJoinWithProduits() Adds a RIGHT JOIN clause and with to the query using the Produits relation
 * @method     ChildVenteQuery innerJoinWithProduits() Adds a INNER JOIN clause and with to the query using the Produits relation
 *
 * @method     \PersonneQuery|\ProduitsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildVente findOne(ConnectionInterface $con = null) Return the first ChildVente matching the query
 * @method     ChildVente findOneOrCreate(ConnectionInterface $con = null) Return the first ChildVente matching the query, or a new ChildVente object populated from the query conditions when no match is found
 *
 * @method     ChildVente findOneByIdvente(int $idvente) Return the first ChildVente filtered by the idvente column
 * @method     ChildVente findOneByProduitsIdproduits(int $produits_idproduits) Return the first ChildVente filtered by the produits_idproduits column
 * @method     ChildVente findOneByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return the first ChildVente filtered by the produits_societe_idsociete column
 * @method     ChildVente findOneByDatevente(string $Datevente) Return the first ChildVente filtered by the Datevente column
 * @method     ChildVente findOneByQuantitevente(int $Quantitevente) Return the first ChildVente filtered by the Quantitevente column
 * @method     ChildVente findOneByMontantvente(int $Montantvente) Return the first ChildVente filtered by the Montantvente column
 * @method     ChildVente findOneByPersonneIdpersonne(int $Personne_idPersonne) Return the first ChildVente filtered by the Personne_idPersonne column
 * @method     ChildVente findOneByPersonneRolepersonneIdrolepersonne(int $Personne_rolePersonne_idrolePersonne) Return the first ChildVente filtered by the Personne_rolePersonne_idrolePersonne column *

 * @method     ChildVente requirePk($key, ConnectionInterface $con = null) Return the ChildVente by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVente requireOne(ConnectionInterface $con = null) Return the first ChildVente matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVente requireOneByIdvente(int $idvente) Return the first ChildVente filtered by the idvente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVente requireOneByProduitsIdproduits(int $produits_idproduits) Return the first ChildVente filtered by the produits_idproduits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVente requireOneByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return the first ChildVente filtered by the produits_societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVente requireOneByDatevente(string $Datevente) Return the first ChildVente filtered by the Datevente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVente requireOneByQuantitevente(int $Quantitevente) Return the first ChildVente filtered by the Quantitevente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVente requireOneByMontantvente(int $Montantvente) Return the first ChildVente filtered by the Montantvente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVente requireOneByPersonneIdpersonne(int $Personne_idPersonne) Return the first ChildVente filtered by the Personne_idPersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildVente requireOneByPersonneRolepersonneIdrolepersonne(int $Personne_rolePersonne_idrolePersonne) Return the first ChildVente filtered by the Personne_rolePersonne_idrolePersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildVente[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildVente objects based on current ModelCriteria
 * @method     ChildVente[]|ObjectCollection findByIdvente(int $idvente) Return ChildVente objects filtered by the idvente column
 * @method     ChildVente[]|ObjectCollection findByProduitsIdproduits(int $produits_idproduits) Return ChildVente objects filtered by the produits_idproduits column
 * @method     ChildVente[]|ObjectCollection findByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return ChildVente objects filtered by the produits_societe_idsociete column
 * @method     ChildVente[]|ObjectCollection findByDatevente(string $Datevente) Return ChildVente objects filtered by the Datevente column
 * @method     ChildVente[]|ObjectCollection findByQuantitevente(int $Quantitevente) Return ChildVente objects filtered by the Quantitevente column
 * @method     ChildVente[]|ObjectCollection findByMontantvente(int $Montantvente) Return ChildVente objects filtered by the Montantvente column
 * @method     ChildVente[]|ObjectCollection findByPersonneIdpersonne(int $Personne_idPersonne) Return ChildVente objects filtered by the Personne_idPersonne column
 * @method     ChildVente[]|ObjectCollection findByPersonneRolepersonneIdrolepersonne(int $Personne_rolePersonne_idrolePersonne) Return ChildVente objects filtered by the Personne_rolePersonne_idrolePersonne column
 * @method     ChildVente[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class VenteQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\VenteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Vente', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildVenteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildVenteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildVenteQuery) {
            return $criteria;
        }
        $query = new ChildVenteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56, 78, 91), $con);
     * </code>
     *
     * @param array[$idvente, $produits_idproduits, $produits_societe_idsociete, $Personne_idPersonne, $Personne_rolePersonne_idrolePersonne] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildVente|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VenteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = VenteTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2]), (null === $key[3] || is_scalar($key[3]) || is_callable([$key[3], '__toString']) ? (string) $key[3] : $key[3]), (null === $key[4] || is_scalar($key[4]) || is_callable([$key[4], '__toString']) ? (string) $key[4] : $key[4])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVente A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idvente, produits_idproduits, produits_societe_idsociete, Datevente, Quantitevente, Montantvente, Personne_idPersonne, Personne_rolePersonne_idrolePersonne FROM vente WHERE idvente = :p0 AND produits_idproduits = :p1 AND produits_societe_idsociete = :p2 AND Personne_idPersonne = :p3 AND Personne_rolePersonne_idrolePersonne = :p4';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->bindValue(':p3', $key[3], PDO::PARAM_INT);
            $stmt->bindValue(':p4', $key[4], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildVente $obj */
            $obj = new ChildVente();
            $obj->hydrate($row);
            VenteTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2]), (null === $key[3] || is_scalar($key[3]) || is_callable([$key[3], '__toString']) ? (string) $key[3] : $key[3]), (null === $key[4] || is_scalar($key[4]) || is_callable([$key[4], '__toString']) ? (string) $key[4] : $key[4])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildVente|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(VenteTableMap::COL_IDVENTE, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(VenteTableMap::COL_PRODUITS_IDPRODUITS, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(VenteTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $key[2], Criteria::EQUAL);
        $this->addUsingAlias(VenteTableMap::COL_PERSONNE_IDPERSONNE, $key[3], Criteria::EQUAL);
        $this->addUsingAlias(VenteTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $key[4], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(VenteTableMap::COL_IDVENTE, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(VenteTableMap::COL_PRODUITS_IDPRODUITS, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(VenteTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $cton3 = $this->getNewCriterion(VenteTableMap::COL_PERSONNE_IDPERSONNE, $key[3], Criteria::EQUAL);
            $cton0->addAnd($cton3);
            $cton4 = $this->getNewCriterion(VenteTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $key[4], Criteria::EQUAL);
            $cton0->addAnd($cton4);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idvente column
     *
     * Example usage:
     * <code>
     * $query->filterByIdvente(1234); // WHERE idvente = 1234
     * $query->filterByIdvente(array(12, 34)); // WHERE idvente IN (12, 34)
     * $query->filterByIdvente(array('min' => 12)); // WHERE idvente > 12
     * </code>
     *
     * @param     mixed $idvente The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByIdvente($idvente = null, $comparison = null)
    {
        if (is_array($idvente)) {
            $useMinMax = false;
            if (isset($idvente['min'])) {
                $this->addUsingAlias(VenteTableMap::COL_IDVENTE, $idvente['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idvente['max'])) {
                $this->addUsingAlias(VenteTableMap::COL_IDVENTE, $idvente['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VenteTableMap::COL_IDVENTE, $idvente, $comparison);
    }

    /**
     * Filter the query on the produits_idproduits column
     *
     * Example usage:
     * <code>
     * $query->filterByProduitsIdproduits(1234); // WHERE produits_idproduits = 1234
     * $query->filterByProduitsIdproduits(array(12, 34)); // WHERE produits_idproduits IN (12, 34)
     * $query->filterByProduitsIdproduits(array('min' => 12)); // WHERE produits_idproduits > 12
     * </code>
     *
     * @see       filterByProduits()
     *
     * @param     mixed $produitsIdproduits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByProduitsIdproduits($produitsIdproduits = null, $comparison = null)
    {
        if (is_array($produitsIdproduits)) {
            $useMinMax = false;
            if (isset($produitsIdproduits['min'])) {
                $this->addUsingAlias(VenteTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produitsIdproduits['max'])) {
                $this->addUsingAlias(VenteTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VenteTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits, $comparison);
    }

    /**
     * Filter the query on the produits_societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterByProduitsSocieteIdsociete(1234); // WHERE produits_societe_idsociete = 1234
     * $query->filterByProduitsSocieteIdsociete(array(12, 34)); // WHERE produits_societe_idsociete IN (12, 34)
     * $query->filterByProduitsSocieteIdsociete(array('min' => 12)); // WHERE produits_societe_idsociete > 12
     * </code>
     *
     * @see       filterByProduits()
     *
     * @param     mixed $produitsSocieteIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByProduitsSocieteIdsociete($produitsSocieteIdsociete = null, $comparison = null)
    {
        if (is_array($produitsSocieteIdsociete)) {
            $useMinMax = false;
            if (isset($produitsSocieteIdsociete['min'])) {
                $this->addUsingAlias(VenteTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produitsSocieteIdsociete['max'])) {
                $this->addUsingAlias(VenteTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VenteTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete, $comparison);
    }

    /**
     * Filter the query on the Datevente column
     *
     * Example usage:
     * <code>
     * $query->filterByDatevente('fooValue');   // WHERE Datevente = 'fooValue'
     * $query->filterByDatevente('%fooValue%', Criteria::LIKE); // WHERE Datevente LIKE '%fooValue%'
     * </code>
     *
     * @param     string $datevente The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByDatevente($datevente = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($datevente)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VenteTableMap::COL_DATEVENTE, $datevente, $comparison);
    }

    /**
     * Filter the query on the Quantitevente column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantitevente(1234); // WHERE Quantitevente = 1234
     * $query->filterByQuantitevente(array(12, 34)); // WHERE Quantitevente IN (12, 34)
     * $query->filterByQuantitevente(array('min' => 12)); // WHERE Quantitevente > 12
     * </code>
     *
     * @param     mixed $quantitevente The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByQuantitevente($quantitevente = null, $comparison = null)
    {
        if (is_array($quantitevente)) {
            $useMinMax = false;
            if (isset($quantitevente['min'])) {
                $this->addUsingAlias(VenteTableMap::COL_QUANTITEVENTE, $quantitevente['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantitevente['max'])) {
                $this->addUsingAlias(VenteTableMap::COL_QUANTITEVENTE, $quantitevente['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VenteTableMap::COL_QUANTITEVENTE, $quantitevente, $comparison);
    }

    /**
     * Filter the query on the Montantvente column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantvente(1234); // WHERE Montantvente = 1234
     * $query->filterByMontantvente(array(12, 34)); // WHERE Montantvente IN (12, 34)
     * $query->filterByMontantvente(array('min' => 12)); // WHERE Montantvente > 12
     * </code>
     *
     * @param     mixed $montantvente The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByMontantvente($montantvente = null, $comparison = null)
    {
        if (is_array($montantvente)) {
            $useMinMax = false;
            if (isset($montantvente['min'])) {
                $this->addUsingAlias(VenteTableMap::COL_MONTANTVENTE, $montantvente['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantvente['max'])) {
                $this->addUsingAlias(VenteTableMap::COL_MONTANTVENTE, $montantvente['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VenteTableMap::COL_MONTANTVENTE, $montantvente, $comparison);
    }

    /**
     * Filter the query on the Personne_idPersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonneIdpersonne(1234); // WHERE Personne_idPersonne = 1234
     * $query->filterByPersonneIdpersonne(array(12, 34)); // WHERE Personne_idPersonne IN (12, 34)
     * $query->filterByPersonneIdpersonne(array('min' => 12)); // WHERE Personne_idPersonne > 12
     * </code>
     *
     * @see       filterByPersonne()
     *
     * @param     mixed $personneIdpersonne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByPersonneIdpersonne($personneIdpersonne = null, $comparison = null)
    {
        if (is_array($personneIdpersonne)) {
            $useMinMax = false;
            if (isset($personneIdpersonne['min'])) {
                $this->addUsingAlias(VenteTableMap::COL_PERSONNE_IDPERSONNE, $personneIdpersonne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personneIdpersonne['max'])) {
                $this->addUsingAlias(VenteTableMap::COL_PERSONNE_IDPERSONNE, $personneIdpersonne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VenteTableMap::COL_PERSONNE_IDPERSONNE, $personneIdpersonne, $comparison);
    }

    /**
     * Filter the query on the Personne_rolePersonne_idrolePersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonneRolepersonneIdrolepersonne(1234); // WHERE Personne_rolePersonne_idrolePersonne = 1234
     * $query->filterByPersonneRolepersonneIdrolepersonne(array(12, 34)); // WHERE Personne_rolePersonne_idrolePersonne IN (12, 34)
     * $query->filterByPersonneRolepersonneIdrolepersonne(array('min' => 12)); // WHERE Personne_rolePersonne_idrolePersonne > 12
     * </code>
     *
     * @see       filterByPersonne()
     *
     * @param     mixed $personneRolepersonneIdrolepersonne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function filterByPersonneRolepersonneIdrolepersonne($personneRolepersonneIdrolepersonne = null, $comparison = null)
    {
        if (is_array($personneRolepersonneIdrolepersonne)) {
            $useMinMax = false;
            if (isset($personneRolepersonneIdrolepersonne['min'])) {
                $this->addUsingAlias(VenteTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $personneRolepersonneIdrolepersonne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personneRolepersonneIdrolepersonne['max'])) {
                $this->addUsingAlias(VenteTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $personneRolepersonneIdrolepersonne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(VenteTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $personneRolepersonneIdrolepersonne, $comparison);
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne $personne The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVenteQuery The current query, for fluid interface
     */
    public function filterByPersonne($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(VenteTableMap::COL_PERSONNE_IDPERSONNE, $personne->getIdpersonne(), $comparison)
                ->addUsingAlias(VenteTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $personne->getRolepersonneIdrolepersonne(), $comparison);
        } else {
            throw new PropelException('filterByPersonne() only accepts arguments of type \Personne');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Personne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function joinPersonne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Personne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Personne');
        }

        return $this;
    }

    /**
     * Use the Personne relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Personne', '\PersonneQuery');
    }

    /**
     * Filter the query by a related \Produits object
     *
     * @param \Produits $produits The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildVenteQuery The current query, for fluid interface
     */
    public function filterByProduits($produits, $comparison = null)
    {
        if ($produits instanceof \Produits) {
            return $this
                ->addUsingAlias(VenteTableMap::COL_PRODUITS_IDPRODUITS, $produits->getIdproduits(), $comparison)
                ->addUsingAlias(VenteTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produits->getSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByProduits() only accepts arguments of type \Produits');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produits relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function joinProduits($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produits');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produits');
        }

        return $this;
    }

    /**
     * Use the Produits relation Produits object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProduitsQuery A secondary query class using the current class as primary query
     */
    public function useProduitsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduits($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produits', '\ProduitsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildVente $vente Object to remove from the list of results
     *
     * @return $this|ChildVenteQuery The current query, for fluid interface
     */
    public function prune($vente = null)
    {
        if ($vente) {
            $this->addCond('pruneCond0', $this->getAliasedColName(VenteTableMap::COL_IDVENTE), $vente->getIdvente(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(VenteTableMap::COL_PRODUITS_IDPRODUITS), $vente->getProduitsIdproduits(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(VenteTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE), $vente->getProduitsSocieteIdsociete(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond3', $this->getAliasedColName(VenteTableMap::COL_PERSONNE_IDPERSONNE), $vente->getPersonneIdpersonne(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond4', $this->getAliasedColName(VenteTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE), $vente->getPersonneRolepersonneIdrolepersonne(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2', 'pruneCond3', 'pruneCond4'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the vente table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VenteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            VenteTableMap::clearInstancePool();
            VenteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VenteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(VenteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            VenteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            VenteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // VenteQuery
