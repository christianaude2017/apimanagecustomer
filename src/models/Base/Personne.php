<?php

namespace Base;

use \Commune as ChildCommune;
use \CommuneQuery as ChildCommuneQuery;
use \Contrat as ChildContrat;
use \ContratQuery as ChildContratQuery;
use \Personne as ChildPersonne;
use \PersonneQuery as ChildPersonneQuery;
use \Prospection as ChildProspection;
use \ProspectionQuery as ChildProspectionQuery;
use \ResponsableAgent as ChildResponsableAgent;
use \ResponsableAgentQuery as ChildResponsableAgentQuery;
use \Rolepersonne as ChildRolepersonne;
use \RolepersonneQuery as ChildRolepersonneQuery;
use \Societe as ChildSociete;
use \SocieteQuery as ChildSocieteQuery;
use \Vente as ChildVente;
use \VenteQuery as ChildVenteQuery;
use \Ville as ChildVille;
use \VilleQuery as ChildVilleQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\ContratTableMap;
use Map\PersonneTableMap;
use Map\ProspectionTableMap;
use Map\ResponsableAgentTableMap;
use Map\VenteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'personne' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Personne implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\PersonneTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idpersonne field.
     *
     * @var        int
     */
    protected $idpersonne;

    /**
     * The value for the nompersonne field.
     *
     * @var        string
     */
    protected $nompersonne;

    /**
     * The value for the prenompersonne field.
     *
     * @var        string
     */
    protected $prenompersonne;

    /**
     * The value for the sexe field.
     *
     * @var        string
     */
    protected $sexe;

    /**
     * The value for the datenaissance field.
     *
     * @var        DateTime
     */
    protected $datenaissance;

    /**
     * The value for the rolepersonne_idrolepersonne field.
     *
     * @var        int
     */
    protected $rolepersonne_idrolepersonne;

    /**
     * The value for the commune_idcommune field.
     *
     * @var        int
     */
    protected $commune_idcommune;

    /**
     * The value for the ville_idville field.
     *
     * @var        int
     */
    protected $ville_idville;

    /**
     * The value for the ville_pays_idpays field.
     *
     * @var        int
     */
    protected $ville_pays_idpays;

    /**
     * The value for the societe_idsociete field.
     *
     * @var        int
     */
    protected $societe_idsociete;

    /**
     * @var        ChildCommune
     */
    protected $aCommune;

    /**
     * @var        ChildRolepersonne
     */
    protected $aRolepersonne;

    /**
     * @var        ChildSociete
     */
    protected $aSociete;

    /**
     * @var        ChildVille
     */
    protected $aVille;

    /**
     * @var        ObjectCollection|ChildContrat[] Collection to store aggregation of ChildContrat objects.
     */
    protected $collContratsRelatedByPersonneClientIdrolepersonneClient;
    protected $collContratsRelatedByPersonneClientIdrolepersonneClientPartial;

    /**
     * @var        ObjectCollection|ChildContrat[] Collection to store aggregation of ChildContrat objects.
     */
    protected $collContratsRelatedByPersonneAgentIdrolepersonneAgent;
    protected $collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial;

    /**
     * @var        ObjectCollection|ChildProspection[] Collection to store aggregation of ChildProspection objects.
     */
    protected $collProspections;
    protected $collProspectionsPartial;

    /**
     * @var        ObjectCollection|ChildResponsableAgent[] Collection to store aggregation of ChildResponsableAgent objects.
     */
    protected $collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable;
    protected $collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial;

    /**
     * @var        ObjectCollection|ChildResponsableAgent[] Collection to store aggregation of ChildResponsableAgent objects.
     */
    protected $collResponsableAgentsRelatedByPersonneIdagentIdroleagent;
    protected $collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial;

    /**
     * @var        ObjectCollection|ChildVente[] Collection to store aggregation of ChildVente objects.
     */
    protected $collVentes;
    protected $collVentesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContrat[]
     */
    protected $contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildContrat[]
     */
    protected $contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProspection[]
     */
    protected $prospectionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildResponsableAgent[]
     */
    protected $responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildResponsableAgent[]
     */
    protected $responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildVente[]
     */
    protected $ventesScheduledForDeletion = null;

    /**
     * Initializes internal state of Base\Personne object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Personne</code> instance.  If
     * <code>obj</code> is an instance of <code>Personne</code>, delegates to
     * <code>equals(Personne)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Personne The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idpersonne] column value.
     *
     * @return int
     */
    public function getIdpersonne()
    {
        return $this->idpersonne;
    }

    /**
     * Get the [nompersonne] column value.
     *
     * @return string
     */
    public function getNompersonne()
    {
        return $this->nompersonne;
    }

    /**
     * Get the [prenompersonne] column value.
     *
     * @return string
     */
    public function getPrenompersonne()
    {
        return $this->prenompersonne;
    }

    /**
     * Get the [sexe] column value.
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Get the [optionally formatted] temporal [datenaissance] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatenaissance($format = NULL)
    {
        if ($format === null) {
            return $this->datenaissance;
        } else {
            return $this->datenaissance instanceof \DateTimeInterface ? $this->datenaissance->format($format) : null;
        }
    }

    /**
     * Get the [rolepersonne_idrolepersonne] column value.
     *
     * @return int
     */
    public function getRolepersonneIdrolepersonne()
    {
        return $this->rolepersonne_idrolepersonne;
    }

    /**
     * Get the [commune_idcommune] column value.
     *
     * @return int
     */
    public function getCommuneIdcommune()
    {
        return $this->commune_idcommune;
    }

    /**
     * Get the [ville_idville] column value.
     *
     * @return int
     */
    public function getVilleIdville()
    {
        return $this->ville_idville;
    }

    /**
     * Get the [ville_pays_idpays] column value.
     *
     * @return int
     */
    public function getVillePaysIdpays()
    {
        return $this->ville_pays_idpays;
    }

    /**
     * Get the [societe_idsociete] column value.
     *
     * @return int
     */
    public function getSocieteIdsociete()
    {
        return $this->societe_idsociete;
    }

    /**
     * Set the value of [idpersonne] column.
     *
     * @param int $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setIdpersonne($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idpersonne !== $v) {
            $this->idpersonne = $v;
            $this->modifiedColumns[PersonneTableMap::COL_IDPERSONNE] = true;
        }

        return $this;
    } // setIdpersonne()

    /**
     * Set the value of [nompersonne] column.
     *
     * @param string $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setNompersonne($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nompersonne !== $v) {
            $this->nompersonne = $v;
            $this->modifiedColumns[PersonneTableMap::COL_NOMPERSONNE] = true;
        }

        return $this;
    } // setNompersonne()

    /**
     * Set the value of [prenompersonne] column.
     *
     * @param string $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setPrenompersonne($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prenompersonne !== $v) {
            $this->prenompersonne = $v;
            $this->modifiedColumns[PersonneTableMap::COL_PRENOMPERSONNE] = true;
        }

        return $this;
    } // setPrenompersonne()

    /**
     * Set the value of [sexe] column.
     *
     * @param string $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setSexe($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sexe !== $v) {
            $this->sexe = $v;
            $this->modifiedColumns[PersonneTableMap::COL_SEXE] = true;
        }

        return $this;
    } // setSexe()

    /**
     * Sets the value of [datenaissance] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setDatenaissance($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->datenaissance !== null || $dt !== null) {
            if ($this->datenaissance === null || $dt === null || $dt->format("Y-m-d") !== $this->datenaissance->format("Y-m-d")) {
                $this->datenaissance = $dt === null ? null : clone $dt;
                $this->modifiedColumns[PersonneTableMap::COL_DATENAISSANCE] = true;
            }
        } // if either are not null

        return $this;
    } // setDatenaissance()

    /**
     * Set the value of [rolepersonne_idrolepersonne] column.
     *
     * @param int $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setRolepersonneIdrolepersonne($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->rolepersonne_idrolepersonne !== $v) {
            $this->rolepersonne_idrolepersonne = $v;
            $this->modifiedColumns[PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE] = true;
        }

        if ($this->aRolepersonne !== null && $this->aRolepersonne->getIdrolepersonne() !== $v) {
            $this->aRolepersonne = null;
        }

        return $this;
    } // setRolepersonneIdrolepersonne()

    /**
     * Set the value of [commune_idcommune] column.
     *
     * @param int $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setCommuneIdcommune($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->commune_idcommune !== $v) {
            $this->commune_idcommune = $v;
            $this->modifiedColumns[PersonneTableMap::COL_COMMUNE_IDCOMMUNE] = true;
        }

        if ($this->aCommune !== null && $this->aCommune->getIdcommune() !== $v) {
            $this->aCommune = null;
        }

        return $this;
    } // setCommuneIdcommune()

    /**
     * Set the value of [ville_idville] column.
     *
     * @param int $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setVilleIdville($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ville_idville !== $v) {
            $this->ville_idville = $v;
            $this->modifiedColumns[PersonneTableMap::COL_VILLE_IDVILLE] = true;
        }

        if ($this->aVille !== null && $this->aVille->getIdville() !== $v) {
            $this->aVille = null;
        }

        return $this;
    } // setVilleIdville()

    /**
     * Set the value of [ville_pays_idpays] column.
     *
     * @param int $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setVillePaysIdpays($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ville_pays_idpays !== $v) {
            $this->ville_pays_idpays = $v;
            $this->modifiedColumns[PersonneTableMap::COL_VILLE_PAYS_IDPAYS] = true;
        }

        if ($this->aVille !== null && $this->aVille->getPaysIdpays() !== $v) {
            $this->aVille = null;
        }

        return $this;
    } // setVillePaysIdpays()

    /**
     * Set the value of [societe_idsociete] column.
     *
     * @param int $v new value
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function setSocieteIdsociete($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->societe_idsociete !== $v) {
            $this->societe_idsociete = $v;
            $this->modifiedColumns[PersonneTableMap::COL_SOCIETE_IDSOCIETE] = true;
        }

        if ($this->aSociete !== null && $this->aSociete->getIdsociete() !== $v) {
            $this->aSociete = null;
        }

        return $this;
    } // setSocieteIdsociete()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PersonneTableMap::translateFieldName('Idpersonne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idpersonne = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PersonneTableMap::translateFieldName('Nompersonne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nompersonne = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PersonneTableMap::translateFieldName('Prenompersonne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prenompersonne = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PersonneTableMap::translateFieldName('Sexe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sexe = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PersonneTableMap::translateFieldName('Datenaissance', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->datenaissance = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PersonneTableMap::translateFieldName('RolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)];
            $this->rolepersonne_idrolepersonne = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : PersonneTableMap::translateFieldName('CommuneIdcommune', TableMap::TYPE_PHPNAME, $indexType)];
            $this->commune_idcommune = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : PersonneTableMap::translateFieldName('VilleIdville', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ville_idville = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : PersonneTableMap::translateFieldName('VillePaysIdpays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ville_pays_idpays = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : PersonneTableMap::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)];
            $this->societe_idsociete = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = PersonneTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Personne'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aRolepersonne !== null && $this->rolepersonne_idrolepersonne !== $this->aRolepersonne->getIdrolepersonne()) {
            $this->aRolepersonne = null;
        }
        if ($this->aCommune !== null && $this->commune_idcommune !== $this->aCommune->getIdcommune()) {
            $this->aCommune = null;
        }
        if ($this->aVille !== null && $this->ville_idville !== $this->aVille->getIdville()) {
            $this->aVille = null;
        }
        if ($this->aVille !== null && $this->ville_pays_idpays !== $this->aVille->getPaysIdpays()) {
            $this->aVille = null;
        }
        if ($this->aSociete !== null && $this->societe_idsociete !== $this->aSociete->getIdsociete()) {
            $this->aSociete = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PersonneTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPersonneQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommune = null;
            $this->aRolepersonne = null;
            $this->aSociete = null;
            $this->aVille = null;
            $this->collContratsRelatedByPersonneClientIdrolepersonneClient = null;

            $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent = null;

            $this->collProspections = null;

            $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable = null;

            $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent = null;

            $this->collVentes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Personne::setDeleted()
     * @see Personne::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersonneTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPersonneQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PersonneTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PersonneTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommune !== null) {
                if ($this->aCommune->isModified() || $this->aCommune->isNew()) {
                    $affectedRows += $this->aCommune->save($con);
                }
                $this->setCommune($this->aCommune);
            }

            if ($this->aRolepersonne !== null) {
                if ($this->aRolepersonne->isModified() || $this->aRolepersonne->isNew()) {
                    $affectedRows += $this->aRolepersonne->save($con);
                }
                $this->setRolepersonne($this->aRolepersonne);
            }

            if ($this->aSociete !== null) {
                if ($this->aSociete->isModified() || $this->aSociete->isNew()) {
                    $affectedRows += $this->aSociete->save($con);
                }
                $this->setSociete($this->aSociete);
            }

            if ($this->aVille !== null) {
                if ($this->aVille->isModified() || $this->aVille->isNew()) {
                    $affectedRows += $this->aVille->save($con);
                }
                $this->setVille($this->aVille);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion !== null) {
                if (!$this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion->isEmpty()) {
                    \ContratQuery::create()
                        ->filterByPrimaryKeys($this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion = null;
                }
            }

            if ($this->collContratsRelatedByPersonneClientIdrolepersonneClient !== null) {
                foreach ($this->collContratsRelatedByPersonneClientIdrolepersonneClient as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion !== null) {
                if (!$this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion->isEmpty()) {
                    \ContratQuery::create()
                        ->filterByPrimaryKeys($this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion = null;
                }
            }

            if ($this->collContratsRelatedByPersonneAgentIdrolepersonneAgent !== null) {
                foreach ($this->collContratsRelatedByPersonneAgentIdrolepersonneAgent as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->prospectionsScheduledForDeletion !== null) {
                if (!$this->prospectionsScheduledForDeletion->isEmpty()) {
                    \ProspectionQuery::create()
                        ->filterByPrimaryKeys($this->prospectionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->prospectionsScheduledForDeletion = null;
                }
            }

            if ($this->collProspections !== null) {
                foreach ($this->collProspections as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion !== null) {
                if (!$this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion->isEmpty()) {
                    \ResponsableAgentQuery::create()
                        ->filterByPrimaryKeys($this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion = null;
                }
            }

            if ($this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable !== null) {
                foreach ($this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion !== null) {
                if (!$this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion->isEmpty()) {
                    \ResponsableAgentQuery::create()
                        ->filterByPrimaryKeys($this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion = null;
                }
            }

            if ($this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent !== null) {
                foreach ($this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->ventesScheduledForDeletion !== null) {
                if (!$this->ventesScheduledForDeletion->isEmpty()) {
                    \VenteQuery::create()
                        ->filterByPrimaryKeys($this->ventesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->ventesScheduledForDeletion = null;
                }
            }

            if ($this->collVentes !== null) {
                foreach ($this->collVentes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[PersonneTableMap::COL_IDPERSONNE] = true;
        if (null !== $this->idpersonne) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . PersonneTableMap::COL_IDPERSONNE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PersonneTableMap::COL_IDPERSONNE)) {
            $modifiedColumns[':p' . $index++]  = 'idPersonne';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_NOMPERSONNE)) {
            $modifiedColumns[':p' . $index++]  = 'NomPersonne';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_PRENOMPERSONNE)) {
            $modifiedColumns[':p' . $index++]  = 'PrenomPersonne';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_SEXE)) {
            $modifiedColumns[':p' . $index++]  = 'sexe';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_DATENAISSANCE)) {
            $modifiedColumns[':p' . $index++]  = 'datenaissance';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE)) {
            $modifiedColumns[':p' . $index++]  = 'rolePersonne_idrolePersonne';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_COMMUNE_IDCOMMUNE)) {
            $modifiedColumns[':p' . $index++]  = 'commune_idcommune';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_VILLE_IDVILLE)) {
            $modifiedColumns[':p' . $index++]  = 'ville_idville';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_VILLE_PAYS_IDPAYS)) {
            $modifiedColumns[':p' . $index++]  = 'ville_pays_idpays';
        }
        if ($this->isColumnModified(PersonneTableMap::COL_SOCIETE_IDSOCIETE)) {
            $modifiedColumns[':p' . $index++]  = 'societe_idsociete';
        }

        $sql = sprintf(
            'INSERT INTO personne (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idPersonne':
                        $stmt->bindValue($identifier, $this->idpersonne, PDO::PARAM_INT);
                        break;
                    case 'NomPersonne':
                        $stmt->bindValue($identifier, $this->nompersonne, PDO::PARAM_STR);
                        break;
                    case 'PrenomPersonne':
                        $stmt->bindValue($identifier, $this->prenompersonne, PDO::PARAM_STR);
                        break;
                    case 'sexe':
                        $stmt->bindValue($identifier, $this->sexe, PDO::PARAM_STR);
                        break;
                    case 'datenaissance':
                        $stmt->bindValue($identifier, $this->datenaissance ? $this->datenaissance->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'rolePersonne_idrolePersonne':
                        $stmt->bindValue($identifier, $this->rolepersonne_idrolepersonne, PDO::PARAM_INT);
                        break;
                    case 'commune_idcommune':
                        $stmt->bindValue($identifier, $this->commune_idcommune, PDO::PARAM_INT);
                        break;
                    case 'ville_idville':
                        $stmt->bindValue($identifier, $this->ville_idville, PDO::PARAM_INT);
                        break;
                    case 'ville_pays_idpays':
                        $stmt->bindValue($identifier, $this->ville_pays_idpays, PDO::PARAM_INT);
                        break;
                    case 'societe_idsociete':
                        $stmt->bindValue($identifier, $this->societe_idsociete, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdpersonne($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PersonneTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdpersonne();
                break;
            case 1:
                return $this->getNompersonne();
                break;
            case 2:
                return $this->getPrenompersonne();
                break;
            case 3:
                return $this->getSexe();
                break;
            case 4:
                return $this->getDatenaissance();
                break;
            case 5:
                return $this->getRolepersonneIdrolepersonne();
                break;
            case 6:
                return $this->getCommuneIdcommune();
                break;
            case 7:
                return $this->getVilleIdville();
                break;
            case 8:
                return $this->getVillePaysIdpays();
                break;
            case 9:
                return $this->getSocieteIdsociete();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Personne'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Personne'][$this->hashCode()] = true;
        $keys = PersonneTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdpersonne(),
            $keys[1] => $this->getNompersonne(),
            $keys[2] => $this->getPrenompersonne(),
            $keys[3] => $this->getSexe(),
            $keys[4] => $this->getDatenaissance(),
            $keys[5] => $this->getRolepersonneIdrolepersonne(),
            $keys[6] => $this->getCommuneIdcommune(),
            $keys[7] => $this->getVilleIdville(),
            $keys[8] => $this->getVillePaysIdpays(),
            $keys[9] => $this->getSocieteIdsociete(),
        );
        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCommune) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'commune';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'commune';
                        break;
                    default:
                        $key = 'Commune';
                }

                $result[$key] = $this->aCommune->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRolepersonne) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'rolepersonne';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'rolepersonne';
                        break;
                    default:
                        $key = 'Rolepersonne';
                }

                $result[$key] = $this->aRolepersonne->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSociete) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'societe';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'societe';
                        break;
                    default:
                        $key = 'Societe';
                }

                $result[$key] = $this->aSociete->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aVille) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ville';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ville';
                        break;
                    default:
                        $key = 'Ville';
                }

                $result[$key] = $this->aVille->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collContratsRelatedByPersonneClientIdrolepersonneClient) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contrats';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contrats';
                        break;
                    default:
                        $key = 'Contrats';
                }

                $result[$key] = $this->collContratsRelatedByPersonneClientIdrolepersonneClient->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contrats';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contrats';
                        break;
                    default:
                        $key = 'Contrats';
                }

                $result[$key] = $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProspections) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'prospections';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'prospections';
                        break;
                    default:
                        $key = 'Prospections';
                }

                $result[$key] = $this->collProspections->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'responsableAgents';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'responsable_agents';
                        break;
                    default:
                        $key = 'ResponsableAgents';
                }

                $result[$key] = $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'responsableAgents';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'responsable_agents';
                        break;
                    default:
                        $key = 'ResponsableAgents';
                }

                $result[$key] = $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVentes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'ventes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ventes';
                        break;
                    default:
                        $key = 'Ventes';
                }

                $result[$key] = $this->collVentes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Personne
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PersonneTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Personne
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdpersonne($value);
                break;
            case 1:
                $this->setNompersonne($value);
                break;
            case 2:
                $this->setPrenompersonne($value);
                break;
            case 3:
                $this->setSexe($value);
                break;
            case 4:
                $this->setDatenaissance($value);
                break;
            case 5:
                $this->setRolepersonneIdrolepersonne($value);
                break;
            case 6:
                $this->setCommuneIdcommune($value);
                break;
            case 7:
                $this->setVilleIdville($value);
                break;
            case 8:
                $this->setVillePaysIdpays($value);
                break;
            case 9:
                $this->setSocieteIdsociete($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PersonneTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdpersonne($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNompersonne($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPrenompersonne($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setSexe($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDatenaissance($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setRolepersonneIdrolepersonne($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCommuneIdcommune($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setVilleIdville($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setVillePaysIdpays($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setSocieteIdsociete($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Personne The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PersonneTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PersonneTableMap::COL_IDPERSONNE)) {
            $criteria->add(PersonneTableMap::COL_IDPERSONNE, $this->idpersonne);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_NOMPERSONNE)) {
            $criteria->add(PersonneTableMap::COL_NOMPERSONNE, $this->nompersonne);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_PRENOMPERSONNE)) {
            $criteria->add(PersonneTableMap::COL_PRENOMPERSONNE, $this->prenompersonne);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_SEXE)) {
            $criteria->add(PersonneTableMap::COL_SEXE, $this->sexe);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_DATENAISSANCE)) {
            $criteria->add(PersonneTableMap::COL_DATENAISSANCE, $this->datenaissance);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE)) {
            $criteria->add(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $this->rolepersonne_idrolepersonne);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_COMMUNE_IDCOMMUNE)) {
            $criteria->add(PersonneTableMap::COL_COMMUNE_IDCOMMUNE, $this->commune_idcommune);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_VILLE_IDVILLE)) {
            $criteria->add(PersonneTableMap::COL_VILLE_IDVILLE, $this->ville_idville);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_VILLE_PAYS_IDPAYS)) {
            $criteria->add(PersonneTableMap::COL_VILLE_PAYS_IDPAYS, $this->ville_pays_idpays);
        }
        if ($this->isColumnModified(PersonneTableMap::COL_SOCIETE_IDSOCIETE)) {
            $criteria->add(PersonneTableMap::COL_SOCIETE_IDSOCIETE, $this->societe_idsociete);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPersonneQuery::create();
        $criteria->add(PersonneTableMap::COL_IDPERSONNE, $this->idpersonne);
        $criteria->add(PersonneTableMap::COL_ROLEPERSONNE_IDROLEPERSONNE, $this->rolepersonne_idrolepersonne);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdpersonne() &&
            null !== $this->getRolepersonneIdrolepersonne();

        $validPrimaryKeyFKs = 1;
        $primaryKeyFKs = [];

        //relation fk_Personne_rolePersonne to table rolepersonne
        if ($this->aRolepersonne && $hash = spl_object_hash($this->aRolepersonne)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getIdpersonne();
        $pks[1] = $this->getRolepersonneIdrolepersonne();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param      array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setIdpersonne($keys[0]);
        $this->setRolepersonneIdrolepersonne($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return (null === $this->getIdpersonne()) && (null === $this->getRolepersonneIdrolepersonne());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Personne (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNompersonne($this->getNompersonne());
        $copyObj->setPrenompersonne($this->getPrenompersonne());
        $copyObj->setSexe($this->getSexe());
        $copyObj->setDatenaissance($this->getDatenaissance());
        $copyObj->setRolepersonneIdrolepersonne($this->getRolepersonneIdrolepersonne());
        $copyObj->setCommuneIdcommune($this->getCommuneIdcommune());
        $copyObj->setVilleIdville($this->getVilleIdville());
        $copyObj->setVillePaysIdpays($this->getVillePaysIdpays());
        $copyObj->setSocieteIdsociete($this->getSocieteIdsociete());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getContratsRelatedByPersonneClientIdrolepersonneClient() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContratRelatedByPersonneClientIdrolepersonneClient($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContratsRelatedByPersonneAgentIdrolepersonneAgent() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContratRelatedByPersonneAgentIdrolepersonneAgent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProspections() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProspection($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getResponsableAgentsRelatedByPersonneIdagentIdroleagent() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addResponsableAgentRelatedByPersonneIdagentIdroleagent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVentes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVente($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdpersonne(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Personne Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCommune object.
     *
     * @param  ChildCommune $v
     * @return $this|\Personne The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommune(ChildCommune $v = null)
    {
        if ($v === null) {
            $this->setCommuneIdcommune(NULL);
        } else {
            $this->setCommuneIdcommune($v->getIdcommune());
        }

        $this->aCommune = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCommune object, it will not be re-added.
        if ($v !== null) {
            $v->addPersonne($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCommune object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCommune The associated ChildCommune object.
     * @throws PropelException
     */
    public function getCommune(ConnectionInterface $con = null)
    {
        if ($this->aCommune === null && ($this->commune_idcommune != 0)) {
            $this->aCommune = ChildCommuneQuery::create()->findPk($this->commune_idcommune, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommune->addPersonnes($this);
             */
        }

        return $this->aCommune;
    }

    /**
     * Declares an association between this object and a ChildRolepersonne object.
     *
     * @param  ChildRolepersonne $v
     * @return $this|\Personne The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRolepersonne(ChildRolepersonne $v = null)
    {
        if ($v === null) {
            $this->setRolepersonneIdrolepersonne(NULL);
        } else {
            $this->setRolepersonneIdrolepersonne($v->getIdrolepersonne());
        }

        $this->aRolepersonne = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildRolepersonne object, it will not be re-added.
        if ($v !== null) {
            $v->addPersonne($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildRolepersonne object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildRolepersonne The associated ChildRolepersonne object.
     * @throws PropelException
     */
    public function getRolepersonne(ConnectionInterface $con = null)
    {
        if ($this->aRolepersonne === null && ($this->rolepersonne_idrolepersonne != 0)) {
            $this->aRolepersonne = ChildRolepersonneQuery::create()->findPk($this->rolepersonne_idrolepersonne, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRolepersonne->addPersonnes($this);
             */
        }

        return $this->aRolepersonne;
    }

    /**
     * Declares an association between this object and a ChildSociete object.
     *
     * @param  ChildSociete $v
     * @return $this|\Personne The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSociete(ChildSociete $v = null)
    {
        if ($v === null) {
            $this->setSocieteIdsociete(NULL);
        } else {
            $this->setSocieteIdsociete($v->getIdsociete());
        }

        $this->aSociete = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSociete object, it will not be re-added.
        if ($v !== null) {
            $v->addPersonne($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSociete object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSociete The associated ChildSociete object.
     * @throws PropelException
     */
    public function getSociete(ConnectionInterface $con = null)
    {
        if ($this->aSociete === null && ($this->societe_idsociete != 0)) {
            $this->aSociete = ChildSocieteQuery::create()->findPk($this->societe_idsociete, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSociete->addPersonnes($this);
             */
        }

        return $this->aSociete;
    }

    /**
     * Declares an association between this object and a ChildVille object.
     *
     * @param  ChildVille $v
     * @return $this|\Personne The current object (for fluent API support)
     * @throws PropelException
     */
    public function setVille(ChildVille $v = null)
    {
        if ($v === null) {
            $this->setVilleIdville(NULL);
        } else {
            $this->setVilleIdville($v->getIdville());
        }

        if ($v === null) {
            $this->setVillePaysIdpays(NULL);
        } else {
            $this->setVillePaysIdpays($v->getPaysIdpays());
        }

        $this->aVille = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildVille object, it will not be re-added.
        if ($v !== null) {
            $v->addPersonne($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildVille object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildVille The associated ChildVille object.
     * @throws PropelException
     */
    public function getVille(ConnectionInterface $con = null)
    {
        if ($this->aVille === null && ($this->ville_idville != 0 && $this->ville_pays_idpays != 0)) {
            $this->aVille = ChildVilleQuery::create()->findPk(array($this->ville_idville, $this->ville_pays_idpays), $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aVille->addPersonnes($this);
             */
        }

        return $this->aVille;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ContratRelatedByPersonneClientIdrolepersonneClient' == $relationName) {
            $this->initContratsRelatedByPersonneClientIdrolepersonneClient();
            return;
        }
        if ('ContratRelatedByPersonneAgentIdrolepersonneAgent' == $relationName) {
            $this->initContratsRelatedByPersonneAgentIdrolepersonneAgent();
            return;
        }
        if ('Prospection' == $relationName) {
            $this->initProspections();
            return;
        }
        if ('ResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable' == $relationName) {
            $this->initResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable();
            return;
        }
        if ('ResponsableAgentRelatedByPersonneIdagentIdroleagent' == $relationName) {
            $this->initResponsableAgentsRelatedByPersonneIdagentIdroleagent();
            return;
        }
        if ('Vente' == $relationName) {
            $this->initVentes();
            return;
        }
    }

    /**
     * Clears out the collContratsRelatedByPersonneClientIdrolepersonneClient collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContratsRelatedByPersonneClientIdrolepersonneClient()
     */
    public function clearContratsRelatedByPersonneClientIdrolepersonneClient()
    {
        $this->collContratsRelatedByPersonneClientIdrolepersonneClient = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContratsRelatedByPersonneClientIdrolepersonneClient collection loaded partially.
     */
    public function resetPartialContratsRelatedByPersonneClientIdrolepersonneClient($v = true)
    {
        $this->collContratsRelatedByPersonneClientIdrolepersonneClientPartial = $v;
    }

    /**
     * Initializes the collContratsRelatedByPersonneClientIdrolepersonneClient collection.
     *
     * By default this just sets the collContratsRelatedByPersonneClientIdrolepersonneClient collection to an empty array (like clearcollContratsRelatedByPersonneClientIdrolepersonneClient());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContratsRelatedByPersonneClientIdrolepersonneClient($overrideExisting = true)
    {
        if (null !== $this->collContratsRelatedByPersonneClientIdrolepersonneClient && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContratTableMap::getTableMap()->getCollectionClassName();

        $this->collContratsRelatedByPersonneClientIdrolepersonneClient = new $collectionClassName;
        $this->collContratsRelatedByPersonneClientIdrolepersonneClient->setModel('\Contrat');
    }

    /**
     * Gets an array of ChildContrat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersonne is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContrat[] List of ChildContrat objects
     * @throws PropelException
     */
    public function getContratsRelatedByPersonneClientIdrolepersonneClient(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContratsRelatedByPersonneClientIdrolepersonneClientPartial && !$this->isNew();
        if (null === $this->collContratsRelatedByPersonneClientIdrolepersonneClient || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContratsRelatedByPersonneClientIdrolepersonneClient) {
                // return empty collection
                $this->initContratsRelatedByPersonneClientIdrolepersonneClient();
            } else {
                $collContratsRelatedByPersonneClientIdrolepersonneClient = ChildContratQuery::create(null, $criteria)
                    ->filterByPersonneRelatedByPersonneClientIdrolepersonneClient($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContratsRelatedByPersonneClientIdrolepersonneClientPartial && count($collContratsRelatedByPersonneClientIdrolepersonneClient)) {
                        $this->initContratsRelatedByPersonneClientIdrolepersonneClient(false);

                        foreach ($collContratsRelatedByPersonneClientIdrolepersonneClient as $obj) {
                            if (false == $this->collContratsRelatedByPersonneClientIdrolepersonneClient->contains($obj)) {
                                $this->collContratsRelatedByPersonneClientIdrolepersonneClient->append($obj);
                            }
                        }

                        $this->collContratsRelatedByPersonneClientIdrolepersonneClientPartial = true;
                    }

                    return $collContratsRelatedByPersonneClientIdrolepersonneClient;
                }

                if ($partial && $this->collContratsRelatedByPersonneClientIdrolepersonneClient) {
                    foreach ($this->collContratsRelatedByPersonneClientIdrolepersonneClient as $obj) {
                        if ($obj->isNew()) {
                            $collContratsRelatedByPersonneClientIdrolepersonneClient[] = $obj;
                        }
                    }
                }

                $this->collContratsRelatedByPersonneClientIdrolepersonneClient = $collContratsRelatedByPersonneClientIdrolepersonneClient;
                $this->collContratsRelatedByPersonneClientIdrolepersonneClientPartial = false;
            }
        }

        return $this->collContratsRelatedByPersonneClientIdrolepersonneClient;
    }

    /**
     * Sets a collection of ChildContrat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contratsRelatedByPersonneClientIdrolepersonneClient A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function setContratsRelatedByPersonneClientIdrolepersonneClient(Collection $contratsRelatedByPersonneClientIdrolepersonneClient, ConnectionInterface $con = null)
    {
        /** @var ChildContrat[] $contratsRelatedByPersonneClientIdrolepersonneClientToDelete */
        $contratsRelatedByPersonneClientIdrolepersonneClientToDelete = $this->getContratsRelatedByPersonneClientIdrolepersonneClient(new Criteria(), $con)->diff($contratsRelatedByPersonneClientIdrolepersonneClient);


        $this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion = $contratsRelatedByPersonneClientIdrolepersonneClientToDelete;

        foreach ($contratsRelatedByPersonneClientIdrolepersonneClientToDelete as $contratRelatedByPersonneClientIdrolepersonneClientRemoved) {
            $contratRelatedByPersonneClientIdrolepersonneClientRemoved->setPersonneRelatedByPersonneClientIdrolepersonneClient(null);
        }

        $this->collContratsRelatedByPersonneClientIdrolepersonneClient = null;
        foreach ($contratsRelatedByPersonneClientIdrolepersonneClient as $contratRelatedByPersonneClientIdrolepersonneClient) {
            $this->addContratRelatedByPersonneClientIdrolepersonneClient($contratRelatedByPersonneClientIdrolepersonneClient);
        }

        $this->collContratsRelatedByPersonneClientIdrolepersonneClient = $contratsRelatedByPersonneClientIdrolepersonneClient;
        $this->collContratsRelatedByPersonneClientIdrolepersonneClientPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Contrat objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Contrat objects.
     * @throws PropelException
     */
    public function countContratsRelatedByPersonneClientIdrolepersonneClient(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContratsRelatedByPersonneClientIdrolepersonneClientPartial && !$this->isNew();
        if (null === $this->collContratsRelatedByPersonneClientIdrolepersonneClient || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContratsRelatedByPersonneClientIdrolepersonneClient) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContratsRelatedByPersonneClientIdrolepersonneClient());
            }

            $query = ChildContratQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersonneRelatedByPersonneClientIdrolepersonneClient($this)
                ->count($con);
        }

        return count($this->collContratsRelatedByPersonneClientIdrolepersonneClient);
    }

    /**
     * Method called to associate a ChildContrat object to this object
     * through the ChildContrat foreign key attribute.
     *
     * @param  ChildContrat $l ChildContrat
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function addContratRelatedByPersonneClientIdrolepersonneClient(ChildContrat $l)
    {
        if ($this->collContratsRelatedByPersonneClientIdrolepersonneClient === null) {
            $this->initContratsRelatedByPersonneClientIdrolepersonneClient();
            $this->collContratsRelatedByPersonneClientIdrolepersonneClientPartial = true;
        }

        if (!$this->collContratsRelatedByPersonneClientIdrolepersonneClient->contains($l)) {
            $this->doAddContratRelatedByPersonneClientIdrolepersonneClient($l);

            if ($this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion and $this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion->contains($l)) {
                $this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion->remove($this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContrat $contratRelatedByPersonneClientIdrolepersonneClient The ChildContrat object to add.
     */
    protected function doAddContratRelatedByPersonneClientIdrolepersonneClient(ChildContrat $contratRelatedByPersonneClientIdrolepersonneClient)
    {
        $this->collContratsRelatedByPersonneClientIdrolepersonneClient[]= $contratRelatedByPersonneClientIdrolepersonneClient;
        $contratRelatedByPersonneClientIdrolepersonneClient->setPersonneRelatedByPersonneClientIdrolepersonneClient($this);
    }

    /**
     * @param  ChildContrat $contratRelatedByPersonneClientIdrolepersonneClient The ChildContrat object to remove.
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function removeContratRelatedByPersonneClientIdrolepersonneClient(ChildContrat $contratRelatedByPersonneClientIdrolepersonneClient)
    {
        if ($this->getContratsRelatedByPersonneClientIdrolepersonneClient()->contains($contratRelatedByPersonneClientIdrolepersonneClient)) {
            $pos = $this->collContratsRelatedByPersonneClientIdrolepersonneClient->search($contratRelatedByPersonneClientIdrolepersonneClient);
            $this->collContratsRelatedByPersonneClientIdrolepersonneClient->remove($pos);
            if (null === $this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion) {
                $this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion = clone $this->collContratsRelatedByPersonneClientIdrolepersonneClient;
                $this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion->clear();
            }
            $this->contratsRelatedByPersonneClientIdrolepersonneClientScheduledForDeletion[]= clone $contratRelatedByPersonneClientIdrolepersonneClient;
            $contratRelatedByPersonneClientIdrolepersonneClient->setPersonneRelatedByPersonneClientIdrolepersonneClient(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Personne is new, it will return
     * an empty collection; or if this Personne has previously
     * been saved, it will retrieve related ContratsRelatedByPersonneClientIdrolepersonneClient from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Personne.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContrat[] List of ChildContrat objects
     */
    public function getContratsRelatedByPersonneClientIdrolepersonneClientJoinProduits(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContratQuery::create(null, $criteria);
        $query->joinWith('Produits', $joinBehavior);

        return $this->getContratsRelatedByPersonneClientIdrolepersonneClient($query, $con);
    }

    /**
     * Clears out the collContratsRelatedByPersonneAgentIdrolepersonneAgent collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContratsRelatedByPersonneAgentIdrolepersonneAgent()
     */
    public function clearContratsRelatedByPersonneAgentIdrolepersonneAgent()
    {
        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContratsRelatedByPersonneAgentIdrolepersonneAgent collection loaded partially.
     */
    public function resetPartialContratsRelatedByPersonneAgentIdrolepersonneAgent($v = true)
    {
        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial = $v;
    }

    /**
     * Initializes the collContratsRelatedByPersonneAgentIdrolepersonneAgent collection.
     *
     * By default this just sets the collContratsRelatedByPersonneAgentIdrolepersonneAgent collection to an empty array (like clearcollContratsRelatedByPersonneAgentIdrolepersonneAgent());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContratsRelatedByPersonneAgentIdrolepersonneAgent($overrideExisting = true)
    {
        if (null !== $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContratTableMap::getTableMap()->getCollectionClassName();

        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent = new $collectionClassName;
        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent->setModel('\Contrat');
    }

    /**
     * Gets an array of ChildContrat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersonne is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildContrat[] List of ChildContrat objects
     * @throws PropelException
     */
    public function getContratsRelatedByPersonneAgentIdrolepersonneAgent(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial && !$this->isNew();
        if (null === $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent) {
                // return empty collection
                $this->initContratsRelatedByPersonneAgentIdrolepersonneAgent();
            } else {
                $collContratsRelatedByPersonneAgentIdrolepersonneAgent = ChildContratQuery::create(null, $criteria)
                    ->filterByPersonneRelatedByPersonneAgentIdrolepersonneAgent($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial && count($collContratsRelatedByPersonneAgentIdrolepersonneAgent)) {
                        $this->initContratsRelatedByPersonneAgentIdrolepersonneAgent(false);

                        foreach ($collContratsRelatedByPersonneAgentIdrolepersonneAgent as $obj) {
                            if (false == $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent->contains($obj)) {
                                $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent->append($obj);
                            }
                        }

                        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial = true;
                    }

                    return $collContratsRelatedByPersonneAgentIdrolepersonneAgent;
                }

                if ($partial && $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent) {
                    foreach ($this->collContratsRelatedByPersonneAgentIdrolepersonneAgent as $obj) {
                        if ($obj->isNew()) {
                            $collContratsRelatedByPersonneAgentIdrolepersonneAgent[] = $obj;
                        }
                    }
                }

                $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent = $collContratsRelatedByPersonneAgentIdrolepersonneAgent;
                $this->collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial = false;
            }
        }

        return $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent;
    }

    /**
     * Sets a collection of ChildContrat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contratsRelatedByPersonneAgentIdrolepersonneAgent A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function setContratsRelatedByPersonneAgentIdrolepersonneAgent(Collection $contratsRelatedByPersonneAgentIdrolepersonneAgent, ConnectionInterface $con = null)
    {
        /** @var ChildContrat[] $contratsRelatedByPersonneAgentIdrolepersonneAgentToDelete */
        $contratsRelatedByPersonneAgentIdrolepersonneAgentToDelete = $this->getContratsRelatedByPersonneAgentIdrolepersonneAgent(new Criteria(), $con)->diff($contratsRelatedByPersonneAgentIdrolepersonneAgent);


        $this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion = $contratsRelatedByPersonneAgentIdrolepersonneAgentToDelete;

        foreach ($contratsRelatedByPersonneAgentIdrolepersonneAgentToDelete as $contratRelatedByPersonneAgentIdrolepersonneAgentRemoved) {
            $contratRelatedByPersonneAgentIdrolepersonneAgentRemoved->setPersonneRelatedByPersonneAgentIdrolepersonneAgent(null);
        }

        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent = null;
        foreach ($contratsRelatedByPersonneAgentIdrolepersonneAgent as $contratRelatedByPersonneAgentIdrolepersonneAgent) {
            $this->addContratRelatedByPersonneAgentIdrolepersonneAgent($contratRelatedByPersonneAgentIdrolepersonneAgent);
        }

        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent = $contratsRelatedByPersonneAgentIdrolepersonneAgent;
        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Contrat objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Contrat objects.
     * @throws PropelException
     */
    public function countContratsRelatedByPersonneAgentIdrolepersonneAgent(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial && !$this->isNew();
        if (null === $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContratsRelatedByPersonneAgentIdrolepersonneAgent());
            }

            $query = ChildContratQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersonneRelatedByPersonneAgentIdrolepersonneAgent($this)
                ->count($con);
        }

        return count($this->collContratsRelatedByPersonneAgentIdrolepersonneAgent);
    }

    /**
     * Method called to associate a ChildContrat object to this object
     * through the ChildContrat foreign key attribute.
     *
     * @param  ChildContrat $l ChildContrat
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function addContratRelatedByPersonneAgentIdrolepersonneAgent(ChildContrat $l)
    {
        if ($this->collContratsRelatedByPersonneAgentIdrolepersonneAgent === null) {
            $this->initContratsRelatedByPersonneAgentIdrolepersonneAgent();
            $this->collContratsRelatedByPersonneAgentIdrolepersonneAgentPartial = true;
        }

        if (!$this->collContratsRelatedByPersonneAgentIdrolepersonneAgent->contains($l)) {
            $this->doAddContratRelatedByPersonneAgentIdrolepersonneAgent($l);

            if ($this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion and $this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion->contains($l)) {
                $this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion->remove($this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildContrat $contratRelatedByPersonneAgentIdrolepersonneAgent The ChildContrat object to add.
     */
    protected function doAddContratRelatedByPersonneAgentIdrolepersonneAgent(ChildContrat $contratRelatedByPersonneAgentIdrolepersonneAgent)
    {
        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent[]= $contratRelatedByPersonneAgentIdrolepersonneAgent;
        $contratRelatedByPersonneAgentIdrolepersonneAgent->setPersonneRelatedByPersonneAgentIdrolepersonneAgent($this);
    }

    /**
     * @param  ChildContrat $contratRelatedByPersonneAgentIdrolepersonneAgent The ChildContrat object to remove.
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function removeContratRelatedByPersonneAgentIdrolepersonneAgent(ChildContrat $contratRelatedByPersonneAgentIdrolepersonneAgent)
    {
        if ($this->getContratsRelatedByPersonneAgentIdrolepersonneAgent()->contains($contratRelatedByPersonneAgentIdrolepersonneAgent)) {
            $pos = $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent->search($contratRelatedByPersonneAgentIdrolepersonneAgent);
            $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent->remove($pos);
            if (null === $this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion) {
                $this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion = clone $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent;
                $this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion->clear();
            }
            $this->contratsRelatedByPersonneAgentIdrolepersonneAgentScheduledForDeletion[]= clone $contratRelatedByPersonneAgentIdrolepersonneAgent;
            $contratRelatedByPersonneAgentIdrolepersonneAgent->setPersonneRelatedByPersonneAgentIdrolepersonneAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Personne is new, it will return
     * an empty collection; or if this Personne has previously
     * been saved, it will retrieve related ContratsRelatedByPersonneAgentIdrolepersonneAgent from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Personne.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildContrat[] List of ChildContrat objects
     */
    public function getContratsRelatedByPersonneAgentIdrolepersonneAgentJoinProduits(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildContratQuery::create(null, $criteria);
        $query->joinWith('Produits', $joinBehavior);

        return $this->getContratsRelatedByPersonneAgentIdrolepersonneAgent($query, $con);
    }

    /**
     * Clears out the collProspections collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProspections()
     */
    public function clearProspections()
    {
        $this->collProspections = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProspections collection loaded partially.
     */
    public function resetPartialProspections($v = true)
    {
        $this->collProspectionsPartial = $v;
    }

    /**
     * Initializes the collProspections collection.
     *
     * By default this just sets the collProspections collection to an empty array (like clearcollProspections());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProspections($overrideExisting = true)
    {
        if (null !== $this->collProspections && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProspectionTableMap::getTableMap()->getCollectionClassName();

        $this->collProspections = new $collectionClassName;
        $this->collProspections->setModel('\Prospection');
    }

    /**
     * Gets an array of ChildProspection objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersonne is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProspection[] List of ChildProspection objects
     * @throws PropelException
     */
    public function getProspections(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProspectionsPartial && !$this->isNew();
        if (null === $this->collProspections || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProspections) {
                // return empty collection
                $this->initProspections();
            } else {
                $collProspections = ChildProspectionQuery::create(null, $criteria)
                    ->filterByPersonne($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProspectionsPartial && count($collProspections)) {
                        $this->initProspections(false);

                        foreach ($collProspections as $obj) {
                            if (false == $this->collProspections->contains($obj)) {
                                $this->collProspections->append($obj);
                            }
                        }

                        $this->collProspectionsPartial = true;
                    }

                    return $collProspections;
                }

                if ($partial && $this->collProspections) {
                    foreach ($this->collProspections as $obj) {
                        if ($obj->isNew()) {
                            $collProspections[] = $obj;
                        }
                    }
                }

                $this->collProspections = $collProspections;
                $this->collProspectionsPartial = false;
            }
        }

        return $this->collProspections;
    }

    /**
     * Sets a collection of ChildProspection objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $prospections A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function setProspections(Collection $prospections, ConnectionInterface $con = null)
    {
        /** @var ChildProspection[] $prospectionsToDelete */
        $prospectionsToDelete = $this->getProspections(new Criteria(), $con)->diff($prospections);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->prospectionsScheduledForDeletion = clone $prospectionsToDelete;

        foreach ($prospectionsToDelete as $prospectionRemoved) {
            $prospectionRemoved->setPersonne(null);
        }

        $this->collProspections = null;
        foreach ($prospections as $prospection) {
            $this->addProspection($prospection);
        }

        $this->collProspections = $prospections;
        $this->collProspectionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Prospection objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Prospection objects.
     * @throws PropelException
     */
    public function countProspections(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProspectionsPartial && !$this->isNew();
        if (null === $this->collProspections || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProspections) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProspections());
            }

            $query = ChildProspectionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersonne($this)
                ->count($con);
        }

        return count($this->collProspections);
    }

    /**
     * Method called to associate a ChildProspection object to this object
     * through the ChildProspection foreign key attribute.
     *
     * @param  ChildProspection $l ChildProspection
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function addProspection(ChildProspection $l)
    {
        if ($this->collProspections === null) {
            $this->initProspections();
            $this->collProspectionsPartial = true;
        }

        if (!$this->collProspections->contains($l)) {
            $this->doAddProspection($l);

            if ($this->prospectionsScheduledForDeletion and $this->prospectionsScheduledForDeletion->contains($l)) {
                $this->prospectionsScheduledForDeletion->remove($this->prospectionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProspection $prospection The ChildProspection object to add.
     */
    protected function doAddProspection(ChildProspection $prospection)
    {
        $this->collProspections[]= $prospection;
        $prospection->setPersonne($this);
    }

    /**
     * @param  ChildProspection $prospection The ChildProspection object to remove.
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function removeProspection(ChildProspection $prospection)
    {
        if ($this->getProspections()->contains($prospection)) {
            $pos = $this->collProspections->search($prospection);
            $this->collProspections->remove($pos);
            if (null === $this->prospectionsScheduledForDeletion) {
                $this->prospectionsScheduledForDeletion = clone $this->collProspections;
                $this->prospectionsScheduledForDeletion->clear();
            }
            $this->prospectionsScheduledForDeletion[]= clone $prospection;
            $prospection->setPersonne(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Personne is new, it will return
     * an empty collection; or if this Personne has previously
     * been saved, it will retrieve related Prospections from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Personne.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProspection[] List of ChildProspection objects
     */
    public function getProspectionsJoinProduits(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProspectionQuery::create(null, $criteria);
        $query->joinWith('Produits', $joinBehavior);

        return $this->getProspections($query, $con);
    }

    /**
     * Clears out the collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable()
     */
    public function clearResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable()
    {
        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable collection loaded partially.
     */
    public function resetPartialResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable($v = true)
    {
        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial = $v;
    }

    /**
     * Initializes the collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable collection.
     *
     * By default this just sets the collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable collection to an empty array (like clearcollResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable($overrideExisting = true)
    {
        if (null !== $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable && !$overrideExisting) {
            return;
        }

        $collectionClassName = ResponsableAgentTableMap::getTableMap()->getCollectionClassName();

        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable = new $collectionClassName;
        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable->setModel('\ResponsableAgent');
    }

    /**
     * Gets an array of ChildResponsableAgent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersonne is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildResponsableAgent[] List of ChildResponsableAgent objects
     * @throws PropelException
     */
    public function getResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial && !$this->isNew();
        if (null === $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable) {
                // return empty collection
                $this->initResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable();
            } else {
                $collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable = ChildResponsableAgentQuery::create(null, $criteria)
                    ->filterByPersonneRelatedByPersonneIdresponsableIdroleresponsable($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial && count($collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable)) {
                        $this->initResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable(false);

                        foreach ($collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable as $obj) {
                            if (false == $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable->contains($obj)) {
                                $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable->append($obj);
                            }
                        }

                        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial = true;
                    }

                    return $collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable;
                }

                if ($partial && $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable) {
                    foreach ($this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable as $obj) {
                        if ($obj->isNew()) {
                            $collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable[] = $obj;
                        }
                    }
                }

                $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable = $collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable;
                $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial = false;
            }
        }

        return $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable;
    }

    /**
     * Sets a collection of ChildResponsableAgent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $responsableAgentsRelatedByPersonneIdresponsableIdroleresponsable A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function setResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable(Collection $responsableAgentsRelatedByPersonneIdresponsableIdroleresponsable, ConnectionInterface $con = null)
    {
        /** @var ChildResponsableAgent[] $responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableToDelete */
        $responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableToDelete = $this->getResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable(new Criteria(), $con)->diff($responsableAgentsRelatedByPersonneIdresponsableIdroleresponsable);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion = clone $responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableToDelete;

        foreach ($responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableToDelete as $responsableAgentRelatedByPersonneIdresponsableIdroleresponsableRemoved) {
            $responsableAgentRelatedByPersonneIdresponsableIdroleresponsableRemoved->setPersonneRelatedByPersonneIdresponsableIdroleresponsable(null);
        }

        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable = null;
        foreach ($responsableAgentsRelatedByPersonneIdresponsableIdroleresponsable as $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable) {
            $this->addResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($responsableAgentRelatedByPersonneIdresponsableIdroleresponsable);
        }

        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable = $responsableAgentsRelatedByPersonneIdresponsableIdroleresponsable;
        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial = false;

        return $this;
    }

    /**
     * Returns the number of related ResponsableAgent objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ResponsableAgent objects.
     * @throws PropelException
     */
    public function countResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial && !$this->isNew();
        if (null === $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable());
            }

            $query = ChildResponsableAgentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersonneRelatedByPersonneIdresponsableIdroleresponsable($this)
                ->count($con);
        }

        return count($this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable);
    }

    /**
     * Method called to associate a ChildResponsableAgent object to this object
     * through the ChildResponsableAgent foreign key attribute.
     *
     * @param  ChildResponsableAgent $l ChildResponsableAgent
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function addResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable(ChildResponsableAgent $l)
    {
        if ($this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable === null) {
            $this->initResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable();
            $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsablePartial = true;
        }

        if (!$this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable->contains($l)) {
            $this->doAddResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable($l);

            if ($this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion and $this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion->contains($l)) {
                $this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion->remove($this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildResponsableAgent $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable The ChildResponsableAgent object to add.
     */
    protected function doAddResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable(ChildResponsableAgent $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable)
    {
        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable[]= $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable;
        $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable->setPersonneRelatedByPersonneIdresponsableIdroleresponsable($this);
    }

    /**
     * @param  ChildResponsableAgent $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable The ChildResponsableAgent object to remove.
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function removeResponsableAgentRelatedByPersonneIdresponsableIdroleresponsable(ChildResponsableAgent $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable)
    {
        if ($this->getResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable()->contains($responsableAgentRelatedByPersonneIdresponsableIdroleresponsable)) {
            $pos = $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable->search($responsableAgentRelatedByPersonneIdresponsableIdroleresponsable);
            $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable->remove($pos);
            if (null === $this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion) {
                $this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion = clone $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable;
                $this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion->clear();
            }
            $this->responsableAgentsRelatedByPersonneIdresponsableIdroleresponsableScheduledForDeletion[]= clone $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable;
            $responsableAgentRelatedByPersonneIdresponsableIdroleresponsable->setPersonneRelatedByPersonneIdresponsableIdroleresponsable(null);
        }

        return $this;
    }

    /**
     * Clears out the collResponsableAgentsRelatedByPersonneIdagentIdroleagent collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addResponsableAgentsRelatedByPersonneIdagentIdroleagent()
     */
    public function clearResponsableAgentsRelatedByPersonneIdagentIdroleagent()
    {
        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collResponsableAgentsRelatedByPersonneIdagentIdroleagent collection loaded partially.
     */
    public function resetPartialResponsableAgentsRelatedByPersonneIdagentIdroleagent($v = true)
    {
        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial = $v;
    }

    /**
     * Initializes the collResponsableAgentsRelatedByPersonneIdagentIdroleagent collection.
     *
     * By default this just sets the collResponsableAgentsRelatedByPersonneIdagentIdroleagent collection to an empty array (like clearcollResponsableAgentsRelatedByPersonneIdagentIdroleagent());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initResponsableAgentsRelatedByPersonneIdagentIdroleagent($overrideExisting = true)
    {
        if (null !== $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent && !$overrideExisting) {
            return;
        }

        $collectionClassName = ResponsableAgentTableMap::getTableMap()->getCollectionClassName();

        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent = new $collectionClassName;
        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent->setModel('\ResponsableAgent');
    }

    /**
     * Gets an array of ChildResponsableAgent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersonne is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildResponsableAgent[] List of ChildResponsableAgent objects
     * @throws PropelException
     */
    public function getResponsableAgentsRelatedByPersonneIdagentIdroleagent(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial && !$this->isNew();
        if (null === $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent) {
                // return empty collection
                $this->initResponsableAgentsRelatedByPersonneIdagentIdroleagent();
            } else {
                $collResponsableAgentsRelatedByPersonneIdagentIdroleagent = ChildResponsableAgentQuery::create(null, $criteria)
                    ->filterByPersonneRelatedByPersonneIdagentIdroleagent($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial && count($collResponsableAgentsRelatedByPersonneIdagentIdroleagent)) {
                        $this->initResponsableAgentsRelatedByPersonneIdagentIdroleagent(false);

                        foreach ($collResponsableAgentsRelatedByPersonneIdagentIdroleagent as $obj) {
                            if (false == $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent->contains($obj)) {
                                $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent->append($obj);
                            }
                        }

                        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial = true;
                    }

                    return $collResponsableAgentsRelatedByPersonneIdagentIdroleagent;
                }

                if ($partial && $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent) {
                    foreach ($this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent as $obj) {
                        if ($obj->isNew()) {
                            $collResponsableAgentsRelatedByPersonneIdagentIdroleagent[] = $obj;
                        }
                    }
                }

                $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent = $collResponsableAgentsRelatedByPersonneIdagentIdroleagent;
                $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial = false;
            }
        }

        return $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent;
    }

    /**
     * Sets a collection of ChildResponsableAgent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $responsableAgentsRelatedByPersonneIdagentIdroleagent A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function setResponsableAgentsRelatedByPersonneIdagentIdroleagent(Collection $responsableAgentsRelatedByPersonneIdagentIdroleagent, ConnectionInterface $con = null)
    {
        /** @var ChildResponsableAgent[] $responsableAgentsRelatedByPersonneIdagentIdroleagentToDelete */
        $responsableAgentsRelatedByPersonneIdagentIdroleagentToDelete = $this->getResponsableAgentsRelatedByPersonneIdagentIdroleagent(new Criteria(), $con)->diff($responsableAgentsRelatedByPersonneIdagentIdroleagent);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion = clone $responsableAgentsRelatedByPersonneIdagentIdroleagentToDelete;

        foreach ($responsableAgentsRelatedByPersonneIdagentIdroleagentToDelete as $responsableAgentRelatedByPersonneIdagentIdroleagentRemoved) {
            $responsableAgentRelatedByPersonneIdagentIdroleagentRemoved->setPersonneRelatedByPersonneIdagentIdroleagent(null);
        }

        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent = null;
        foreach ($responsableAgentsRelatedByPersonneIdagentIdroleagent as $responsableAgentRelatedByPersonneIdagentIdroleagent) {
            $this->addResponsableAgentRelatedByPersonneIdagentIdroleagent($responsableAgentRelatedByPersonneIdagentIdroleagent);
        }

        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent = $responsableAgentsRelatedByPersonneIdagentIdroleagent;
        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ResponsableAgent objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ResponsableAgent objects.
     * @throws PropelException
     */
    public function countResponsableAgentsRelatedByPersonneIdagentIdroleagent(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial && !$this->isNew();
        if (null === $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getResponsableAgentsRelatedByPersonneIdagentIdroleagent());
            }

            $query = ChildResponsableAgentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersonneRelatedByPersonneIdagentIdroleagent($this)
                ->count($con);
        }

        return count($this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent);
    }

    /**
     * Method called to associate a ChildResponsableAgent object to this object
     * through the ChildResponsableAgent foreign key attribute.
     *
     * @param  ChildResponsableAgent $l ChildResponsableAgent
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function addResponsableAgentRelatedByPersonneIdagentIdroleagent(ChildResponsableAgent $l)
    {
        if ($this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent === null) {
            $this->initResponsableAgentsRelatedByPersonneIdagentIdroleagent();
            $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagentPartial = true;
        }

        if (!$this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent->contains($l)) {
            $this->doAddResponsableAgentRelatedByPersonneIdagentIdroleagent($l);

            if ($this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion and $this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion->contains($l)) {
                $this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion->remove($this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildResponsableAgent $responsableAgentRelatedByPersonneIdagentIdroleagent The ChildResponsableAgent object to add.
     */
    protected function doAddResponsableAgentRelatedByPersonneIdagentIdroleagent(ChildResponsableAgent $responsableAgentRelatedByPersonneIdagentIdroleagent)
    {
        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent[]= $responsableAgentRelatedByPersonneIdagentIdroleagent;
        $responsableAgentRelatedByPersonneIdagentIdroleagent->setPersonneRelatedByPersonneIdagentIdroleagent($this);
    }

    /**
     * @param  ChildResponsableAgent $responsableAgentRelatedByPersonneIdagentIdroleagent The ChildResponsableAgent object to remove.
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function removeResponsableAgentRelatedByPersonneIdagentIdroleagent(ChildResponsableAgent $responsableAgentRelatedByPersonneIdagentIdroleagent)
    {
        if ($this->getResponsableAgentsRelatedByPersonneIdagentIdroleagent()->contains($responsableAgentRelatedByPersonneIdagentIdroleagent)) {
            $pos = $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent->search($responsableAgentRelatedByPersonneIdagentIdroleagent);
            $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent->remove($pos);
            if (null === $this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion) {
                $this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion = clone $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent;
                $this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion->clear();
            }
            $this->responsableAgentsRelatedByPersonneIdagentIdroleagentScheduledForDeletion[]= clone $responsableAgentRelatedByPersonneIdagentIdroleagent;
            $responsableAgentRelatedByPersonneIdagentIdroleagent->setPersonneRelatedByPersonneIdagentIdroleagent(null);
        }

        return $this;
    }

    /**
     * Clears out the collVentes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addVentes()
     */
    public function clearVentes()
    {
        $this->collVentes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collVentes collection loaded partially.
     */
    public function resetPartialVentes($v = true)
    {
        $this->collVentesPartial = $v;
    }

    /**
     * Initializes the collVentes collection.
     *
     * By default this just sets the collVentes collection to an empty array (like clearcollVentes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVentes($overrideExisting = true)
    {
        if (null !== $this->collVentes && !$overrideExisting) {
            return;
        }

        $collectionClassName = VenteTableMap::getTableMap()->getCollectionClassName();

        $this->collVentes = new $collectionClassName;
        $this->collVentes->setModel('\Vente');
    }

    /**
     * Gets an array of ChildVente objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPersonne is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildVente[] List of ChildVente objects
     * @throws PropelException
     */
    public function getVentes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collVentesPartial && !$this->isNew();
        if (null === $this->collVentes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collVentes) {
                // return empty collection
                $this->initVentes();
            } else {
                $collVentes = ChildVenteQuery::create(null, $criteria)
                    ->filterByPersonne($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collVentesPartial && count($collVentes)) {
                        $this->initVentes(false);

                        foreach ($collVentes as $obj) {
                            if (false == $this->collVentes->contains($obj)) {
                                $this->collVentes->append($obj);
                            }
                        }

                        $this->collVentesPartial = true;
                    }

                    return $collVentes;
                }

                if ($partial && $this->collVentes) {
                    foreach ($this->collVentes as $obj) {
                        if ($obj->isNew()) {
                            $collVentes[] = $obj;
                        }
                    }
                }

                $this->collVentes = $collVentes;
                $this->collVentesPartial = false;
            }
        }

        return $this->collVentes;
    }

    /**
     * Sets a collection of ChildVente objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $ventes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function setVentes(Collection $ventes, ConnectionInterface $con = null)
    {
        /** @var ChildVente[] $ventesToDelete */
        $ventesToDelete = $this->getVentes(new Criteria(), $con)->diff($ventes);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->ventesScheduledForDeletion = clone $ventesToDelete;

        foreach ($ventesToDelete as $venteRemoved) {
            $venteRemoved->setPersonne(null);
        }

        $this->collVentes = null;
        foreach ($ventes as $vente) {
            $this->addVente($vente);
        }

        $this->collVentes = $ventes;
        $this->collVentesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Vente objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Vente objects.
     * @throws PropelException
     */
    public function countVentes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collVentesPartial && !$this->isNew();
        if (null === $this->collVentes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVentes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getVentes());
            }

            $query = ChildVenteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPersonne($this)
                ->count($con);
        }

        return count($this->collVentes);
    }

    /**
     * Method called to associate a ChildVente object to this object
     * through the ChildVente foreign key attribute.
     *
     * @param  ChildVente $l ChildVente
     * @return $this|\Personne The current object (for fluent API support)
     */
    public function addVente(ChildVente $l)
    {
        if ($this->collVentes === null) {
            $this->initVentes();
            $this->collVentesPartial = true;
        }

        if (!$this->collVentes->contains($l)) {
            $this->doAddVente($l);

            if ($this->ventesScheduledForDeletion and $this->ventesScheduledForDeletion->contains($l)) {
                $this->ventesScheduledForDeletion->remove($this->ventesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildVente $vente The ChildVente object to add.
     */
    protected function doAddVente(ChildVente $vente)
    {
        $this->collVentes[]= $vente;
        $vente->setPersonne($this);
    }

    /**
     * @param  ChildVente $vente The ChildVente object to remove.
     * @return $this|ChildPersonne The current object (for fluent API support)
     */
    public function removeVente(ChildVente $vente)
    {
        if ($this->getVentes()->contains($vente)) {
            $pos = $this->collVentes->search($vente);
            $this->collVentes->remove($pos);
            if (null === $this->ventesScheduledForDeletion) {
                $this->ventesScheduledForDeletion = clone $this->collVentes;
                $this->ventesScheduledForDeletion->clear();
            }
            $this->ventesScheduledForDeletion[]= clone $vente;
            $vente->setPersonne(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Personne is new, it will return
     * an empty collection; or if this Personne has previously
     * been saved, it will retrieve related Ventes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Personne.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildVente[] List of ChildVente objects
     */
    public function getVentesJoinProduits(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildVenteQuery::create(null, $criteria);
        $query->joinWith('Produits', $joinBehavior);

        return $this->getVentes($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCommune) {
            $this->aCommune->removePersonne($this);
        }
        if (null !== $this->aRolepersonne) {
            $this->aRolepersonne->removePersonne($this);
        }
        if (null !== $this->aSociete) {
            $this->aSociete->removePersonne($this);
        }
        if (null !== $this->aVille) {
            $this->aVille->removePersonne($this);
        }
        $this->idpersonne = null;
        $this->nompersonne = null;
        $this->prenompersonne = null;
        $this->sexe = null;
        $this->datenaissance = null;
        $this->rolepersonne_idrolepersonne = null;
        $this->commune_idcommune = null;
        $this->ville_idville = null;
        $this->ville_pays_idpays = null;
        $this->societe_idsociete = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collContratsRelatedByPersonneClientIdrolepersonneClient) {
                foreach ($this->collContratsRelatedByPersonneClientIdrolepersonneClient as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContratsRelatedByPersonneAgentIdrolepersonneAgent) {
                foreach ($this->collContratsRelatedByPersonneAgentIdrolepersonneAgent as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProspections) {
                foreach ($this->collProspections as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable) {
                foreach ($this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent) {
                foreach ($this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVentes) {
                foreach ($this->collVentes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collContratsRelatedByPersonneClientIdrolepersonneClient = null;
        $this->collContratsRelatedByPersonneAgentIdrolepersonneAgent = null;
        $this->collProspections = null;
        $this->collResponsableAgentsRelatedByPersonneIdresponsableIdroleresponsable = null;
        $this->collResponsableAgentsRelatedByPersonneIdagentIdroleagent = null;
        $this->collVentes = null;
        $this->aCommune = null;
        $this->aRolepersonne = null;
        $this->aSociete = null;
        $this->aVille = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PersonneTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
