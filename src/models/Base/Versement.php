<?php

namespace Base;

use \Contrat as ChildContrat;
use \ContratQuery as ChildContratQuery;
use \Modepaiement as ChildModepaiement;
use \ModepaiementQuery as ChildModepaiementQuery;
use \Mois as ChildMois;
use \MoisQuery as ChildMoisQuery;
use \VersementQuery as ChildVersementQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\VersementTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'versement' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Versement implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\VersementTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the idversement field.
     *
     * @var        int
     */
    protected $idversement;

    /**
     * The value for the dateversement field.
     *
     * @var        DateTime
     */
    protected $dateversement;

    /**
     * The value for the montantversement field.
     *
     * @var        int
     */
    protected $montantversement;

    /**
     * The value for the numerocheque field.
     *
     * @var        string
     */
    protected $numerocheque;

    /**
     * The value for the versement_idmois field.
     *
     * @var        int
     */
    protected $versement_idmois;

    /**
     * The value for the contrat_idcontrat field.
     *
     * @var        int
     */
    protected $contrat_idcontrat;

    /**
     * The value for the contrat_produits_idproduits field.
     *
     * @var        int
     */
    protected $contrat_produits_idproduits;

    /**
     * The value for the contrat_produits_societe_idsociete field.
     *
     * @var        int
     */
    protected $contrat_produits_societe_idsociete;

    /**
     * The value for the modepaiement_idmodepaiement field.
     *
     * @var        int
     */
    protected $modepaiement_idmodepaiement;

    /**
     * @var        ChildContrat
     */
    protected $aContrat;

    /**
     * @var        ChildModepaiement
     */
    protected $aModepaiement;

    /**
     * @var        ChildMois
     */
    protected $aMois;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Base\Versement object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Versement</code> instance.  If
     * <code>obj</code> is an instance of <code>Versement</code>, delegates to
     * <code>equals(Versement)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Versement The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idversement] column value.
     *
     * @return int
     */
    public function getIdversement()
    {
        return $this->idversement;
    }

    /**
     * Get the [optionally formatted] temporal [dateversement] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateversement($format = NULL)
    {
        if ($format === null) {
            return $this->dateversement;
        } else {
            return $this->dateversement instanceof \DateTimeInterface ? $this->dateversement->format($format) : null;
        }
    }

    /**
     * Get the [montantversement] column value.
     *
     * @return int
     */
    public function getMontantversement()
    {
        return $this->montantversement;
    }

    /**
     * Get the [numerocheque] column value.
     *
     * @return string
     */
    public function getNumerocheque()
    {
        return $this->numerocheque;
    }

    /**
     * Get the [versement_idmois] column value.
     *
     * @return int
     */
    public function getVersementIdmois()
    {
        return $this->versement_idmois;
    }

    /**
     * Get the [contrat_idcontrat] column value.
     *
     * @return int
     */
    public function getContratIdcontrat()
    {
        return $this->contrat_idcontrat;
    }

    /**
     * Get the [contrat_produits_idproduits] column value.
     *
     * @return int
     */
    public function getContratProduitsIdproduits()
    {
        return $this->contrat_produits_idproduits;
    }

    /**
     * Get the [contrat_produits_societe_idsociete] column value.
     *
     * @return int
     */
    public function getContratProduitsSocieteIdsociete()
    {
        return $this->contrat_produits_societe_idsociete;
    }

    /**
     * Get the [modepaiement_idmodepaiement] column value.
     *
     * @return int
     */
    public function getModepaiementIdmodepaiement()
    {
        return $this->modepaiement_idmodepaiement;
    }

    /**
     * Set the value of [idversement] column.
     *
     * @param int $v new value
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setIdversement($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idversement !== $v) {
            $this->idversement = $v;
            $this->modifiedColumns[VersementTableMap::COL_IDVERSEMENT] = true;
        }

        return $this;
    } // setIdversement()

    /**
     * Sets the value of [dateversement] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setDateversement($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->dateversement !== null || $dt !== null) {
            if ($this->dateversement === null || $dt === null || $dt->format("Y-m-d") !== $this->dateversement->format("Y-m-d")) {
                $this->dateversement = $dt === null ? null : clone $dt;
                $this->modifiedColumns[VersementTableMap::COL_DATEVERSEMENT] = true;
            }
        } // if either are not null

        return $this;
    } // setDateversement()

    /**
     * Set the value of [montantversement] column.
     *
     * @param int $v new value
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setMontantversement($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->montantversement !== $v) {
            $this->montantversement = $v;
            $this->modifiedColumns[VersementTableMap::COL_MONTANTVERSEMENT] = true;
        }

        return $this;
    } // setMontantversement()

    /**
     * Set the value of [numerocheque] column.
     *
     * @param string $v new value
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setNumerocheque($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->numerocheque !== $v) {
            $this->numerocheque = $v;
            $this->modifiedColumns[VersementTableMap::COL_NUMEROCHEQUE] = true;
        }

        return $this;
    } // setNumerocheque()

    /**
     * Set the value of [versement_idmois] column.
     *
     * @param int $v new value
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setVersementIdmois($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->versement_idmois !== $v) {
            $this->versement_idmois = $v;
            $this->modifiedColumns[VersementTableMap::COL_VERSEMENT_IDMOIS] = true;
        }

        if ($this->aMois !== null && $this->aMois->getIdmois() !== $v) {
            $this->aMois = null;
        }

        return $this;
    } // setVersementIdmois()

    /**
     * Set the value of [contrat_idcontrat] column.
     *
     * @param int $v new value
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setContratIdcontrat($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->contrat_idcontrat !== $v) {
            $this->contrat_idcontrat = $v;
            $this->modifiedColumns[VersementTableMap::COL_CONTRAT_IDCONTRAT] = true;
        }

        if ($this->aContrat !== null && $this->aContrat->getIdcontrat() !== $v) {
            $this->aContrat = null;
        }

        return $this;
    } // setContratIdcontrat()

    /**
     * Set the value of [contrat_produits_idproduits] column.
     *
     * @param int $v new value
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setContratProduitsIdproduits($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->contrat_produits_idproduits !== $v) {
            $this->contrat_produits_idproduits = $v;
            $this->modifiedColumns[VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS] = true;
        }

        if ($this->aContrat !== null && $this->aContrat->getProduitsIdproduits1() !== $v) {
            $this->aContrat = null;
        }

        return $this;
    } // setContratProduitsIdproduits()

    /**
     * Set the value of [contrat_produits_societe_idsociete] column.
     *
     * @param int $v new value
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setContratProduitsSocieteIdsociete($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->contrat_produits_societe_idsociete !== $v) {
            $this->contrat_produits_societe_idsociete = $v;
            $this->modifiedColumns[VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE] = true;
        }

        if ($this->aContrat !== null && $this->aContrat->getProduitsSocieteIdsociete() !== $v) {
            $this->aContrat = null;
        }

        return $this;
    } // setContratProduitsSocieteIdsociete()

    /**
     * Set the value of [modepaiement_idmodepaiement] column.
     *
     * @param int $v new value
     * @return $this|\Versement The current object (for fluent API support)
     */
    public function setModepaiementIdmodepaiement($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->modepaiement_idmodepaiement !== $v) {
            $this->modepaiement_idmodepaiement = $v;
            $this->modifiedColumns[VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT] = true;
        }

        if ($this->aModepaiement !== null && $this->aModepaiement->getIdmodepaiement() !== $v) {
            $this->aModepaiement = null;
        }

        return $this;
    } // setModepaiementIdmodepaiement()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : VersementTableMap::translateFieldName('Idversement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idversement = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : VersementTableMap::translateFieldName('Dateversement', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->dateversement = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : VersementTableMap::translateFieldName('Montantversement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->montantversement = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : VersementTableMap::translateFieldName('Numerocheque', TableMap::TYPE_PHPNAME, $indexType)];
            $this->numerocheque = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : VersementTableMap::translateFieldName('VersementIdmois', TableMap::TYPE_PHPNAME, $indexType)];
            $this->versement_idmois = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : VersementTableMap::translateFieldName('ContratIdcontrat', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contrat_idcontrat = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : VersementTableMap::translateFieldName('ContratProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contrat_produits_idproduits = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : VersementTableMap::translateFieldName('ContratProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contrat_produits_societe_idsociete = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : VersementTableMap::translateFieldName('ModepaiementIdmodepaiement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->modepaiement_idmodepaiement = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 9; // 9 = VersementTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Versement'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aMois !== null && $this->versement_idmois !== $this->aMois->getIdmois()) {
            $this->aMois = null;
        }
        if ($this->aContrat !== null && $this->contrat_idcontrat !== $this->aContrat->getIdcontrat()) {
            $this->aContrat = null;
        }
        if ($this->aContrat !== null && $this->contrat_produits_idproduits !== $this->aContrat->getProduitsIdproduits1()) {
            $this->aContrat = null;
        }
        if ($this->aContrat !== null && $this->contrat_produits_societe_idsociete !== $this->aContrat->getProduitsSocieteIdsociete()) {
            $this->aContrat = null;
        }
        if ($this->aModepaiement !== null && $this->modepaiement_idmodepaiement !== $this->aModepaiement->getIdmodepaiement()) {
            $this->aModepaiement = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(VersementTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildVersementQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aContrat = null;
            $this->aModepaiement = null;
            $this->aMois = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Versement::setDeleted()
     * @see Versement::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(VersementTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildVersementQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(VersementTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                VersementTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aContrat !== null) {
                if ($this->aContrat->isModified() || $this->aContrat->isNew()) {
                    $affectedRows += $this->aContrat->save($con);
                }
                $this->setContrat($this->aContrat);
            }

            if ($this->aModepaiement !== null) {
                if ($this->aModepaiement->isModified() || $this->aModepaiement->isNew()) {
                    $affectedRows += $this->aModepaiement->save($con);
                }
                $this->setModepaiement($this->aModepaiement);
            }

            if ($this->aMois !== null) {
                if ($this->aMois->isModified() || $this->aMois->isNew()) {
                    $affectedRows += $this->aMois->save($con);
                }
                $this->setMois($this->aMois);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[VersementTableMap::COL_IDVERSEMENT] = true;
        if (null !== $this->idversement) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . VersementTableMap::COL_IDVERSEMENT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(VersementTableMap::COL_IDVERSEMENT)) {
            $modifiedColumns[':p' . $index++]  = 'idversement';
        }
        if ($this->isColumnModified(VersementTableMap::COL_DATEVERSEMENT)) {
            $modifiedColumns[':p' . $index++]  = 'dateVersement';
        }
        if ($this->isColumnModified(VersementTableMap::COL_MONTANTVERSEMENT)) {
            $modifiedColumns[':p' . $index++]  = 'montantversement';
        }
        if ($this->isColumnModified(VersementTableMap::COL_NUMEROCHEQUE)) {
            $modifiedColumns[':p' . $index++]  = 'NumeroCheque';
        }
        if ($this->isColumnModified(VersementTableMap::COL_VERSEMENT_IDMOIS)) {
            $modifiedColumns[':p' . $index++]  = 'versement_idmois';
        }
        if ($this->isColumnModified(VersementTableMap::COL_CONTRAT_IDCONTRAT)) {
            $modifiedColumns[':p' . $index++]  = 'Contrat_idContrat';
        }
        if ($this->isColumnModified(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS)) {
            $modifiedColumns[':p' . $index++]  = 'Contrat_produits_idproduits';
        }
        if ($this->isColumnModified(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE)) {
            $modifiedColumns[':p' . $index++]  = 'Contrat_produits_societe_idsociete';
        }
        if ($this->isColumnModified(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT)) {
            $modifiedColumns[':p' . $index++]  = 'modePaiement_idmodePaiement';
        }

        $sql = sprintf(
            'INSERT INTO versement (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idversement':
                        $stmt->bindValue($identifier, $this->idversement, PDO::PARAM_INT);
                        break;
                    case 'dateVersement':
                        $stmt->bindValue($identifier, $this->dateversement ? $this->dateversement->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'montantversement':
                        $stmt->bindValue($identifier, $this->montantversement, PDO::PARAM_INT);
                        break;
                    case 'NumeroCheque':
                        $stmt->bindValue($identifier, $this->numerocheque, PDO::PARAM_STR);
                        break;
                    case 'versement_idmois':
                        $stmt->bindValue($identifier, $this->versement_idmois, PDO::PARAM_INT);
                        break;
                    case 'Contrat_idContrat':
                        $stmt->bindValue($identifier, $this->contrat_idcontrat, PDO::PARAM_INT);
                        break;
                    case 'Contrat_produits_idproduits':
                        $stmt->bindValue($identifier, $this->contrat_produits_idproduits, PDO::PARAM_INT);
                        break;
                    case 'Contrat_produits_societe_idsociete':
                        $stmt->bindValue($identifier, $this->contrat_produits_societe_idsociete, PDO::PARAM_INT);
                        break;
                    case 'modePaiement_idmodePaiement':
                        $stmt->bindValue($identifier, $this->modepaiement_idmodepaiement, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdversement($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = VersementTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdversement();
                break;
            case 1:
                return $this->getDateversement();
                break;
            case 2:
                return $this->getMontantversement();
                break;
            case 3:
                return $this->getNumerocheque();
                break;
            case 4:
                return $this->getVersementIdmois();
                break;
            case 5:
                return $this->getContratIdcontrat();
                break;
            case 6:
                return $this->getContratProduitsIdproduits();
                break;
            case 7:
                return $this->getContratProduitsSocieteIdsociete();
                break;
            case 8:
                return $this->getModepaiementIdmodepaiement();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Versement'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Versement'][$this->hashCode()] = true;
        $keys = VersementTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdversement(),
            $keys[1] => $this->getDateversement(),
            $keys[2] => $this->getMontantversement(),
            $keys[3] => $this->getNumerocheque(),
            $keys[4] => $this->getVersementIdmois(),
            $keys[5] => $this->getContratIdcontrat(),
            $keys[6] => $this->getContratProduitsIdproduits(),
            $keys[7] => $this->getContratProduitsSocieteIdsociete(),
            $keys[8] => $this->getModepaiementIdmodepaiement(),
        );
        if ($result[$keys[1]] instanceof \DateTimeInterface) {
            $result[$keys[1]] = $result[$keys[1]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aContrat) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contrat';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contrat';
                        break;
                    default:
                        $key = 'Contrat';
                }

                $result[$key] = $this->aContrat->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aModepaiement) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'modepaiement';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'modepaiement';
                        break;
                    default:
                        $key = 'Modepaiement';
                }

                $result[$key] = $this->aModepaiement->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMois) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mois';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mois';
                        break;
                    default:
                        $key = 'Mois';
                }

                $result[$key] = $this->aMois->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Versement
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = VersementTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Versement
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdversement($value);
                break;
            case 1:
                $this->setDateversement($value);
                break;
            case 2:
                $this->setMontantversement($value);
                break;
            case 3:
                $this->setNumerocheque($value);
                break;
            case 4:
                $this->setVersementIdmois($value);
                break;
            case 5:
                $this->setContratIdcontrat($value);
                break;
            case 6:
                $this->setContratProduitsIdproduits($value);
                break;
            case 7:
                $this->setContratProduitsSocieteIdsociete($value);
                break;
            case 8:
                $this->setModepaiementIdmodepaiement($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = VersementTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdversement($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDateversement($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setMontantversement($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNumerocheque($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setVersementIdmois($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setContratIdcontrat($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setContratProduitsIdproduits($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setContratProduitsSocieteIdsociete($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setModepaiementIdmodepaiement($arr[$keys[8]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Versement The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(VersementTableMap::DATABASE_NAME);

        if ($this->isColumnModified(VersementTableMap::COL_IDVERSEMENT)) {
            $criteria->add(VersementTableMap::COL_IDVERSEMENT, $this->idversement);
        }
        if ($this->isColumnModified(VersementTableMap::COL_DATEVERSEMENT)) {
            $criteria->add(VersementTableMap::COL_DATEVERSEMENT, $this->dateversement);
        }
        if ($this->isColumnModified(VersementTableMap::COL_MONTANTVERSEMENT)) {
            $criteria->add(VersementTableMap::COL_MONTANTVERSEMENT, $this->montantversement);
        }
        if ($this->isColumnModified(VersementTableMap::COL_NUMEROCHEQUE)) {
            $criteria->add(VersementTableMap::COL_NUMEROCHEQUE, $this->numerocheque);
        }
        if ($this->isColumnModified(VersementTableMap::COL_VERSEMENT_IDMOIS)) {
            $criteria->add(VersementTableMap::COL_VERSEMENT_IDMOIS, $this->versement_idmois);
        }
        if ($this->isColumnModified(VersementTableMap::COL_CONTRAT_IDCONTRAT)) {
            $criteria->add(VersementTableMap::COL_CONTRAT_IDCONTRAT, $this->contrat_idcontrat);
        }
        if ($this->isColumnModified(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS)) {
            $criteria->add(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $this->contrat_produits_idproduits);
        }
        if ($this->isColumnModified(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE)) {
            $criteria->add(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $this->contrat_produits_societe_idsociete);
        }
        if ($this->isColumnModified(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT)) {
            $criteria->add(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT, $this->modepaiement_idmodepaiement);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildVersementQuery::create();
        $criteria->add(VersementTableMap::COL_IDVERSEMENT, $this->idversement);
        $criteria->add(VersementTableMap::COL_VERSEMENT_IDMOIS, $this->versement_idmois);
        $criteria->add(VersementTableMap::COL_CONTRAT_IDCONTRAT, $this->contrat_idcontrat);
        $criteria->add(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $this->contrat_produits_idproduits);
        $criteria->add(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $this->contrat_produits_societe_idsociete);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdversement() &&
            null !== $this->getVersementIdmois() &&
            null !== $this->getContratIdcontrat() &&
            null !== $this->getContratProduitsIdproduits() &&
            null !== $this->getContratProduitsSocieteIdsociete();

        $validPrimaryKeyFKs = 4;
        $primaryKeyFKs = [];

        //relation fk_versement_Contrat1 to table contrat
        if ($this->aContrat && $hash = spl_object_hash($this->aContrat)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        //relation fk_versement_mois1 to table mois
        if ($this->aMois && $hash = spl_object_hash($this->aMois)) {
            $primaryKeyFKs[] = $hash;
        } else {
            $validPrimaryKeyFKs = false;
        }

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getIdversement();
        $pks[1] = $this->getVersementIdmois();
        $pks[2] = $this->getContratIdcontrat();
        $pks[3] = $this->getContratProduitsIdproduits();
        $pks[4] = $this->getContratProduitsSocieteIdsociete();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param      array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setIdversement($keys[0]);
        $this->setVersementIdmois($keys[1]);
        $this->setContratIdcontrat($keys[2]);
        $this->setContratProduitsIdproduits($keys[3]);
        $this->setContratProduitsSocieteIdsociete($keys[4]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return (null === $this->getIdversement()) && (null === $this->getVersementIdmois()) && (null === $this->getContratIdcontrat()) && (null === $this->getContratProduitsIdproduits()) && (null === $this->getContratProduitsSocieteIdsociete());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Versement (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDateversement($this->getDateversement());
        $copyObj->setMontantversement($this->getMontantversement());
        $copyObj->setNumerocheque($this->getNumerocheque());
        $copyObj->setVersementIdmois($this->getVersementIdmois());
        $copyObj->setContratIdcontrat($this->getContratIdcontrat());
        $copyObj->setContratProduitsIdproduits($this->getContratProduitsIdproduits());
        $copyObj->setContratProduitsSocieteIdsociete($this->getContratProduitsSocieteIdsociete());
        $copyObj->setModepaiementIdmodepaiement($this->getModepaiementIdmodepaiement());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdversement(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Versement Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildContrat object.
     *
     * @param  ChildContrat $v
     * @return $this|\Versement The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContrat(ChildContrat $v = null)
    {
        if ($v === null) {
            $this->setContratIdcontrat(NULL);
        } else {
            $this->setContratIdcontrat($v->getIdcontrat());
        }

        if ($v === null) {
            $this->setContratProduitsIdproduits(NULL);
        } else {
            $this->setContratProduitsIdproduits($v->getProduitsIdproduits1());
        }

        if ($v === null) {
            $this->setContratProduitsSocieteIdsociete(NULL);
        } else {
            $this->setContratProduitsSocieteIdsociete($v->getProduitsSocieteIdsociete());
        }

        $this->aContrat = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildContrat object, it will not be re-added.
        if ($v !== null) {
            $v->addVersement($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildContrat object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildContrat The associated ChildContrat object.
     * @throws PropelException
     */
    public function getContrat(ConnectionInterface $con = null)
    {
        if ($this->aContrat === null && ($this->contrat_idcontrat != 0 && $this->contrat_produits_idproduits != 0 && $this->contrat_produits_societe_idsociete != 0)) {
            $this->aContrat = ChildContratQuery::create()->findPk(array($this->contrat_idcontrat, $this->contrat_produits_idproduits, $this->contrat_produits_societe_idsociete), $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContrat->addVersements($this);
             */
        }

        return $this->aContrat;
    }

    /**
     * Declares an association between this object and a ChildModepaiement object.
     *
     * @param  ChildModepaiement $v
     * @return $this|\Versement The current object (for fluent API support)
     * @throws PropelException
     */
    public function setModepaiement(ChildModepaiement $v = null)
    {
        if ($v === null) {
            $this->setModepaiementIdmodepaiement(NULL);
        } else {
            $this->setModepaiementIdmodepaiement($v->getIdmodepaiement());
        }

        $this->aModepaiement = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildModepaiement object, it will not be re-added.
        if ($v !== null) {
            $v->addVersement($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildModepaiement object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildModepaiement The associated ChildModepaiement object.
     * @throws PropelException
     */
    public function getModepaiement(ConnectionInterface $con = null)
    {
        if ($this->aModepaiement === null && ($this->modepaiement_idmodepaiement != 0)) {
            $this->aModepaiement = ChildModepaiementQuery::create()->findPk($this->modepaiement_idmodepaiement, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aModepaiement->addVersements($this);
             */
        }

        return $this->aModepaiement;
    }

    /**
     * Declares an association between this object and a ChildMois object.
     *
     * @param  ChildMois $v
     * @return $this|\Versement The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMois(ChildMois $v = null)
    {
        if ($v === null) {
            $this->setVersementIdmois(NULL);
        } else {
            $this->setVersementIdmois($v->getIdmois());
        }

        $this->aMois = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMois object, it will not be re-added.
        if ($v !== null) {
            $v->addVersement($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMois object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMois The associated ChildMois object.
     * @throws PropelException
     */
    public function getMois(ConnectionInterface $con = null)
    {
        if ($this->aMois === null && ($this->versement_idmois != 0)) {
            $this->aMois = ChildMoisQuery::create()->findPk($this->versement_idmois, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMois->addVersements($this);
             */
        }

        return $this->aMois;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aContrat) {
            $this->aContrat->removeVersement($this);
        }
        if (null !== $this->aModepaiement) {
            $this->aModepaiement->removeVersement($this);
        }
        if (null !== $this->aMois) {
            $this->aMois->removeVersement($this);
        }
        $this->idversement = null;
        $this->dateversement = null;
        $this->montantversement = null;
        $this->numerocheque = null;
        $this->versement_idmois = null;
        $this->contrat_idcontrat = null;
        $this->contrat_produits_idproduits = null;
        $this->contrat_produits_societe_idsociete = null;
        $this->modepaiement_idmodepaiement = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aContrat = null;
        $this->aModepaiement = null;
        $this->aMois = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(VersementTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
