<?php

namespace Base;

use \Prime as ChildPrime;
use \PrimeQuery as ChildPrimeQuery;
use \Exception;
use \PDO;
use Map\PrimeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'prime' table.
 *
 *
 *
 * @method     ChildPrimeQuery orderByIdprime($order = Criteria::ASC) Order by the idprime column
 * @method     ChildPrimeQuery orderByTauxprime($order = Criteria::ASC) Order by the tauxprime column
 * @method     ChildPrimeQuery orderByProduitsIdproduits($order = Criteria::ASC) Order by the produits_idproduits column
 * @method     ChildPrimeQuery orderByProduitsSocieteIdsociete($order = Criteria::ASC) Order by the produits_societe_idsociete column
 *
 * @method     ChildPrimeQuery groupByIdprime() Group by the idprime column
 * @method     ChildPrimeQuery groupByTauxprime() Group by the tauxprime column
 * @method     ChildPrimeQuery groupByProduitsIdproduits() Group by the produits_idproduits column
 * @method     ChildPrimeQuery groupByProduitsSocieteIdsociete() Group by the produits_societe_idsociete column
 *
 * @method     ChildPrimeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPrimeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPrimeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPrimeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPrimeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPrimeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPrimeQuery leftJoinProduits($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produits relation
 * @method     ChildPrimeQuery rightJoinProduits($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produits relation
 * @method     ChildPrimeQuery innerJoinProduits($relationAlias = null) Adds a INNER JOIN clause to the query using the Produits relation
 *
 * @method     ChildPrimeQuery joinWithProduits($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produits relation
 *
 * @method     ChildPrimeQuery leftJoinWithProduits() Adds a LEFT JOIN clause and with to the query using the Produits relation
 * @method     ChildPrimeQuery rightJoinWithProduits() Adds a RIGHT JOIN clause and with to the query using the Produits relation
 * @method     ChildPrimeQuery innerJoinWithProduits() Adds a INNER JOIN clause and with to the query using the Produits relation
 *
 * @method     \ProduitsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPrime findOne(ConnectionInterface $con = null) Return the first ChildPrime matching the query
 * @method     ChildPrime findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPrime matching the query, or a new ChildPrime object populated from the query conditions when no match is found
 *
 * @method     ChildPrime findOneByIdprime(int $idprime) Return the first ChildPrime filtered by the idprime column
 * @method     ChildPrime findOneByTauxprime(int $tauxprime) Return the first ChildPrime filtered by the tauxprime column
 * @method     ChildPrime findOneByProduitsIdproduits(int $produits_idproduits) Return the first ChildPrime filtered by the produits_idproduits column
 * @method     ChildPrime findOneByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return the first ChildPrime filtered by the produits_societe_idsociete column *

 * @method     ChildPrime requirePk($key, ConnectionInterface $con = null) Return the ChildPrime by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrime requireOne(ConnectionInterface $con = null) Return the first ChildPrime matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrime requireOneByIdprime(int $idprime) Return the first ChildPrime filtered by the idprime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrime requireOneByTauxprime(int $tauxprime) Return the first ChildPrime filtered by the tauxprime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrime requireOneByProduitsIdproduits(int $produits_idproduits) Return the first ChildPrime filtered by the produits_idproduits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrime requireOneByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return the first ChildPrime filtered by the produits_societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrime[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPrime objects based on current ModelCriteria
 * @method     ChildPrime[]|ObjectCollection findByIdprime(int $idprime) Return ChildPrime objects filtered by the idprime column
 * @method     ChildPrime[]|ObjectCollection findByTauxprime(int $tauxprime) Return ChildPrime objects filtered by the tauxprime column
 * @method     ChildPrime[]|ObjectCollection findByProduitsIdproduits(int $produits_idproduits) Return ChildPrime objects filtered by the produits_idproduits column
 * @method     ChildPrime[]|ObjectCollection findByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return ChildPrime objects filtered by the produits_societe_idsociete column
 * @method     ChildPrime[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PrimeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\PrimeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Prime', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPrimeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPrimeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPrimeQuery) {
            return $criteria;
        }
        $query = new ChildPrimeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$idprime, $produits_idproduits, $produits_societe_idsociete] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPrime|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PrimeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PrimeTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrime A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idprime, tauxprime, produits_idproduits, produits_societe_idsociete FROM prime WHERE idprime = :p0 AND produits_idproduits = :p1 AND produits_societe_idsociete = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPrime $obj */
            $obj = new ChildPrime();
            $obj->hydrate($row);
            PrimeTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPrime|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPrimeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(PrimeTableMap::COL_IDPRIME, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(PrimeTableMap::COL_PRODUITS_IDPRODUITS, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPrimeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(PrimeTableMap::COL_IDPRIME, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(PrimeTableMap::COL_PRODUITS_IDPRODUITS, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idprime column
     *
     * Example usage:
     * <code>
     * $query->filterByIdprime(1234); // WHERE idprime = 1234
     * $query->filterByIdprime(array(12, 34)); // WHERE idprime IN (12, 34)
     * $query->filterByIdprime(array('min' => 12)); // WHERE idprime > 12
     * </code>
     *
     * @param     mixed $idprime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrimeQuery The current query, for fluid interface
     */
    public function filterByIdprime($idprime = null, $comparison = null)
    {
        if (is_array($idprime)) {
            $useMinMax = false;
            if (isset($idprime['min'])) {
                $this->addUsingAlias(PrimeTableMap::COL_IDPRIME, $idprime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idprime['max'])) {
                $this->addUsingAlias(PrimeTableMap::COL_IDPRIME, $idprime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrimeTableMap::COL_IDPRIME, $idprime, $comparison);
    }

    /**
     * Filter the query on the tauxprime column
     *
     * Example usage:
     * <code>
     * $query->filterByTauxprime(1234); // WHERE tauxprime = 1234
     * $query->filterByTauxprime(array(12, 34)); // WHERE tauxprime IN (12, 34)
     * $query->filterByTauxprime(array('min' => 12)); // WHERE tauxprime > 12
     * </code>
     *
     * @param     mixed $tauxprime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrimeQuery The current query, for fluid interface
     */
    public function filterByTauxprime($tauxprime = null, $comparison = null)
    {
        if (is_array($tauxprime)) {
            $useMinMax = false;
            if (isset($tauxprime['min'])) {
                $this->addUsingAlias(PrimeTableMap::COL_TAUXPRIME, $tauxprime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tauxprime['max'])) {
                $this->addUsingAlias(PrimeTableMap::COL_TAUXPRIME, $tauxprime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrimeTableMap::COL_TAUXPRIME, $tauxprime, $comparison);
    }

    /**
     * Filter the query on the produits_idproduits column
     *
     * Example usage:
     * <code>
     * $query->filterByProduitsIdproduits(1234); // WHERE produits_idproduits = 1234
     * $query->filterByProduitsIdproduits(array(12, 34)); // WHERE produits_idproduits IN (12, 34)
     * $query->filterByProduitsIdproduits(array('min' => 12)); // WHERE produits_idproduits > 12
     * </code>
     *
     * @see       filterByProduits()
     *
     * @param     mixed $produitsIdproduits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrimeQuery The current query, for fluid interface
     */
    public function filterByProduitsIdproduits($produitsIdproduits = null, $comparison = null)
    {
        if (is_array($produitsIdproduits)) {
            $useMinMax = false;
            if (isset($produitsIdproduits['min'])) {
                $this->addUsingAlias(PrimeTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produitsIdproduits['max'])) {
                $this->addUsingAlias(PrimeTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrimeTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits, $comparison);
    }

    /**
     * Filter the query on the produits_societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterByProduitsSocieteIdsociete(1234); // WHERE produits_societe_idsociete = 1234
     * $query->filterByProduitsSocieteIdsociete(array(12, 34)); // WHERE produits_societe_idsociete IN (12, 34)
     * $query->filterByProduitsSocieteIdsociete(array('min' => 12)); // WHERE produits_societe_idsociete > 12
     * </code>
     *
     * @see       filterByProduits()
     *
     * @param     mixed $produitsSocieteIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPrimeQuery The current query, for fluid interface
     */
    public function filterByProduitsSocieteIdsociete($produitsSocieteIdsociete = null, $comparison = null)
    {
        if (is_array($produitsSocieteIdsociete)) {
            $useMinMax = false;
            if (isset($produitsSocieteIdsociete['min'])) {
                $this->addUsingAlias(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produitsSocieteIdsociete['max'])) {
                $this->addUsingAlias(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete, $comparison);
    }

    /**
     * Filter the query by a related \Produits object
     *
     * @param \Produits $produits The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrimeQuery The current query, for fluid interface
     */
    public function filterByProduits($produits, $comparison = null)
    {
        if ($produits instanceof \Produits) {
            return $this
                ->addUsingAlias(PrimeTableMap::COL_PRODUITS_IDPRODUITS, $produits->getIdproduits(), $comparison)
                ->addUsingAlias(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produits->getSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByProduits() only accepts arguments of type \Produits');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produits relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPrimeQuery The current query, for fluid interface
     */
    public function joinProduits($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produits');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produits');
        }

        return $this;
    }

    /**
     * Use the Produits relation Produits object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProduitsQuery A secondary query class using the current class as primary query
     */
    public function useProduitsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduits($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produits', '\ProduitsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPrime $prime Object to remove from the list of results
     *
     * @return $this|ChildPrimeQuery The current query, for fluid interface
     */
    public function prune($prime = null)
    {
        if ($prime) {
            $this->addCond('pruneCond0', $this->getAliasedColName(PrimeTableMap::COL_IDPRIME), $prime->getIdprime(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(PrimeTableMap::COL_PRODUITS_IDPRODUITS), $prime->getProduitsIdproduits(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE), $prime->getProduitsSocieteIdsociete(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the prime table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrimeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PrimeTableMap::clearInstancePool();
            PrimeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrimeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PrimeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PrimeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PrimeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PrimeQuery
