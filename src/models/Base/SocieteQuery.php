<?php

namespace Base;

use \Societe as ChildSociete;
use \SocieteQuery as ChildSocieteQuery;
use \Exception;
use \PDO;
use Map\SocieteTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'societe' table.
 *
 *
 *
 * @method     ChildSocieteQuery orderByIdsociete($order = Criteria::ASC) Order by the idsociete column
 * @method     ChildSocieteQuery orderByLibellesociete($order = Criteria::ASC) Order by the Libellesociete column
 * @method     ChildSocieteQuery orderByVilleIdville($order = Criteria::ASC) Order by the ville_idville column
 * @method     ChildSocieteQuery orderByVillePaysIdpays($order = Criteria::ASC) Order by the ville_pays_idpays column
 *
 * @method     ChildSocieteQuery groupByIdsociete() Group by the idsociete column
 * @method     ChildSocieteQuery groupByLibellesociete() Group by the Libellesociete column
 * @method     ChildSocieteQuery groupByVilleIdville() Group by the ville_idville column
 * @method     ChildSocieteQuery groupByVillePaysIdpays() Group by the ville_pays_idpays column
 *
 * @method     ChildSocieteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSocieteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSocieteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSocieteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSocieteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSocieteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSocieteQuery leftJoinVille($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ville relation
 * @method     ChildSocieteQuery rightJoinVille($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ville relation
 * @method     ChildSocieteQuery innerJoinVille($relationAlias = null) Adds a INNER JOIN clause to the query using the Ville relation
 *
 * @method     ChildSocieteQuery joinWithVille($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ville relation
 *
 * @method     ChildSocieteQuery leftJoinWithVille() Adds a LEFT JOIN clause and with to the query using the Ville relation
 * @method     ChildSocieteQuery rightJoinWithVille() Adds a RIGHT JOIN clause and with to the query using the Ville relation
 * @method     ChildSocieteQuery innerJoinWithVille() Adds a INNER JOIN clause and with to the query using the Ville relation
 *
 * @method     ChildSocieteQuery leftJoinCatgorieproduit($relationAlias = null) Adds a LEFT JOIN clause to the query using the Catgorieproduit relation
 * @method     ChildSocieteQuery rightJoinCatgorieproduit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Catgorieproduit relation
 * @method     ChildSocieteQuery innerJoinCatgorieproduit($relationAlias = null) Adds a INNER JOIN clause to the query using the Catgorieproduit relation
 *
 * @method     ChildSocieteQuery joinWithCatgorieproduit($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Catgorieproduit relation
 *
 * @method     ChildSocieteQuery leftJoinWithCatgorieproduit() Adds a LEFT JOIN clause and with to the query using the Catgorieproduit relation
 * @method     ChildSocieteQuery rightJoinWithCatgorieproduit() Adds a RIGHT JOIN clause and with to the query using the Catgorieproduit relation
 * @method     ChildSocieteQuery innerJoinWithCatgorieproduit() Adds a INNER JOIN clause and with to the query using the Catgorieproduit relation
 *
 * @method     ChildSocieteQuery leftJoinModepaiement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Modepaiement relation
 * @method     ChildSocieteQuery rightJoinModepaiement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Modepaiement relation
 * @method     ChildSocieteQuery innerJoinModepaiement($relationAlias = null) Adds a INNER JOIN clause to the query using the Modepaiement relation
 *
 * @method     ChildSocieteQuery joinWithModepaiement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Modepaiement relation
 *
 * @method     ChildSocieteQuery leftJoinWithModepaiement() Adds a LEFT JOIN clause and with to the query using the Modepaiement relation
 * @method     ChildSocieteQuery rightJoinWithModepaiement() Adds a RIGHT JOIN clause and with to the query using the Modepaiement relation
 * @method     ChildSocieteQuery innerJoinWithModepaiement() Adds a INNER JOIN clause and with to the query using the Modepaiement relation
 *
 * @method     ChildSocieteQuery leftJoinPersonne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Personne relation
 * @method     ChildSocieteQuery rightJoinPersonne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Personne relation
 * @method     ChildSocieteQuery innerJoinPersonne($relationAlias = null) Adds a INNER JOIN clause to the query using the Personne relation
 *
 * @method     ChildSocieteQuery joinWithPersonne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Personne relation
 *
 * @method     ChildSocieteQuery leftJoinWithPersonne() Adds a LEFT JOIN clause and with to the query using the Personne relation
 * @method     ChildSocieteQuery rightJoinWithPersonne() Adds a RIGHT JOIN clause and with to the query using the Personne relation
 * @method     ChildSocieteQuery innerJoinWithPersonne() Adds a INNER JOIN clause and with to the query using the Personne relation
 *
 * @method     ChildSocieteQuery leftJoinProduits($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produits relation
 * @method     ChildSocieteQuery rightJoinProduits($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produits relation
 * @method     ChildSocieteQuery innerJoinProduits($relationAlias = null) Adds a INNER JOIN clause to the query using the Produits relation
 *
 * @method     ChildSocieteQuery joinWithProduits($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produits relation
 *
 * @method     ChildSocieteQuery leftJoinWithProduits() Adds a LEFT JOIN clause and with to the query using the Produits relation
 * @method     ChildSocieteQuery rightJoinWithProduits() Adds a RIGHT JOIN clause and with to the query using the Produits relation
 * @method     ChildSocieteQuery innerJoinWithProduits() Adds a INNER JOIN clause and with to the query using the Produits relation
 *
 * @method     ChildSocieteQuery leftJoinRolepersonne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Rolepersonne relation
 * @method     ChildSocieteQuery rightJoinRolepersonne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Rolepersonne relation
 * @method     ChildSocieteQuery innerJoinRolepersonne($relationAlias = null) Adds a INNER JOIN clause to the query using the Rolepersonne relation
 *
 * @method     ChildSocieteQuery joinWithRolepersonne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Rolepersonne relation
 *
 * @method     ChildSocieteQuery leftJoinWithRolepersonne() Adds a LEFT JOIN clause and with to the query using the Rolepersonne relation
 * @method     ChildSocieteQuery rightJoinWithRolepersonne() Adds a RIGHT JOIN clause and with to the query using the Rolepersonne relation
 * @method     ChildSocieteQuery innerJoinWithRolepersonne() Adds a INNER JOIN clause and with to the query using the Rolepersonne relation
 *
 * @method     \VilleQuery|\CatgorieproduitQuery|\ModepaiementQuery|\PersonneQuery|\ProduitsQuery|\RolepersonneQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSociete findOne(ConnectionInterface $con = null) Return the first ChildSociete matching the query
 * @method     ChildSociete findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSociete matching the query, or a new ChildSociete object populated from the query conditions when no match is found
 *
 * @method     ChildSociete findOneByIdsociete(int $idsociete) Return the first ChildSociete filtered by the idsociete column
 * @method     ChildSociete findOneByLibellesociete(string $Libellesociete) Return the first ChildSociete filtered by the Libellesociete column
 * @method     ChildSociete findOneByVilleIdville(int $ville_idville) Return the first ChildSociete filtered by the ville_idville column
 * @method     ChildSociete findOneByVillePaysIdpays(int $ville_pays_idpays) Return the first ChildSociete filtered by the ville_pays_idpays column *

 * @method     ChildSociete requirePk($key, ConnectionInterface $con = null) Return the ChildSociete by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSociete requireOne(ConnectionInterface $con = null) Return the first ChildSociete matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSociete requireOneByIdsociete(int $idsociete) Return the first ChildSociete filtered by the idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSociete requireOneByLibellesociete(string $Libellesociete) Return the first ChildSociete filtered by the Libellesociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSociete requireOneByVilleIdville(int $ville_idville) Return the first ChildSociete filtered by the ville_idville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSociete requireOneByVillePaysIdpays(int $ville_pays_idpays) Return the first ChildSociete filtered by the ville_pays_idpays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSociete[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSociete objects based on current ModelCriteria
 * @method     ChildSociete[]|ObjectCollection findByIdsociete(int $idsociete) Return ChildSociete objects filtered by the idsociete column
 * @method     ChildSociete[]|ObjectCollection findByLibellesociete(string $Libellesociete) Return ChildSociete objects filtered by the Libellesociete column
 * @method     ChildSociete[]|ObjectCollection findByVilleIdville(int $ville_idville) Return ChildSociete objects filtered by the ville_idville column
 * @method     ChildSociete[]|ObjectCollection findByVillePaysIdpays(int $ville_pays_idpays) Return ChildSociete objects filtered by the ville_pays_idpays column
 * @method     ChildSociete[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SocieteQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\SocieteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Societe', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSocieteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSocieteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSocieteQuery) {
            return $criteria;
        }
        $query = new ChildSocieteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSociete|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SocieteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SocieteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSociete A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idsociete, Libellesociete, ville_idville, ville_pays_idpays FROM societe WHERE idsociete = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSociete $obj */
            $obj = new ChildSociete();
            $obj->hydrate($row);
            SocieteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSociete|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterByIdsociete(1234); // WHERE idsociete = 1234
     * $query->filterByIdsociete(array(12, 34)); // WHERE idsociete IN (12, 34)
     * $query->filterByIdsociete(array('min' => 12)); // WHERE idsociete > 12
     * </code>
     *
     * @param     mixed $idsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByIdsociete($idsociete = null, $comparison = null)
    {
        if (is_array($idsociete)) {
            $useMinMax = false;
            if (isset($idsociete['min'])) {
                $this->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $idsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idsociete['max'])) {
                $this->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $idsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $idsociete, $comparison);
    }

    /**
     * Filter the query on the Libellesociete column
     *
     * Example usage:
     * <code>
     * $query->filterByLibellesociete('fooValue');   // WHERE Libellesociete = 'fooValue'
     * $query->filterByLibellesociete('%fooValue%', Criteria::LIKE); // WHERE Libellesociete LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libellesociete The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByLibellesociete($libellesociete = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libellesociete)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SocieteTableMap::COL_LIBELLESOCIETE, $libellesociete, $comparison);
    }

    /**
     * Filter the query on the ville_idville column
     *
     * Example usage:
     * <code>
     * $query->filterByVilleIdville(1234); // WHERE ville_idville = 1234
     * $query->filterByVilleIdville(array(12, 34)); // WHERE ville_idville IN (12, 34)
     * $query->filterByVilleIdville(array('min' => 12)); // WHERE ville_idville > 12
     * </code>
     *
     * @see       filterByVille()
     *
     * @param     mixed $villeIdville The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByVilleIdville($villeIdville = null, $comparison = null)
    {
        if (is_array($villeIdville)) {
            $useMinMax = false;
            if (isset($villeIdville['min'])) {
                $this->addUsingAlias(SocieteTableMap::COL_VILLE_IDVILLE, $villeIdville['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($villeIdville['max'])) {
                $this->addUsingAlias(SocieteTableMap::COL_VILLE_IDVILLE, $villeIdville['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SocieteTableMap::COL_VILLE_IDVILLE, $villeIdville, $comparison);
    }

    /**
     * Filter the query on the ville_pays_idpays column
     *
     * Example usage:
     * <code>
     * $query->filterByVillePaysIdpays(1234); // WHERE ville_pays_idpays = 1234
     * $query->filterByVillePaysIdpays(array(12, 34)); // WHERE ville_pays_idpays IN (12, 34)
     * $query->filterByVillePaysIdpays(array('min' => 12)); // WHERE ville_pays_idpays > 12
     * </code>
     *
     * @see       filterByVille()
     *
     * @param     mixed $villePaysIdpays The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByVillePaysIdpays($villePaysIdpays = null, $comparison = null)
    {
        if (is_array($villePaysIdpays)) {
            $useMinMax = false;
            if (isset($villePaysIdpays['min'])) {
                $this->addUsingAlias(SocieteTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($villePaysIdpays['max'])) {
                $this->addUsingAlias(SocieteTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SocieteTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays, $comparison);
    }

    /**
     * Filter the query by a related \Ville object
     *
     * @param \Ville $ville The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByVille($ville, $comparison = null)
    {
        if ($ville instanceof \Ville) {
            return $this
                ->addUsingAlias(SocieteTableMap::COL_VILLE_IDVILLE, $ville->getIdville(), $comparison)
                ->addUsingAlias(SocieteTableMap::COL_VILLE_PAYS_IDPAYS, $ville->getPaysIdpays(), $comparison);
        } else {
            throw new PropelException('filterByVille() only accepts arguments of type \Ville');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ville relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function joinVille($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ville');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ville');
        }

        return $this;
    }

    /**
     * Use the Ville relation Ville object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VilleQuery A secondary query class using the current class as primary query
     */
    public function useVilleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVille($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ville', '\VilleQuery');
    }

    /**
     * Filter the query by a related \Catgorieproduit object
     *
     * @param \Catgorieproduit|ObjectCollection $catgorieproduit the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByCatgorieproduit($catgorieproduit, $comparison = null)
    {
        if ($catgorieproduit instanceof \Catgorieproduit) {
            return $this
                ->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $catgorieproduit->getSocieteIdsociete(), $comparison);
        } elseif ($catgorieproduit instanceof ObjectCollection) {
            return $this
                ->useCatgorieproduitQuery()
                ->filterByPrimaryKeys($catgorieproduit->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCatgorieproduit() only accepts arguments of type \Catgorieproduit or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Catgorieproduit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function joinCatgorieproduit($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Catgorieproduit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Catgorieproduit');
        }

        return $this;
    }

    /**
     * Use the Catgorieproduit relation Catgorieproduit object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \CatgorieproduitQuery A secondary query class using the current class as primary query
     */
    public function useCatgorieproduitQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCatgorieproduit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Catgorieproduit', '\CatgorieproduitQuery');
    }

    /**
     * Filter the query by a related \Modepaiement object
     *
     * @param \Modepaiement|ObjectCollection $modepaiement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByModepaiement($modepaiement, $comparison = null)
    {
        if ($modepaiement instanceof \Modepaiement) {
            return $this
                ->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $modepaiement->getSocieteIdsociete(), $comparison);
        } elseif ($modepaiement instanceof ObjectCollection) {
            return $this
                ->useModepaiementQuery()
                ->filterByPrimaryKeys($modepaiement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByModepaiement() only accepts arguments of type \Modepaiement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Modepaiement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function joinModepaiement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Modepaiement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Modepaiement');
        }

        return $this;
    }

    /**
     * Use the Modepaiement relation Modepaiement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ModepaiementQuery A secondary query class using the current class as primary query
     */
    public function useModepaiementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinModepaiement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Modepaiement', '\ModepaiementQuery');
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne|ObjectCollection $personne the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByPersonne($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $personne->getSocieteIdsociete(), $comparison);
        } elseif ($personne instanceof ObjectCollection) {
            return $this
                ->usePersonneQuery()
                ->filterByPrimaryKeys($personne->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPersonne() only accepts arguments of type \Personne or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Personne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function joinPersonne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Personne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Personne');
        }

        return $this;
    }

    /**
     * Use the Personne relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Personne', '\PersonneQuery');
    }

    /**
     * Filter the query by a related \Produits object
     *
     * @param \Produits|ObjectCollection $produits the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByProduits($produits, $comparison = null)
    {
        if ($produits instanceof \Produits) {
            return $this
                ->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $produits->getSocieteIdsociete(), $comparison);
        } elseif ($produits instanceof ObjectCollection) {
            return $this
                ->useProduitsQuery()
                ->filterByPrimaryKeys($produits->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProduits() only accepts arguments of type \Produits or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produits relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function joinProduits($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produits');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produits');
        }

        return $this;
    }

    /**
     * Use the Produits relation Produits object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProduitsQuery A secondary query class using the current class as primary query
     */
    public function useProduitsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduits($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produits', '\ProduitsQuery');
    }

    /**
     * Filter the query by a related \Rolepersonne object
     *
     * @param \Rolepersonne|ObjectCollection $rolepersonne the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSocieteQuery The current query, for fluid interface
     */
    public function filterByRolepersonne($rolepersonne, $comparison = null)
    {
        if ($rolepersonne instanceof \Rolepersonne) {
            return $this
                ->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $rolepersonne->getSocieteIdsociete(), $comparison);
        } elseif ($rolepersonne instanceof ObjectCollection) {
            return $this
                ->useRolepersonneQuery()
                ->filterByPrimaryKeys($rolepersonne->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRolepersonne() only accepts arguments of type \Rolepersonne or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Rolepersonne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function joinRolepersonne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Rolepersonne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Rolepersonne');
        }

        return $this;
    }

    /**
     * Use the Rolepersonne relation Rolepersonne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RolepersonneQuery A secondary query class using the current class as primary query
     */
    public function useRolepersonneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRolepersonne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Rolepersonne', '\RolepersonneQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSociete $societe Object to remove from the list of results
     *
     * @return $this|ChildSocieteQuery The current query, for fluid interface
     */
    public function prune($societe = null)
    {
        if ($societe) {
            $this->addUsingAlias(SocieteTableMap::COL_IDSOCIETE, $societe->getIdsociete(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the societe table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SocieteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SocieteTableMap::clearInstancePool();
            SocieteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SocieteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SocieteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SocieteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SocieteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SocieteQuery
