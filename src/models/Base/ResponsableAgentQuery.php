<?php

namespace Base;

use \ResponsableAgent as ChildResponsableAgent;
use \ResponsableAgentQuery as ChildResponsableAgentQuery;
use \Exception;
use \PDO;
use Map\ResponsableAgentTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'responsable_agent' table.
 *
 *
 *
 * @method     ChildResponsableAgentQuery orderByIdresponsable($order = Criteria::ASC) Order by the idResponsable column
 * @method     ChildResponsableAgentQuery orderByPersonneIdresponsable($order = Criteria::ASC) Order by the Personne_idResponsable column
 * @method     ChildResponsableAgentQuery orderByIdroleresponsable($order = Criteria::ASC) Order by the idroleResponsable column
 * @method     ChildResponsableAgentQuery orderByPersonneIdagent($order = Criteria::ASC) Order by the Personne_idAgent column
 * @method     ChildResponsableAgentQuery orderByIdroleagent($order = Criteria::ASC) Order by the idroleAgent column
 * @method     ChildResponsableAgentQuery orderByDatedebutresponsabilite($order = Criteria::ASC) Order by the DateDebutResponsabilite column
 *
 * @method     ChildResponsableAgentQuery groupByIdresponsable() Group by the idResponsable column
 * @method     ChildResponsableAgentQuery groupByPersonneIdresponsable() Group by the Personne_idResponsable column
 * @method     ChildResponsableAgentQuery groupByIdroleresponsable() Group by the idroleResponsable column
 * @method     ChildResponsableAgentQuery groupByPersonneIdagent() Group by the Personne_idAgent column
 * @method     ChildResponsableAgentQuery groupByIdroleagent() Group by the idroleAgent column
 * @method     ChildResponsableAgentQuery groupByDatedebutresponsabilite() Group by the DateDebutResponsabilite column
 *
 * @method     ChildResponsableAgentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildResponsableAgentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildResponsableAgentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildResponsableAgentQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildResponsableAgentQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildResponsableAgentQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildResponsableAgentQuery leftJoinPersonneRelatedByPersonneIdresponsableIdroleresponsable($relationAlias = null) Adds a LEFT JOIN clause to the query using the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation
 * @method     ChildResponsableAgentQuery rightJoinPersonneRelatedByPersonneIdresponsableIdroleresponsable($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation
 * @method     ChildResponsableAgentQuery innerJoinPersonneRelatedByPersonneIdresponsableIdroleresponsable($relationAlias = null) Adds a INNER JOIN clause to the query using the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation
 *
 * @method     ChildResponsableAgentQuery joinWithPersonneRelatedByPersonneIdresponsableIdroleresponsable($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation
 *
 * @method     ChildResponsableAgentQuery leftJoinWithPersonneRelatedByPersonneIdresponsableIdroleresponsable() Adds a LEFT JOIN clause and with to the query using the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation
 * @method     ChildResponsableAgentQuery rightJoinWithPersonneRelatedByPersonneIdresponsableIdroleresponsable() Adds a RIGHT JOIN clause and with to the query using the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation
 * @method     ChildResponsableAgentQuery innerJoinWithPersonneRelatedByPersonneIdresponsableIdroleresponsable() Adds a INNER JOIN clause and with to the query using the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation
 *
 * @method     ChildResponsableAgentQuery leftJoinPersonneRelatedByPersonneIdagentIdroleagent($relationAlias = null) Adds a LEFT JOIN clause to the query using the PersonneRelatedByPersonneIdagentIdroleagent relation
 * @method     ChildResponsableAgentQuery rightJoinPersonneRelatedByPersonneIdagentIdroleagent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PersonneRelatedByPersonneIdagentIdroleagent relation
 * @method     ChildResponsableAgentQuery innerJoinPersonneRelatedByPersonneIdagentIdroleagent($relationAlias = null) Adds a INNER JOIN clause to the query using the PersonneRelatedByPersonneIdagentIdroleagent relation
 *
 * @method     ChildResponsableAgentQuery joinWithPersonneRelatedByPersonneIdagentIdroleagent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PersonneRelatedByPersonneIdagentIdroleagent relation
 *
 * @method     ChildResponsableAgentQuery leftJoinWithPersonneRelatedByPersonneIdagentIdroleagent() Adds a LEFT JOIN clause and with to the query using the PersonneRelatedByPersonneIdagentIdroleagent relation
 * @method     ChildResponsableAgentQuery rightJoinWithPersonneRelatedByPersonneIdagentIdroleagent() Adds a RIGHT JOIN clause and with to the query using the PersonneRelatedByPersonneIdagentIdroleagent relation
 * @method     ChildResponsableAgentQuery innerJoinWithPersonneRelatedByPersonneIdagentIdroleagent() Adds a INNER JOIN clause and with to the query using the PersonneRelatedByPersonneIdagentIdroleagent relation
 *
 * @method     \PersonneQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildResponsableAgent findOne(ConnectionInterface $con = null) Return the first ChildResponsableAgent matching the query
 * @method     ChildResponsableAgent findOneOrCreate(ConnectionInterface $con = null) Return the first ChildResponsableAgent matching the query, or a new ChildResponsableAgent object populated from the query conditions when no match is found
 *
 * @method     ChildResponsableAgent findOneByIdresponsable(int $idResponsable) Return the first ChildResponsableAgent filtered by the idResponsable column
 * @method     ChildResponsableAgent findOneByPersonneIdresponsable(int $Personne_idResponsable) Return the first ChildResponsableAgent filtered by the Personne_idResponsable column
 * @method     ChildResponsableAgent findOneByIdroleresponsable(int $idroleResponsable) Return the first ChildResponsableAgent filtered by the idroleResponsable column
 * @method     ChildResponsableAgent findOneByPersonneIdagent(int $Personne_idAgent) Return the first ChildResponsableAgent filtered by the Personne_idAgent column
 * @method     ChildResponsableAgent findOneByIdroleagent(int $idroleAgent) Return the first ChildResponsableAgent filtered by the idroleAgent column
 * @method     ChildResponsableAgent findOneByDatedebutresponsabilite(string $DateDebutResponsabilite) Return the first ChildResponsableAgent filtered by the DateDebutResponsabilite column *

 * @method     ChildResponsableAgent requirePk($key, ConnectionInterface $con = null) Return the ChildResponsableAgent by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildResponsableAgent requireOne(ConnectionInterface $con = null) Return the first ChildResponsableAgent matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildResponsableAgent requireOneByIdresponsable(int $idResponsable) Return the first ChildResponsableAgent filtered by the idResponsable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildResponsableAgent requireOneByPersonneIdresponsable(int $Personne_idResponsable) Return the first ChildResponsableAgent filtered by the Personne_idResponsable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildResponsableAgent requireOneByIdroleresponsable(int $idroleResponsable) Return the first ChildResponsableAgent filtered by the idroleResponsable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildResponsableAgent requireOneByPersonneIdagent(int $Personne_idAgent) Return the first ChildResponsableAgent filtered by the Personne_idAgent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildResponsableAgent requireOneByIdroleagent(int $idroleAgent) Return the first ChildResponsableAgent filtered by the idroleAgent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildResponsableAgent requireOneByDatedebutresponsabilite(string $DateDebutResponsabilite) Return the first ChildResponsableAgent filtered by the DateDebutResponsabilite column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildResponsableAgent[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildResponsableAgent objects based on current ModelCriteria
 * @method     ChildResponsableAgent[]|ObjectCollection findByIdresponsable(int $idResponsable) Return ChildResponsableAgent objects filtered by the idResponsable column
 * @method     ChildResponsableAgent[]|ObjectCollection findByPersonneIdresponsable(int $Personne_idResponsable) Return ChildResponsableAgent objects filtered by the Personne_idResponsable column
 * @method     ChildResponsableAgent[]|ObjectCollection findByIdroleresponsable(int $idroleResponsable) Return ChildResponsableAgent objects filtered by the idroleResponsable column
 * @method     ChildResponsableAgent[]|ObjectCollection findByPersonneIdagent(int $Personne_idAgent) Return ChildResponsableAgent objects filtered by the Personne_idAgent column
 * @method     ChildResponsableAgent[]|ObjectCollection findByIdroleagent(int $idroleAgent) Return ChildResponsableAgent objects filtered by the idroleAgent column
 * @method     ChildResponsableAgent[]|ObjectCollection findByDatedebutresponsabilite(string $DateDebutResponsabilite) Return ChildResponsableAgent objects filtered by the DateDebutResponsabilite column
 * @method     ChildResponsableAgent[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ResponsableAgentQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ResponsableAgentQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ResponsableAgent', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildResponsableAgentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildResponsableAgentQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildResponsableAgentQuery) {
            return $criteria;
        }
        $query = new ChildResponsableAgentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56, 78, 91), $con);
     * </code>
     *
     * @param array[$idResponsable, $Personne_idResponsable, $idroleResponsable, $Personne_idAgent, $idroleAgent] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildResponsableAgent|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ResponsableAgentTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ResponsableAgentTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2]), (null === $key[3] || is_scalar($key[3]) || is_callable([$key[3], '__toString']) ? (string) $key[3] : $key[3]), (null === $key[4] || is_scalar($key[4]) || is_callable([$key[4], '__toString']) ? (string) $key[4] : $key[4])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildResponsableAgent A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idResponsable, Personne_idResponsable, idroleResponsable, Personne_idAgent, idroleAgent, DateDebutResponsabilite FROM responsable_agent WHERE idResponsable = :p0 AND Personne_idResponsable = :p1 AND idroleResponsable = :p2 AND Personne_idAgent = :p3 AND idroleAgent = :p4';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->bindValue(':p3', $key[3], PDO::PARAM_INT);
            $stmt->bindValue(':p4', $key[4], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildResponsableAgent $obj */
            $obj = new ChildResponsableAgent();
            $obj->hydrate($row);
            ResponsableAgentTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2]), (null === $key[3] || is_scalar($key[3]) || is_callable([$key[3], '__toString']) ? (string) $key[3] : $key[3]), (null === $key[4] || is_scalar($key[4]) || is_callable([$key[4], '__toString']) ? (string) $key[4] : $key[4])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildResponsableAgent|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ResponsableAgentTableMap::COL_IDRESPONSABLE, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDRESPONSABLE, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(ResponsableAgentTableMap::COL_IDROLERESPONSABLE, $key[2], Criteria::EQUAL);
        $this->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDAGENT, $key[3], Criteria::EQUAL);
        $this->addUsingAlias(ResponsableAgentTableMap::COL_IDROLEAGENT, $key[4], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ResponsableAgentTableMap::COL_IDRESPONSABLE, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ResponsableAgentTableMap::COL_PERSONNE_IDRESPONSABLE, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(ResponsableAgentTableMap::COL_IDROLERESPONSABLE, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $cton3 = $this->getNewCriterion(ResponsableAgentTableMap::COL_PERSONNE_IDAGENT, $key[3], Criteria::EQUAL);
            $cton0->addAnd($cton3);
            $cton4 = $this->getNewCriterion(ResponsableAgentTableMap::COL_IDROLEAGENT, $key[4], Criteria::EQUAL);
            $cton0->addAnd($cton4);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idResponsable column
     *
     * Example usage:
     * <code>
     * $query->filterByIdresponsable(1234); // WHERE idResponsable = 1234
     * $query->filterByIdresponsable(array(12, 34)); // WHERE idResponsable IN (12, 34)
     * $query->filterByIdresponsable(array('min' => 12)); // WHERE idResponsable > 12
     * </code>
     *
     * @param     mixed $idresponsable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByIdresponsable($idresponsable = null, $comparison = null)
    {
        if (is_array($idresponsable)) {
            $useMinMax = false;
            if (isset($idresponsable['min'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_IDRESPONSABLE, $idresponsable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idresponsable['max'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_IDRESPONSABLE, $idresponsable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsableAgentTableMap::COL_IDRESPONSABLE, $idresponsable, $comparison);
    }

    /**
     * Filter the query on the Personne_idResponsable column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonneIdresponsable(1234); // WHERE Personne_idResponsable = 1234
     * $query->filterByPersonneIdresponsable(array(12, 34)); // WHERE Personne_idResponsable IN (12, 34)
     * $query->filterByPersonneIdresponsable(array('min' => 12)); // WHERE Personne_idResponsable > 12
     * </code>
     *
     * @see       filterByPersonneRelatedByPersonneIdresponsableIdroleresponsable()
     *
     * @param     mixed $personneIdresponsable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByPersonneIdresponsable($personneIdresponsable = null, $comparison = null)
    {
        if (is_array($personneIdresponsable)) {
            $useMinMax = false;
            if (isset($personneIdresponsable['min'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDRESPONSABLE, $personneIdresponsable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personneIdresponsable['max'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDRESPONSABLE, $personneIdresponsable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDRESPONSABLE, $personneIdresponsable, $comparison);
    }

    /**
     * Filter the query on the idroleResponsable column
     *
     * Example usage:
     * <code>
     * $query->filterByIdroleresponsable(1234); // WHERE idroleResponsable = 1234
     * $query->filterByIdroleresponsable(array(12, 34)); // WHERE idroleResponsable IN (12, 34)
     * $query->filterByIdroleresponsable(array('min' => 12)); // WHERE idroleResponsable > 12
     * </code>
     *
     * @see       filterByPersonneRelatedByPersonneIdresponsableIdroleresponsable()
     *
     * @param     mixed $idroleresponsable The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByIdroleresponsable($idroleresponsable = null, $comparison = null)
    {
        if (is_array($idroleresponsable)) {
            $useMinMax = false;
            if (isset($idroleresponsable['min'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_IDROLERESPONSABLE, $idroleresponsable['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idroleresponsable['max'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_IDROLERESPONSABLE, $idroleresponsable['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsableAgentTableMap::COL_IDROLERESPONSABLE, $idroleresponsable, $comparison);
    }

    /**
     * Filter the query on the Personne_idAgent column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonneIdagent(1234); // WHERE Personne_idAgent = 1234
     * $query->filterByPersonneIdagent(array(12, 34)); // WHERE Personne_idAgent IN (12, 34)
     * $query->filterByPersonneIdagent(array('min' => 12)); // WHERE Personne_idAgent > 12
     * </code>
     *
     * @see       filterByPersonneRelatedByPersonneIdagentIdroleagent()
     *
     * @param     mixed $personneIdagent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByPersonneIdagent($personneIdagent = null, $comparison = null)
    {
        if (is_array($personneIdagent)) {
            $useMinMax = false;
            if (isset($personneIdagent['min'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDAGENT, $personneIdagent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personneIdagent['max'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDAGENT, $personneIdagent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDAGENT, $personneIdagent, $comparison);
    }

    /**
     * Filter the query on the idroleAgent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdroleagent(1234); // WHERE idroleAgent = 1234
     * $query->filterByIdroleagent(array(12, 34)); // WHERE idroleAgent IN (12, 34)
     * $query->filterByIdroleagent(array('min' => 12)); // WHERE idroleAgent > 12
     * </code>
     *
     * @see       filterByPersonneRelatedByPersonneIdagentIdroleagent()
     *
     * @param     mixed $idroleagent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByIdroleagent($idroleagent = null, $comparison = null)
    {
        if (is_array($idroleagent)) {
            $useMinMax = false;
            if (isset($idroleagent['min'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_IDROLEAGENT, $idroleagent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idroleagent['max'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_IDROLEAGENT, $idroleagent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsableAgentTableMap::COL_IDROLEAGENT, $idroleagent, $comparison);
    }

    /**
     * Filter the query on the DateDebutResponsabilite column
     *
     * Example usage:
     * <code>
     * $query->filterByDatedebutresponsabilite('2011-03-14'); // WHERE DateDebutResponsabilite = '2011-03-14'
     * $query->filterByDatedebutresponsabilite('now'); // WHERE DateDebutResponsabilite = '2011-03-14'
     * $query->filterByDatedebutresponsabilite(array('max' => 'yesterday')); // WHERE DateDebutResponsabilite > '2011-03-13'
     * </code>
     *
     * @param     mixed $datedebutresponsabilite The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByDatedebutresponsabilite($datedebutresponsabilite = null, $comparison = null)
    {
        if (is_array($datedebutresponsabilite)) {
            $useMinMax = false;
            if (isset($datedebutresponsabilite['min'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_DATEDEBUTRESPONSABILITE, $datedebutresponsabilite['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datedebutresponsabilite['max'])) {
                $this->addUsingAlias(ResponsableAgentTableMap::COL_DATEDEBUTRESPONSABILITE, $datedebutresponsabilite['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResponsableAgentTableMap::COL_DATEDEBUTRESPONSABILITE, $datedebutresponsabilite, $comparison);
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne $personne The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByPersonneRelatedByPersonneIdresponsableIdroleresponsable($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDRESPONSABLE, $personne->getIdpersonne(), $comparison)
                ->addUsingAlias(ResponsableAgentTableMap::COL_IDROLERESPONSABLE, $personne->getRolepersonneIdrolepersonne(), $comparison);
        } else {
            throw new PropelException('filterByPersonneRelatedByPersonneIdresponsableIdroleresponsable() only accepts arguments of type \Personne');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function joinPersonneRelatedByPersonneIdresponsableIdroleresponsable($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PersonneRelatedByPersonneIdresponsableIdroleresponsable');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PersonneRelatedByPersonneIdresponsableIdroleresponsable');
        }

        return $this;
    }

    /**
     * Use the PersonneRelatedByPersonneIdresponsableIdroleresponsable relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneRelatedByPersonneIdresponsableIdroleresponsableQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonneRelatedByPersonneIdresponsableIdroleresponsable($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PersonneRelatedByPersonneIdresponsableIdroleresponsable', '\PersonneQuery');
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne $personne The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function filterByPersonneRelatedByPersonneIdagentIdroleagent($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(ResponsableAgentTableMap::COL_PERSONNE_IDAGENT, $personne->getIdpersonne(), $comparison)
                ->addUsingAlias(ResponsableAgentTableMap::COL_IDROLEAGENT, $personne->getRolepersonneIdrolepersonne(), $comparison);
        } else {
            throw new PropelException('filterByPersonneRelatedByPersonneIdagentIdroleagent() only accepts arguments of type \Personne');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PersonneRelatedByPersonneIdagentIdroleagent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function joinPersonneRelatedByPersonneIdagentIdroleagent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PersonneRelatedByPersonneIdagentIdroleagent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PersonneRelatedByPersonneIdagentIdroleagent');
        }

        return $this;
    }

    /**
     * Use the PersonneRelatedByPersonneIdagentIdroleagent relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneRelatedByPersonneIdagentIdroleagentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonneRelatedByPersonneIdagentIdroleagent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PersonneRelatedByPersonneIdagentIdroleagent', '\PersonneQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildResponsableAgent $responsableAgent Object to remove from the list of results
     *
     * @return $this|ChildResponsableAgentQuery The current query, for fluid interface
     */
    public function prune($responsableAgent = null)
    {
        if ($responsableAgent) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ResponsableAgentTableMap::COL_IDRESPONSABLE), $responsableAgent->getIdresponsable(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ResponsableAgentTableMap::COL_PERSONNE_IDRESPONSABLE), $responsableAgent->getPersonneIdresponsable(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(ResponsableAgentTableMap::COL_IDROLERESPONSABLE), $responsableAgent->getIdroleresponsable(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond3', $this->getAliasedColName(ResponsableAgentTableMap::COL_PERSONNE_IDAGENT), $responsableAgent->getPersonneIdagent(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond4', $this->getAliasedColName(ResponsableAgentTableMap::COL_IDROLEAGENT), $responsableAgent->getIdroleagent(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2', 'pruneCond3', 'pruneCond4'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the responsable_agent table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ResponsableAgentTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ResponsableAgentTableMap::clearInstancePool();
            ResponsableAgentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ResponsableAgentTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ResponsableAgentTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ResponsableAgentTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ResponsableAgentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ResponsableAgentQuery
