<?php

namespace Base;

use \Commune as ChildCommune;
use \CommuneQuery as ChildCommuneQuery;
use \Exception;
use \PDO;
use Map\CommuneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'commune' table.
 *
 *
 *
 * @method     ChildCommuneQuery orderByIdcommune($order = Criteria::ASC) Order by the idcommune column
 * @method     ChildCommuneQuery orderByLibellecommune($order = Criteria::ASC) Order by the Libellecommune column
 * @method     ChildCommuneQuery orderByVilleIdville($order = Criteria::ASC) Order by the ville_idville column
 * @method     ChildCommuneQuery orderByVillePaysIdpays($order = Criteria::ASC) Order by the ville_pays_idpays column
 *
 * @method     ChildCommuneQuery groupByIdcommune() Group by the idcommune column
 * @method     ChildCommuneQuery groupByLibellecommune() Group by the Libellecommune column
 * @method     ChildCommuneQuery groupByVilleIdville() Group by the ville_idville column
 * @method     ChildCommuneQuery groupByVillePaysIdpays() Group by the ville_pays_idpays column
 *
 * @method     ChildCommuneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCommuneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCommuneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCommuneQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCommuneQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCommuneQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCommuneQuery leftJoinVille($relationAlias = null) Adds a LEFT JOIN clause to the query using the Ville relation
 * @method     ChildCommuneQuery rightJoinVille($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Ville relation
 * @method     ChildCommuneQuery innerJoinVille($relationAlias = null) Adds a INNER JOIN clause to the query using the Ville relation
 *
 * @method     ChildCommuneQuery joinWithVille($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Ville relation
 *
 * @method     ChildCommuneQuery leftJoinWithVille() Adds a LEFT JOIN clause and with to the query using the Ville relation
 * @method     ChildCommuneQuery rightJoinWithVille() Adds a RIGHT JOIN clause and with to the query using the Ville relation
 * @method     ChildCommuneQuery innerJoinWithVille() Adds a INNER JOIN clause and with to the query using the Ville relation
 *
 * @method     ChildCommuneQuery leftJoinPersonne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Personne relation
 * @method     ChildCommuneQuery rightJoinPersonne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Personne relation
 * @method     ChildCommuneQuery innerJoinPersonne($relationAlias = null) Adds a INNER JOIN clause to the query using the Personne relation
 *
 * @method     ChildCommuneQuery joinWithPersonne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Personne relation
 *
 * @method     ChildCommuneQuery leftJoinWithPersonne() Adds a LEFT JOIN clause and with to the query using the Personne relation
 * @method     ChildCommuneQuery rightJoinWithPersonne() Adds a RIGHT JOIN clause and with to the query using the Personne relation
 * @method     ChildCommuneQuery innerJoinWithPersonne() Adds a INNER JOIN clause and with to the query using the Personne relation
 *
 * @method     \VilleQuery|\PersonneQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCommune findOne(ConnectionInterface $con = null) Return the first ChildCommune matching the query
 * @method     ChildCommune findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCommune matching the query, or a new ChildCommune object populated from the query conditions when no match is found
 *
 * @method     ChildCommune findOneByIdcommune(int $idcommune) Return the first ChildCommune filtered by the idcommune column
 * @method     ChildCommune findOneByLibellecommune(string $Libellecommune) Return the first ChildCommune filtered by the Libellecommune column
 * @method     ChildCommune findOneByVilleIdville(int $ville_idville) Return the first ChildCommune filtered by the ville_idville column
 * @method     ChildCommune findOneByVillePaysIdpays(int $ville_pays_idpays) Return the first ChildCommune filtered by the ville_pays_idpays column *

 * @method     ChildCommune requirePk($key, ConnectionInterface $con = null) Return the ChildCommune by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOne(ConnectionInterface $con = null) Return the first ChildCommune matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommune requireOneByIdcommune(int $idcommune) Return the first ChildCommune filtered by the idcommune column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByLibellecommune(string $Libellecommune) Return the first ChildCommune filtered by the Libellecommune column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByVilleIdville(int $ville_idville) Return the first ChildCommune filtered by the ville_idville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCommune requireOneByVillePaysIdpays(int $ville_pays_idpays) Return the first ChildCommune filtered by the ville_pays_idpays column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCommune[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCommune objects based on current ModelCriteria
 * @method     ChildCommune[]|ObjectCollection findByIdcommune(int $idcommune) Return ChildCommune objects filtered by the idcommune column
 * @method     ChildCommune[]|ObjectCollection findByLibellecommune(string $Libellecommune) Return ChildCommune objects filtered by the Libellecommune column
 * @method     ChildCommune[]|ObjectCollection findByVilleIdville(int $ville_idville) Return ChildCommune objects filtered by the ville_idville column
 * @method     ChildCommune[]|ObjectCollection findByVillePaysIdpays(int $ville_pays_idpays) Return ChildCommune objects filtered by the ville_pays_idpays column
 * @method     ChildCommune[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CommuneQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\CommuneQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Commune', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCommuneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCommuneQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCommuneQuery) {
            return $criteria;
        }
        $query = new ChildCommuneQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCommune|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CommuneTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CommuneTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCommune A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idcommune, Libellecommune, ville_idville, ville_pays_idpays FROM commune WHERE idcommune = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCommune $obj */
            $obj = new ChildCommune();
            $obj->hydrate($row);
            CommuneTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCommune|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommuneTableMap::COL_IDCOMMUNE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommuneTableMap::COL_IDCOMMUNE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idcommune column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcommune(1234); // WHERE idcommune = 1234
     * $query->filterByIdcommune(array(12, 34)); // WHERE idcommune IN (12, 34)
     * $query->filterByIdcommune(array('min' => 12)); // WHERE idcommune > 12
     * </code>
     *
     * @param     mixed $idcommune The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByIdcommune($idcommune = null, $comparison = null)
    {
        if (is_array($idcommune)) {
            $useMinMax = false;
            if (isset($idcommune['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_IDCOMMUNE, $idcommune['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcommune['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_IDCOMMUNE, $idcommune['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_IDCOMMUNE, $idcommune, $comparison);
    }

    /**
     * Filter the query on the Libellecommune column
     *
     * Example usage:
     * <code>
     * $query->filterByLibellecommune('fooValue');   // WHERE Libellecommune = 'fooValue'
     * $query->filterByLibellecommune('%fooValue%', Criteria::LIKE); // WHERE Libellecommune LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libellecommune The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByLibellecommune($libellecommune = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libellecommune)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_LIBELLECOMMUNE, $libellecommune, $comparison);
    }

    /**
     * Filter the query on the ville_idville column
     *
     * Example usage:
     * <code>
     * $query->filterByVilleIdville(1234); // WHERE ville_idville = 1234
     * $query->filterByVilleIdville(array(12, 34)); // WHERE ville_idville IN (12, 34)
     * $query->filterByVilleIdville(array('min' => 12)); // WHERE ville_idville > 12
     * </code>
     *
     * @see       filterByVille()
     *
     * @param     mixed $villeIdville The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByVilleIdville($villeIdville = null, $comparison = null)
    {
        if (is_array($villeIdville)) {
            $useMinMax = false;
            if (isset($villeIdville['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_VILLE_IDVILLE, $villeIdville['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($villeIdville['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_VILLE_IDVILLE, $villeIdville['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_VILLE_IDVILLE, $villeIdville, $comparison);
    }

    /**
     * Filter the query on the ville_pays_idpays column
     *
     * Example usage:
     * <code>
     * $query->filterByVillePaysIdpays(1234); // WHERE ville_pays_idpays = 1234
     * $query->filterByVillePaysIdpays(array(12, 34)); // WHERE ville_pays_idpays IN (12, 34)
     * $query->filterByVillePaysIdpays(array('min' => 12)); // WHERE ville_pays_idpays > 12
     * </code>
     *
     * @see       filterByVille()
     *
     * @param     mixed $villePaysIdpays The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByVillePaysIdpays($villePaysIdpays = null, $comparison = null)
    {
        if (is_array($villePaysIdpays)) {
            $useMinMax = false;
            if (isset($villePaysIdpays['min'])) {
                $this->addUsingAlias(CommuneTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($villePaysIdpays['max'])) {
                $this->addUsingAlias(CommuneTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommuneTableMap::COL_VILLE_PAYS_IDPAYS, $villePaysIdpays, $comparison);
    }

    /**
     * Filter the query by a related \Ville object
     *
     * @param \Ville $ville The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByVille($ville, $comparison = null)
    {
        if ($ville instanceof \Ville) {
            return $this
                ->addUsingAlias(CommuneTableMap::COL_VILLE_IDVILLE, $ville->getIdville(), $comparison)
                ->addUsingAlias(CommuneTableMap::COL_VILLE_PAYS_IDPAYS, $ville->getPaysIdpays(), $comparison);
        } else {
            throw new PropelException('filterByVille() only accepts arguments of type \Ville');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Ville relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function joinVille($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Ville');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Ville');
        }

        return $this;
    }

    /**
     * Use the Ville relation Ville object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VilleQuery A secondary query class using the current class as primary query
     */
    public function useVilleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVille($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Ville', '\VilleQuery');
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne|ObjectCollection $personne the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCommuneQuery The current query, for fluid interface
     */
    public function filterByPersonne($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(CommuneTableMap::COL_IDCOMMUNE, $personne->getCommuneIdcommune(), $comparison);
        } elseif ($personne instanceof ObjectCollection) {
            return $this
                ->usePersonneQuery()
                ->filterByPrimaryKeys($personne->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPersonne() only accepts arguments of type \Personne or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Personne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function joinPersonne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Personne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Personne');
        }

        return $this;
    }

    /**
     * Use the Personne relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Personne', '\PersonneQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCommune $commune Object to remove from the list of results
     *
     * @return $this|ChildCommuneQuery The current query, for fluid interface
     */
    public function prune($commune = null)
    {
        if ($commune) {
            $this->addUsingAlias(CommuneTableMap::COL_IDCOMMUNE, $commune->getIdcommune(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the commune table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommuneTableMap::clearInstancePool();
            CommuneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CommuneTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CommuneTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CommuneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CommuneQuery
