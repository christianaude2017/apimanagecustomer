<?php

namespace Base;

use \Prospection as ChildProspection;
use \ProspectionQuery as ChildProspectionQuery;
use \Exception;
use \PDO;
use Map\ProspectionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'prospection' table.
 *
 *
 *
 * @method     ChildProspectionQuery orderByIdprospection($order = Criteria::ASC) Order by the idprospection column
 * @method     ChildProspectionQuery orderByNomprospect($order = Criteria::ASC) Order by the NomProspect column
 * @method     ChildProspectionQuery orderByPrenomprospection($order = Criteria::ASC) Order by the Prenomprospection column
 * @method     ChildProspectionQuery orderByTelprospect($order = Criteria::ASC) Order by the TelProspect column
 * @method     ChildProspectionQuery orderByEmailprospect($order = Criteria::ASC) Order by the EmailProspect column
 * @method     ChildProspectionQuery orderByDatepriserdv($order = Criteria::ASC) Order by the DatePriseRDV column
 * @method     ChildProspectionQuery orderByDaterdv($order = Criteria::ASC) Order by the DateRDV column
 * @method     ChildProspectionQuery orderByTypeprospect($order = Criteria::ASC) Order by the typeprospect column
 * @method     ChildProspectionQuery orderByFonctionprospect($order = Criteria::ASC) Order by the fonctionprospect column
 * @method     ChildProspectionQuery orderByIdpersonneagent($order = Criteria::ASC) Order by the idPersonneAgent column
 * @method     ChildProspectionQuery orderByPersonneRolepersonneIdrolepersonne($order = Criteria::ASC) Order by the Personne_rolePersonne_idrolePersonne column
 * @method     ChildProspectionQuery orderByProduitsIdproduits($order = Criteria::ASC) Order by the produits_idproduits column
 * @method     ChildProspectionQuery orderByProduitsSocieteIdsociete($order = Criteria::ASC) Order by the produits_societe_idsociete column
 *
 * @method     ChildProspectionQuery groupByIdprospection() Group by the idprospection column
 * @method     ChildProspectionQuery groupByNomprospect() Group by the NomProspect column
 * @method     ChildProspectionQuery groupByPrenomprospection() Group by the Prenomprospection column
 * @method     ChildProspectionQuery groupByTelprospect() Group by the TelProspect column
 * @method     ChildProspectionQuery groupByEmailprospect() Group by the EmailProspect column
 * @method     ChildProspectionQuery groupByDatepriserdv() Group by the DatePriseRDV column
 * @method     ChildProspectionQuery groupByDaterdv() Group by the DateRDV column
 * @method     ChildProspectionQuery groupByTypeprospect() Group by the typeprospect column
 * @method     ChildProspectionQuery groupByFonctionprospect() Group by the fonctionprospect column
 * @method     ChildProspectionQuery groupByIdpersonneagent() Group by the idPersonneAgent column
 * @method     ChildProspectionQuery groupByPersonneRolepersonneIdrolepersonne() Group by the Personne_rolePersonne_idrolePersonne column
 * @method     ChildProspectionQuery groupByProduitsIdproduits() Group by the produits_idproduits column
 * @method     ChildProspectionQuery groupByProduitsSocieteIdsociete() Group by the produits_societe_idsociete column
 *
 * @method     ChildProspectionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProspectionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProspectionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProspectionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProspectionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProspectionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProspectionQuery leftJoinPersonne($relationAlias = null) Adds a LEFT JOIN clause to the query using the Personne relation
 * @method     ChildProspectionQuery rightJoinPersonne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Personne relation
 * @method     ChildProspectionQuery innerJoinPersonne($relationAlias = null) Adds a INNER JOIN clause to the query using the Personne relation
 *
 * @method     ChildProspectionQuery joinWithPersonne($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Personne relation
 *
 * @method     ChildProspectionQuery leftJoinWithPersonne() Adds a LEFT JOIN clause and with to the query using the Personne relation
 * @method     ChildProspectionQuery rightJoinWithPersonne() Adds a RIGHT JOIN clause and with to the query using the Personne relation
 * @method     ChildProspectionQuery innerJoinWithPersonne() Adds a INNER JOIN clause and with to the query using the Personne relation
 *
 * @method     ChildProspectionQuery leftJoinProduits($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produits relation
 * @method     ChildProspectionQuery rightJoinProduits($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produits relation
 * @method     ChildProspectionQuery innerJoinProduits($relationAlias = null) Adds a INNER JOIN clause to the query using the Produits relation
 *
 * @method     ChildProspectionQuery joinWithProduits($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produits relation
 *
 * @method     ChildProspectionQuery leftJoinWithProduits() Adds a LEFT JOIN clause and with to the query using the Produits relation
 * @method     ChildProspectionQuery rightJoinWithProduits() Adds a RIGHT JOIN clause and with to the query using the Produits relation
 * @method     ChildProspectionQuery innerJoinWithProduits() Adds a INNER JOIN clause and with to the query using the Produits relation
 *
 * @method     \PersonneQuery|\ProduitsQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProspection findOne(ConnectionInterface $con = null) Return the first ChildProspection matching the query
 * @method     ChildProspection findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProspection matching the query, or a new ChildProspection object populated from the query conditions when no match is found
 *
 * @method     ChildProspection findOneByIdprospection(int $idprospection) Return the first ChildProspection filtered by the idprospection column
 * @method     ChildProspection findOneByNomprospect(string $NomProspect) Return the first ChildProspection filtered by the NomProspect column
 * @method     ChildProspection findOneByPrenomprospection(string $Prenomprospection) Return the first ChildProspection filtered by the Prenomprospection column
 * @method     ChildProspection findOneByTelprospect(string $TelProspect) Return the first ChildProspection filtered by the TelProspect column
 * @method     ChildProspection findOneByEmailprospect(string $EmailProspect) Return the first ChildProspection filtered by the EmailProspect column
 * @method     ChildProspection findOneByDatepriserdv(string $DatePriseRDV) Return the first ChildProspection filtered by the DatePriseRDV column
 * @method     ChildProspection findOneByDaterdv(string $DateRDV) Return the first ChildProspection filtered by the DateRDV column
 * @method     ChildProspection findOneByTypeprospect(int $typeprospect) Return the first ChildProspection filtered by the typeprospect column
 * @method     ChildProspection findOneByFonctionprospect(string $fonctionprospect) Return the first ChildProspection filtered by the fonctionprospect column
 * @method     ChildProspection findOneByIdpersonneagent(int $idPersonneAgent) Return the first ChildProspection filtered by the idPersonneAgent column
 * @method     ChildProspection findOneByPersonneRolepersonneIdrolepersonne(int $Personne_rolePersonne_idrolePersonne) Return the first ChildProspection filtered by the Personne_rolePersonne_idrolePersonne column
 * @method     ChildProspection findOneByProduitsIdproduits(int $produits_idproduits) Return the first ChildProspection filtered by the produits_idproduits column
 * @method     ChildProspection findOneByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return the first ChildProspection filtered by the produits_societe_idsociete column *

 * @method     ChildProspection requirePk($key, ConnectionInterface $con = null) Return the ChildProspection by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOne(ConnectionInterface $con = null) Return the first ChildProspection matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProspection requireOneByIdprospection(int $idprospection) Return the first ChildProspection filtered by the idprospection column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByNomprospect(string $NomProspect) Return the first ChildProspection filtered by the NomProspect column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByPrenomprospection(string $Prenomprospection) Return the first ChildProspection filtered by the Prenomprospection column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByTelprospect(string $TelProspect) Return the first ChildProspection filtered by the TelProspect column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByEmailprospect(string $EmailProspect) Return the first ChildProspection filtered by the EmailProspect column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByDatepriserdv(string $DatePriseRDV) Return the first ChildProspection filtered by the DatePriseRDV column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByDaterdv(string $DateRDV) Return the first ChildProspection filtered by the DateRDV column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByTypeprospect(int $typeprospect) Return the first ChildProspection filtered by the typeprospect column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByFonctionprospect(string $fonctionprospect) Return the first ChildProspection filtered by the fonctionprospect column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByIdpersonneagent(int $idPersonneAgent) Return the first ChildProspection filtered by the idPersonneAgent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByPersonneRolepersonneIdrolepersonne(int $Personne_rolePersonne_idrolePersonne) Return the first ChildProspection filtered by the Personne_rolePersonne_idrolePersonne column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByProduitsIdproduits(int $produits_idproduits) Return the first ChildProspection filtered by the produits_idproduits column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProspection requireOneByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return the first ChildProspection filtered by the produits_societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProspection[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProspection objects based on current ModelCriteria
 * @method     ChildProspection[]|ObjectCollection findByIdprospection(int $idprospection) Return ChildProspection objects filtered by the idprospection column
 * @method     ChildProspection[]|ObjectCollection findByNomprospect(string $NomProspect) Return ChildProspection objects filtered by the NomProspect column
 * @method     ChildProspection[]|ObjectCollection findByPrenomprospection(string $Prenomprospection) Return ChildProspection objects filtered by the Prenomprospection column
 * @method     ChildProspection[]|ObjectCollection findByTelprospect(string $TelProspect) Return ChildProspection objects filtered by the TelProspect column
 * @method     ChildProspection[]|ObjectCollection findByEmailprospect(string $EmailProspect) Return ChildProspection objects filtered by the EmailProspect column
 * @method     ChildProspection[]|ObjectCollection findByDatepriserdv(string $DatePriseRDV) Return ChildProspection objects filtered by the DatePriseRDV column
 * @method     ChildProspection[]|ObjectCollection findByDaterdv(string $DateRDV) Return ChildProspection objects filtered by the DateRDV column
 * @method     ChildProspection[]|ObjectCollection findByTypeprospect(int $typeprospect) Return ChildProspection objects filtered by the typeprospect column
 * @method     ChildProspection[]|ObjectCollection findByFonctionprospect(string $fonctionprospect) Return ChildProspection objects filtered by the fonctionprospect column
 * @method     ChildProspection[]|ObjectCollection findByIdpersonneagent(int $idPersonneAgent) Return ChildProspection objects filtered by the idPersonneAgent column
 * @method     ChildProspection[]|ObjectCollection findByPersonneRolepersonneIdrolepersonne(int $Personne_rolePersonne_idrolePersonne) Return ChildProspection objects filtered by the Personne_rolePersonne_idrolePersonne column
 * @method     ChildProspection[]|ObjectCollection findByProduitsIdproduits(int $produits_idproduits) Return ChildProspection objects filtered by the produits_idproduits column
 * @method     ChildProspection[]|ObjectCollection findByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return ChildProspection objects filtered by the produits_societe_idsociete column
 * @method     ChildProspection[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProspectionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ProspectionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Prospection', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProspectionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProspectionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProspectionQuery) {
            return $criteria;
        }
        $query = new ChildProspectionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56, 78, 91), $con);
     * </code>
     *
     * @param array[$idprospection, $idPersonneAgent, $Personne_rolePersonne_idrolePersonne, $produits_idproduits, $produits_societe_idsociete] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProspection|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProspectionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProspectionTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2]), (null === $key[3] || is_scalar($key[3]) || is_callable([$key[3], '__toString']) ? (string) $key[3] : $key[3]), (null === $key[4] || is_scalar($key[4]) || is_callable([$key[4], '__toString']) ? (string) $key[4] : $key[4])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProspection A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idprospection, NomProspect, Prenomprospection, TelProspect, EmailProspect, DatePriseRDV, DateRDV, typeprospect, fonctionprospect, idPersonneAgent, Personne_rolePersonne_idrolePersonne, produits_idproduits, produits_societe_idsociete FROM prospection WHERE idprospection = :p0 AND idPersonneAgent = :p1 AND Personne_rolePersonne_idrolePersonne = :p2 AND produits_idproduits = :p3 AND produits_societe_idsociete = :p4';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->bindValue(':p3', $key[3], PDO::PARAM_INT);
            $stmt->bindValue(':p4', $key[4], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProspection $obj */
            $obj = new ChildProspection();
            $obj->hydrate($row);
            ProspectionTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2]), (null === $key[3] || is_scalar($key[3]) || is_callable([$key[3], '__toString']) ? (string) $key[3] : $key[3]), (null === $key[4] || is_scalar($key[4]) || is_callable([$key[4], '__toString']) ? (string) $key[4] : $key[4])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProspection|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ProspectionTableMap::COL_IDPROSPECTION, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ProspectionTableMap::COL_IDPERSONNEAGENT, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $key[2], Criteria::EQUAL);
        $this->addUsingAlias(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $key[3], Criteria::EQUAL);
        $this->addUsingAlias(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $key[4], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ProspectionTableMap::COL_IDPROSPECTION, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ProspectionTableMap::COL_IDPERSONNEAGENT, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $cton3 = $this->getNewCriterion(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $key[3], Criteria::EQUAL);
            $cton0->addAnd($cton3);
            $cton4 = $this->getNewCriterion(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $key[4], Criteria::EQUAL);
            $cton0->addAnd($cton4);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idprospection column
     *
     * Example usage:
     * <code>
     * $query->filterByIdprospection(1234); // WHERE idprospection = 1234
     * $query->filterByIdprospection(array(12, 34)); // WHERE idprospection IN (12, 34)
     * $query->filterByIdprospection(array('min' => 12)); // WHERE idprospection > 12
     * </code>
     *
     * @param     mixed $idprospection The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByIdprospection($idprospection = null, $comparison = null)
    {
        if (is_array($idprospection)) {
            $useMinMax = false;
            if (isset($idprospection['min'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_IDPROSPECTION, $idprospection['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idprospection['max'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_IDPROSPECTION, $idprospection['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_IDPROSPECTION, $idprospection, $comparison);
    }

    /**
     * Filter the query on the NomProspect column
     *
     * Example usage:
     * <code>
     * $query->filterByNomprospect('fooValue');   // WHERE NomProspect = 'fooValue'
     * $query->filterByNomprospect('%fooValue%', Criteria::LIKE); // WHERE NomProspect LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomprospect The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByNomprospect($nomprospect = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomprospect)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_NOMPROSPECT, $nomprospect, $comparison);
    }

    /**
     * Filter the query on the Prenomprospection column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenomprospection('fooValue');   // WHERE Prenomprospection = 'fooValue'
     * $query->filterByPrenomprospection('%fooValue%', Criteria::LIKE); // WHERE Prenomprospection LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenomprospection The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByPrenomprospection($prenomprospection = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenomprospection)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_PRENOMPROSPECTION, $prenomprospection, $comparison);
    }

    /**
     * Filter the query on the TelProspect column
     *
     * Example usage:
     * <code>
     * $query->filterByTelprospect('fooValue');   // WHERE TelProspect = 'fooValue'
     * $query->filterByTelprospect('%fooValue%', Criteria::LIKE); // WHERE TelProspect LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telprospect The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByTelprospect($telprospect = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telprospect)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_TELPROSPECT, $telprospect, $comparison);
    }

    /**
     * Filter the query on the EmailProspect column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailprospect('fooValue');   // WHERE EmailProspect = 'fooValue'
     * $query->filterByEmailprospect('%fooValue%', Criteria::LIKE); // WHERE EmailProspect LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailprospect The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByEmailprospect($emailprospect = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailprospect)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_EMAILPROSPECT, $emailprospect, $comparison);
    }

    /**
     * Filter the query on the DatePriseRDV column
     *
     * Example usage:
     * <code>
     * $query->filterByDatepriserdv('2011-03-14'); // WHERE DatePriseRDV = '2011-03-14'
     * $query->filterByDatepriserdv('now'); // WHERE DatePriseRDV = '2011-03-14'
     * $query->filterByDatepriserdv(array('max' => 'yesterday')); // WHERE DatePriseRDV > '2011-03-13'
     * </code>
     *
     * @param     mixed $datepriserdv The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByDatepriserdv($datepriserdv = null, $comparison = null)
    {
        if (is_array($datepriserdv)) {
            $useMinMax = false;
            if (isset($datepriserdv['min'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_DATEPRISERDV, $datepriserdv['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datepriserdv['max'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_DATEPRISERDV, $datepriserdv['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_DATEPRISERDV, $datepriserdv, $comparison);
    }

    /**
     * Filter the query on the DateRDV column
     *
     * Example usage:
     * <code>
     * $query->filterByDaterdv('2011-03-14'); // WHERE DateRDV = '2011-03-14'
     * $query->filterByDaterdv('now'); // WHERE DateRDV = '2011-03-14'
     * $query->filterByDaterdv(array('max' => 'yesterday')); // WHERE DateRDV > '2011-03-13'
     * </code>
     *
     * @param     mixed $daterdv The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByDaterdv($daterdv = null, $comparison = null)
    {
        if (is_array($daterdv)) {
            $useMinMax = false;
            if (isset($daterdv['min'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_DATERDV, $daterdv['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($daterdv['max'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_DATERDV, $daterdv['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_DATERDV, $daterdv, $comparison);
    }

    /**
     * Filter the query on the typeprospect column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeprospect(1234); // WHERE typeprospect = 1234
     * $query->filterByTypeprospect(array(12, 34)); // WHERE typeprospect IN (12, 34)
     * $query->filterByTypeprospect(array('min' => 12)); // WHERE typeprospect > 12
     * </code>
     *
     * @param     mixed $typeprospect The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByTypeprospect($typeprospect = null, $comparison = null)
    {
        if (is_array($typeprospect)) {
            $useMinMax = false;
            if (isset($typeprospect['min'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_TYPEPROSPECT, $typeprospect['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeprospect['max'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_TYPEPROSPECT, $typeprospect['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_TYPEPROSPECT, $typeprospect, $comparison);
    }

    /**
     * Filter the query on the fonctionprospect column
     *
     * Example usage:
     * <code>
     * $query->filterByFonctionprospect('fooValue');   // WHERE fonctionprospect = 'fooValue'
     * $query->filterByFonctionprospect('%fooValue%', Criteria::LIKE); // WHERE fonctionprospect LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fonctionprospect The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByFonctionprospect($fonctionprospect = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fonctionprospect)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_FONCTIONPROSPECT, $fonctionprospect, $comparison);
    }

    /**
     * Filter the query on the idPersonneAgent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdpersonneagent(1234); // WHERE idPersonneAgent = 1234
     * $query->filterByIdpersonneagent(array(12, 34)); // WHERE idPersonneAgent IN (12, 34)
     * $query->filterByIdpersonneagent(array('min' => 12)); // WHERE idPersonneAgent > 12
     * </code>
     *
     * @see       filterByPersonne()
     *
     * @param     mixed $idpersonneagent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByIdpersonneagent($idpersonneagent = null, $comparison = null)
    {
        if (is_array($idpersonneagent)) {
            $useMinMax = false;
            if (isset($idpersonneagent['min'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_IDPERSONNEAGENT, $idpersonneagent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idpersonneagent['max'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_IDPERSONNEAGENT, $idpersonneagent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_IDPERSONNEAGENT, $idpersonneagent, $comparison);
    }

    /**
     * Filter the query on the Personne_rolePersonne_idrolePersonne column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonneRolepersonneIdrolepersonne(1234); // WHERE Personne_rolePersonne_idrolePersonne = 1234
     * $query->filterByPersonneRolepersonneIdrolepersonne(array(12, 34)); // WHERE Personne_rolePersonne_idrolePersonne IN (12, 34)
     * $query->filterByPersonneRolepersonneIdrolepersonne(array('min' => 12)); // WHERE Personne_rolePersonne_idrolePersonne > 12
     * </code>
     *
     * @see       filterByPersonne()
     *
     * @param     mixed $personneRolepersonneIdrolepersonne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByPersonneRolepersonneIdrolepersonne($personneRolepersonneIdrolepersonne = null, $comparison = null)
    {
        if (is_array($personneRolepersonneIdrolepersonne)) {
            $useMinMax = false;
            if (isset($personneRolepersonneIdrolepersonne['min'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $personneRolepersonneIdrolepersonne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personneRolepersonneIdrolepersonne['max'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $personneRolepersonneIdrolepersonne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $personneRolepersonneIdrolepersonne, $comparison);
    }

    /**
     * Filter the query on the produits_idproduits column
     *
     * Example usage:
     * <code>
     * $query->filterByProduitsIdproduits(1234); // WHERE produits_idproduits = 1234
     * $query->filterByProduitsIdproduits(array(12, 34)); // WHERE produits_idproduits IN (12, 34)
     * $query->filterByProduitsIdproduits(array('min' => 12)); // WHERE produits_idproduits > 12
     * </code>
     *
     * @see       filterByProduits()
     *
     * @param     mixed $produitsIdproduits The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByProduitsIdproduits($produitsIdproduits = null, $comparison = null)
    {
        if (is_array($produitsIdproduits)) {
            $useMinMax = false;
            if (isset($produitsIdproduits['min'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produitsIdproduits['max'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $produitsIdproduits, $comparison);
    }

    /**
     * Filter the query on the produits_societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterByProduitsSocieteIdsociete(1234); // WHERE produits_societe_idsociete = 1234
     * $query->filterByProduitsSocieteIdsociete(array(12, 34)); // WHERE produits_societe_idsociete IN (12, 34)
     * $query->filterByProduitsSocieteIdsociete(array('min' => 12)); // WHERE produits_societe_idsociete > 12
     * </code>
     *
     * @see       filterByProduits()
     *
     * @param     mixed $produitsSocieteIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByProduitsSocieteIdsociete($produitsSocieteIdsociete = null, $comparison = null)
    {
        if (is_array($produitsSocieteIdsociete)) {
            $useMinMax = false;
            if (isset($produitsSocieteIdsociete['min'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produitsSocieteIdsociete['max'])) {
                $this->addUsingAlias(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete, $comparison);
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne $personne The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByPersonne($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(ProspectionTableMap::COL_IDPERSONNEAGENT, $personne->getIdpersonne(), $comparison)
                ->addUsingAlias(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $personne->getRolepersonneIdrolepersonne(), $comparison);
        } else {
            throw new PropelException('filterByPersonne() only accepts arguments of type \Personne');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Personne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function joinPersonne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Personne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Personne');
        }

        return $this;
    }

    /**
     * Use the Personne relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Personne', '\PersonneQuery');
    }

    /**
     * Filter the query by a related \Produits object
     *
     * @param \Produits $produits The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProspectionQuery The current query, for fluid interface
     */
    public function filterByProduits($produits, $comparison = null)
    {
        if ($produits instanceof \Produits) {
            return $this
                ->addUsingAlias(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $produits->getIdproduits(), $comparison)
                ->addUsingAlias(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produits->getSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByProduits() only accepts arguments of type \Produits');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produits relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function joinProduits($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produits');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produits');
        }

        return $this;
    }

    /**
     * Use the Produits relation Produits object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProduitsQuery A secondary query class using the current class as primary query
     */
    public function useProduitsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduits($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produits', '\ProduitsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProspection $prospection Object to remove from the list of results
     *
     * @return $this|ChildProspectionQuery The current query, for fluid interface
     */
    public function prune($prospection = null)
    {
        if ($prospection) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ProspectionTableMap::COL_IDPROSPECTION), $prospection->getIdprospection(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ProspectionTableMap::COL_IDPERSONNEAGENT), $prospection->getIdpersonneagent(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE), $prospection->getPersonneRolepersonneIdrolepersonne(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond3', $this->getAliasedColName(ProspectionTableMap::COL_PRODUITS_IDPRODUITS), $prospection->getProduitsIdproduits(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond4', $this->getAliasedColName(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE), $prospection->getProduitsSocieteIdsociete(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2', 'pruneCond3', 'pruneCond4'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the prospection table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProspectionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProspectionTableMap::clearInstancePool();
            ProspectionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProspectionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProspectionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProspectionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProspectionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProspectionQuery
