<?php

namespace Base;

use \Mois as ChildMois;
use \MoisQuery as ChildMoisQuery;
use \Exception;
use \PDO;
use Map\MoisTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mois' table.
 *
 *
 *
 * @method     ChildMoisQuery orderByIdmois($order = Criteria::ASC) Order by the idmois column
 * @method     ChildMoisQuery orderByLibellemois($order = Criteria::ASC) Order by the Libellemois column
 *
 * @method     ChildMoisQuery groupByIdmois() Group by the idmois column
 * @method     ChildMoisQuery groupByLibellemois() Group by the Libellemois column
 *
 * @method     ChildMoisQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMoisQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMoisQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMoisQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMoisQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMoisQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMoisQuery leftJoinVersement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Versement relation
 * @method     ChildMoisQuery rightJoinVersement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Versement relation
 * @method     ChildMoisQuery innerJoinVersement($relationAlias = null) Adds a INNER JOIN clause to the query using the Versement relation
 *
 * @method     ChildMoisQuery joinWithVersement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Versement relation
 *
 * @method     ChildMoisQuery leftJoinWithVersement() Adds a LEFT JOIN clause and with to the query using the Versement relation
 * @method     ChildMoisQuery rightJoinWithVersement() Adds a RIGHT JOIN clause and with to the query using the Versement relation
 * @method     ChildMoisQuery innerJoinWithVersement() Adds a INNER JOIN clause and with to the query using the Versement relation
 *
 * @method     \VersementQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMois findOne(ConnectionInterface $con = null) Return the first ChildMois matching the query
 * @method     ChildMois findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMois matching the query, or a new ChildMois object populated from the query conditions when no match is found
 *
 * @method     ChildMois findOneByIdmois(int $idmois) Return the first ChildMois filtered by the idmois column
 * @method     ChildMois findOneByLibellemois(string $Libellemois) Return the first ChildMois filtered by the Libellemois column *

 * @method     ChildMois requirePk($key, ConnectionInterface $con = null) Return the ChildMois by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMois requireOne(ConnectionInterface $con = null) Return the first ChildMois matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMois requireOneByIdmois(int $idmois) Return the first ChildMois filtered by the idmois column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMois requireOneByLibellemois(string $Libellemois) Return the first ChildMois filtered by the Libellemois column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMois[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMois objects based on current ModelCriteria
 * @method     ChildMois[]|ObjectCollection findByIdmois(int $idmois) Return ChildMois objects filtered by the idmois column
 * @method     ChildMois[]|ObjectCollection findByLibellemois(string $Libellemois) Return ChildMois objects filtered by the Libellemois column
 * @method     ChildMois[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MoisQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\MoisQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Mois', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMoisQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMoisQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMoisQuery) {
            return $criteria;
        }
        $query = new ChildMoisQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMois|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MoisTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MoisTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMois A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idmois, Libellemois FROM mois WHERE idmois = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMois $obj */
            $obj = new ChildMois();
            $obj->hydrate($row);
            MoisTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMois|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMoisQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MoisTableMap::COL_IDMOIS, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMoisQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MoisTableMap::COL_IDMOIS, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the idmois column
     *
     * Example usage:
     * <code>
     * $query->filterByIdmois(1234); // WHERE idmois = 1234
     * $query->filterByIdmois(array(12, 34)); // WHERE idmois IN (12, 34)
     * $query->filterByIdmois(array('min' => 12)); // WHERE idmois > 12
     * </code>
     *
     * @param     mixed $idmois The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMoisQuery The current query, for fluid interface
     */
    public function filterByIdmois($idmois = null, $comparison = null)
    {
        if (is_array($idmois)) {
            $useMinMax = false;
            if (isset($idmois['min'])) {
                $this->addUsingAlias(MoisTableMap::COL_IDMOIS, $idmois['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idmois['max'])) {
                $this->addUsingAlias(MoisTableMap::COL_IDMOIS, $idmois['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MoisTableMap::COL_IDMOIS, $idmois, $comparison);
    }

    /**
     * Filter the query on the Libellemois column
     *
     * Example usage:
     * <code>
     * $query->filterByLibellemois('fooValue');   // WHERE Libellemois = 'fooValue'
     * $query->filterByLibellemois('%fooValue%', Criteria::LIKE); // WHERE Libellemois LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libellemois The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMoisQuery The current query, for fluid interface
     */
    public function filterByLibellemois($libellemois = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libellemois)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MoisTableMap::COL_LIBELLEMOIS, $libellemois, $comparison);
    }

    /**
     * Filter the query by a related \Versement object
     *
     * @param \Versement|ObjectCollection $versement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMoisQuery The current query, for fluid interface
     */
    public function filterByVersement($versement, $comparison = null)
    {
        if ($versement instanceof \Versement) {
            return $this
                ->addUsingAlias(MoisTableMap::COL_IDMOIS, $versement->getVersementIdmois(), $comparison);
        } elseif ($versement instanceof ObjectCollection) {
            return $this
                ->useVersementQuery()
                ->filterByPrimaryKeys($versement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVersement() only accepts arguments of type \Versement or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Versement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMoisQuery The current query, for fluid interface
     */
    public function joinVersement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Versement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Versement');
        }

        return $this;
    }

    /**
     * Use the Versement relation Versement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VersementQuery A secondary query class using the current class as primary query
     */
    public function useVersementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVersement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Versement', '\VersementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMois $mois Object to remove from the list of results
     *
     * @return $this|ChildMoisQuery The current query, for fluid interface
     */
    public function prune($mois = null)
    {
        if ($mois) {
            $this->addUsingAlias(MoisTableMap::COL_IDMOIS, $mois->getIdmois(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mois table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MoisTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MoisTableMap::clearInstancePool();
            MoisTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MoisTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MoisTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MoisTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MoisTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MoisQuery
