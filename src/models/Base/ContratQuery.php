<?php

namespace Base;

use \Contrat as ChildContrat;
use \ContratQuery as ChildContratQuery;
use \Exception;
use \PDO;
use Map\ContratTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'contrat' table.
 *
 *
 *
 * @method     ChildContratQuery orderByIdcontrat($order = Criteria::ASC) Order by the idContrat column
 * @method     ChildContratQuery orderByContratQuantiteProduit($order = Criteria::ASC) Order by the Contrat_quantite_produit column
 * @method     ChildContratQuery orderByPersonneClient($order = Criteria::ASC) Order by the Personne_client column
 * @method     ChildContratQuery orderByIdrolepersonneClient($order = Criteria::ASC) Order by the idrolePersonne_client column
 * @method     ChildContratQuery orderByPersonneAgent($order = Criteria::ASC) Order by the Personne_agent column
 * @method     ChildContratQuery orderByIdrolepersonneAgent($order = Criteria::ASC) Order by the idrolePersonne_agent column
 * @method     ChildContratQuery orderByDatecreationcontrat($order = Criteria::ASC) Order by the DateCreationContrat column
 * @method     ChildContratQuery orderByMontantcontrat($order = Criteria::ASC) Order by the MontantContrat column
 * @method     ChildContratQuery orderByContratmontantprime($order = Criteria::ASC) Order by the ContratMontantPrime column
 * @method     ChildContratQuery orderByProduitsIdproduits1($order = Criteria::ASC) Order by the produits_idproduits1 column
 * @method     ChildContratQuery orderByProduitsSocieteIdsociete($order = Criteria::ASC) Order by the produits_societe_idsociete column
 *
 * @method     ChildContratQuery groupByIdcontrat() Group by the idContrat column
 * @method     ChildContratQuery groupByContratQuantiteProduit() Group by the Contrat_quantite_produit column
 * @method     ChildContratQuery groupByPersonneClient() Group by the Personne_client column
 * @method     ChildContratQuery groupByIdrolepersonneClient() Group by the idrolePersonne_client column
 * @method     ChildContratQuery groupByPersonneAgent() Group by the Personne_agent column
 * @method     ChildContratQuery groupByIdrolepersonneAgent() Group by the idrolePersonne_agent column
 * @method     ChildContratQuery groupByDatecreationcontrat() Group by the DateCreationContrat column
 * @method     ChildContratQuery groupByMontantcontrat() Group by the MontantContrat column
 * @method     ChildContratQuery groupByContratmontantprime() Group by the ContratMontantPrime column
 * @method     ChildContratQuery groupByProduitsIdproduits1() Group by the produits_idproduits1 column
 * @method     ChildContratQuery groupByProduitsSocieteIdsociete() Group by the produits_societe_idsociete column
 *
 * @method     ChildContratQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContratQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContratQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContratQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContratQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContratQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContratQuery leftJoinProduits($relationAlias = null) Adds a LEFT JOIN clause to the query using the Produits relation
 * @method     ChildContratQuery rightJoinProduits($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Produits relation
 * @method     ChildContratQuery innerJoinProduits($relationAlias = null) Adds a INNER JOIN clause to the query using the Produits relation
 *
 * @method     ChildContratQuery joinWithProduits($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Produits relation
 *
 * @method     ChildContratQuery leftJoinWithProduits() Adds a LEFT JOIN clause and with to the query using the Produits relation
 * @method     ChildContratQuery rightJoinWithProduits() Adds a RIGHT JOIN clause and with to the query using the Produits relation
 * @method     ChildContratQuery innerJoinWithProduits() Adds a INNER JOIN clause and with to the query using the Produits relation
 *
 * @method     ChildContratQuery leftJoinPersonneRelatedByPersonneClientIdrolepersonneClient($relationAlias = null) Adds a LEFT JOIN clause to the query using the PersonneRelatedByPersonneClientIdrolepersonneClient relation
 * @method     ChildContratQuery rightJoinPersonneRelatedByPersonneClientIdrolepersonneClient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PersonneRelatedByPersonneClientIdrolepersonneClient relation
 * @method     ChildContratQuery innerJoinPersonneRelatedByPersonneClientIdrolepersonneClient($relationAlias = null) Adds a INNER JOIN clause to the query using the PersonneRelatedByPersonneClientIdrolepersonneClient relation
 *
 * @method     ChildContratQuery joinWithPersonneRelatedByPersonneClientIdrolepersonneClient($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PersonneRelatedByPersonneClientIdrolepersonneClient relation
 *
 * @method     ChildContratQuery leftJoinWithPersonneRelatedByPersonneClientIdrolepersonneClient() Adds a LEFT JOIN clause and with to the query using the PersonneRelatedByPersonneClientIdrolepersonneClient relation
 * @method     ChildContratQuery rightJoinWithPersonneRelatedByPersonneClientIdrolepersonneClient() Adds a RIGHT JOIN clause and with to the query using the PersonneRelatedByPersonneClientIdrolepersonneClient relation
 * @method     ChildContratQuery innerJoinWithPersonneRelatedByPersonneClientIdrolepersonneClient() Adds a INNER JOIN clause and with to the query using the PersonneRelatedByPersonneClientIdrolepersonneClient relation
 *
 * @method     ChildContratQuery leftJoinPersonneRelatedByPersonneAgentIdrolepersonneAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation
 * @method     ChildContratQuery rightJoinPersonneRelatedByPersonneAgentIdrolepersonneAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation
 * @method     ChildContratQuery innerJoinPersonneRelatedByPersonneAgentIdrolepersonneAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation
 *
 * @method     ChildContratQuery joinWithPersonneRelatedByPersonneAgentIdrolepersonneAgent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation
 *
 * @method     ChildContratQuery leftJoinWithPersonneRelatedByPersonneAgentIdrolepersonneAgent() Adds a LEFT JOIN clause and with to the query using the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation
 * @method     ChildContratQuery rightJoinWithPersonneRelatedByPersonneAgentIdrolepersonneAgent() Adds a RIGHT JOIN clause and with to the query using the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation
 * @method     ChildContratQuery innerJoinWithPersonneRelatedByPersonneAgentIdrolepersonneAgent() Adds a INNER JOIN clause and with to the query using the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation
 *
 * @method     ChildContratQuery leftJoinVersement($relationAlias = null) Adds a LEFT JOIN clause to the query using the Versement relation
 * @method     ChildContratQuery rightJoinVersement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Versement relation
 * @method     ChildContratQuery innerJoinVersement($relationAlias = null) Adds a INNER JOIN clause to the query using the Versement relation
 *
 * @method     ChildContratQuery joinWithVersement($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Versement relation
 *
 * @method     ChildContratQuery leftJoinWithVersement() Adds a LEFT JOIN clause and with to the query using the Versement relation
 * @method     ChildContratQuery rightJoinWithVersement() Adds a RIGHT JOIN clause and with to the query using the Versement relation
 * @method     ChildContratQuery innerJoinWithVersement() Adds a INNER JOIN clause and with to the query using the Versement relation
 *
 * @method     \ProduitsQuery|\PersonneQuery|\VersementQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContrat findOne(ConnectionInterface $con = null) Return the first ChildContrat matching the query
 * @method     ChildContrat findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContrat matching the query, or a new ChildContrat object populated from the query conditions when no match is found
 *
 * @method     ChildContrat findOneByIdcontrat(int $idContrat) Return the first ChildContrat filtered by the idContrat column
 * @method     ChildContrat findOneByContratQuantiteProduit(int $Contrat_quantite_produit) Return the first ChildContrat filtered by the Contrat_quantite_produit column
 * @method     ChildContrat findOneByPersonneClient(int $Personne_client) Return the first ChildContrat filtered by the Personne_client column
 * @method     ChildContrat findOneByIdrolepersonneClient(int $idrolePersonne_client) Return the first ChildContrat filtered by the idrolePersonne_client column
 * @method     ChildContrat findOneByPersonneAgent(int $Personne_agent) Return the first ChildContrat filtered by the Personne_agent column
 * @method     ChildContrat findOneByIdrolepersonneAgent(int $idrolePersonne_agent) Return the first ChildContrat filtered by the idrolePersonne_agent column
 * @method     ChildContrat findOneByDatecreationcontrat(string $DateCreationContrat) Return the first ChildContrat filtered by the DateCreationContrat column
 * @method     ChildContrat findOneByMontantcontrat(int $MontantContrat) Return the first ChildContrat filtered by the MontantContrat column
 * @method     ChildContrat findOneByContratmontantprime(int $ContratMontantPrime) Return the first ChildContrat filtered by the ContratMontantPrime column
 * @method     ChildContrat findOneByProduitsIdproduits1(int $produits_idproduits1) Return the first ChildContrat filtered by the produits_idproduits1 column
 * @method     ChildContrat findOneByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return the first ChildContrat filtered by the produits_societe_idsociete column *

 * @method     ChildContrat requirePk($key, ConnectionInterface $con = null) Return the ChildContrat by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOne(ConnectionInterface $con = null) Return the first ChildContrat matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContrat requireOneByIdcontrat(int $idContrat) Return the first ChildContrat filtered by the idContrat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByContratQuantiteProduit(int $Contrat_quantite_produit) Return the first ChildContrat filtered by the Contrat_quantite_produit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByPersonneClient(int $Personne_client) Return the first ChildContrat filtered by the Personne_client column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByIdrolepersonneClient(int $idrolePersonne_client) Return the first ChildContrat filtered by the idrolePersonne_client column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByPersonneAgent(int $Personne_agent) Return the first ChildContrat filtered by the Personne_agent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByIdrolepersonneAgent(int $idrolePersonne_agent) Return the first ChildContrat filtered by the idrolePersonne_agent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByDatecreationcontrat(string $DateCreationContrat) Return the first ChildContrat filtered by the DateCreationContrat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByMontantcontrat(int $MontantContrat) Return the first ChildContrat filtered by the MontantContrat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByContratmontantprime(int $ContratMontantPrime) Return the first ChildContrat filtered by the ContratMontantPrime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByProduitsIdproduits1(int $produits_idproduits1) Return the first ChildContrat filtered by the produits_idproduits1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContrat requireOneByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return the first ChildContrat filtered by the produits_societe_idsociete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContrat[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContrat objects based on current ModelCriteria
 * @method     ChildContrat[]|ObjectCollection findByIdcontrat(int $idContrat) Return ChildContrat objects filtered by the idContrat column
 * @method     ChildContrat[]|ObjectCollection findByContratQuantiteProduit(int $Contrat_quantite_produit) Return ChildContrat objects filtered by the Contrat_quantite_produit column
 * @method     ChildContrat[]|ObjectCollection findByPersonneClient(int $Personne_client) Return ChildContrat objects filtered by the Personne_client column
 * @method     ChildContrat[]|ObjectCollection findByIdrolepersonneClient(int $idrolePersonne_client) Return ChildContrat objects filtered by the idrolePersonne_client column
 * @method     ChildContrat[]|ObjectCollection findByPersonneAgent(int $Personne_agent) Return ChildContrat objects filtered by the Personne_agent column
 * @method     ChildContrat[]|ObjectCollection findByIdrolepersonneAgent(int $idrolePersonne_agent) Return ChildContrat objects filtered by the idrolePersonne_agent column
 * @method     ChildContrat[]|ObjectCollection findByDatecreationcontrat(string $DateCreationContrat) Return ChildContrat objects filtered by the DateCreationContrat column
 * @method     ChildContrat[]|ObjectCollection findByMontantcontrat(int $MontantContrat) Return ChildContrat objects filtered by the MontantContrat column
 * @method     ChildContrat[]|ObjectCollection findByContratmontantprime(int $ContratMontantPrime) Return ChildContrat objects filtered by the ContratMontantPrime column
 * @method     ChildContrat[]|ObjectCollection findByProduitsIdproduits1(int $produits_idproduits1) Return ChildContrat objects filtered by the produits_idproduits1 column
 * @method     ChildContrat[]|ObjectCollection findByProduitsSocieteIdsociete(int $produits_societe_idsociete) Return ChildContrat objects filtered by the produits_societe_idsociete column
 * @method     ChildContrat[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContratQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ContratQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Contrat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContratQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContratQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContratQuery) {
            return $criteria;
        }
        $query = new ChildContratQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array[$idContrat, $produits_idproduits1, $produits_societe_idsociete] $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContrat|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContratTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContratTableMap::getInstanceFromPool(serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]))))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContrat A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idContrat, Contrat_quantite_produit, Personne_client, idrolePersonne_client, Personne_agent, idrolePersonne_agent, DateCreationContrat, MontantContrat, ContratMontantPrime, produits_idproduits1, produits_societe_idsociete FROM contrat WHERE idContrat = :p0 AND produits_idproduits1 = :p1 AND produits_societe_idsociete = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContrat $obj */
            $obj = new ChildContrat();
            $obj->hydrate($row);
            ContratTableMap::addInstanceToPool($obj, serialize([(null === $key[0] || is_scalar($key[0]) || is_callable([$key[0], '__toString']) ? (string) $key[0] : $key[0]), (null === $key[1] || is_scalar($key[1]) || is_callable([$key[1], '__toString']) ? (string) $key[1] : $key[1]), (null === $key[2] || is_scalar($key[2]) || is_callable([$key[2], '__toString']) ? (string) $key[2] : $key[2])]));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContrat|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(ContratTableMap::COL_IDCONTRAT, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(ContratTableMap::COL_PRODUITS_IDPRODUITS1, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(ContratTableMap::COL_IDCONTRAT, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(ContratTableMap::COL_PRODUITS_IDPRODUITS1, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the idContrat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdcontrat(1234); // WHERE idContrat = 1234
     * $query->filterByIdcontrat(array(12, 34)); // WHERE idContrat IN (12, 34)
     * $query->filterByIdcontrat(array('min' => 12)); // WHERE idContrat > 12
     * </code>
     *
     * @param     mixed $idcontrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByIdcontrat($idcontrat = null, $comparison = null)
    {
        if (is_array($idcontrat)) {
            $useMinMax = false;
            if (isset($idcontrat['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_IDCONTRAT, $idcontrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idcontrat['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_IDCONTRAT, $idcontrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_IDCONTRAT, $idcontrat, $comparison);
    }

    /**
     * Filter the query on the Contrat_quantite_produit column
     *
     * Example usage:
     * <code>
     * $query->filterByContratQuantiteProduit(1234); // WHERE Contrat_quantite_produit = 1234
     * $query->filterByContratQuantiteProduit(array(12, 34)); // WHERE Contrat_quantite_produit IN (12, 34)
     * $query->filterByContratQuantiteProduit(array('min' => 12)); // WHERE Contrat_quantite_produit > 12
     * </code>
     *
     * @param     mixed $contratQuantiteProduit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByContratQuantiteProduit($contratQuantiteProduit = null, $comparison = null)
    {
        if (is_array($contratQuantiteProduit)) {
            $useMinMax = false;
            if (isset($contratQuantiteProduit['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_CONTRAT_QUANTITE_PRODUIT, $contratQuantiteProduit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contratQuantiteProduit['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_CONTRAT_QUANTITE_PRODUIT, $contratQuantiteProduit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_CONTRAT_QUANTITE_PRODUIT, $contratQuantiteProduit, $comparison);
    }

    /**
     * Filter the query on the Personne_client column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonneClient(1234); // WHERE Personne_client = 1234
     * $query->filterByPersonneClient(array(12, 34)); // WHERE Personne_client IN (12, 34)
     * $query->filterByPersonneClient(array('min' => 12)); // WHERE Personne_client > 12
     * </code>
     *
     * @see       filterByPersonneRelatedByPersonneClientIdrolepersonneClient()
     *
     * @param     mixed $personneClient The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByPersonneClient($personneClient = null, $comparison = null)
    {
        if (is_array($personneClient)) {
            $useMinMax = false;
            if (isset($personneClient['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_PERSONNE_CLIENT, $personneClient['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personneClient['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_PERSONNE_CLIENT, $personneClient['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_PERSONNE_CLIENT, $personneClient, $comparison);
    }

    /**
     * Filter the query on the idrolePersonne_client column
     *
     * Example usage:
     * <code>
     * $query->filterByIdrolepersonneClient(1234); // WHERE idrolePersonne_client = 1234
     * $query->filterByIdrolepersonneClient(array(12, 34)); // WHERE idrolePersonne_client IN (12, 34)
     * $query->filterByIdrolepersonneClient(array('min' => 12)); // WHERE idrolePersonne_client > 12
     * </code>
     *
     * @see       filterByPersonneRelatedByPersonneClientIdrolepersonneClient()
     *
     * @param     mixed $idrolepersonneClient The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByIdrolepersonneClient($idrolepersonneClient = null, $comparison = null)
    {
        if (is_array($idrolepersonneClient)) {
            $useMinMax = false;
            if (isset($idrolepersonneClient['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_IDROLEPERSONNE_CLIENT, $idrolepersonneClient['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idrolepersonneClient['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_IDROLEPERSONNE_CLIENT, $idrolepersonneClient['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_IDROLEPERSONNE_CLIENT, $idrolepersonneClient, $comparison);
    }

    /**
     * Filter the query on the Personne_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonneAgent(1234); // WHERE Personne_agent = 1234
     * $query->filterByPersonneAgent(array(12, 34)); // WHERE Personne_agent IN (12, 34)
     * $query->filterByPersonneAgent(array('min' => 12)); // WHERE Personne_agent > 12
     * </code>
     *
     * @see       filterByPersonneRelatedByPersonneAgentIdrolepersonneAgent()
     *
     * @param     mixed $personneAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByPersonneAgent($personneAgent = null, $comparison = null)
    {
        if (is_array($personneAgent)) {
            $useMinMax = false;
            if (isset($personneAgent['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_PERSONNE_AGENT, $personneAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personneAgent['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_PERSONNE_AGENT, $personneAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_PERSONNE_AGENT, $personneAgent, $comparison);
    }

    /**
     * Filter the query on the idrolePersonne_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdrolepersonneAgent(1234); // WHERE idrolePersonne_agent = 1234
     * $query->filterByIdrolepersonneAgent(array(12, 34)); // WHERE idrolePersonne_agent IN (12, 34)
     * $query->filterByIdrolepersonneAgent(array('min' => 12)); // WHERE idrolePersonne_agent > 12
     * </code>
     *
     * @see       filterByPersonneRelatedByPersonneAgentIdrolepersonneAgent()
     *
     * @param     mixed $idrolepersonneAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByIdrolepersonneAgent($idrolepersonneAgent = null, $comparison = null)
    {
        if (is_array($idrolepersonneAgent)) {
            $useMinMax = false;
            if (isset($idrolepersonneAgent['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_IDROLEPERSONNE_AGENT, $idrolepersonneAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idrolepersonneAgent['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_IDROLEPERSONNE_AGENT, $idrolepersonneAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_IDROLEPERSONNE_AGENT, $idrolepersonneAgent, $comparison);
    }

    /**
     * Filter the query on the DateCreationContrat column
     *
     * Example usage:
     * <code>
     * $query->filterByDatecreationcontrat('2011-03-14'); // WHERE DateCreationContrat = '2011-03-14'
     * $query->filterByDatecreationcontrat('now'); // WHERE DateCreationContrat = '2011-03-14'
     * $query->filterByDatecreationcontrat(array('max' => 'yesterday')); // WHERE DateCreationContrat > '2011-03-13'
     * </code>
     *
     * @param     mixed $datecreationcontrat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByDatecreationcontrat($datecreationcontrat = null, $comparison = null)
    {
        if (is_array($datecreationcontrat)) {
            $useMinMax = false;
            if (isset($datecreationcontrat['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_DATECREATIONCONTRAT, $datecreationcontrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datecreationcontrat['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_DATECREATIONCONTRAT, $datecreationcontrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_DATECREATIONCONTRAT, $datecreationcontrat, $comparison);
    }

    /**
     * Filter the query on the MontantContrat column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantcontrat(1234); // WHERE MontantContrat = 1234
     * $query->filterByMontantcontrat(array(12, 34)); // WHERE MontantContrat IN (12, 34)
     * $query->filterByMontantcontrat(array('min' => 12)); // WHERE MontantContrat > 12
     * </code>
     *
     * @param     mixed $montantcontrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByMontantcontrat($montantcontrat = null, $comparison = null)
    {
        if (is_array($montantcontrat)) {
            $useMinMax = false;
            if (isset($montantcontrat['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_MONTANTCONTRAT, $montantcontrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantcontrat['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_MONTANTCONTRAT, $montantcontrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_MONTANTCONTRAT, $montantcontrat, $comparison);
    }

    /**
     * Filter the query on the ContratMontantPrime column
     *
     * Example usage:
     * <code>
     * $query->filterByContratmontantprime(1234); // WHERE ContratMontantPrime = 1234
     * $query->filterByContratmontantprime(array(12, 34)); // WHERE ContratMontantPrime IN (12, 34)
     * $query->filterByContratmontantprime(array('min' => 12)); // WHERE ContratMontantPrime > 12
     * </code>
     *
     * @param     mixed $contratmontantprime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByContratmontantprime($contratmontantprime = null, $comparison = null)
    {
        if (is_array($contratmontantprime)) {
            $useMinMax = false;
            if (isset($contratmontantprime['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_CONTRATMONTANTPRIME, $contratmontantprime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contratmontantprime['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_CONTRATMONTANTPRIME, $contratmontantprime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_CONTRATMONTANTPRIME, $contratmontantprime, $comparison);
    }

    /**
     * Filter the query on the produits_idproduits1 column
     *
     * Example usage:
     * <code>
     * $query->filterByProduitsIdproduits1(1234); // WHERE produits_idproduits1 = 1234
     * $query->filterByProduitsIdproduits1(array(12, 34)); // WHERE produits_idproduits1 IN (12, 34)
     * $query->filterByProduitsIdproduits1(array('min' => 12)); // WHERE produits_idproduits1 > 12
     * </code>
     *
     * @see       filterByProduits()
     *
     * @param     mixed $produitsIdproduits1 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByProduitsIdproduits1($produitsIdproduits1 = null, $comparison = null)
    {
        if (is_array($produitsIdproduits1)) {
            $useMinMax = false;
            if (isset($produitsIdproduits1['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_PRODUITS_IDPRODUITS1, $produitsIdproduits1['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produitsIdproduits1['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_PRODUITS_IDPRODUITS1, $produitsIdproduits1['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_PRODUITS_IDPRODUITS1, $produitsIdproduits1, $comparison);
    }

    /**
     * Filter the query on the produits_societe_idsociete column
     *
     * Example usage:
     * <code>
     * $query->filterByProduitsSocieteIdsociete(1234); // WHERE produits_societe_idsociete = 1234
     * $query->filterByProduitsSocieteIdsociete(array(12, 34)); // WHERE produits_societe_idsociete IN (12, 34)
     * $query->filterByProduitsSocieteIdsociete(array('min' => 12)); // WHERE produits_societe_idsociete > 12
     * </code>
     *
     * @see       filterByProduits()
     *
     * @param     mixed $produitsSocieteIdsociete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function filterByProduitsSocieteIdsociete($produitsSocieteIdsociete = null, $comparison = null)
    {
        if (is_array($produitsSocieteIdsociete)) {
            $useMinMax = false;
            if (isset($produitsSocieteIdsociete['min'])) {
                $this->addUsingAlias(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($produitsSocieteIdsociete['max'])) {
                $this->addUsingAlias(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produitsSocieteIdsociete, $comparison);
    }

    /**
     * Filter the query by a related \Produits object
     *
     * @param \Produits $produits The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContratQuery The current query, for fluid interface
     */
    public function filterByProduits($produits, $comparison = null)
    {
        if ($produits instanceof \Produits) {
            return $this
                ->addUsingAlias(ContratTableMap::COL_PRODUITS_IDPRODUITS1, $produits->getIdproduits(), $comparison)
                ->addUsingAlias(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $produits->getSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByProduits() only accepts arguments of type \Produits');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Produits relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function joinProduits($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Produits');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Produits');
        }

        return $this;
    }

    /**
     * Use the Produits relation Produits object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProduitsQuery A secondary query class using the current class as primary query
     */
    public function useProduitsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduits($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Produits', '\ProduitsQuery');
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne $personne The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContratQuery The current query, for fluid interface
     */
    public function filterByPersonneRelatedByPersonneClientIdrolepersonneClient($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(ContratTableMap::COL_PERSONNE_CLIENT, $personne->getIdpersonne(), $comparison)
                ->addUsingAlias(ContratTableMap::COL_IDROLEPERSONNE_CLIENT, $personne->getRolepersonneIdrolepersonne(), $comparison);
        } else {
            throw new PropelException('filterByPersonneRelatedByPersonneClientIdrolepersonneClient() only accepts arguments of type \Personne');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PersonneRelatedByPersonneClientIdrolepersonneClient relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function joinPersonneRelatedByPersonneClientIdrolepersonneClient($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PersonneRelatedByPersonneClientIdrolepersonneClient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PersonneRelatedByPersonneClientIdrolepersonneClient');
        }

        return $this;
    }

    /**
     * Use the PersonneRelatedByPersonneClientIdrolepersonneClient relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneRelatedByPersonneClientIdrolepersonneClientQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonneRelatedByPersonneClientIdrolepersonneClient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PersonneRelatedByPersonneClientIdrolepersonneClient', '\PersonneQuery');
    }

    /**
     * Filter the query by a related \Personne object
     *
     * @param \Personne $personne The related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContratQuery The current query, for fluid interface
     */
    public function filterByPersonneRelatedByPersonneAgentIdrolepersonneAgent($personne, $comparison = null)
    {
        if ($personne instanceof \Personne) {
            return $this
                ->addUsingAlias(ContratTableMap::COL_PERSONNE_AGENT, $personne->getIdpersonne(), $comparison)
                ->addUsingAlias(ContratTableMap::COL_IDROLEPERSONNE_AGENT, $personne->getRolepersonneIdrolepersonne(), $comparison);
        } else {
            throw new PropelException('filterByPersonneRelatedByPersonneAgentIdrolepersonneAgent() only accepts arguments of type \Personne');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function joinPersonneRelatedByPersonneAgentIdrolepersonneAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PersonneRelatedByPersonneAgentIdrolepersonneAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PersonneRelatedByPersonneAgentIdrolepersonneAgent');
        }

        return $this;
    }

    /**
     * Use the PersonneRelatedByPersonneAgentIdrolepersonneAgent relation Personne object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PersonneQuery A secondary query class using the current class as primary query
     */
    public function usePersonneRelatedByPersonneAgentIdrolepersonneAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPersonneRelatedByPersonneAgentIdrolepersonneAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PersonneRelatedByPersonneAgentIdrolepersonneAgent', '\PersonneQuery');
    }

    /**
     * Filter the query by a related \Versement object
     *
     * @param \Versement|ObjectCollection $versement the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildContratQuery The current query, for fluid interface
     */
    public function filterByVersement($versement, $comparison = null)
    {
        if ($versement instanceof \Versement) {
            return $this
                ->addUsingAlias(ContratTableMap::COL_IDCONTRAT, $versement->getContratIdcontrat(), $comparison)
                ->addUsingAlias(ContratTableMap::COL_PRODUITS_IDPRODUITS1, $versement->getContratProduitsIdproduits(), $comparison)
                ->addUsingAlias(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $versement->getContratProduitsSocieteIdsociete(), $comparison);
        } else {
            throw new PropelException('filterByVersement() only accepts arguments of type \Versement');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Versement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function joinVersement($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Versement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Versement');
        }

        return $this;
    }

    /**
     * Use the Versement relation Versement object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \VersementQuery A secondary query class using the current class as primary query
     */
    public function useVersementQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVersement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Versement', '\VersementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContrat $contrat Object to remove from the list of results
     *
     * @return $this|ChildContratQuery The current query, for fluid interface
     */
    public function prune($contrat = null)
    {
        if ($contrat) {
            $this->addCond('pruneCond0', $this->getAliasedColName(ContratTableMap::COL_IDCONTRAT), $contrat->getIdcontrat(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(ContratTableMap::COL_PRODUITS_IDPRODUITS1), $contrat->getProduitsIdproduits1(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE), $contrat->getProduitsSocieteIdsociete(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

    /**
     * Deletes all rows from the contrat table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContratTableMap::clearInstancePool();
            ContratTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContratTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContratTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContratTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ContratQuery
