<?php

namespace Map;

use \Prospection;
use \ProspectionQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'prospection' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ProspectionTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ProspectionTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'prospection';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Prospection';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Prospection';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the idprospection field
     */
    const COL_IDPROSPECTION = 'prospection.idprospection';

    /**
     * the column name for the NomProspect field
     */
    const COL_NOMPROSPECT = 'prospection.NomProspect';

    /**
     * the column name for the Prenomprospection field
     */
    const COL_PRENOMPROSPECTION = 'prospection.Prenomprospection';

    /**
     * the column name for the TelProspect field
     */
    const COL_TELPROSPECT = 'prospection.TelProspect';

    /**
     * the column name for the EmailProspect field
     */
    const COL_EMAILPROSPECT = 'prospection.EmailProspect';

    /**
     * the column name for the DatePriseRDV field
     */
    const COL_DATEPRISERDV = 'prospection.DatePriseRDV';

    /**
     * the column name for the DateRDV field
     */
    const COL_DATERDV = 'prospection.DateRDV';

    /**
     * the column name for the typeprospect field
     */
    const COL_TYPEPROSPECT = 'prospection.typeprospect';

    /**
     * the column name for the fonctionprospect field
     */
    const COL_FONCTIONPROSPECT = 'prospection.fonctionprospect';

    /**
     * the column name for the idPersonneAgent field
     */
    const COL_IDPERSONNEAGENT = 'prospection.idPersonneAgent';

    /**
     * the column name for the Personne_rolePersonne_idrolePersonne field
     */
    const COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE = 'prospection.Personne_rolePersonne_idrolePersonne';

    /**
     * the column name for the produits_idproduits field
     */
    const COL_PRODUITS_IDPRODUITS = 'prospection.produits_idproduits';

    /**
     * the column name for the produits_societe_idsociete field
     */
    const COL_PRODUITS_SOCIETE_IDSOCIETE = 'prospection.produits_societe_idsociete';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idprospection', 'Nomprospect', 'Prenomprospection', 'Telprospect', 'Emailprospect', 'Datepriserdv', 'Daterdv', 'Typeprospect', 'Fonctionprospect', 'Idpersonneagent', 'PersonneRolepersonneIdrolepersonne', 'ProduitsIdproduits', 'ProduitsSocieteIdsociete', ),
        self::TYPE_CAMELNAME     => array('idprospection', 'nomprospect', 'prenomprospection', 'telprospect', 'emailprospect', 'datepriserdv', 'daterdv', 'typeprospect', 'fonctionprospect', 'idpersonneagent', 'personneRolepersonneIdrolepersonne', 'produitsIdproduits', 'produitsSocieteIdsociete', ),
        self::TYPE_COLNAME       => array(ProspectionTableMap::COL_IDPROSPECTION, ProspectionTableMap::COL_NOMPROSPECT, ProspectionTableMap::COL_PRENOMPROSPECTION, ProspectionTableMap::COL_TELPROSPECT, ProspectionTableMap::COL_EMAILPROSPECT, ProspectionTableMap::COL_DATEPRISERDV, ProspectionTableMap::COL_DATERDV, ProspectionTableMap::COL_TYPEPROSPECT, ProspectionTableMap::COL_FONCTIONPROSPECT, ProspectionTableMap::COL_IDPERSONNEAGENT, ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, ProspectionTableMap::COL_PRODUITS_IDPRODUITS, ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, ),
        self::TYPE_FIELDNAME     => array('idprospection', 'NomProspect', 'Prenomprospection', 'TelProspect', 'EmailProspect', 'DatePriseRDV', 'DateRDV', 'typeprospect', 'fonctionprospect', 'idPersonneAgent', 'Personne_rolePersonne_idrolePersonne', 'produits_idproduits', 'produits_societe_idsociete', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idprospection' => 0, 'Nomprospect' => 1, 'Prenomprospection' => 2, 'Telprospect' => 3, 'Emailprospect' => 4, 'Datepriserdv' => 5, 'Daterdv' => 6, 'Typeprospect' => 7, 'Fonctionprospect' => 8, 'Idpersonneagent' => 9, 'PersonneRolepersonneIdrolepersonne' => 10, 'ProduitsIdproduits' => 11, 'ProduitsSocieteIdsociete' => 12, ),
        self::TYPE_CAMELNAME     => array('idprospection' => 0, 'nomprospect' => 1, 'prenomprospection' => 2, 'telprospect' => 3, 'emailprospect' => 4, 'datepriserdv' => 5, 'daterdv' => 6, 'typeprospect' => 7, 'fonctionprospect' => 8, 'idpersonneagent' => 9, 'personneRolepersonneIdrolepersonne' => 10, 'produitsIdproduits' => 11, 'produitsSocieteIdsociete' => 12, ),
        self::TYPE_COLNAME       => array(ProspectionTableMap::COL_IDPROSPECTION => 0, ProspectionTableMap::COL_NOMPROSPECT => 1, ProspectionTableMap::COL_PRENOMPROSPECTION => 2, ProspectionTableMap::COL_TELPROSPECT => 3, ProspectionTableMap::COL_EMAILPROSPECT => 4, ProspectionTableMap::COL_DATEPRISERDV => 5, ProspectionTableMap::COL_DATERDV => 6, ProspectionTableMap::COL_TYPEPROSPECT => 7, ProspectionTableMap::COL_FONCTIONPROSPECT => 8, ProspectionTableMap::COL_IDPERSONNEAGENT => 9, ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE => 10, ProspectionTableMap::COL_PRODUITS_IDPRODUITS => 11, ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE => 12, ),
        self::TYPE_FIELDNAME     => array('idprospection' => 0, 'NomProspect' => 1, 'Prenomprospection' => 2, 'TelProspect' => 3, 'EmailProspect' => 4, 'DatePriseRDV' => 5, 'DateRDV' => 6, 'typeprospect' => 7, 'fonctionprospect' => 8, 'idPersonneAgent' => 9, 'Personne_rolePersonne_idrolePersonne' => 10, 'produits_idproduits' => 11, 'produits_societe_idsociete' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('prospection');
        $this->setPhpName('Prospection');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Prospection');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idprospection', 'Idprospection', 'INTEGER', true, null, null);
        $this->addColumn('NomProspect', 'Nomprospect', 'VARCHAR', false, 85, null);
        $this->addColumn('Prenomprospection', 'Prenomprospection', 'VARCHAR', false, 105, null);
        $this->addColumn('TelProspect', 'Telprospect', 'VARCHAR', false, 45, null);
        $this->addColumn('EmailProspect', 'Emailprospect', 'VARCHAR', false, 45, null);
        $this->addColumn('DatePriseRDV', 'Datepriserdv', 'DATE', false, null, null);
        $this->addColumn('DateRDV', 'Daterdv', 'DATE', false, null, null);
        $this->addColumn('typeprospect', 'Typeprospect', 'INTEGER', false, null, null);
        $this->addColumn('fonctionprospect', 'Fonctionprospect', 'VARCHAR', false, 100, null);
        $this->addForeignPrimaryKey('idPersonneAgent', 'Idpersonneagent', 'INTEGER' , 'personne', 'idPersonne', true, null, null);
        $this->addForeignPrimaryKey('Personne_rolePersonne_idrolePersonne', 'PersonneRolepersonneIdrolepersonne', 'INTEGER' , 'personne', 'rolePersonne_idrolePersonne', true, null, null);
        $this->addForeignPrimaryKey('produits_idproduits', 'ProduitsIdproduits', 'INTEGER' , 'produits', 'idproduits', true, null, null);
        $this->addForeignPrimaryKey('produits_societe_idsociete', 'ProduitsSocieteIdsociete', 'INTEGER' , 'produits', 'societe_idsociete', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Personne', '\\Personne', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':idPersonneAgent',
    1 => ':idPersonne',
  ),
  1 =>
  array (
    0 => ':Personne_rolePersonne_idrolePersonne',
    1 => ':rolePersonne_idrolePersonne',
  ),
), null, null, null, false);
        $this->addRelation('Produits', '\\Produits', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':produits_idproduits',
    1 => ':idproduits',
  ),
  1 =>
  array (
    0 => ':produits_societe_idsociete',
    1 => ':societe_idsociete',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Prospection $obj A \Prospection object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize([(null === $obj->getIdprospection() || is_scalar($obj->getIdprospection()) || is_callable([$obj->getIdprospection(), '__toString']) ? (string) $obj->getIdprospection() : $obj->getIdprospection()), (null === $obj->getIdpersonneagent() || is_scalar($obj->getIdpersonneagent()) || is_callable([$obj->getIdpersonneagent(), '__toString']) ? (string) $obj->getIdpersonneagent() : $obj->getIdpersonneagent()), (null === $obj->getPersonneRolepersonneIdrolepersonne() || is_scalar($obj->getPersonneRolepersonneIdrolepersonne()) || is_callable([$obj->getPersonneRolepersonneIdrolepersonne(), '__toString']) ? (string) $obj->getPersonneRolepersonneIdrolepersonne() : $obj->getPersonneRolepersonneIdrolepersonne()), (null === $obj->getProduitsIdproduits() || is_scalar($obj->getProduitsIdproduits()) || is_callable([$obj->getProduitsIdproduits(), '__toString']) ? (string) $obj->getProduitsIdproduits() : $obj->getProduitsIdproduits()), (null === $obj->getProduitsSocieteIdsociete() || is_scalar($obj->getProduitsSocieteIdsociete()) || is_callable([$obj->getProduitsSocieteIdsociete(), '__toString']) ? (string) $obj->getProduitsSocieteIdsociete() : $obj->getProduitsSocieteIdsociete())]);
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Prospection object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Prospection) {
                $key = serialize([(null === $value->getIdprospection() || is_scalar($value->getIdprospection()) || is_callable([$value->getIdprospection(), '__toString']) ? (string) $value->getIdprospection() : $value->getIdprospection()), (null === $value->getIdpersonneagent() || is_scalar($value->getIdpersonneagent()) || is_callable([$value->getIdpersonneagent(), '__toString']) ? (string) $value->getIdpersonneagent() : $value->getIdpersonneagent()), (null === $value->getPersonneRolepersonneIdrolepersonne() || is_scalar($value->getPersonneRolepersonneIdrolepersonne()) || is_callable([$value->getPersonneRolepersonneIdrolepersonne(), '__toString']) ? (string) $value->getPersonneRolepersonneIdrolepersonne() : $value->getPersonneRolepersonneIdrolepersonne()), (null === $value->getProduitsIdproduits() || is_scalar($value->getProduitsIdproduits()) || is_callable([$value->getProduitsIdproduits(), '__toString']) ? (string) $value->getProduitsIdproduits() : $value->getProduitsIdproduits()), (null === $value->getProduitsSocieteIdsociete() || is_scalar($value->getProduitsSocieteIdsociete()) || is_callable([$value->getProduitsSocieteIdsociete(), '__toString']) ? (string) $value->getProduitsSocieteIdsociete() : $value->getProduitsSocieteIdsociete())]);

            } elseif (is_array($value) && count($value) === 5) {
                // assume we've been passed a primary key";
                $key = serialize([(null === $value[0] || is_scalar($value[0]) || is_callable([$value[0], '__toString']) ? (string) $value[0] : $value[0]), (null === $value[1] || is_scalar($value[1]) || is_callable([$value[1], '__toString']) ? (string) $value[1] : $value[1]), (null === $value[2] || is_scalar($value[2]) || is_callable([$value[2], '__toString']) ? (string) $value[2] : $value[2]), (null === $value[3] || is_scalar($value[3]) || is_callable([$value[3], '__toString']) ? (string) $value[3] : $value[3]), (null === $value[4] || is_scalar($value[4]) || is_callable([$value[4], '__toString']) ? (string) $value[4] : $value[4])]);
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Prospection object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprospection', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('Idpersonneagent', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('PersonneRolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 11 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 12 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize([(null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprospection', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprospection', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprospection', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprospection', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprospection', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('Idpersonneagent', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('Idpersonneagent', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('Idpersonneagent', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('Idpersonneagent', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('Idpersonneagent', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('PersonneRolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('PersonneRolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('PersonneRolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('PersonneRolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('PersonneRolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 11 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 11 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 11 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 11 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 11 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 12 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 12 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 12 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 12 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 12 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)])]);
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idprospection', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 9 + $offset
                : self::translateFieldName('Idpersonneagent', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 10 + $offset
                : self::translateFieldName('PersonneRolepersonneIdrolepersonne', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 11 + $offset
                : self::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 12 + $offset
                : self::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProspectionTableMap::CLASS_DEFAULT : ProspectionTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Prospection object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProspectionTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProspectionTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProspectionTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProspectionTableMap::OM_CLASS;
            /** @var Prospection $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProspectionTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProspectionTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProspectionTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Prospection $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProspectionTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProspectionTableMap::COL_IDPROSPECTION);
            $criteria->addSelectColumn(ProspectionTableMap::COL_NOMPROSPECT);
            $criteria->addSelectColumn(ProspectionTableMap::COL_PRENOMPROSPECTION);
            $criteria->addSelectColumn(ProspectionTableMap::COL_TELPROSPECT);
            $criteria->addSelectColumn(ProspectionTableMap::COL_EMAILPROSPECT);
            $criteria->addSelectColumn(ProspectionTableMap::COL_DATEPRISERDV);
            $criteria->addSelectColumn(ProspectionTableMap::COL_DATERDV);
            $criteria->addSelectColumn(ProspectionTableMap::COL_TYPEPROSPECT);
            $criteria->addSelectColumn(ProspectionTableMap::COL_FONCTIONPROSPECT);
            $criteria->addSelectColumn(ProspectionTableMap::COL_IDPERSONNEAGENT);
            $criteria->addSelectColumn(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE);
            $criteria->addSelectColumn(ProspectionTableMap::COL_PRODUITS_IDPRODUITS);
            $criteria->addSelectColumn(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE);
        } else {
            $criteria->addSelectColumn($alias . '.idprospection');
            $criteria->addSelectColumn($alias . '.NomProspect');
            $criteria->addSelectColumn($alias . '.Prenomprospection');
            $criteria->addSelectColumn($alias . '.TelProspect');
            $criteria->addSelectColumn($alias . '.EmailProspect');
            $criteria->addSelectColumn($alias . '.DatePriseRDV');
            $criteria->addSelectColumn($alias . '.DateRDV');
            $criteria->addSelectColumn($alias . '.typeprospect');
            $criteria->addSelectColumn($alias . '.fonctionprospect');
            $criteria->addSelectColumn($alias . '.idPersonneAgent');
            $criteria->addSelectColumn($alias . '.Personne_rolePersonne_idrolePersonne');
            $criteria->addSelectColumn($alias . '.produits_idproduits');
            $criteria->addSelectColumn($alias . '.produits_societe_idsociete');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProspectionTableMap::DATABASE_NAME)->getTable(ProspectionTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProspectionTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProspectionTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProspectionTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Prospection or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Prospection object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProspectionTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Prospection) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProspectionTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(ProspectionTableMap::COL_IDPROSPECTION, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(ProspectionTableMap::COL_IDPERSONNEAGENT, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(ProspectionTableMap::COL_PERSONNE_ROLEPERSONNE_IDROLEPERSONNE, $value[2]));
                $criterion->addAnd($criteria->getNewCriterion(ProspectionTableMap::COL_PRODUITS_IDPRODUITS, $value[3]));
                $criterion->addAnd($criteria->getNewCriterion(ProspectionTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $value[4]));
                $criteria->addOr($criterion);
            }
        }

        $query = ProspectionQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProspectionTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProspectionTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the prospection table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProspectionQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Prospection or Criteria object.
     *
     * @param mixed               $criteria Criteria or Prospection object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProspectionTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Prospection object
        }

        if ($criteria->containsKey(ProspectionTableMap::COL_IDPROSPECTION) && $criteria->keyContainsValue(ProspectionTableMap::COL_IDPROSPECTION) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProspectionTableMap::COL_IDPROSPECTION.')');
        }


        // Set the correct dbName
        $query = ProspectionQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProspectionTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProspectionTableMap::buildTableMap();
