<?php

namespace Map;

use \Modepaiement;
use \ModepaiementQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'modepaiement' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ModepaiementTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ModepaiementTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'modepaiement';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Modepaiement';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Modepaiement';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the idmodePaiement field
     */
    const COL_IDMODEPAIEMENT = 'modepaiement.idmodePaiement';

    /**
     * the column name for the LibellemodePaiement field
     */
    const COL_LIBELLEMODEPAIEMENT = 'modepaiement.LibellemodePaiement';

    /**
     * the column name for the Banque_idBanque field
     */
    const COL_BANQUE_IDBANQUE = 'modepaiement.Banque_idBanque';

    /**
     * the column name for the societe_idsociete field
     */
    const COL_SOCIETE_IDSOCIETE = 'modepaiement.societe_idsociete';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idmodepaiement', 'Libellemodepaiement', 'BanqueIdbanque', 'SocieteIdsociete', ),
        self::TYPE_CAMELNAME     => array('idmodepaiement', 'libellemodepaiement', 'banqueIdbanque', 'societeIdsociete', ),
        self::TYPE_COLNAME       => array(ModepaiementTableMap::COL_IDMODEPAIEMENT, ModepaiementTableMap::COL_LIBELLEMODEPAIEMENT, ModepaiementTableMap::COL_BANQUE_IDBANQUE, ModepaiementTableMap::COL_SOCIETE_IDSOCIETE, ),
        self::TYPE_FIELDNAME     => array('idmodePaiement', 'LibellemodePaiement', 'Banque_idBanque', 'societe_idsociete', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idmodepaiement' => 0, 'Libellemodepaiement' => 1, 'BanqueIdbanque' => 2, 'SocieteIdsociete' => 3, ),
        self::TYPE_CAMELNAME     => array('idmodepaiement' => 0, 'libellemodepaiement' => 1, 'banqueIdbanque' => 2, 'societeIdsociete' => 3, ),
        self::TYPE_COLNAME       => array(ModepaiementTableMap::COL_IDMODEPAIEMENT => 0, ModepaiementTableMap::COL_LIBELLEMODEPAIEMENT => 1, ModepaiementTableMap::COL_BANQUE_IDBANQUE => 2, ModepaiementTableMap::COL_SOCIETE_IDSOCIETE => 3, ),
        self::TYPE_FIELDNAME     => array('idmodePaiement' => 0, 'LibellemodePaiement' => 1, 'Banque_idBanque' => 2, 'societe_idsociete' => 3, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('modepaiement');
        $this->setPhpName('Modepaiement');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Modepaiement');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idmodePaiement', 'Idmodepaiement', 'INTEGER', true, null, null);
        $this->addColumn('LibellemodePaiement', 'Libellemodepaiement', 'VARCHAR', false, 45, null);
        $this->addForeignKey('Banque_idBanque', 'BanqueIdbanque', 'INTEGER', 'banque', 'idBanque', true, null, null);
        $this->addForeignKey('societe_idsociete', 'SocieteIdsociete', 'INTEGER', 'societe', 'idsociete', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Banque', '\\Banque', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':Banque_idBanque',
    1 => ':idBanque',
  ),
), null, null, null, false);
        $this->addRelation('Societe', '\\Societe', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':societe_idsociete',
    1 => ':idsociete',
  ),
), null, null, null, false);
        $this->addRelation('Versement', '\\Versement', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':modePaiement_idmodePaiement',
    1 => ':idmodePaiement',
  ),
), null, null, 'Versements', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idmodepaiement', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idmodepaiement', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idmodepaiement', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idmodepaiement', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idmodepaiement', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idmodepaiement', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idmodepaiement', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ModepaiementTableMap::CLASS_DEFAULT : ModepaiementTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Modepaiement object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ModepaiementTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ModepaiementTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ModepaiementTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ModepaiementTableMap::OM_CLASS;
            /** @var Modepaiement $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ModepaiementTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ModepaiementTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ModepaiementTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Modepaiement $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ModepaiementTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ModepaiementTableMap::COL_IDMODEPAIEMENT);
            $criteria->addSelectColumn(ModepaiementTableMap::COL_LIBELLEMODEPAIEMENT);
            $criteria->addSelectColumn(ModepaiementTableMap::COL_BANQUE_IDBANQUE);
            $criteria->addSelectColumn(ModepaiementTableMap::COL_SOCIETE_IDSOCIETE);
        } else {
            $criteria->addSelectColumn($alias . '.idmodePaiement');
            $criteria->addSelectColumn($alias . '.LibellemodePaiement');
            $criteria->addSelectColumn($alias . '.Banque_idBanque');
            $criteria->addSelectColumn($alias . '.societe_idsociete');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ModepaiementTableMap::DATABASE_NAME)->getTable(ModepaiementTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ModepaiementTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ModepaiementTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ModepaiementTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Modepaiement or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Modepaiement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ModepaiementTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Modepaiement) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ModepaiementTableMap::DATABASE_NAME);
            $criteria->add(ModepaiementTableMap::COL_IDMODEPAIEMENT, (array) $values, Criteria::IN);
        }

        $query = ModepaiementQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ModepaiementTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ModepaiementTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the modepaiement table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ModepaiementQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Modepaiement or Criteria object.
     *
     * @param mixed               $criteria Criteria or Modepaiement object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ModepaiementTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Modepaiement object
        }

        if ($criteria->containsKey(ModepaiementTableMap::COL_IDMODEPAIEMENT) && $criteria->keyContainsValue(ModepaiementTableMap::COL_IDMODEPAIEMENT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ModepaiementTableMap::COL_IDMODEPAIEMENT.')');
        }


        // Set the correct dbName
        $query = ModepaiementQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ModepaiementTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ModepaiementTableMap::buildTableMap();
