<?php

namespace Map;

use \Contrat;
use \ContratQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'contrat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ContratTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ContratTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'contrat';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Contrat';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Contrat';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the idContrat field
     */
    const COL_IDCONTRAT = 'contrat.idContrat';

    /**
     * the column name for the Contrat_quantite_produit field
     */
    const COL_CONTRAT_QUANTITE_PRODUIT = 'contrat.Contrat_quantite_produit';

    /**
     * the column name for the Personne_client field
     */
    const COL_PERSONNE_CLIENT = 'contrat.Personne_client';

    /**
     * the column name for the idrolePersonne_client field
     */
    const COL_IDROLEPERSONNE_CLIENT = 'contrat.idrolePersonne_client';

    /**
     * the column name for the Personne_agent field
     */
    const COL_PERSONNE_AGENT = 'contrat.Personne_agent';

    /**
     * the column name for the idrolePersonne_agent field
     */
    const COL_IDROLEPERSONNE_AGENT = 'contrat.idrolePersonne_agent';

    /**
     * the column name for the DateCreationContrat field
     */
    const COL_DATECREATIONCONTRAT = 'contrat.DateCreationContrat';

    /**
     * the column name for the MontantContrat field
     */
    const COL_MONTANTCONTRAT = 'contrat.MontantContrat';

    /**
     * the column name for the ContratMontantPrime field
     */
    const COL_CONTRATMONTANTPRIME = 'contrat.ContratMontantPrime';

    /**
     * the column name for the produits_idproduits1 field
     */
    const COL_PRODUITS_IDPRODUITS1 = 'contrat.produits_idproduits1';

    /**
     * the column name for the produits_societe_idsociete field
     */
    const COL_PRODUITS_SOCIETE_IDSOCIETE = 'contrat.produits_societe_idsociete';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idcontrat', 'ContratQuantiteProduit', 'PersonneClient', 'IdrolepersonneClient', 'PersonneAgent', 'IdrolepersonneAgent', 'Datecreationcontrat', 'Montantcontrat', 'Contratmontantprime', 'ProduitsIdproduits1', 'ProduitsSocieteIdsociete', ),
        self::TYPE_CAMELNAME     => array('idcontrat', 'contratQuantiteProduit', 'personneClient', 'idrolepersonneClient', 'personneAgent', 'idrolepersonneAgent', 'datecreationcontrat', 'montantcontrat', 'contratmontantprime', 'produitsIdproduits1', 'produitsSocieteIdsociete', ),
        self::TYPE_COLNAME       => array(ContratTableMap::COL_IDCONTRAT, ContratTableMap::COL_CONTRAT_QUANTITE_PRODUIT, ContratTableMap::COL_PERSONNE_CLIENT, ContratTableMap::COL_IDROLEPERSONNE_CLIENT, ContratTableMap::COL_PERSONNE_AGENT, ContratTableMap::COL_IDROLEPERSONNE_AGENT, ContratTableMap::COL_DATECREATIONCONTRAT, ContratTableMap::COL_MONTANTCONTRAT, ContratTableMap::COL_CONTRATMONTANTPRIME, ContratTableMap::COL_PRODUITS_IDPRODUITS1, ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, ),
        self::TYPE_FIELDNAME     => array('idContrat', 'Contrat_quantite_produit', 'Personne_client', 'idrolePersonne_client', 'Personne_agent', 'idrolePersonne_agent', 'DateCreationContrat', 'MontantContrat', 'ContratMontantPrime', 'produits_idproduits1', 'produits_societe_idsociete', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idcontrat' => 0, 'ContratQuantiteProduit' => 1, 'PersonneClient' => 2, 'IdrolepersonneClient' => 3, 'PersonneAgent' => 4, 'IdrolepersonneAgent' => 5, 'Datecreationcontrat' => 6, 'Montantcontrat' => 7, 'Contratmontantprime' => 8, 'ProduitsIdproduits1' => 9, 'ProduitsSocieteIdsociete' => 10, ),
        self::TYPE_CAMELNAME     => array('idcontrat' => 0, 'contratQuantiteProduit' => 1, 'personneClient' => 2, 'idrolepersonneClient' => 3, 'personneAgent' => 4, 'idrolepersonneAgent' => 5, 'datecreationcontrat' => 6, 'montantcontrat' => 7, 'contratmontantprime' => 8, 'produitsIdproduits1' => 9, 'produitsSocieteIdsociete' => 10, ),
        self::TYPE_COLNAME       => array(ContratTableMap::COL_IDCONTRAT => 0, ContratTableMap::COL_CONTRAT_QUANTITE_PRODUIT => 1, ContratTableMap::COL_PERSONNE_CLIENT => 2, ContratTableMap::COL_IDROLEPERSONNE_CLIENT => 3, ContratTableMap::COL_PERSONNE_AGENT => 4, ContratTableMap::COL_IDROLEPERSONNE_AGENT => 5, ContratTableMap::COL_DATECREATIONCONTRAT => 6, ContratTableMap::COL_MONTANTCONTRAT => 7, ContratTableMap::COL_CONTRATMONTANTPRIME => 8, ContratTableMap::COL_PRODUITS_IDPRODUITS1 => 9, ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE => 10, ),
        self::TYPE_FIELDNAME     => array('idContrat' => 0, 'Contrat_quantite_produit' => 1, 'Personne_client' => 2, 'idrolePersonne_client' => 3, 'Personne_agent' => 4, 'idrolePersonne_agent' => 5, 'DateCreationContrat' => 6, 'MontantContrat' => 7, 'ContratMontantPrime' => 8, 'produits_idproduits1' => 9, 'produits_societe_idsociete' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('contrat');
        $this->setPhpName('Contrat');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Contrat');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idContrat', 'Idcontrat', 'INTEGER', true, null, null);
        $this->addColumn('Contrat_quantite_produit', 'ContratQuantiteProduit', 'INTEGER', false, null, null);
        $this->addForeignKey('Personne_client', 'PersonneClient', 'INTEGER', 'personne', 'idPersonne', true, null, null);
        $this->addForeignKey('idrolePersonne_client', 'IdrolepersonneClient', 'INTEGER', 'personne', 'rolePersonne_idrolePersonne', true, null, null);
        $this->addForeignKey('Personne_agent', 'PersonneAgent', 'INTEGER', 'personne', 'idPersonne', true, null, null);
        $this->addForeignKey('idrolePersonne_agent', 'IdrolepersonneAgent', 'INTEGER', 'personne', 'rolePersonne_idrolePersonne', true, null, null);
        $this->addColumn('DateCreationContrat', 'Datecreationcontrat', 'DATE', false, null, null);
        $this->addColumn('MontantContrat', 'Montantcontrat', 'INTEGER', false, null, null);
        $this->addColumn('ContratMontantPrime', 'Contratmontantprime', 'INTEGER', false, null, null);
        $this->addForeignPrimaryKey('produits_idproduits1', 'ProduitsIdproduits1', 'INTEGER' , 'produits', 'idproduits', true, null, null);
        $this->addForeignPrimaryKey('produits_societe_idsociete', 'ProduitsSocieteIdsociete', 'INTEGER' , 'produits', 'societe_idsociete', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Produits', '\\Produits', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':produits_idproduits1',
    1 => ':idproduits',
  ),
  1 =>
  array (
    0 => ':produits_societe_idsociete',
    1 => ':societe_idsociete',
  ),
), null, null, null, false);
        $this->addRelation('PersonneRelatedByPersonneClientIdrolepersonneClient', '\\Personne', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':Personne_client',
    1 => ':idPersonne',
  ),
  1 =>
  array (
    0 => ':idrolePersonne_client',
    1 => ':rolePersonne_idrolePersonne',
  ),
), null, null, null, false);
        $this->addRelation('PersonneRelatedByPersonneAgentIdrolepersonneAgent', '\\Personne', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':Personne_agent',
    1 => ':idPersonne',
  ),
  1 =>
  array (
    0 => ':idrolePersonne_agent',
    1 => ':rolePersonne_idrolePersonne',
  ),
), null, null, null, false);
        $this->addRelation('Versement', '\\Versement', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':Contrat_idContrat',
    1 => ':idContrat',
  ),
  1 =>
  array (
    0 => ':Contrat_produits_idproduits',
    1 => ':produits_idproduits1',
  ),
  2 =>
  array (
    0 => ':Contrat_produits_societe_idsociete',
    1 => ':produits_societe_idsociete',
  ),
), null, null, 'Versements', false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Contrat $obj A \Contrat object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize([(null === $obj->getIdcontrat() || is_scalar($obj->getIdcontrat()) || is_callable([$obj->getIdcontrat(), '__toString']) ? (string) $obj->getIdcontrat() : $obj->getIdcontrat()), (null === $obj->getProduitsIdproduits1() || is_scalar($obj->getProduitsIdproduits1()) || is_callable([$obj->getProduitsIdproduits1(), '__toString']) ? (string) $obj->getProduitsIdproduits1() : $obj->getProduitsIdproduits1()), (null === $obj->getProduitsSocieteIdsociete() || is_scalar($obj->getProduitsSocieteIdsociete()) || is_callable([$obj->getProduitsSocieteIdsociete(), '__toString']) ? (string) $obj->getProduitsSocieteIdsociete() : $obj->getProduitsSocieteIdsociete())]);
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Contrat object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Contrat) {
                $key = serialize([(null === $value->getIdcontrat() || is_scalar($value->getIdcontrat()) || is_callable([$value->getIdcontrat(), '__toString']) ? (string) $value->getIdcontrat() : $value->getIdcontrat()), (null === $value->getProduitsIdproduits1() || is_scalar($value->getProduitsIdproduits1()) || is_callable([$value->getProduitsIdproduits1(), '__toString']) ? (string) $value->getProduitsIdproduits1() : $value->getProduitsIdproduits1()), (null === $value->getProduitsSocieteIdsociete() || is_scalar($value->getProduitsSocieteIdsociete()) || is_callable([$value->getProduitsSocieteIdsociete(), '__toString']) ? (string) $value->getProduitsSocieteIdsociete() : $value->getProduitsSocieteIdsociete())]);

            } elseif (is_array($value) && count($value) === 3) {
                // assume we've been passed a primary key";
                $key = serialize([(null === $value[0] || is_scalar($value[0]) || is_callable([$value[0], '__toString']) ? (string) $value[0] : $value[0]), (null === $value[1] || is_scalar($value[1]) || is_callable([$value[1], '__toString']) ? (string) $value[1] : $value[1]), (null === $value[2] || is_scalar($value[2]) || is_callable([$value[2], '__toString']) ? (string) $value[2] : $value[2])]);
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Contrat object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrat', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('ProduitsIdproduits1', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize([(null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrat', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrat', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrat', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrat', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcontrat', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('ProduitsIdproduits1', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('ProduitsIdproduits1', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('ProduitsIdproduits1', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('ProduitsIdproduits1', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 9 + $offset : static::translateFieldName('ProduitsIdproduits1', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 10 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)])]);
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idcontrat', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 9 + $offset
                : self::translateFieldName('ProduitsIdproduits1', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 10 + $offset
                : self::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ContratTableMap::CLASS_DEFAULT : ContratTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Contrat object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ContratTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ContratTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ContratTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ContratTableMap::OM_CLASS;
            /** @var Contrat $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ContratTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ContratTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ContratTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Contrat $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ContratTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ContratTableMap::COL_IDCONTRAT);
            $criteria->addSelectColumn(ContratTableMap::COL_CONTRAT_QUANTITE_PRODUIT);
            $criteria->addSelectColumn(ContratTableMap::COL_PERSONNE_CLIENT);
            $criteria->addSelectColumn(ContratTableMap::COL_IDROLEPERSONNE_CLIENT);
            $criteria->addSelectColumn(ContratTableMap::COL_PERSONNE_AGENT);
            $criteria->addSelectColumn(ContratTableMap::COL_IDROLEPERSONNE_AGENT);
            $criteria->addSelectColumn(ContratTableMap::COL_DATECREATIONCONTRAT);
            $criteria->addSelectColumn(ContratTableMap::COL_MONTANTCONTRAT);
            $criteria->addSelectColumn(ContratTableMap::COL_CONTRATMONTANTPRIME);
            $criteria->addSelectColumn(ContratTableMap::COL_PRODUITS_IDPRODUITS1);
            $criteria->addSelectColumn(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE);
        } else {
            $criteria->addSelectColumn($alias . '.idContrat');
            $criteria->addSelectColumn($alias . '.Contrat_quantite_produit');
            $criteria->addSelectColumn($alias . '.Personne_client');
            $criteria->addSelectColumn($alias . '.idrolePersonne_client');
            $criteria->addSelectColumn($alias . '.Personne_agent');
            $criteria->addSelectColumn($alias . '.idrolePersonne_agent');
            $criteria->addSelectColumn($alias . '.DateCreationContrat');
            $criteria->addSelectColumn($alias . '.MontantContrat');
            $criteria->addSelectColumn($alias . '.ContratMontantPrime');
            $criteria->addSelectColumn($alias . '.produits_idproduits1');
            $criteria->addSelectColumn($alias . '.produits_societe_idsociete');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ContratTableMap::DATABASE_NAME)->getTable(ContratTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ContratTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ContratTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ContratTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Contrat or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Contrat object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Contrat) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ContratTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(ContratTableMap::COL_IDCONTRAT, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(ContratTableMap::COL_PRODUITS_IDPRODUITS1, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(ContratTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $value[2]));
                $criteria->addOr($criterion);
            }
        }

        $query = ContratQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ContratTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ContratTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the contrat table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ContratQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Contrat or Criteria object.
     *
     * @param mixed               $criteria Criteria or Contrat object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContratTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Contrat object
        }

        if ($criteria->containsKey(ContratTableMap::COL_IDCONTRAT) && $criteria->keyContainsValue(ContratTableMap::COL_IDCONTRAT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ContratTableMap::COL_IDCONTRAT.')');
        }


        // Set the correct dbName
        $query = ContratQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ContratTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ContratTableMap::buildTableMap();
