<?php

namespace Map;

use \Produits;
use \ProduitsQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'produits' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ProduitsTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ProduitsTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'produits';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Produits';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Produits';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the idproduits field
     */
    const COL_IDPRODUITS = 'produits.idproduits';

    /**
     * the column name for the Libelleproduits field
     */
    const COL_LIBELLEPRODUITS = 'produits.Libelleproduits';

    /**
     * the column name for the idcatgorieproduit field
     */
    const COL_IDCATGORIEPRODUIT = 'produits.idcatgorieproduit';

    /**
     * the column name for the Montantproduits field
     */
    const COL_MONTANTPRODUITS = 'produits.Montantproduits';

    /**
     * the column name for the societe_idsociete field
     */
    const COL_SOCIETE_IDSOCIETE = 'produits.societe_idsociete';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idproduits', 'Libelleproduits', 'Idcatgorieproduit', 'Montantproduits', 'SocieteIdsociete', ),
        self::TYPE_CAMELNAME     => array('idproduits', 'libelleproduits', 'idcatgorieproduit', 'montantproduits', 'societeIdsociete', ),
        self::TYPE_COLNAME       => array(ProduitsTableMap::COL_IDPRODUITS, ProduitsTableMap::COL_LIBELLEPRODUITS, ProduitsTableMap::COL_IDCATGORIEPRODUIT, ProduitsTableMap::COL_MONTANTPRODUITS, ProduitsTableMap::COL_SOCIETE_IDSOCIETE, ),
        self::TYPE_FIELDNAME     => array('idproduits', 'Libelleproduits', 'idcatgorieproduit', 'Montantproduits', 'societe_idsociete', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idproduits' => 0, 'Libelleproduits' => 1, 'Idcatgorieproduit' => 2, 'Montantproduits' => 3, 'SocieteIdsociete' => 4, ),
        self::TYPE_CAMELNAME     => array('idproduits' => 0, 'libelleproduits' => 1, 'idcatgorieproduit' => 2, 'montantproduits' => 3, 'societeIdsociete' => 4, ),
        self::TYPE_COLNAME       => array(ProduitsTableMap::COL_IDPRODUITS => 0, ProduitsTableMap::COL_LIBELLEPRODUITS => 1, ProduitsTableMap::COL_IDCATGORIEPRODUIT => 2, ProduitsTableMap::COL_MONTANTPRODUITS => 3, ProduitsTableMap::COL_SOCIETE_IDSOCIETE => 4, ),
        self::TYPE_FIELDNAME     => array('idproduits' => 0, 'Libelleproduits' => 1, 'idcatgorieproduit' => 2, 'Montantproduits' => 3, 'societe_idsociete' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('produits');
        $this->setPhpName('Produits');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Produits');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idproduits', 'Idproduits', 'INTEGER', true, null, null);
        $this->addColumn('Libelleproduits', 'Libelleproduits', 'VARCHAR', false, 145, null);
        $this->addForeignKey('idcatgorieproduit', 'Idcatgorieproduit', 'INTEGER', 'catgorieproduit', 'idcatgorieproduit', true, null, null);
        $this->addColumn('Montantproduits', 'Montantproduits', 'INTEGER', false, null, null);
        $this->addForeignPrimaryKey('societe_idsociete', 'SocieteIdsociete', 'INTEGER' , 'societe', 'idsociete', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Catgorieproduit', '\\Catgorieproduit', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':idcatgorieproduit',
    1 => ':idcatgorieproduit',
  ),
), null, null, null, false);
        $this->addRelation('Societe', '\\Societe', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':societe_idsociete',
    1 => ':idsociete',
  ),
), null, null, null, false);
        $this->addRelation('Contrat', '\\Contrat', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produits_idproduits1',
    1 => ':idproduits',
  ),
  1 =>
  array (
    0 => ':produits_societe_idsociete',
    1 => ':societe_idsociete',
  ),
), null, null, 'Contrats', false);
        $this->addRelation('Prime', '\\Prime', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produits_idproduits',
    1 => ':idproduits',
  ),
  1 =>
  array (
    0 => ':produits_societe_idsociete',
    1 => ':societe_idsociete',
  ),
), null, null, 'Primes', false);
        $this->addRelation('Prospection', '\\Prospection', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produits_idproduits',
    1 => ':idproduits',
  ),
  1 =>
  array (
    0 => ':produits_societe_idsociete',
    1 => ':societe_idsociete',
  ),
), null, null, 'Prospections', false);
        $this->addRelation('Vente', '\\Vente', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':produits_idproduits',
    1 => ':idproduits',
  ),
  1 =>
  array (
    0 => ':produits_societe_idsociete',
    1 => ':societe_idsociete',
  ),
), null, null, 'Ventes', false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Produits $obj A \Produits object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize([(null === $obj->getIdproduits() || is_scalar($obj->getIdproduits()) || is_callable([$obj->getIdproduits(), '__toString']) ? (string) $obj->getIdproduits() : $obj->getIdproduits()), (null === $obj->getSocieteIdsociete() || is_scalar($obj->getSocieteIdsociete()) || is_callable([$obj->getSocieteIdsociete(), '__toString']) ? (string) $obj->getSocieteIdsociete() : $obj->getSocieteIdsociete())]);
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Produits object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Produits) {
                $key = serialize([(null === $value->getIdproduits() || is_scalar($value->getIdproduits()) || is_callable([$value->getIdproduits(), '__toString']) ? (string) $value->getIdproduits() : $value->getIdproduits()), (null === $value->getSocieteIdsociete() || is_scalar($value->getSocieteIdsociete()) || is_callable([$value->getSocieteIdsociete(), '__toString']) ? (string) $value->getSocieteIdsociete() : $value->getSocieteIdsociete())]);

            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key";
                $key = serialize([(null === $value[0] || is_scalar($value[0]) || is_callable([$value[0], '__toString']) ? (string) $value[0] : $value[0]), (null === $value[1] || is_scalar($value[1]) || is_callable([$value[1], '__toString']) ? (string) $value[1] : $value[1])]);
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Produits object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idproduits', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize([(null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idproduits', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idproduits', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idproduits', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idproduits', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idproduits', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)])]);
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idproduits', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 4 + $offset
                : self::translateFieldName('SocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProduitsTableMap::CLASS_DEFAULT : ProduitsTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Produits object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProduitsTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProduitsTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProduitsTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProduitsTableMap::OM_CLASS;
            /** @var Produits $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProduitsTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProduitsTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProduitsTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Produits $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProduitsTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProduitsTableMap::COL_IDPRODUITS);
            $criteria->addSelectColumn(ProduitsTableMap::COL_LIBELLEPRODUITS);
            $criteria->addSelectColumn(ProduitsTableMap::COL_IDCATGORIEPRODUIT);
            $criteria->addSelectColumn(ProduitsTableMap::COL_MONTANTPRODUITS);
            $criteria->addSelectColumn(ProduitsTableMap::COL_SOCIETE_IDSOCIETE);
        } else {
            $criteria->addSelectColumn($alias . '.idproduits');
            $criteria->addSelectColumn($alias . '.Libelleproduits');
            $criteria->addSelectColumn($alias . '.idcatgorieproduit');
            $criteria->addSelectColumn($alias . '.Montantproduits');
            $criteria->addSelectColumn($alias . '.societe_idsociete');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProduitsTableMap::DATABASE_NAME)->getTable(ProduitsTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProduitsTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProduitsTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProduitsTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Produits or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Produits object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduitsTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Produits) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProduitsTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(ProduitsTableMap::COL_IDPRODUITS, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(ProduitsTableMap::COL_SOCIETE_IDSOCIETE, $value[1]));
                $criteria->addOr($criterion);
            }
        }

        $query = ProduitsQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProduitsTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProduitsTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the produits table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProduitsQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Produits or Criteria object.
     *
     * @param mixed               $criteria Criteria or Produits object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProduitsTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Produits object
        }

        if ($criteria->containsKey(ProduitsTableMap::COL_IDPRODUITS) && $criteria->keyContainsValue(ProduitsTableMap::COL_IDPRODUITS) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProduitsTableMap::COL_IDPRODUITS.')');
        }


        // Set the correct dbName
        $query = ProduitsQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProduitsTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProduitsTableMap::buildTableMap();
