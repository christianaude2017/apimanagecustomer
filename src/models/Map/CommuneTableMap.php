<?php

namespace Map;

use \Commune;
use \CommuneQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'commune' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class CommuneTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.CommuneTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'commune';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Commune';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Commune';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the idcommune field
     */
    const COL_IDCOMMUNE = 'commune.idcommune';

    /**
     * the column name for the Libellecommune field
     */
    const COL_LIBELLECOMMUNE = 'commune.Libellecommune';

    /**
     * the column name for the ville_idville field
     */
    const COL_VILLE_IDVILLE = 'commune.ville_idville';

    /**
     * the column name for the ville_pays_idpays field
     */
    const COL_VILLE_PAYS_IDPAYS = 'commune.ville_pays_idpays';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idcommune', 'Libellecommune', 'VilleIdville', 'VillePaysIdpays', ),
        self::TYPE_CAMELNAME     => array('idcommune', 'libellecommune', 'villeIdville', 'villePaysIdpays', ),
        self::TYPE_COLNAME       => array(CommuneTableMap::COL_IDCOMMUNE, CommuneTableMap::COL_LIBELLECOMMUNE, CommuneTableMap::COL_VILLE_IDVILLE, CommuneTableMap::COL_VILLE_PAYS_IDPAYS, ),
        self::TYPE_FIELDNAME     => array('idcommune', 'Libellecommune', 'ville_idville', 'ville_pays_idpays', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idcommune' => 0, 'Libellecommune' => 1, 'VilleIdville' => 2, 'VillePaysIdpays' => 3, ),
        self::TYPE_CAMELNAME     => array('idcommune' => 0, 'libellecommune' => 1, 'villeIdville' => 2, 'villePaysIdpays' => 3, ),
        self::TYPE_COLNAME       => array(CommuneTableMap::COL_IDCOMMUNE => 0, CommuneTableMap::COL_LIBELLECOMMUNE => 1, CommuneTableMap::COL_VILLE_IDVILLE => 2, CommuneTableMap::COL_VILLE_PAYS_IDPAYS => 3, ),
        self::TYPE_FIELDNAME     => array('idcommune' => 0, 'Libellecommune' => 1, 'ville_idville' => 2, 'ville_pays_idpays' => 3, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('commune');
        $this->setPhpName('Commune');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Commune');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idcommune', 'Idcommune', 'INTEGER', true, null, null);
        $this->addColumn('Libellecommune', 'Libellecommune', 'VARCHAR', false, 45, null);
        $this->addForeignKey('ville_idville', 'VilleIdville', 'INTEGER', 'ville', 'idville', true, null, null);
        $this->addForeignKey('ville_pays_idpays', 'VillePaysIdpays', 'INTEGER', 'ville', 'pays_idpays', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Ville', '\\Ville', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ville_idville',
    1 => ':idville',
  ),
  1 =>
  array (
    0 => ':ville_pays_idpays',
    1 => ':pays_idpays',
  ),
), null, null, null, false);
        $this->addRelation('Personne', '\\Personne', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':commune_idcommune',
    1 => ':idcommune',
  ),
), null, null, 'Personnes', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcommune', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcommune', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcommune', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcommune', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcommune', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idcommune', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idcommune', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CommuneTableMap::CLASS_DEFAULT : CommuneTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Commune object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CommuneTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CommuneTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CommuneTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommuneTableMap::OM_CLASS;
            /** @var Commune $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CommuneTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CommuneTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CommuneTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Commune $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommuneTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommuneTableMap::COL_IDCOMMUNE);
            $criteria->addSelectColumn(CommuneTableMap::COL_LIBELLECOMMUNE);
            $criteria->addSelectColumn(CommuneTableMap::COL_VILLE_IDVILLE);
            $criteria->addSelectColumn(CommuneTableMap::COL_VILLE_PAYS_IDPAYS);
        } else {
            $criteria->addSelectColumn($alias . '.idcommune');
            $criteria->addSelectColumn($alias . '.Libellecommune');
            $criteria->addSelectColumn($alias . '.ville_idville');
            $criteria->addSelectColumn($alias . '.ville_pays_idpays');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CommuneTableMap::DATABASE_NAME)->getTable(CommuneTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CommuneTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CommuneTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CommuneTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Commune or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Commune object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Commune) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommuneTableMap::DATABASE_NAME);
            $criteria->add(CommuneTableMap::COL_IDCOMMUNE, (array) $values, Criteria::IN);
        }

        $query = CommuneQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CommuneTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CommuneTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the commune table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CommuneQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Commune or Criteria object.
     *
     * @param mixed               $criteria Criteria or Commune object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CommuneTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Commune object
        }

        if ($criteria->containsKey(CommuneTableMap::COL_IDCOMMUNE) && $criteria->keyContainsValue(CommuneTableMap::COL_IDCOMMUNE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommuneTableMap::COL_IDCOMMUNE.')');
        }


        // Set the correct dbName
        $query = CommuneQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CommuneTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CommuneTableMap::buildTableMap();
