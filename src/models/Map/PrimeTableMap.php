<?php

namespace Map;

use \Prime;
use \PrimeQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'prime' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class PrimeTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PrimeTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'prime';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Prime';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Prime';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the idprime field
     */
    const COL_IDPRIME = 'prime.idprime';

    /**
     * the column name for the tauxprime field
     */
    const COL_TAUXPRIME = 'prime.tauxprime';

    /**
     * the column name for the produits_idproduits field
     */
    const COL_PRODUITS_IDPRODUITS = 'prime.produits_idproduits';

    /**
     * the column name for the produits_societe_idsociete field
     */
    const COL_PRODUITS_SOCIETE_IDSOCIETE = 'prime.produits_societe_idsociete';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idprime', 'Tauxprime', 'ProduitsIdproduits', 'ProduitsSocieteIdsociete', ),
        self::TYPE_CAMELNAME     => array('idprime', 'tauxprime', 'produitsIdproduits', 'produitsSocieteIdsociete', ),
        self::TYPE_COLNAME       => array(PrimeTableMap::COL_IDPRIME, PrimeTableMap::COL_TAUXPRIME, PrimeTableMap::COL_PRODUITS_IDPRODUITS, PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, ),
        self::TYPE_FIELDNAME     => array('idprime', 'tauxprime', 'produits_idproduits', 'produits_societe_idsociete', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idprime' => 0, 'Tauxprime' => 1, 'ProduitsIdproduits' => 2, 'ProduitsSocieteIdsociete' => 3, ),
        self::TYPE_CAMELNAME     => array('idprime' => 0, 'tauxprime' => 1, 'produitsIdproduits' => 2, 'produitsSocieteIdsociete' => 3, ),
        self::TYPE_COLNAME       => array(PrimeTableMap::COL_IDPRIME => 0, PrimeTableMap::COL_TAUXPRIME => 1, PrimeTableMap::COL_PRODUITS_IDPRODUITS => 2, PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE => 3, ),
        self::TYPE_FIELDNAME     => array('idprime' => 0, 'tauxprime' => 1, 'produits_idproduits' => 2, 'produits_societe_idsociete' => 3, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('prime');
        $this->setPhpName('Prime');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Prime');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idprime', 'Idprime', 'INTEGER', true, null, null);
        $this->addColumn('tauxprime', 'Tauxprime', 'INTEGER', false, null, null);
        $this->addForeignPrimaryKey('produits_idproduits', 'ProduitsIdproduits', 'INTEGER' , 'produits', 'idproduits', true, null, null);
        $this->addForeignPrimaryKey('produits_societe_idsociete', 'ProduitsSocieteIdsociete', 'INTEGER' , 'produits', 'societe_idsociete', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Produits', '\\Produits', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':produits_idproduits',
    1 => ':idproduits',
  ),
  1 =>
  array (
    0 => ':produits_societe_idsociete',
    1 => ':societe_idsociete',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Prime $obj A \Prime object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize([(null === $obj->getIdprime() || is_scalar($obj->getIdprime()) || is_callable([$obj->getIdprime(), '__toString']) ? (string) $obj->getIdprime() : $obj->getIdprime()), (null === $obj->getProduitsIdproduits() || is_scalar($obj->getProduitsIdproduits()) || is_callable([$obj->getProduitsIdproduits(), '__toString']) ? (string) $obj->getProduitsIdproduits() : $obj->getProduitsIdproduits()), (null === $obj->getProduitsSocieteIdsociete() || is_scalar($obj->getProduitsSocieteIdsociete()) || is_callable([$obj->getProduitsSocieteIdsociete(), '__toString']) ? (string) $obj->getProduitsSocieteIdsociete() : $obj->getProduitsSocieteIdsociete())]);
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Prime object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Prime) {
                $key = serialize([(null === $value->getIdprime() || is_scalar($value->getIdprime()) || is_callable([$value->getIdprime(), '__toString']) ? (string) $value->getIdprime() : $value->getIdprime()), (null === $value->getProduitsIdproduits() || is_scalar($value->getProduitsIdproduits()) || is_callable([$value->getProduitsIdproduits(), '__toString']) ? (string) $value->getProduitsIdproduits() : $value->getProduitsIdproduits()), (null === $value->getProduitsSocieteIdsociete() || is_scalar($value->getProduitsSocieteIdsociete()) || is_callable([$value->getProduitsSocieteIdsociete(), '__toString']) ? (string) $value->getProduitsSocieteIdsociete() : $value->getProduitsSocieteIdsociete())]);

            } elseif (is_array($value) && count($value) === 3) {
                // assume we've been passed a primary key";
                $key = serialize([(null === $value[0] || is_scalar($value[0]) || is_callable([$value[0], '__toString']) ? (string) $value[0] : $value[0]), (null === $value[1] || is_scalar($value[1]) || is_callable([$value[1], '__toString']) ? (string) $value[1] : $value[1]), (null === $value[2] || is_scalar($value[2]) || is_callable([$value[2], '__toString']) ? (string) $value[2] : $value[2])]);
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Prime object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprime', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize([(null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprime', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprime', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprime', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprime', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idprime', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 2 + $offset : static::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 3 + $offset : static::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)])]);
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idprime', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 2 + $offset
                : self::translateFieldName('ProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 3 + $offset
                : self::translateFieldName('ProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PrimeTableMap::CLASS_DEFAULT : PrimeTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Prime object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PrimeTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PrimeTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PrimeTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PrimeTableMap::OM_CLASS;
            /** @var Prime $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PrimeTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PrimeTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PrimeTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Prime $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PrimeTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PrimeTableMap::COL_IDPRIME);
            $criteria->addSelectColumn(PrimeTableMap::COL_TAUXPRIME);
            $criteria->addSelectColumn(PrimeTableMap::COL_PRODUITS_IDPRODUITS);
            $criteria->addSelectColumn(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE);
        } else {
            $criteria->addSelectColumn($alias . '.idprime');
            $criteria->addSelectColumn($alias . '.tauxprime');
            $criteria->addSelectColumn($alias . '.produits_idproduits');
            $criteria->addSelectColumn($alias . '.produits_societe_idsociete');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PrimeTableMap::DATABASE_NAME)->getTable(PrimeTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PrimeTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PrimeTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PrimeTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Prime or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Prime object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrimeTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Prime) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PrimeTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(PrimeTableMap::COL_IDPRIME, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(PrimeTableMap::COL_PRODUITS_IDPRODUITS, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(PrimeTableMap::COL_PRODUITS_SOCIETE_IDSOCIETE, $value[2]));
                $criteria->addOr($criterion);
            }
        }

        $query = PrimeQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PrimeTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PrimeTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the prime table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PrimeQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Prime or Criteria object.
     *
     * @param mixed               $criteria Criteria or Prime object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PrimeTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Prime object
        }

        if ($criteria->containsKey(PrimeTableMap::COL_IDPRIME) && $criteria->keyContainsValue(PrimeTableMap::COL_IDPRIME) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PrimeTableMap::COL_IDPRIME.')');
        }


        // Set the correct dbName
        $query = PrimeQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PrimeTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PrimeTableMap::buildTableMap();
