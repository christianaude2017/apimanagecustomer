<?php

namespace Map;

use \Versement;
use \VersementQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'versement' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class VersementTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.VersementTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'versement';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Versement';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Versement';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the idversement field
     */
    const COL_IDVERSEMENT = 'versement.idversement';

    /**
     * the column name for the dateVersement field
     */
    const COL_DATEVERSEMENT = 'versement.dateVersement';

    /**
     * the column name for the montantversement field
     */
    const COL_MONTANTVERSEMENT = 'versement.montantversement';

    /**
     * the column name for the NumeroCheque field
     */
    const COL_NUMEROCHEQUE = 'versement.NumeroCheque';

    /**
     * the column name for the versement_idmois field
     */
    const COL_VERSEMENT_IDMOIS = 'versement.versement_idmois';

    /**
     * the column name for the Contrat_idContrat field
     */
    const COL_CONTRAT_IDCONTRAT = 'versement.Contrat_idContrat';

    /**
     * the column name for the Contrat_produits_idproduits field
     */
    const COL_CONTRAT_PRODUITS_IDPRODUITS = 'versement.Contrat_produits_idproduits';

    /**
     * the column name for the Contrat_produits_societe_idsociete field
     */
    const COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE = 'versement.Contrat_produits_societe_idsociete';

    /**
     * the column name for the modePaiement_idmodePaiement field
     */
    const COL_MODEPAIEMENT_IDMODEPAIEMENT = 'versement.modePaiement_idmodePaiement';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Idversement', 'Dateversement', 'Montantversement', 'Numerocheque', 'VersementIdmois', 'ContratIdcontrat', 'ContratProduitsIdproduits', 'ContratProduitsSocieteIdsociete', 'ModepaiementIdmodepaiement', ),
        self::TYPE_CAMELNAME     => array('idversement', 'dateversement', 'montantversement', 'numerocheque', 'versementIdmois', 'contratIdcontrat', 'contratProduitsIdproduits', 'contratProduitsSocieteIdsociete', 'modepaiementIdmodepaiement', ),
        self::TYPE_COLNAME       => array(VersementTableMap::COL_IDVERSEMENT, VersementTableMap::COL_DATEVERSEMENT, VersementTableMap::COL_MONTANTVERSEMENT, VersementTableMap::COL_NUMEROCHEQUE, VersementTableMap::COL_VERSEMENT_IDMOIS, VersementTableMap::COL_CONTRAT_IDCONTRAT, VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT, ),
        self::TYPE_FIELDNAME     => array('idversement', 'dateVersement', 'montantversement', 'NumeroCheque', 'versement_idmois', 'Contrat_idContrat', 'Contrat_produits_idproduits', 'Contrat_produits_societe_idsociete', 'modePaiement_idmodePaiement', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Idversement' => 0, 'Dateversement' => 1, 'Montantversement' => 2, 'Numerocheque' => 3, 'VersementIdmois' => 4, 'ContratIdcontrat' => 5, 'ContratProduitsIdproduits' => 6, 'ContratProduitsSocieteIdsociete' => 7, 'ModepaiementIdmodepaiement' => 8, ),
        self::TYPE_CAMELNAME     => array('idversement' => 0, 'dateversement' => 1, 'montantversement' => 2, 'numerocheque' => 3, 'versementIdmois' => 4, 'contratIdcontrat' => 5, 'contratProduitsIdproduits' => 6, 'contratProduitsSocieteIdsociete' => 7, 'modepaiementIdmodepaiement' => 8, ),
        self::TYPE_COLNAME       => array(VersementTableMap::COL_IDVERSEMENT => 0, VersementTableMap::COL_DATEVERSEMENT => 1, VersementTableMap::COL_MONTANTVERSEMENT => 2, VersementTableMap::COL_NUMEROCHEQUE => 3, VersementTableMap::COL_VERSEMENT_IDMOIS => 4, VersementTableMap::COL_CONTRAT_IDCONTRAT => 5, VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS => 6, VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE => 7, VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT => 8, ),
        self::TYPE_FIELDNAME     => array('idversement' => 0, 'dateVersement' => 1, 'montantversement' => 2, 'NumeroCheque' => 3, 'versement_idmois' => 4, 'Contrat_idContrat' => 5, 'Contrat_produits_idproduits' => 6, 'Contrat_produits_societe_idsociete' => 7, 'modePaiement_idmodePaiement' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('versement');
        $this->setPhpName('Versement');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Versement');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('idversement', 'Idversement', 'INTEGER', true, null, null);
        $this->addColumn('dateVersement', 'Dateversement', 'DATE', false, null, null);
        $this->addColumn('montantversement', 'Montantversement', 'INTEGER', false, null, null);
        $this->addColumn('NumeroCheque', 'Numerocheque', 'VARCHAR', false, 45, null);
        $this->addForeignPrimaryKey('versement_idmois', 'VersementIdmois', 'INTEGER' , 'mois', 'idmois', true, null, null);
        $this->addForeignPrimaryKey('Contrat_idContrat', 'ContratIdcontrat', 'INTEGER' , 'contrat', 'idContrat', true, null, null);
        $this->addForeignPrimaryKey('Contrat_produits_idproduits', 'ContratProduitsIdproduits', 'INTEGER' , 'contrat', 'produits_idproduits1', true, null, null);
        $this->addForeignPrimaryKey('Contrat_produits_societe_idsociete', 'ContratProduitsSocieteIdsociete', 'INTEGER' , 'contrat', 'produits_societe_idsociete', true, null, null);
        $this->addForeignKey('modePaiement_idmodePaiement', 'ModepaiementIdmodepaiement', 'INTEGER', 'modepaiement', 'idmodePaiement', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Contrat', '\\Contrat', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':Contrat_idContrat',
    1 => ':idContrat',
  ),
  1 =>
  array (
    0 => ':Contrat_produits_idproduits',
    1 => ':produits_idproduits1',
  ),
  2 =>
  array (
    0 => ':Contrat_produits_societe_idsociete',
    1 => ':produits_societe_idsociete',
  ),
), null, null, null, false);
        $this->addRelation('Modepaiement', '\\Modepaiement', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':modePaiement_idmodePaiement',
    1 => ':idmodePaiement',
  ),
), null, null, null, false);
        $this->addRelation('Mois', '\\Mois', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':versement_idmois',
    1 => ':idmois',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \Versement $obj A \Versement object.
     * @param string $key             (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize([(null === $obj->getIdversement() || is_scalar($obj->getIdversement()) || is_callable([$obj->getIdversement(), '__toString']) ? (string) $obj->getIdversement() : $obj->getIdversement()), (null === $obj->getVersementIdmois() || is_scalar($obj->getVersementIdmois()) || is_callable([$obj->getVersementIdmois(), '__toString']) ? (string) $obj->getVersementIdmois() : $obj->getVersementIdmois()), (null === $obj->getContratIdcontrat() || is_scalar($obj->getContratIdcontrat()) || is_callable([$obj->getContratIdcontrat(), '__toString']) ? (string) $obj->getContratIdcontrat() : $obj->getContratIdcontrat()), (null === $obj->getContratProduitsIdproduits() || is_scalar($obj->getContratProduitsIdproduits()) || is_callable([$obj->getContratProduitsIdproduits(), '__toString']) ? (string) $obj->getContratProduitsIdproduits() : $obj->getContratProduitsIdproduits()), (null === $obj->getContratProduitsSocieteIdsociete() || is_scalar($obj->getContratProduitsSocieteIdsociete()) || is_callable([$obj->getContratProduitsSocieteIdsociete(), '__toString']) ? (string) $obj->getContratProduitsSocieteIdsociete() : $obj->getContratProduitsSocieteIdsociete())]);
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \Versement object or a primary key value.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \Versement) {
                $key = serialize([(null === $value->getIdversement() || is_scalar($value->getIdversement()) || is_callable([$value->getIdversement(), '__toString']) ? (string) $value->getIdversement() : $value->getIdversement()), (null === $value->getVersementIdmois() || is_scalar($value->getVersementIdmois()) || is_callable([$value->getVersementIdmois(), '__toString']) ? (string) $value->getVersementIdmois() : $value->getVersementIdmois()), (null === $value->getContratIdcontrat() || is_scalar($value->getContratIdcontrat()) || is_callable([$value->getContratIdcontrat(), '__toString']) ? (string) $value->getContratIdcontrat() : $value->getContratIdcontrat()), (null === $value->getContratProduitsIdproduits() || is_scalar($value->getContratProduitsIdproduits()) || is_callable([$value->getContratProduitsIdproduits(), '__toString']) ? (string) $value->getContratProduitsIdproduits() : $value->getContratProduitsIdproduits()), (null === $value->getContratProduitsSocieteIdsociete() || is_scalar($value->getContratProduitsSocieteIdsociete()) || is_callable([$value->getContratProduitsSocieteIdsociete(), '__toString']) ? (string) $value->getContratProduitsSocieteIdsociete() : $value->getContratProduitsSocieteIdsociete())]);

            } elseif (is_array($value) && count($value) === 5) {
                // assume we've been passed a primary key";
                $key = serialize([(null === $value[0] || is_scalar($value[0]) || is_callable([$value[0], '__toString']) ? (string) $value[0] : $value[0]), (null === $value[1] || is_scalar($value[1]) || is_callable([$value[1], '__toString']) ? (string) $value[1] : $value[1]), (null === $value[2] || is_scalar($value[2]) || is_callable([$value[2], '__toString']) ? (string) $value[2] : $value[2]), (null === $value[3] || is_scalar($value[3]) || is_callable([$value[3], '__toString']) ? (string) $value[3] : $value[3]), (null === $value[4] || is_scalar($value[4]) || is_callable([$value[4], '__toString']) ? (string) $value[4] : $value[4])]);
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \Versement object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idversement', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('VersementIdmois', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 5 + $offset : static::translateFieldName('ContratIdcontrat', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 6 + $offset : static::translateFieldName('ContratProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 7 + $offset : static::translateFieldName('ContratProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize([(null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idversement', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idversement', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idversement', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idversement', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idversement', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('VersementIdmois', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('VersementIdmois', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('VersementIdmois', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('VersementIdmois', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 4 + $offset : static::translateFieldName('VersementIdmois', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 5 + $offset : static::translateFieldName('ContratIdcontrat', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 5 + $offset : static::translateFieldName('ContratIdcontrat', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 5 + $offset : static::translateFieldName('ContratIdcontrat', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 5 + $offset : static::translateFieldName('ContratIdcontrat', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 5 + $offset : static::translateFieldName('ContratIdcontrat', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 6 + $offset : static::translateFieldName('ContratProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 6 + $offset : static::translateFieldName('ContratProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 6 + $offset : static::translateFieldName('ContratProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 6 + $offset : static::translateFieldName('ContratProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 6 + $offset : static::translateFieldName('ContratProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 7 + $offset : static::translateFieldName('ContratProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 7 + $offset : static::translateFieldName('ContratProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 7 + $offset : static::translateFieldName('ContratProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 7 + $offset : static::translateFieldName('ContratProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 7 + $offset : static::translateFieldName('ContratProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)])]);
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idversement', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 4 + $offset
                : self::translateFieldName('VersementIdmois', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 5 + $offset
                : self::translateFieldName('ContratIdcontrat', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 6 + $offset
                : self::translateFieldName('ContratProduitsIdproduits', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 7 + $offset
                : self::translateFieldName('ContratProduitsSocieteIdsociete', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? VersementTableMap::CLASS_DEFAULT : VersementTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Versement object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = VersementTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = VersementTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + VersementTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = VersementTableMap::OM_CLASS;
            /** @var Versement $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            VersementTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = VersementTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = VersementTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Versement $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                VersementTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(VersementTableMap::COL_IDVERSEMENT);
            $criteria->addSelectColumn(VersementTableMap::COL_DATEVERSEMENT);
            $criteria->addSelectColumn(VersementTableMap::COL_MONTANTVERSEMENT);
            $criteria->addSelectColumn(VersementTableMap::COL_NUMEROCHEQUE);
            $criteria->addSelectColumn(VersementTableMap::COL_VERSEMENT_IDMOIS);
            $criteria->addSelectColumn(VersementTableMap::COL_CONTRAT_IDCONTRAT);
            $criteria->addSelectColumn(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS);
            $criteria->addSelectColumn(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE);
            $criteria->addSelectColumn(VersementTableMap::COL_MODEPAIEMENT_IDMODEPAIEMENT);
        } else {
            $criteria->addSelectColumn($alias . '.idversement');
            $criteria->addSelectColumn($alias . '.dateVersement');
            $criteria->addSelectColumn($alias . '.montantversement');
            $criteria->addSelectColumn($alias . '.NumeroCheque');
            $criteria->addSelectColumn($alias . '.versement_idmois');
            $criteria->addSelectColumn($alias . '.Contrat_idContrat');
            $criteria->addSelectColumn($alias . '.Contrat_produits_idproduits');
            $criteria->addSelectColumn($alias . '.Contrat_produits_societe_idsociete');
            $criteria->addSelectColumn($alias . '.modePaiement_idmodePaiement');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(VersementTableMap::DATABASE_NAME)->getTable(VersementTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(VersementTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(VersementTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new VersementTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Versement or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Versement object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VersementTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Versement) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(VersementTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = array($values);
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(VersementTableMap::COL_IDVERSEMENT, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(VersementTableMap::COL_VERSEMENT_IDMOIS, $value[1]));
                $criterion->addAnd($criteria->getNewCriterion(VersementTableMap::COL_CONTRAT_IDCONTRAT, $value[2]));
                $criterion->addAnd($criteria->getNewCriterion(VersementTableMap::COL_CONTRAT_PRODUITS_IDPRODUITS, $value[3]));
                $criterion->addAnd($criteria->getNewCriterion(VersementTableMap::COL_CONTRAT_PRODUITS_SOCIETE_IDSOCIETE, $value[4]));
                $criteria->addOr($criterion);
            }
        }

        $query = VersementQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            VersementTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                VersementTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the versement table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return VersementQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Versement or Criteria object.
     *
     * @param mixed               $criteria Criteria or Versement object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(VersementTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Versement object
        }

        if ($criteria->containsKey(VersementTableMap::COL_IDVERSEMENT) && $criteria->keyContainsValue(VersementTableMap::COL_IDVERSEMENT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.VersementTableMap::COL_IDVERSEMENT.')');
        }


        // Set the correct dbName
        $query = VersementQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // VersementTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
VersementTableMap::buildTableMap();
