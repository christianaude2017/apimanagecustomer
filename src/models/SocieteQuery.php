<?php

use Base\SocieteQuery as BaseSocieteQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'societe' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SocieteQuery extends BaseSocieteQuery
{

}
