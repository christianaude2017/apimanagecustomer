<?php

use Base\ResponsableAgentQuery as BaseResponsableAgentQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'responsable_agent' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ResponsableAgentQuery extends BaseResponsableAgentQuery
{

}
