<?php

use Base\VersementQuery as BaseVersementQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'versement' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class VersementQuery extends BaseVersementQuery
{

}
