<?php

use Base\Rolepersonne as BaseRolepersonne;

/**
 * Skeleton subclass for representing a row from the 'rolepersonne' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Rolepersonne extends BaseRolepersonne
{

}
