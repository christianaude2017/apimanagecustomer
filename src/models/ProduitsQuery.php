<?php

use Base\ProduitsQuery as BaseProduitsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'produits' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProduitsQuery extends BaseProduitsQuery
{

}
